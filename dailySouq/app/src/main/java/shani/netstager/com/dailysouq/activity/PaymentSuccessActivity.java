package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ebs.android.sdk.PaymentRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by user on 27/01/16.
 */
public class PaymentSuccessActivity extends Activity {
    String payment_id;
    String PaymentId;
    String AccountId;
    String MerchantRefNo;
    String Amount;
    String DateCreated;
    String Description;
    String Mode;
    String IsFlagged;
    String BillingName;
    String BillingAddress;
    String BillingCity;
    String BillingState;
    String BillingPostalCode;
    String BillingCountry;
    String BillingPhone;
    String BillingEmail;
    String DeliveryName;
    String DeliveryAddress;
    String DeliveryCity;
    String DeliveryState;
    String DeliveryPostalCode;
    String DeliveryCountry;
    String DeliveryPhone;
    String PaymentStatus;
    String PaymentMode;
    String SecureHash;
    String ResponseCode;
    SharedPreferences myprefs;
    int PAYMENT_GATEWAY;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    String userVLogIn;
    TextView tarnsactuon_id, order_id, order_amount;
    TextView tv_payment_success_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment_success);
        tarnsactuon_id = (TextView) findViewById(R.id.paymentvalue);
        order_id = (TextView) findViewById(R.id.ordervalue);
        order_amount = (TextView) findViewById(R.id.totalvalue);
        tv_payment_success_title = (TextView) findViewById(R.id.tv_payment_success_title);


        Intent intent = getIntent();

        payment_id = intent.getStringExtra("payment_id");
        System.out.println("payment_id" + " " + payment_id);

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

        PAYMENT_GATEWAY = myprefs.getInt("PAYMENT_GATEWAY", 0);
        userVLogIn = myprefs.getString("shaniusrid", null);
        Log.i("PAYMENT_GATEWAY", PAYMENT_GATEWAY + "_>");

        getJsonReport();

        if (!PaymentStatus.equalsIgnoreCase("Failed")) {
            myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

            PAYMENT_GATEWAY = myprefs.getInt("PAYMENT_GATEWAY", 0);
            userVLogIn = myprefs.getString("shaniusrid", null);
            Log.i("PAYMENT_GATEWAY", PAYMENT_GATEWAY + "_>");
            if (PAYMENT_GATEWAY == 1) {
                //ewallet add money
                new AsyncTaskEwalletSaveJson().execute();
            } else if (PAYMENT_GATEWAY == 2) {
                new AsyncOrderTransactioidUpdateJsson().execute();
            }//its used for trnsfer order payment
            else if (PAYMENT_GATEWAY == 3) {

                new AsyncBuyJson().execute();

            }
/*
            SharedPreferences.Editor editor = myprefs.edit();
            editor.remove("PAYMENT_GATEWAY");
            editor.apply();*/

        } else {
            //new AsyncOrderTransactioidUpdateJssonFailed().execute();
            onBackPressed();
        }


        SharedPreferences.Editor editor = myprefs.edit();
        editor.remove("PAYMENT_GATEWAY");
        editor.apply();

       /* Button btn_payment_success = (Button) findViewById(R.id.btn_payment_success);
        btn_payment_success.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                PAYMENT_GATEWAY=myprefs.getInt("PAYMENT_GATEWAY",0);
                userVLogIn = myprefs.getString("shaniusrid", null);
                Log.i("PAYMENT_GATEWAY",PAYMENT_GATEWAY+"_>");
                if(PAYMENT_GATEWAY==1){
                    //ewallet add money
                    new AsyncTaskEwalletSaveJson().execute();
                }
                else if(PAYMENT_GATEWAY==2){
                    new AsyncOrderTransactioidUpdateJsson().execute();
                }//its used for trnsfer order payment
                else if(PAYMENT_GATEWAY==3){

                    new  AsyncBuyJson().execute();

                }

                SharedPreferences.Editor editor = myprefs.edit();
                editor.remove("PAYMENT_GATEWAY");
                editor.apply();




            }
        });*/

    }

    public class AsyncOrderTransactioidUpdateJssonFailed extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        String transactionid = "0";
        int jsonlen = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PaymentSuccessActivity.this);
//            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                //Log.i("AFTER_OREDER_ID",AFTER_OREDER_ID+"->");
                JSONObject jobj = jParser.updatetransactionid(userVLogIn, MerchantRefNo, "0", "Failed");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            mProgressDialog.dismiss();
            if (jsonlen == 0) {
//                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
//                alertDialog.setTitle(getString(R.string.error));
//                alertDialog.show();
            } else {
                if (dataJsonArr.length() == 1) {

                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    alertDialog.setMessage(getString(R.string.payment_failure));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }


        }
    }

    private void getJsonReport() {
        String response = payment_id;

        JSONObject jObject;
        try {
            jObject = new JSONObject(response.toString());
            PaymentRequest.getInstance().setPaymentResponse(response.toString());


            PaymentId = jObject.getString("PaymentId");

            AccountId = jObject.getString("AccountId");
            MerchantRefNo = jObject.getString("MerchantRefNo");
            Amount = jObject.getString("Amount");
            DateCreated = jObject.getString("DateCreated");
            Description = jObject.getString("Description");
            Mode = jObject.getString("Mode");
            IsFlagged = jObject.getString("IsFlagged");
            BillingName = jObject.getString("BillingName");

            BillingAddress = jObject.getString("BillingAddress");
            BillingCity = jObject.getString("BillingCity");
            BillingState = jObject.getString("BillingState");
            BillingPostalCode = jObject.getString("BillingPostalCode");
            BillingCountry = jObject.getString("BillingCountry");
            BillingPhone = jObject.getString("BillingPhone");
            BillingEmail = jObject.getString("BillingEmail");
            DeliveryName = jObject.getString("DeliveryName");
            DeliveryAddress = jObject.getString("DeliveryAddress");
            DeliveryCity = jObject.getString("DeliveryCity");
            DeliveryState = jObject.getString("DeliveryState");
            DeliveryPostalCode = jObject.getString("DeliveryPostalCode");
            DeliveryCountry = jObject.getString("DeliveryCountry");
            DeliveryPhone = jObject.getString("DeliveryPhone");
            PaymentStatus = jObject.getString("PaymentStatus");
            PaymentMode = jObject.getString("PaymentMode");
            SecureHash = jObject.getString("SecureHash");
            ResponseCode = jObject.getString("ResponseCode");
            PaymentRequest.getInstance().setPaymentId(PaymentId);


//            TableLayout table_payment = (TableLayout) findViewById(R.id.table_payment);
            ArrayList<String> arrlist = new ArrayList<String>();
            arrlist.add("PaymentId");
            arrlist.add("AccountId ");
            arrlist.add("MerchantRefNo");
            arrlist.add("Amount");
            arrlist.add("DateCreated");
            arrlist.add("Description");
            arrlist.add("Mode");
            arrlist.add("IsFlagged");
            arrlist.add("BillingName");
            arrlist.add("BillingAddress");
            arrlist.add("BillingCity");
            arrlist.add("BillingState");
            arrlist.add("BillingPostalCode");
            arrlist.add("BillingCountry");
            arrlist.add("BillingPhone");
            arrlist.add("BillingEmail");
            arrlist.add("DeliveryName");
            arrlist.add("DeliveryAddress");
            arrlist.add("DeliveryCity");
            arrlist.add("DeliveryState");
            arrlist.add("DeliveryPostalCode");
            arrlist.add("DeliveryCountry");
            arrlist.add("DeliveryPhone");
            arrlist.add("PaymentStatus");
            arrlist.add("PaymentMode");
            arrlist.add("SecureHash");
            arrlist.add("ResponseCode");

            ArrayList<String> arrlist1 = new ArrayList<String>();
            arrlist1.add(PaymentId);
            arrlist1.add(AccountId);
            arrlist1.add(MerchantRefNo);
            arrlist1.add(Amount);
            arrlist1.add(DateCreated);
            arrlist1.add(Description);
            arrlist1.add(Mode);
            arrlist1.add(IsFlagged);
            arrlist1.add(BillingName);
            arrlist1.add(BillingAddress);
            arrlist1.add(BillingCity);
            arrlist1.add(BillingState);
            arrlist1.add(BillingPostalCode);
            arrlist1.add(BillingCountry);
            arrlist1.add(BillingPhone);
            arrlist1.add(BillingEmail);
            arrlist1.add(DeliveryName);
            arrlist1.add(DeliveryAddress);
            arrlist1.add(DeliveryCity);
            arrlist1.add(DeliveryState);
            arrlist1.add(DeliveryPostalCode);
            arrlist1.add(DeliveryCountry);
            arrlist1.add(DeliveryPhone);
            arrlist1.add(PaymentStatus);
            arrlist1.add(PaymentMode);
            arrlist1.add(SecureHash);
            arrlist1.add("ResponseCode");

//            for (int i = 0; i < arrlist.size(); i++) {
//                TableRow row = new TableRow(this);
//
//                TextView textH = new TextView(this);
//                TextView textC = new TextView(this);
//                TextView textV = new TextView(this);
//
//                textH.setText(arrlist.get(i));
//                textC.setText(":  ");
//                textV.setText(arrlist1.get(i));
//                textV.setTypeface(null, Typeface.BOLD);
//
//                row.addView(textH);
//                row.addView(textC);
//                row.addView(textV);
//
//                table_payment.addView(row);
//            }


            tarnsactuon_id.setText(PaymentId);
            order_id.setText(MerchantRefNo);
            order_amount.setText(Amount);
            //tv_payment_success_title.setText("Payment of Rs: " + Amount + " Recieved by Dialy Souq");
            if (!PaymentStatus.equalsIgnoreCase("Failed")) {
                tv_payment_success_title.setText("Payment of Rs: " + Amount + " Recieved by Dialy Souq");
            }else {
                tv_payment_success_title.setText("Payment Failed");
            }

            /*
             * tv_payment_id.setText("PaymentId : " + PaymentId + "\n" +
             * "AccountId : " + AccountId + "\n" + "MerchantRefNo : " +
             * MerchantRefNo + "\n" + "Amount : " + Amount + "\n" +
             * "DateCreated : " + DateCreated + "\n" + "Description : " +
             * Description + "\n" + "Mode : " + Mode + "\n" + "IsFlagged: " +
             * IsFlagged + "\n" + "BillingName : " + BillingName + "\n" +
             * "BillingAddress : " + BillingAddress + "\n" + "BillingCity : " +
             * BillingCity + "\n" + "BillingState : " + BillingState + "\n" +
             * "BillingPostalCode : " + BillingPostalCode + "\n" +
             * "BillingCountry : " + BillingCountry + "\n" + "BillingPhone : " +
             * BillingPhone + "\n" + "BillingEmail : " + BillingEmail + "\n" +
             * "DeliveryName : " + DeliveryName + "\n" + "DeliveryAddress : " +
             * DeliveryAddress + "\n" + "DeliveryCity : " + DeliveryCity + "\n"
             * + "DeliveryState : " + DeliveryState + "\n" +
             * "DeliveryPostalCode : " + DeliveryPostalCode + "\n" +
             * "DeliveryCountry : " + DeliveryCountry + "\n" +
             * "DeliveryPhone : " + DeliveryPhone + "\n" + "PaymentStatus : " +
             * PaymentStatus + "\n" + "PaymentMode : " + PaymentMode + "\n" +
             * "SecureHash : " + SecureHash + "\n");
             */

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class AsyncTaskEwalletSaveJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen = 0;


        @Override
        protected String doInBackground(String... arg0) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.ewalletfoundsave("0", "Debit", Amount, userVLogIn, "money add", PaymentId);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);

                alertDialog.setMessage(getString(R.string.order_placed_payment_done));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                Intent in = new Intent(PaymentSuccessActivity.this, Ewallet.class);

                                startActivity(in);

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

            // Log.i("arrivedcount", str_json_cart_count);
            //  new AsyncTaskLinkeduserJson().execute();
            // mProgressDialog.dismiss();
        }
    }

    public class AsyncOrderTransactioidUpdateJsson extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        String transactionid = "0";
        int jsonlen = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PaymentSuccessActivity.this);
//            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject jobj = jParser.updatetransactionid(userVLogIn, MerchantRefNo, PaymentId, "Success");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Log.i("dataJsonArr", dataJsonArr.toString());
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            mProgressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {
                if (dataJsonArr.length() == 1) {

                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    alertDialog.setMessage(getString(R.string.order_placed_payment_done));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    Intent in = new Intent(PaymentSuccessActivity.this, MainActivity.class);

                                    startActivity(in);

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    Intent in = new Intent(PaymentSuccessActivity.this, MainActivity.class);

                                    startActivity(in);

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }


        }
    }


    public class AsyncBuyJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen = 0;
        String result = "";


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentSuccessActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.maketransferorderpayment(MerchantRefNo);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
            if (jsonlen == 0) {

            } else {

                SharedPreferences.Editor editor = myprefs.edit();
                editor.remove("PAYMENT_GATEWAY");
                editor.apply();

                AlertDialog alertDialog = new AlertDialog.Builder(PaymentSuccessActivity.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.transferorder_placed));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(PaymentSuccessActivity.this, MainActivity.class);

                                startActivity(in);

                            }
                        });


                alertDialog.show();
            }


        }
    }


}















/*
package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ebs.android.sdk.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;


public class PaymentSuccessActivity extends Activity {
    String payment_id;
    String PaymentId;
    String AccountId;
    String MerchantRefNo;
    String Amount;
    String DateCreated;
    String Description;
    String Mode;
    String IsFlagged;
    String BillingName;
    String BillingAddress;
    String BillingCity;
    String BillingState;
    String BillingPostalCode;
    String BillingCountry;
    String BillingPhone;
    String BillingEmail;
    String DeliveryName;
    String DeliveryAddress;
    String DeliveryCity;
    String DeliveryState;
    String DeliveryPostalCode;
    String DeliveryCountry;
    String DeliveryPhone;
    String PaymentStatus;
    String PaymentMode;
    String SecureHash;

    LinearLayout tryAgainLayout;
    Button btn_payment_success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment_success);

        tryAgainLayout=(LinearLayout)findViewById(R.id.ll_button);
        btn_payment_success = (Button) findViewById(R.id.btn_payment_success);
        Intent intent = getIntent();

        payment_id = intent.getStringExtra("payment_id");
        System.out.println("Success and Failure response to merchant app..." + " " + payment_id);

        getJsonReport();


        btn_payment_success.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PaymentSuccessActivity.this.finish();

            }
        });


        Button btn_retry = (Button) findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),PaymentActivity.class);
                PaymentSuccessActivity.this.finish();
                startActivity(i);

            }
        });
        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentSuccessActivity.this.finish();
            }
        });


    }

    private void getJsonReport() {
        String response = payment_id;

        ///Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();

        JSONObject jObject;
        try {
            jObject = new JSONObject(response.toString());
            PaymentId = jObject.getString("PaymentId");
            AccountId = jObject.getString("AccountId");
            MerchantRefNo = jObject.getString("MerchantRefNo");
            Amount = jObject.getString("Amount");
            DateCreated = jObject.getString("DateCreated");
            Description = jObject.getString("Description");
            Mode = jObject.getString("Mode");
            IsFlagged = jObject.getString("IsFlagged");
            BillingName = jObject.getString("BillingName");
            BillingAddress = jObject
                    .getString("BillingAddress");
            BillingCity = jObject.getString("BillingCity");
            BillingState = jObject.getString("BillingState");
            BillingPostalCode = jObject
                    .getString("BillingPostalCode");
            BillingCountry = jObject
                    .getString("BillingCountry");
            BillingPhone = jObject.getString("BillingPhone");
            BillingEmail = jObject.getString("BillingEmail");
            DeliveryName = jObject.getString("DeliveryName");
            DeliveryAddress = jObject
                    .getString("DeliveryAddress");
            DeliveryCity = jObject.getString("DeliveryCity");
            DeliveryState = jObject.getString("DeliveryState");
            DeliveryPostalCode = jObject
                    .getString("DeliveryPostalCode");
            DeliveryCountry = jObject
                    .getString("DeliveryCountry");
            DeliveryPhone = jObject.getString("DeliveryPhone");
            PaymentStatus = jObject.getString("PaymentStatus");
            PaymentMode = jObject.getString("PaymentMode");
            SecureHash = jObject.getString("SecureHash");
            System.out.println("paymentid_rsp" + PaymentId);

            if(PaymentStatus.equalsIgnoreCase("failed")){
                tryAgainLayout.setVisibility(View.VISIBLE);
                btn_payment_success.setVisibility(View.GONE);
            }else{
                btn_payment_success.setVisibility(View.VISIBLE);
                tryAgainLayout.setVisibility(View.GONE);
            }

            TableLayout table_payment = (TableLayout) findViewById(R.id.table_payment);
            ArrayList<String> arrlist = new ArrayList<String>();
            arrlist.add("PaymentId");
            arrlist.add("AccountId ");
            arrlist.add("MerchantRefNo");
            arrlist.add("Amount");
            arrlist.add("DateCreated");
            arrlist.add("Description");
            arrlist.add("Mode");
            arrlist.add("IsFlagged");
            arrlist.add("BillingName");
            arrlist.add("BillingAddress");
            arrlist.add("BillingCity");
            arrlist.add("BillingState");
            arrlist.add("BillingPostalCode");
            arrlist.add("BillingCountry");
            arrlist.add("BillingPhone");
            arrlist.add("BillingEmail");
            arrlist.add("DeliveryName");
            arrlist.add("DeliveryAddress");
            arrlist.add("DeliveryCity");
            arrlist.add("DeliveryState");
            arrlist.add("DeliveryPostalCode");
            arrlist.add("DeliveryCountry");
            arrlist.add("DeliveryPhone");
            arrlist.add("PaymentStatus");
            arrlist.add("PaymentMode");
            arrlist.add("SecureHash");

            ArrayList<String> arrlist1 = new ArrayList<String>();
            arrlist1.add(PaymentId);
            arrlist1.add(AccountId );
            arrlist1.add(MerchantRefNo);
            arrlist1.add(Amount);
            arrlist1.add(DateCreated);
            arrlist1.add(Description);
            arrlist1.add(Mode);
            arrlist1.add(IsFlagged);
            arrlist1.add(BillingName);
            arrlist1.add(BillingAddress);
            arrlist1.add(BillingCity);
            arrlist1.add(BillingState);
            arrlist1.add(BillingPostalCode);
            arrlist1.add(BillingCountry);
            arrlist1.add(BillingPhone);
            arrlist1.add(BillingEmail);
            arrlist1.add(DeliveryName);
            arrlist1.add(DeliveryAddress);
            arrlist1.add(DeliveryCity);
            arrlist1.add(DeliveryState);
            arrlist1.add(DeliveryPostalCode);
            arrlist1.add(DeliveryCountry);
            arrlist1.add(DeliveryPhone);
            arrlist1.add(PaymentStatus);
            arrlist1.add(PaymentMode);
            arrlist1.add(SecureHash);

            for(int i=0;i<arrlist.size();i++){
                TableRow row = new TableRow(this);

                TextView textH = new TextView(this);
                TextView textC = new TextView(this);
                TextView textV = new TextView(this);

                textH.setText(arrlist.get(i));
                textC.setText(":  ");
                textV.setText(arrlist1.get(i));
                textV.setTypeface(null, Typeface.BOLD);

                row.addView(textH);
                row.addView(textC);
                row.addView(textV);

                table_payment.addView(row);
            }

			*/
/*tv_payment_id.setText("PaymentId : " + PaymentId
					+ "\n" + "AccountId : " + AccountId + "\n"
					+ "MerchantRefNo : " + MerchantRefNo + "\n"
					+ "Amount : " + Amount + "\n"
					+ "DateCreated : " + DateCreated + "\n"
					+ "Description : " + Description + "\n"
					+ "Mode : " + Mode + "\n" + "IsFlagged: "
					+ IsFlagged + "\n" + "BillingName : "
					+ BillingName + "\n" + "BillingAddress : "
					+ BillingAddress + "\n" + "BillingCity : "
					+ BillingCity + "\n" + "BillingState : "
					+ BillingState + "\n"
					+ "BillingPostalCode : " + BillingPostalCode
					+ "\n" + "BillingCountry : " + BillingCountry
					+ "\n" + "BillingPhone : " + BillingPhone
					+ "\n" + "BillingEmail : " + BillingEmail
					+ "\n" + "DeliveryName : " + DeliveryName
					+ "\n" + "DeliveryAddress : "
					+ DeliveryAddress + "\n" + "DeliveryCity : "
					+ DeliveryCity + "\n" + "DeliveryState : "
					+ DeliveryState + "\n"
					+ "DeliveryPostalCode : "
					+ DeliveryPostalCode + "\n"
					+ "DeliveryCountry : " + DeliveryCountry
					+ "\n" + "DeliveryPhone : " + DeliveryPhone
					+ "\n" + "PaymentStatus : " + PaymentStatus
					+ "\n" + "PaymentMode : " + PaymentMode
					+ "\n" + "SecureHash : " + SecureHash + "\n");*//*


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
*/
