package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

/**
 * Created by prajeeshkk on 11/09/15.
 */
public class SplashActivity extends Activity{



    private static int SPLASH_TIME_OUT=3000;
    SharedPreferences myprefs;
    boolean HIDE_LANGUGE_PAGE=false;
    boolean test2nd=false;
    DS_SP sp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        final Boolean isInternetPresent = cd.isConnectingToInternet();
        sp=new DS_SP(getApplicationContext());

        Log.i("first",isInternetPresent.toString());

        // true or false
        if(isInternetPresent.equals(true)){

            setContentView(R.layout.splash_layout);

        }
        else{

            Log.i("inside not inet", "ok");
            SPLASH_TIME_OUT = 5000;
            Toast.makeText(getApplicationContext(), "No Internet Connection..!", Toast.LENGTH_LONG).show();



        }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        new Handler().postDelayed(new Runnable(){

            @Override
            public void run() {
                try {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

                    Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
                    Log.i("third",isInternetPresent.toString());


                    if(isInternetPresent.equals(true)){

                        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                        HIDE_LANGUGE_PAGE = myprefs.getBoolean("HIDE_LANGUGUE_PAGE", false);
                        if(sp.getlanselection()){

                            Intent in = new Intent(SplashActivity.this, MainActivity.class);
                            // in.putStringArrayListExtra("name", nw);

                            startActivity(in);
                        }else if(!sp.getlanselection()){

                            Intent in = new Intent(SplashActivity.this, LangugeSelection.class);
                            // in.putStringArrayListExtra("name", nw);

                            startActivity(in);
                        }


                    }
                    else{

                        AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_net_failed));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        finish();

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }

                }
                catch (Exception e){
                    e.printStackTrace();
                }


                finish();
            }
        },SPLASH_TIME_OUT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      //  SplashActivity.this.finish();
    }

}




