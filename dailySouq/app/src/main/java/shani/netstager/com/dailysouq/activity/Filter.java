package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 14/10/15.
 */
public class Filter extends Activity
{
    private static Filter sFilter;

    ListView category,brand,prilcelist;
    Button apply,clear;
    //String categoryId;
    ProgressDialog mprogressdialog;
    JSONArray dataJsonArr;
    int categoryLength;
    String[]categoryId;
    String[]categoryName; int BrandLength;
    String[]brandId;
    String[]brndName;

    String[]priceby={"Less than RS.100","RS.100 to RS.500","RS.500 to RS.1000","RS.1000 to RS.2000","Above RS.2000"};



    String bid,userVLogIn,result;
    String catresult,brandresult,refineresult,priceresult;
    ArrayList<String> a=new ArrayList<String>();
    ArrayList<String> b=new ArrayList<String>();
    ArrayList<String> Array_refinelist=new ArrayList<String>();
    ArrayList<String> d=new ArrayList<String>();
    SharedPreferences myprefs;
    int pg=0,productLength,sizeProduct;
    boolean isNewtag;
    int DEFAULT_LANGUGE;
    String str_catogory_id="";
    CheckBox offer_chk,new_chk,stock_chk;
    String offer_chk_str,new_chk_str,stock_chk_str;

    TextView  catslctall,brndselectall,priceselectall,refineselectall;
    boolean refineselectallbool=true,catselctallbool=true,brandselectallbool=true,priceselectallbool=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter);
//internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
            new AsyncTaskFilterJson().execute();

        }
        else{
            AlertDialog alertDialog = new AlertDialog.Builder(Filter.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();
        }

        //getting catogriid usuing intent
        Intent in=getIntent();
        str_catogory_id=in.getStringExtra("Catogoryid_filter");

        sFilter = this;
        category= (ListView) findViewById(R.id.catagoryList);
        new_chk=(CheckBox)findViewById(R.id.chk_new_filter);
        offer_chk=(CheckBox)findViewById(R.id.chk_offer_filter);
        stock_chk=(CheckBox)findViewById(R.id.chk_instock_filter);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        DEFAULT_LANGUGE=myprefs.getInt("DEFAULT_LANGUGE",1);
        userVLogIn = myprefs.getString("shaniusrid", null);
        catslctall=(TextView)findViewById(R.id.slct_cat);
        brndselectall=(TextView)findViewById(R.id.slct_brnd);
        priceselectall=(TextView)findViewById(R.id.slct_prce);
        refineselectall=(TextView)findViewById(R.id.slct_rfneby);

        category.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        apply= (Button) findViewById(R.id.filter);
        clear=(Button)findViewById(R.id.clear);
        brand=(ListView)findViewById(R.id.brandList);
        brand.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        //used to select all refine item checked
        refineselectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(refineselectallbool){
                    refineselectallbool=false;
                    refineselectall.setText("Unselect All");
                    new_chk.setChecked(true);
                    stock_chk.setChecked(true);
                    offer_chk.setChecked(true);
                }
                else {
                    refineselectallbool=true;
                    refineselectall.setText("Select All");
                    new_chk.setChecked(false);
                    stock_chk.setChecked(false);
                    offer_chk.setChecked(false);
                }


            }
        });



//        refine=(ListView)findViewById(R.id.newList);
//        refine.setOnTouchListener(new View.OnTouchListener() {
//            // Setting on Touch Listener for handling the touch inside ScrollView
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // Disallow the touch request for parent scroll on touch of child view
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });
        prilcelist=(ListView)findViewById(R.id.priceList);
        prilcelist.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        ArrayAdapter priceadapt = new ArrayAdapter<String>(Filter.this,android.R.layout.simple_list_item_multiple_choice,priceby);
        prilcelist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        //used for select aall
        priceselectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(priceselectallbool){
                    priceselectall.setText("Unselect All");
                    priceselectallbool=false;
                    for(int i=0;i<=priceby.length;i++){
                        prilcelist.setItemChecked(i,true);
                    }
                }else {
                    priceselectall.setText("Select All");
                    priceselectallbool=true;
                    for(int i=0;i<=priceby.length;i++){
                        prilcelist.setItemChecked(i,false);
                    }
                }

            }
        });


        prilcelist.setAdapter(priceadapt);

        prilcelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                          @Override
                                          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {





                                                priceresult = "";
                                              if (d.contains(priceby[position])) {
                                                  d.remove(priceby[position]);

                                              } else {
                                                  d.add(priceby[position]);

                                              }

                                              for (int i = 0; i < d.size(); i++) {

                                                    priceresult += d.get(i) + ",";



                                              }

                                          }
                                      }

        );



        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userVLogIn != null) {

                } else {
                    userVLogIn = "0";
                }

                if (brandresult != null) {

                } else {
                    brandresult = "";
                }
                if (catresult != null) {

                } else {
                    catresult = "";
                }
                if (priceresult != null) {

                } else {
                    priceresult = "";
                }

                if(new_chk.isChecked()==true){
                    new_chk_str="1";
                }else{
                    new_chk_str="0";
                }
                if(stock_chk.isChecked()==true){
                    stock_chk_str="1";
                }else{
                    stock_chk_str="0";
                }
                if(offer_chk.isChecked()==true){
                    offer_chk_str="1";
                }else{
                    offer_chk_str="0";
                }

                Intent in=new Intent(Filter.this,FilterResult.class);
                in.putExtra("CatResult",catresult);
                in.putExtra("BrandResult",brandresult);
                in.putExtra("priceresult",priceresult);
                in.putExtra("offer",offer_chk_str);
                in.putExtra("stock",stock_chk_str);
                in.putExtra("isNew",new_chk_str);



                startActivity(in);




            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }




    public class AsyncTaskFilterJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int brandjsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mprogressdialog = new ProgressDialog(Filter.this);
            // Set progressdialog title
            mprogressdialog.setTitle("");

            mprogressdialog.setMessage(getString(R.string.loading));
            mprogressdialog.setIndeterminate(false);
            mprogressdialog.setCancelable(false);
            mprogressdialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mprogressdialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                Log.i("lan_filter",DEFAULT_LANGUGE+" "+str_catogory_id);
                JSONObject json = jParser.filterProduct(str_catogory_id,DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d").getJSONObject(0).getJSONArray("Category");
                categoryLength=dataJsonArr.length();

                categoryId = new String[dataJsonArr.length()];
                categoryName = new String[dataJsonArr.length()];

                for (int i = 0; i < dataJsonArr.length(); i++) {

                    JSONObject c = dataJsonArr.getJSONObject(i);
                    categoryId[i] = dataJsonArr.getJSONObject(i).optString("CategoryId");
                    categoryName[i] = c.optString("CategoryName").toString();


                }
                jsonlen=dataJsonArr.length();
                dataJsonArr = productObj.getJSONArray("d").getJSONObject(1).getJSONArray("Brands");
                BrandLength=dataJsonArr.length();
                brandId = new String[dataJsonArr.length()];
                brndName = new String[dataJsonArr.length()];
                for (int i = 0; i < dataJsonArr.length(); i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);
                    brandId[i] = dataJsonArr.getJSONObject(i).optString("BrandId");
                    brndName[i] = c.optString("BrandName").toString();

                }
                brandjsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mprogressdialog.dismiss();
        if(jsonlen==0){

        }else{

            ArrayAdapter adapt = new ArrayAdapter<String>(Filter.this,android.R.layout.simple_list_item_multiple_choice,categoryName);
            category.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            category.setAdapter(adapt);
            //used for select aall
            catslctall=(TextView)findViewById(R.id.slct_cat);
            catslctall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(catselctallbool) {
                        catselctallbool=false;
                        catslctall.setText("Unselect All");
                        for (int i = 0; i <= categoryName.length; i++) {
                            category.setItemChecked(i, true);
                        }
                    }else {
                        catslctall.setText("Select All");
                        catselctallbool=true;
                        if(catselctallbool) {
                            for (int i = 0; i <= categoryName.length; i++) {
                                category.setItemChecked(i, false);
                            }
                        }
                    }
                }
            });


            category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    String result="";

                                                    if (b.contains(categoryId[position])) {
                                                        b.remove(categoryId[position]);

                                                    } else {
                                                        b.add(categoryId[position]);
                                                    }


                                                    for (int i = 0; i < b.size(); i++) {
                                                        int kk = a.size();

                                                        result += b.get(i);
                                                        if (i + 1 != b.size()) {
                                                            result += ",";
                                                        }

                                                    }
                                                    catresult=result;

                                                }
                                            }

            );
        }


        if(brandjsonlen==0){

        }else{
            ArrayAdapter adapter = new ArrayAdapter<String>(Filter.this,android.R.layout.simple_list_item_multiple_choice,brndName);
            brand.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            brand.setAdapter(adapter);
            //used for select aall
            brndselectall=(TextView)findViewById(R.id.slct_brnd);
            brndselectall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(brandselectallbool) {
                        brndselectall.setText("Unselect All");
                        brandselectallbool=false;
                        for (int i = 0; i <= brndName.length; i++) {
                            brand.setItemChecked(i, true);
                        }
                    }else {
                        brndselectall.setText("Select All");
                        brandselectallbool=true;
                        for (int i = 0; i <= brndName.length; i++) {
                            brand.setItemChecked(i, false);
                        }
                    }
                }
            });


            brand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                             @Override
                                             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                 String result="";

                                                 if (a.contains(brandId[position])) {
                                                     a.remove(brandId[position]);

                                                 } else {
                                                     a.add(brandId[position]);

                                                 }


                                                 for (int i = 0; i < a.size(); i++) {
                                                     int kk = a.size();

                                                     result += a.get(i);
                                                     if (i + 1 != a.size()) {
                                                         result += ",";
                                                     }

                                                 }
                                                 brandresult=result;


                                             }
                                         }

            );
        }



            }
        }



}
