package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;

public class AddDeliveryAddress extends AppCompatActivity {

    ImageView profile,cart,quickcheckout,Notification;
    EditText names,adress,cntctno,landmark,picode;
    RelativeLayout cartcounttext;
    Spinner Location,city;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    int spinner_location_default_postion_setting;
    int spinner_city_default_postion_setting;
    JSONArray dataJsonArr;
    CheckBox dfltcheck;
    SharedPreferences myprefs;
    ProgressDialog mProgressDialog;
    String userVLogin,result;
    Button submit,cancel,back_btn;
    String []locationplace;
    String []cityplace;
    String []Locationid;
    String []Locationcode;
    String dfltcheckste;
    String []cityid;
    String poscurtest;
    int where;
    String []citycode;
    int lcposid,citypos;
    TextView title;
    String strnames,stradress,strcntctno,strlandmark,strpicode,str_json_cart_count;
    String deliveryaddresid="0";
    RelativeLayout fullview;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_delivery_address);
        submit=(Button)findViewById(R.id.btn_add_adres);
        names=(EditText)findViewById(R.id.nameinadres);
        adress=(EditText)findViewById(R.id.adinadres1);
        cntctno=(EditText)findViewById(R.id.noinadres);
        landmark=(EditText)findViewById(R.id.lndmarkinadress);
        picode=(EditText)findViewById(R.id.texteVifde);
        Location=(Spinner)findViewById(R.id.texteVie);
        city=(Spinner)findViewById(R.id.cityinadres);
        dfltcheck=(CheckBox)findViewById(R.id.checkBox);
        cancel=(Button)findViewById(R.id.btn_cncl_adres);
        back_btn=(Button)findViewById(R.id.menue_btn);
        profile=(ImageView)findViewById(R.id.profile);
        fullview=(RelativeLayout)findViewById(R.id.add_delivery_full) ;
        fullview.setVisibility(View.GONE);
        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        myprefs = getSharedPreferences("MYPREFS",Context.MODE_PRIVATE);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }



        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogin = myprefs.getString("shaniusrid", null);
        Intent ins=getIntent();
        where=ins.getIntExtra("arrivedfrom",0);

        if(userVLogin!=null){
            userVLogin=myprefs.getString("shaniusrid",userVLogin);

        }
        else{
            userVLogin="0";

        }
        submit.setVisibility(View.GONE);

        try {

            if (userVLogin!="0") {
                fullview.setVisibility(View.VISIBLE);
                submit.setVisibility(View.VISIBLE);

            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent in=new Intent(AddDeliveryAddress.this,Signin.class);
                                AddDeliveryAddress.this.finish();
                                startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();




            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent in = new Intent(AddDeliveryAddress.this, Menu_List.class);
                AddDeliveryAddress.this.finish();
                //startActivity(in);
                 SharedPreferences.Editor editor = myprefs.edit();
                 editor.putString("positionofcurrentback", poscurtest);
                editor.apply();

            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Intent in = new Intent(AddDeliveryAddress.this, Menu_List.class);
                AddDeliveryAddress.this.finish();
               // startActivity(in);
            }
        });

        title.setText(getString(R.string.title_activity_add_delivery_address));

        Intent in=getIntent();
        names.setText(in.getStringExtra("name"));
        adress.setText(in.getStringExtra("adress"));
        landmark.setText(in.getStringExtra("landmark"));
        deliveryaddresid=in.getStringExtra("1");
        spinner_location_default_postion_setting=myprefs.getInt("locationids", 0);
        spinner_city_default_postion_setting=myprefs.getInt("cityid",0);

        picode.setText(in.getStringExtra("postcode"));
        cntctno.setText(in.getStringExtra("phone"));


             //internet checking
          ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
          Boolean isInternetPresent = cd.isConnectingToInternet();
         if(isInternetPresent.equals(true)){
            // new AsyncTaskCartcounttJson().execute();

            new AsyncTaskCity().execute();
        }
        else{
             AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
             alertDialog.setCancelable(false);
             alertDialog.setCanceledOnTouchOutside(false);
             alertDialog.setMessage(getString(R.string.alert_net_failed));
             alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                     new DialogInterface.OnClickListener() {
                         public void onClick(DialogInterface dialog, int which) {


                             AddDeliveryAddress.this.finish();

                             dialog.dismiss();
                         }
                     });

             alertDialog.show();
            AddDeliveryAddress.this.finish();
         }



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strnames=names.getText().toString();
                strcntctno=cntctno.getText().toString();

                stradress=adress.getText().toString();
                strlandmark=landmark.getText().toString();
                strpicode=picode.getText().toString();

                if(strnames.equals("")||stradress.equals("")||strcntctno.equals("")||strpicode.equals("")){
                    names.setError(getString(R.string.plz_fill));

                    cntctno.setError(getString(R.string.plz_fill));


                    adress.setError(getString(R.string.plz_fill));

                    picode.setError(getString(R.string.plz_fill));

                }
                else {



                        if(!dfltcheck.isChecked()==true){
                            dfltcheckste="False";
                        }
                        else{
                            dfltcheckste="true";
                        }



                        new AsyncTaskCarttJson().execute();
                    }




            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                names.setText("");
                cntctno.setText("");
                adress.setText("");
                landmark.setText("");
                picode.setText("");
                AddDeliveryAddress.this.finish();

            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(AddDeliveryAddress.this,EditProfile.class);
                AddDeliveryAddress.this.finish();
                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(AddDeliveryAddress.this,CartView.class);
                AddDeliveryAddress.this.finish();
                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(AddDeliveryAddress.this, Notification.class);
                AddDeliveryAddress.this.finish();
                startActivity(in);

            }
        });

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogin.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddDeliveryAddress.this.finish();
    }






    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int prd_length=0;


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogin);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.optJSONArray("d");

                str_address_quick_checkout= dataJsonArr.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr.optJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactNo");
                    prd_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//


            if(prd_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(AddDeliveryAddress.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {


        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prd_lenth=0;
        @Override
        protected void onPreExecute() {


            // Create a progressdialog
            mProgressDialog = new ProgressDialog(AddDeliveryAddress.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();

                JSONObject json = jParser.AddNewAddress(userVLogin,strnames , strcntctno,stradress,strlandmark ,strpicode ,dfltcheckste,cityid[citypos], Locationid[lcposid],deliveryaddresid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result=dataJsonArr.getJSONObject(0).optString("result");
                prd_lenth=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (prd_lenth == 0) {

                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                if (result.equals("success")) {



                    AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                    alertDialog.setTitle(getString(R.string.order_succsess));
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    if (where == 1) {
                                        Intent in = new Intent(AddDeliveryAddress.this, CheckOut.class);
                                        AddDeliveryAddress.this.finish();
                                        startActivity(in);

                                    }
                                    if (where == 3) {
                                        Intent in = new Intent(AddDeliveryAddress.this, MyProfile.class);
                                        AddDeliveryAddress.this.finish();
                                        startActivity(in);

                                    }
                                    if (where == 4) {
                                        Intent in = new Intent(AddDeliveryAddress.this, DeliveryAddress.class);
                                        AddDeliveryAddress.this.finish();
                                        startActivity(in);

                                    }


                                    AddDeliveryAddress.this.finish();

                                    dialog.dismiss();
                                }
                            });


                    alertDialog.show();

                } else if (result.equals("updated")) {

                    if (dfltcheck.isChecked()) {


                    }


                    AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                    alertDialog.setTitle(getString(R.string.order_updated));
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
//                                Intent in = new Intent(AddDeliveryAddress.this, MainActivity.class);
//                                startActivity(in);
                                    if (where == 2) {

                                        Intent in = new Intent(AddDeliveryAddress.this, DeliveryAddress.class);
                                        AddDeliveryAddress.this.finish();
                                        startActivity(in);

                                    }


                                    AddDeliveryAddress.this.finish();
                                    dialog.dismiss();
                                }
                            });


                    alertDialog.show();


                } else {


                    AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                    alertDialog.setTitle(getString(R.string.error));
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }




            }
        }

    }

    public class AsyncTaskLocation extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prd_length=0;
        @Override
        protected String doInBackground(String... arg0) {


            try {

                Json jParser = new Json();
                JSONObject json = jParser.Location(cityid[citypos]);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Locationid=new String[dataJsonArr.length()];
                Locationcode=new String[dataJsonArr.length()];
                locationplace=new String[dataJsonArr.length()];

                for(int i=0;i<dataJsonArr.length();i++){
                    Locationid[i]=dataJsonArr.getJSONObject(i).optString("LocationId");
                    locationplace[i] = dataJsonArr.getJSONObject(i).optString("LocationName");
                    Locationcode[i]=dataJsonArr.getJSONObject(i).optString("LocationCode");
                }

            prd_length=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (prd_length == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddDeliveryAddress.this, android.
                        R.layout.simple_spinner_dropdown_item, locationplace);
                Location.setAdapter(adapter);


                Location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        SharedPreferences.Editor editor = myprefs.edit();

                        editor.putInt("locationids", position);
                        editor.apply();
                        // Get select item
                        lcposid = Location.getSelectedItemPosition();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });



            }
        }
    }

    public class AsyncTaskCity extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prd_lenth=0;

        @Override
        protected String doInBackground(String... arg0) {

            try {

                Json jParser = new Json();
                JSONObject json = jParser.city();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                cityid=new String[dataJsonArr.length()];
                citycode=new String[dataJsonArr.length()];
                cityplace=new String[dataJsonArr.length()];

                for(int i=0;i<dataJsonArr.length();i++){
                    cityid[i]=dataJsonArr.getJSONObject(i).optString("CityId");
                    cityplace[i] = dataJsonArr.getJSONObject(i).optString("CityName");
                    citycode[i]=dataJsonArr.getJSONObject(i).optString("CityCode");

                }
            prd_lenth=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (prd_lenth == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddDeliveryAddress.this, android.
                        R.layout.simple_spinner_dropdown_item, cityplace);
                city.setAdapter(adapter);


                city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {

                        SharedPreferences.Editor editor = myprefs.edit();

                        editor.putInt("cityid", position);
                        editor.apply();

                        // Get select item
                        citypos = city.getSelectedItemPosition();


                        new AsyncTaskLocation().execute();
                    }//

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });




            }
        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength = 0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogin);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                prdlength = dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (prdlength == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(AddDeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogin.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }

                //  mProgressDialog.dismiss();
            }
        }
    }




}
