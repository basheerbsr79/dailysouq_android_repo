package shani.netstager.com.dailysouq.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomPagerAdapter;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class MainActivity extends Activity {
    public SharedPreferences myprefs,myProducts;
    private Toolbar mToolbar;
    static Context con;
    Spinner spinnerDropDown;
    JSONObject jsonobject;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    JSONArray jsonarray;
    String username;
    String[] catogeryList;
    String[]img;
    int catnameLength;
    int []productID;
    String str_news_result_json;
    int categoryLength;
    Handler mHandler;
    String catNme;
    String pdNme;
    String productName;
    ArrayList<String> nw = new ArrayList<String>();
    ArrayList<String> id= new ArrayList<String>();
    ArrayAdapter<String>srcarradptr;
    MultiAutoCompleteTextView et;
    ImageView go;
    ImageView settings;
    String sortItem;
    ImageView shop,ds_spl,menu_icon,title,ds_history,ds_deals,ds_gift,ds_shopbylist;
    TextView gst,txt_news;
    int sid,productLength;
    Button fblogin;
    private static String APP_ID = "114112302269734";
    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager;
    Timer timer;
    int page = 1;

    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;
    String []prdnamesrch,prdidsrch,catnamesrch,catidsrch;
    ImageView serchgobtn;
    String []categoryName={"All","Category","Products","Product Code"};
    int pos;
    String srchtext,logintype;
    ListView srchlist;
    TextView test;
    PopupMenu popup;
    int[] ispdct_banner;
    boolean firstonly=false;
    DS_SP sp;
    String userVLogIn;
    int int_notcount;
    //serch test


//ArrayAdapter<String> adapter;
//    SearchView search;
//    LinearLayout linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp=new DS_SP(getApplicationContext());

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        logintype = myprefs.getString("logintype", null);

        if(logintype!=null){
            logintype=myprefs.getString("logintype",logintype);

        }
        else{
            logintype="NO";

        }


        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

            /*//call updations
            HttpPost httppostUserName = new HttpPost(
                    "https://androidquery.appspot.com/api/market?app=com.supercell.clashofclans"); //your package name
            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse responseUser = null;
            try {
                responseUser = httpclient
                        .execute(httppostUserName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String responseStringUser = null;
            try {
                responseStringUser = EntityUtils.toString(
                        responseUser.getEntity(), HTTP.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                JSONObject Json = new JSONObject(responseStringUser);
                String newVersion = Json.getString("version");
                Log.i("tver",newVersion);
            } catch (Exception e) {
                e.printStackTrace();
            }*/






            new NewsUpdatesAsync().execute();
            new BannerAsynk().execute();
            if(!sp.getbolfirsttimeonly()){
                sp.setbolfirsttimeonly(true);
                new AsyncTaskNotifCounttJson().execute();
                new AsyncTaskCartcounttJson().execute();
            }

        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }



        this.mHandler = new Handler();

        this.mHandler.postDelayed(m_Runnable, 5000);
        txt_news=(TextView)findViewById(R.id.txt_news);
        //setContentView(R.layout.searchbar);
        menu_icon=(ImageView)findViewById(R.id.menu);
        title=(ImageView)findViewById(R.id.namein_title);
        ds_deals= (ImageView) findViewById(R.id.ds_deals);
        ds_gift= (ImageView) findViewById(R.id.ds_gifts);
        ds_shopbylist= (ImageView) findViewById(R.id.ds_shopbylist);
        gst=(TextView)findViewById(R.id.guest);
        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getApplicationContext(),Menu_List.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Menu_List.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);
            }
        });
        shop= (ImageView) findViewById(R.id.shopImg);
        ds_spl=(ImageView)findViewById(R.id.dsSpecialImg);
        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHandler.removeCallbacks(m_Runnable);
                Intent in = new Intent(MainActivity.this, Categories.class);
                // MainActivity.this.finish();
                startActivity(in);

            }
        });
        ds_history=(ImageView)findViewById(R.id.dshistory);
        ds_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mHandler.removeCallbacks(m_Runnable);
                Intent in = new Intent(MainActivity.this, MyOrderList.class);
                // MainActivity.this.finish();
                startActivity(in);

            }
        });

        ds_spl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DsSpecials.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);
            }
        });


        ds_shopbylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, ShopByListActivity.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);

            }
        });


        ds_gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DsGiftCard.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);
            }
        });

        ds_deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, DsDeals.class);
                // MainActivity.this.finish();
                mHandler.removeCallbacks(m_Runnable);
                startActivity(in);
            }
        });



        setName();


        //serch test
        catogoryspinnerinsrch=(EditText)findViewById(R.id.catSpinner);
        serchtext=(MultiAutoCompleteTextView)findViewById(R.id.Search);
        serchgobtn=(ImageView)findViewById(R.id.goButton);


        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });

        serchgobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srchtext = serchtext.getText().toString();

                if(serchtext.length()==0){
                    serchtext.setError(getString(R.string.srh_here_error));
                }
                else {


                    Intent in = new Intent(MainActivity.this, SerchResult.class);
                    // MainActivity.this.finish();
                    in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                    in.putExtra("text", srchtext);
                    startActivity(in);
                    //new AsyncTaskNotifCounttJson().execute();
                }
            }
        });



        //serch test


        // Inflate the layout for this fragment


        // mToolbar = (Toolbar) findViewById(R.id.toolbar);
        settings=(ImageView)findViewById(R.id.login);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // call popup
                popupMethodbyBsr();



            }





        });




        checkForUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }
    //bsr new popup strt
    private void popupMethodbyBsr() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_signup_signin, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        TextView signup = (TextView) popupView.findViewById(R.id.signup_custom_popup);
        TextView profile = (TextView) popupView.findViewById(R.id.profile_custom_poup);
        TextView changue_languge = (TextView) popupView.findViewById(R.id.languge_custom_poup);
        final String popup_first_option;

        //used to set sign  out when looged user

        if(username!=null){
            signup.setText(getString(R.string.sign_out));
        }
        else
        {
            signup.setText(getString(R.string.signin));;

        }

        popup_first_option=signup.getText().toString().trim();




        signup.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Log.i("Signup",popup_first_option+"we1");
                if(popup_first_option.equals(getString(R.string.sign_out))){
                    //Do ssign out task

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    // alertDialog.setTitle("Alert..!");
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_signout_confirm));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
//                                            if(logintype.trim().equals("FA")) {
//                                                Signin signin = new Signin();
//                                                signin.fblogout();
//                                            }

                                    myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                                    SharedPreferences.Editor editor = myprefs.edit();
                                    editor.remove("bsrnme");
                                    editor.remove("cartcount");
                                    editor.remove("shaniusrid");
                                    editor.remove("useremail");
                                    editor.remove("userphone");
                                    editor.remove("logintype");

                                    editor.remove("ORDERREVIEW_NAME");
                                    editor.remove("ORDERREVIEW_ADDRESS");
                                    editor.remove("ORDERREVIEW_LOCATION");
                                    editor.remove("ORDERREVIEW_PHONE");
                                    editor.apply();
                                    sp.clearall();
                                    Intent in = new Intent(MainActivity.this, Signin.class);
                                    startActivity(in);


                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,  getString(R.string.no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent in = new Intent(MainActivity.this, MainActivity.class);
                                    //MainActivity.this.finish();
                                    startActivity(in);
                                }
                            });

                    alertDialog.show();
                    Log.i("Signup",popup_first_option+"we2");

                }else if(popup_first_option.equals(getString(R.string.signin))){
                    // do sign in task

                    Intent in = new Intent(MainActivity.this, Signin.class);
                    myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.remove("bsrnme");
                    editor.remove("shaniusrid");
                    editor.remove("useremail");
                    editor.remove("userphone");
                    editor.remove("logintype");
                    editor.apply();
                    startActivity(in);

                    Log.i("Signup",popup_first_option+"we3");


                }




                popupWindow.dismiss();


            }});

        profile.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //my profile

                Intent in = new Intent(MainActivity.this, MyProfile.class);
                startActivity(in);

                popupWindow.dismiss();


            }});

        changue_languge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MainActivity.this,LangugeSelection.class);

                startActivity(in);
            }
        });

        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(settings, 150, -30);



    }

    // bsr edit popup end


    //bsr new popup strt srch
    private void SerachTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
        final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
        final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
        final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(all.getText());
                popupWindow.dismiss();
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(products.getText());
                popupWindow.dismiss();
            }
        });
        catogory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(catogory.getText());
                popupWindow.dismiss();
            }
        });
        prdctcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(prdctcode.getText());
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



    }

    // bsr edit popup end srch
//exit by bsr



    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {

            finish(); // finish activity
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.exit_confirm));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                            //remove dflt langge selection

                            SharedPreferences.Editor editor = myprefs.edit();
                            editor.remove("DEFAULT_LANGUGE");
                            editor.apply();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                finishAffinity();
                            }
                            System.exit(0);

                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();
                        }
                    });

            alertDialog.show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);

        }

    }




    void setName(){
        int cnt=0;
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

        username = myprefs.getString("bsrnme", null);

        String test=getString(R.string.hi)+username;

        if(username!=null){
            gst.setText(test.toString());



        }



        else{
            gst.setText(getString(R.string.hi_guest));
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);


        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);


    }


    private final Runnable m_Runnable = new Runnable()
    {
        public void run()

        {

            setName();
            MainActivity.this.mHandler.postDelayed(m_Runnable, 5000);
        }

    };




    public class BannerAsynk extends AsyncTask<String,String,String> {
        int json_length=0;
        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.banner();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();
                productID=new  int[productLength];
                img=new String[productLength];
                ispdct_banner=new int[productLength];
                for(int i=0;i<productLength;i++)
                {
                    JSONObject c= dataJsonArr.getJSONObject(i);

                    productID[i] = c.optInt("RedId");
                    img[i]=c.optString("BannerImageMob").toString();

                    ispdct_banner[i]=c.optInt("BannerType");
                }
                json_length=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }else {


                mCustomPagerAdapter = new CustomPagerAdapter(MainActivity.this, img, productLength, productID,ispdct_banner);
                mViewPager = (ViewPager) findViewById(R.id.pager);

                mViewPager.setAdapter(mCustomPagerAdapter);
                mViewPager.setCurrentItem(0);
                mViewPager.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Execute some code after 2 seconds have passed
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                int currentpageposition=mViewPager.getCurrentItem();
                                mCustomPagerAdapter.setCurrentpage(currentpageposition);
                                mCustomPagerAdapter.setPagingEnabled(true);
                                mCustomPagerAdapter.currentPage=currentpageposition;
                            }
                        }, 1000);
                        mCustomPagerAdapter.setPagingEnabled(false);
                        return false;
                    }
                });
                mCustomPagerAdapter.setTimer(mViewPager, json_length);

            }


        }
    }
    public class NewsUpdatesAsync extends AsyncTask<String,String,String> {
        int json_length=0;
        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.newsupdations();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.optJSONArray("d");
                json_length = dataJsonArr.length();


                str_news_result_json= dataJsonArr.getJSONObject(0).optString("FlashNewsContent").toString();




            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(json_length==0){
                txt_news.setVisibility(View.GONE);
            }else{

                if(str_news_result_json.trim().equals("no news")){
                    txt_news.setVisibility(View.GONE);
                }else {


                    txt_news.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                    txt_news.setSingleLine();
                    txt_news.setMarqueeRepeatLimit(10);
                    txt_news.setFocusable(true);
                    txt_news.setHorizontallyScrolling(true);
                    txt_news.setFocusableInTouchMode(true);
                    txt_news.requestFocus();
                    if(txt_news.length()<80){
                        //  txt_news.setText("<html><body><FONT COLOR=\"#000000\" ><marquee id=\"mrqSlogan\"  direction=\"Left\" style=\"width: auto;\" >text your</marquee></FONT></body></html>");
                        txt_news.setText(str_news_result_json+"                                                                          ");
                    }else {
                        txt_news.setText(str_news_result_json+"                  ");
                    }

                }
            }


        }
    }

    //the api just used for to took notif count dirst time
    public class AsyncTaskNotifCounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {
            //used to aavoid repeat usage
            firstonly=true;

        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                JSONObject json = jParser.notifcount();
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                int_notcount = dataJsonArr.getJSONObject(0).optInt("count");
                jsonlen=dataJsonArr.length();
                //set badge notif count
                Log.i("count of len in main",jsonlen+"");




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            sp.setnotcount(int_notcount);


        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int countcart=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                if (userVLogIn.equalsIgnoreCase("0")) {

                } else{
                    countcart = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                    sp.setcartcount(countcart);
                }



                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


    }

}









