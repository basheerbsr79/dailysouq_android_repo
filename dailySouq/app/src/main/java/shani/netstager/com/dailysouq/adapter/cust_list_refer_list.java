package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;

public class cust_list_refer_list extends BaseAdapter {


    private Activity activity;
    String[] frndname,frndexpdate,refstatus;
    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;

    public cust_list_refer_list(Activity context, int length, String[] frndname, String[] frndexpdate, String[] refstatus) {

        this.activity=context;
        this.rec_count=length;
        this.frndname=frndname;
        this.frndexpdate=frndexpdate;
        this.refstatus=refstatus;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public static class ViewHolder{
        public TextView  frndnametext;
        public TextView expdatetext;
        public TextView statustext;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_refer_list, null);
            holder = new ViewHolder();

            holder.frndnametext = (TextView) vi.findViewById(R.id.cust_refer_name);
            holder.expdatetext = (TextView) vi.findViewById(R.id.cust_refer_expdate);
            holder.statustext = (TextView) vi.findViewById(R.id.cust_refer_status);

            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{


            holder.frndnametext.setText(frndname[position]);
            holder.expdatetext.setText(frndexpdate[position]);
            holder.statustext.setText(refstatus[position]);



        }catch(Exception e){

        }
        return vi;
    }
}
