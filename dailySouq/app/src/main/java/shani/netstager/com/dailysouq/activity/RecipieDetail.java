package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class RecipieDetail extends AppCompatActivity {
    ImageView profile,quickcheckout,Product_img,Notification;
    RelativeLayout cartcounttext;
    TextView title,chef_name,recpie_name,desc,iteminrecipie;
    Button bck_btn;
    String str_name,str_id,str_cheff,str_desc,str_imgpath,str_list_item_recipie;
//    ImageView footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;
    String userVLogIn,str_json_cart_count;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    Button addingrementts;
    int languge;
    String responce_json;
    ImageView btnShare;
    ImageView ivFavrate;
    String strQty="1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipie_detail);

        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        languge=myprefs.getInt("DEFAULT_LANGUGE",1);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        Intent in=getIntent();
        str_id=in.getStringExtra("recipie_id");

        profile=(ImageView)findViewById(R.id.profile);
        Product_img=(ImageView)findViewById(R.id.img_recipi);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        iteminrecipie=(TextView)findViewById(R.id.txt_lst_item_recipie);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        chef_name=(TextView)findViewById(R.id.chef_txt_recpie);
        recpie_name=(TextView)findViewById(R.id.textcust_recipie);
        desc=(TextView)findViewById(R.id.dscrption_txt_recpie);
        addingrementts=(Button)findViewById(R.id.add_cook_ingr);
        btnShare=(ImageView)findViewById(R.id.share);
        ivFavrate =(ImageView)findViewById(R.id.ivfavorate);


        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
             // new AsyncTaskTemptJson().execute();
            new AsyncTaskCarttJson().execute();
        }
        else{
            AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        ivFavrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userVLogIn.equalsIgnoreCase("0")){
                    AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(RecipieDetail.this, Signin.class);

                                    RecipieDetail.this.startActivity(in);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {

                    //do here faverate api
                    new AsyncTaskRecipieWishlist().execute();
                }
            }
        });




        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                share.putExtra(Intent.EXTRA_TEXT,"Please click the link to get the varieties of recipies.. http://dailysouq.in/recipes.aspx");
                startActivity(Intent.createChooser(share, "Share the  recipie to your friends.."));
            }
        });

//        //footer delartions
//
//        footerdsmore=(ImageView)findViewById(R.id.imageView15);
//
//        footerdssp=(ImageView)findViewById(R.id.imageView13);
//        footerdsshop=(ImageView)findViewById(R.id.imageView11);
//        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
//        footerdsgift=(ImageView)findViewById(R.id.imageView14);
//        footerdsmore=(ImageView)findViewById(R.id.imageView15);
//        footerdssp.setImageResource(R.drawable.footerdsspecialsselect);
//
//        //footer clicks
//
//        footerdsshop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in = new Intent(RecipieDetail.this, Categories.class);
//                RecipieDetail.this.finish();
//                startActivity(in);
//            }
//        });
//
//
//        footerdsdeal.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent in=new Intent(RecipieDetail.this,DsDeals.class);
//                RecipieDetail.this.finish();
//                startActivity(in);
//            }
//        });
//
//        footerdsgift.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent in=new Intent(RecipieDetail.this,DsGiftCard.class);
//                RecipieDetail.this.finish();
//                startActivity(in);
//            }
//        });
//
//        footerdsmore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in=new Intent(RecipieDetail.this,Menu_List.class);
//                RecipieDetail.this.finish();
//                startActivity(in);
//            }
//        });
//        footerdssp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in=new Intent(RecipieDetail.this,DsSpecials.class);
//                RecipieDetail.this.finish();
//                startActivity(in);
//            }
//        });


        addingrementts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(userVLogIn.equalsIgnoreCase("0")){
                    AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(RecipieDetail.this, Signin.class);

                                    RecipieDetail.this.startActivity(in);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {

                    Intent in = new Intent(RecipieDetail.this, RecipiePopupItems.class);
                    in.putExtra("PID", str_id);
                    in.putExtra("USERID", userVLogIn);
                    in.putExtra("LANID", languge);
                    in.putExtra("NAME", str_name);
                    in.putExtra("QTY",strQty);

                    startActivity(in);
                }
            }
        });

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });













                profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent in = new Intent(RecipieDetail.this, EditProfile.class);

                        startActivity(in);
                    }
                });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(RecipieDetail.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(RecipieDetail.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(RecipieDetail.this, Notification.class);

                startActivity(in);
            }
        });


        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(RecipieDetail.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.recipiedetail(str_id);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");


                for(int i=0;i<dataJsonArr.length();i++) {


                    str_name = dataJsonArr.getJSONObject(i).optString("recipename");
                    str_cheff = dataJsonArr.getJSONObject(i).optString("chefname");
                    str_imgpath = dataJsonArr.getJSONObject(i).optString("imagepath");
                    str_desc = dataJsonArr.getJSONObject(i).optString("description");
                    str_list_item_recipie=dataJsonArr.getJSONObject(i).optString("listofitems");
                    strQty=dataJsonArr.getJSONObject(i).optString("Quantity");


                }
                jsonlen=dataJsonArr.length();



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();

if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {


    title.setText(str_name);
    chef_name.setText( str_cheff);
    Picasso.with(RecipieDetail.this).load(str_imgpath).into(Product_img);
    recpie_name.setText(str_name);
    iteminrecipie.setText(str_list_item_recipie);
    desc.setText(str_desc + "\n" + "\n" + "\n");

    new AsyncTaskTemptJson().execute();

}




        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }

                //  Log.i("arrivedcount", str_json_cart_count);
            }
          //  mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskTemptJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;

        @Override
        protected void onPreExecute() {

            mProgressDialog = new ProgressDialog(RecipieDetail.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.recipietempsave(userVLogIn,str_id,"0");
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");
                //  String noofperson = dataJsonArr.getJSONObject(0).optString("Qty");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                alertDialog.setTitle(RecipieDetail.this.getString(R.string.not_found));
                alertDialog.show();
            }else {



            }

            // mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskRecipieWishlist extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;

        @Override
        protected void onPreExecute() {

            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.recipiefaveratesave(userVLogIn,str_id);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");
                //  String noofperson = dataJsonArr.getJSONObject(0).optString("Qty");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                alertDialog.setTitle(RecipieDetail.this.getString(R.string.not_found));
                alertDialog.show();
            }else {

                    if(responce_json.equalsIgnoreCase("success")){

                        showAlertDone("succsesfully added to  recipie wishlist");
                    }else  if(responce_json.equalsIgnoreCase("update")){

                        showAlertDone("succsesfully updated in recipie wishlist");
                    }else {
                        showAlert("Something went wrong");
                    }

            }

            // mProgressDialog.dismiss();
        }
    }

    private void showAlertDone(String s) {

        AlertDialog.Builder builder=new AlertDialog.Builder(RecipieDetail.this);
        builder.setTitle("DailySouq");
        builder.setMessage(s)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                Intent in=new Intent(RecipieDetail.this,MainActivity.class);
                                startActivity(in);

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAlert(String s) {

        AlertDialog.Builder builder=new AlertDialog.Builder(RecipieDetail.this);
        builder.setTitle("DailySouq");
        builder.setMessage(s)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing


                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(RecipieDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(RecipieDetail.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}
