package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.CookeryPlay;

/**
 * Created by user on 17/10/15.
 */
public class CustomCookeryView extends BaseAdapter {
    String[]CookeryIds,CookeryTitles,CookeryVideoUrls,CookeryDetailss,CookeryImages,CookeryLists;
    ViewHolder holder;
    LayoutInflater inf;
    int rec_count;
    Context context;
    public static final String API_KEY = "AIzaSyBCkZGHdBZbTe3cyn-oEKpRpfHquUTnsiU";
    int sel_pos=0;
    public CustomCookeryView(Context con, int length, String[] cookeryId, String[] cookeryTitle, String[] cookeryVideoUrl, String[] cookeryDetails, String[] cookeryImage, String[] cookeryList){
        this.context=con;
        this.rec_count=length;
        this.CookeryIds=cookeryId;
        this.CookeryTitles=cookeryTitle;
        this.CookeryVideoUrls=cookeryVideoUrl;
        this.CookeryDetailss=cookeryDetails;
        this.CookeryImages=cookeryImage;
        this.CookeryLists=cookeryList;


        inf=(LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }



    class ViewHolder {
        ImageView img;
        TextView pName;
      //  YouTubePlayerView player;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null) {
            vi = inf.inflate(R.layout.custom_cookery_views, null);
            holder = new ViewHolder();
            holder.img = (ImageView) vi.findViewById(R.id.customckryvewimg);
            holder.pName = (TextView) vi.findViewById(R.id.customckryvewtext);
           // holder.player=(YouTubePlayerView)vi.findViewById(R.id.youtube_player) ;
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        try {

            holder.pName.setText(CookeryTitles[position]);
            Log.i("urlcook",CookeryImages[position]);

            Picasso.with(context).load(CookeryImages[position]).placeholder(R.drawable.placeholder).into(holder.img);

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Intent in =new Intent(context.getApplicationContext(),CookeryPlay.class);
                   in.putExtra("cookery id",CookeryIds[position]);
                   in.putExtra("cookery url",CookeryVideoUrls[position]);
                   in.putExtra("cookery details", CookeryDetailss[position]);
                    in.putExtra("cookery name", CookeryTitles[position]);
                   context.startActivity(in);
                }
            });
            holder.pName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in =new Intent(context.getApplicationContext(),CookeryPlay.class);
                    in.putExtra("cookery id",CookeryIds[position]);
                    in.putExtra("cookery url",CookeryVideoUrls[position]);
                    in.putExtra("cookery details", CookeryDetailss[position]);
                    in.putExtra("cookery name", CookeryTitles[position]);
                    context.startActivity(in);
                }
            });



            /** Initializing YouTube player view **/
           // YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
         //   holder.player.initialize(API_KEY, this);
            setpos(position);


        }
        catch (Exception e){
            e.printStackTrace();
        }






        return vi;
    }
    public void setpos(int p){
      sel_pos=p;
    }

}
