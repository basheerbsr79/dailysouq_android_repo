package shani.netstager.com.dailysouq.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.Json;


public class ResetForgotPassword extends AppCompatActivity {
    EditText ed_frogotreset,ed_psd,ed_cnfrmpsd;
    Button btn_forgot,btn_resnd;
    ImageButton forLogin;
    String forEmail,forpsd,forcnfrm;
    String forpass;
    String useremail;
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_forgot_password);
        ed_frogotreset = (EditText) findViewById(R.id.forgotEdit);
        ed_psd = (EditText) findViewById(R.id.psdfrgt);
        ed_cnfrmpsd = (EditText) findViewById(R.id.cnfrmpsdfrgt);
        btn_forgot = (Button) findViewById(R.id.forgotbutton);
        btn_resnd = (Button) findViewById(R.id.resetbutton);
        forLogin = (ImageButton) findViewById(R.id.forgotimageButton);
        Intent in=getIntent();
        useremail=in.getStringExtra("useremail");
        ed_frogotreset.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "Verification Code" + "</small>"));
        ed_psd.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "New Password" + "</small>"));
        ed_cnfrmpsd.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "Confirm Password" + "</small>"));
        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forEmail = ed_frogotreset.getText().toString();


                //internet checking


                if (ed_psd.getText().toString().equals("") || ed_cnfrmpsd.getText().toString().equals("") || ed_frogotreset.getText().toString().equals("")) {


                    ed_psd.setError(getString(R.string.plz_fill));
                    ed_cnfrmpsd.setError(getString(R.string.plz_fill));
                    ed_frogotreset.setError(getString(R.string.plz_fill));



                } else {


                    if (ed_psd.getText().toString().equals(ed_cnfrmpsd.getText().toString())) {


                        forpsd = ed_psd.getText().toString();
                        forcnfrm = ed_cnfrmpsd.getText().toString();

                        //internet checking
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent.equals(true)) {


                            if (forpsd.length() < 6)

                            {


                                ed_psd.setError(getString(R.string.mustbe6chrtr));

                                ed_cnfrmpsd.setError(getString(R.string.mustbe6chrtr));
                            } else {
                                getForgotPassWord();

                            }


                        } else {

                            AlertDialog alertDialog = new AlertDialog.Builder(ResetForgotPassword.this).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(getString(R.string.alert_net_failed));
                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            onBackPressed();

                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();

                        }


                    } else {
                        //  Toast.makeText(ChangePassword.this, "password mismatch .", Toast.LENGTH_LONG).show();

                        ed_psd.setError(getString(R.string.psd_mismatch));
                        ed_cnfrmpsd.setError(getString(R.string.psd_mismatch));

                    }
                }


            }
        });
        forLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ResetForgotPassword.this, ForgotPassword.class);

                startActivity(in);
            }
        });


        btn_resnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if(isInternetPresent.equals(true)){

                    getResendPassWord();
                    AlertDialog alertDialog = new AlertDialog.Builder(ResetForgotPassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_psd_forgot_send_via_mail));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();


                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(ResetForgotPassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }

            }
        });



    }
    void setreset(){

        ed_frogotreset.setError(getString(R.string.reset_mismacth));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void getForgotPassWord() {
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_forgotpassword_update");
                    jsonn.put("Email",useremail);
                    jsonn.put("VerificationCode",forEmail);
                    jsonn.put("Password", forcnfrm);
                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    forpass = EntityUtils.toString(resEntity);
                    forpass = forpass.trim();

                    JSONObject jasonob = new JSONObject(forpass);
                    result = jasonob.getJSONArray("d").getJSONObject(0).optString("result").toString().trim();

                    if(result.equals("updated")){


                        AlertDialog alertDialog = new AlertDialog.Builder(ResetForgotPassword.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.updated));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        Intent in = new Intent(ResetForgotPassword.this, Signin.class);


                                        startActivity(in);


                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();








                    }
                    else if(result.equals("verification code mismatch")){

                      //  Toast.makeText(getApplicationContext(),getString(R.string.reset_mismacth),Toast.LENGTH_LONG).show();
                       //setreset();
                        showAlert(getString(R.string.reset_mismacth)+" Please try again.");

                    }
                    else{
                        Toast.makeText(getApplicationContext(),getString(R.string.error),Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();
            }
        };
        th.start();

    }

    public void getResendPassWord() {
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_forgotpassword_verificationcode");
                    jsonn.put("Email",useremail);

                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    forpass = EntityUtils.toString(resEntity);
                    forpass = forpass.trim();

                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();
            }
        };
        th.start();

    }


    public void showAlert(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(ResetForgotPassword.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                  finish();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
