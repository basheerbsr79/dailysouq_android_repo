package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import shani.netstager.com.dailysouq.R;

/**
 * Created by prajeeshkk on 10/11/15.
 */
public class CustomGallery extends BaseAdapter {
    ViewHolder holder;
    Context context;
    int imageLength;
    String[]galleryImg;
    LayoutInflater inflater;

    public CustomGallery(Context con, int imLength, String[] img)
    {
        context=con;
        imageLength=imLength;
        galleryImg=img;
        inflater = LayoutInflater.from(this.context);
    }
    @Override
    public int getCount() {
        return imageLength;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

   class ViewHolder
   {
       ImageView imageView;
   }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;

        if (view == null) {

            vi = inflater.inflate(R.layout.imageview, null);
            holder = new ViewHolder();
            holder.imageView = (ImageView) vi.findViewById(R.id.productImage);
            vi.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) vi.getTag();
        }

        try {

            Picasso.with(context).load(galleryImg[position]).into(holder.imageView);
        }
catch (Exception e)
{
    e.printStackTrace();
}

        return vi;
    }
}
