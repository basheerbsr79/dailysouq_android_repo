package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.Products;
import shani.netstager.com.dailysouq.support.DS_SP;


/**
 * Created by prajeeshkk on 21/09/15.
 */
public class CustomGridViewAdapter extends BaseAdapter {

    LayoutInflater inf;
    private Activity activity;
    ArrayList<String> nw = new ArrayList<String>();
    Bitmap bitmap;
    ViewHolder holder;
    private String[] rlno1;
    private String[] stnm1={};
    private String[] arr1;
    RelativeLayout cartcounttext;
    TextView cartcountbadge;
    String cusId;
    DS_SP sp;

    public CustomGridViewAdapter(Activity a, String[] rlnoarray, String[] stnmarray, String[] arrarry, RelativeLayout cartcounttext, TextView cartcountbadge,String cusId) {
        activity = a;
        rlno1 = rlnoarray;
        stnm1 = stnmarray;
        arr1 = arrarry;
        this.cartcounttext= cartcounttext;
        this.cartcountbadge= cartcountbadge;
        this.cusId=cusId;
        sp=new DS_SP(activity.getApplicationContext());
        inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return stnm1.length;

    }

    @Override
    public Object getItem(int arg0) {
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    public static class ViewHolder {

        public TextView rlno;
        public ImageView stnm;


    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub

        int rollnocount = arg0;
        View vi = arg1;

        if (arg1 == null) {

            vi = inf.inflate(R.layout.row_grid, null);
            holder = new ViewHolder();


            holder.rlno = (TextView) vi.findViewById(R.id.grid_item_label);
            holder.stnm = (ImageView) vi.findViewById(R.id.grid_item_image);


            vi.setTag(holder);

        } else {
            holder = (ViewHolder) vi.getTag();
        }
        try {

            new AsyncTaskCartcounttJson().execute();

            holder.rlno.setText(rlno1[rollnocount]);
            final String abc = (rlno1[rollnocount]);


            Picasso.with(activity).load(stnm1[rollnocount]).into(holder.stnm);
            holder.stnm.setScaleType(ImageView.ScaleType.FIT_XY);


            final String idss=(arr1[rollnocount]);


            holder.stnm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in=new Intent(activity,Products.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("ID",idss);
                    in.putExtra("Categoryname",abc);

                    activity.startActivity(in);

                }
            });



        } catch (Exception e) {

        }
        return vi;
    }



    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int str_json_cart_count=0;
        JSONArray dataJsonArr;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                sp.setcartcount(str_json_cart_count);
                Log.i("str_json_cart_count",str_json_cart_count+"");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle(activity.getString(R.string.error));
                alertDialog.show();
            }else {
                sp.setcartcount(str_json_cart_count);
                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(cusId.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }

            // mProgressDialog.dismiss();
        }
    }

}



