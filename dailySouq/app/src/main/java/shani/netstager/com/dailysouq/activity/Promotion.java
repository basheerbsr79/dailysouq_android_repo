package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomPagerAdapterPromotion;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class Promotion extends AppCompatActivity {
    ImageView Notification;
    RelativeLayout cartcounttext;
    TextView title;
    Button bck_btn;
    ProgressDialog mProgressDialog;
    ImageView profile,quickcheckout;
    int qstn_count=0;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;
    String userVLogIn;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    String [] str_promotion_id,str_promotion_text,str_promotion_image,str_promotion_description,str_catid_promotion,str_brandid_promotion;
    ViewPager mViewPager;
    CirclePageIndicator mIndicator;
    ProgressDialog mDialog;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        //setting context
        context = Promotion.this;

        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge =(TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }


        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
            new BannerAsynk().execute();
            //  new AsyncTaskCartcounttJson().execute();
           // new AsyncTaskCarttJson().execute();
        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(Promotion.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        title.setText("Promotions");
        bck_btn=(Button)findViewById(R.id.menue_btn);


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Promotion.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Promotion.this,CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Promotion.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Promotion.this, Notification.class);

                startActivity(in);
            }
        });

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });




        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Promotion.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Promotion.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(Promotion.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

    public class BannerAsynk extends AsyncTask<String,String,String> {
        int json_length=0;
        JSONArray dataJsonArr;

        @Override
        protected void onPreExecute() {
            mDialog=new ProgressDialog(context);
            mDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
            mDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.promotiondailysoq();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                json_length =dataJsonArr.length();
                str_promotion_id=new  String[json_length];
                str_promotion_text=new String[json_length];
                str_promotion_image=new  String[json_length];
                str_promotion_description=new String[json_length];
                str_brandid_promotion=new String[json_length];
                str_catid_promotion=new String[json_length];
                for(int i=0;i<json_length;i++)
                {
                    JSONObject c= dataJsonArr.getJSONObject(i);

                    str_promotion_id[i] = c.optString("ProductId");
                    str_promotion_text[i]= c.optString("PromotionText");
                    str_promotion_image[i] = c.optString("Imagepath");
                    str_brandid_promotion[i] = c.optString("BrandId");
                    str_catid_promotion[i] = c.optString("CategoryId");
                    //str_promotion_description[i]=c.optString("Description").toString();

                }
                json_length=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            mDialog.dismiss();
            if(json_length==0){
               // MyAlert alert=new MyAlert();
               // alert.showAlert(context,"no promotion found");
                Toast.makeText(Promotion.this, "No Promotion found", Toast.LENGTH_SHORT).show();
            }else {

                mViewPager = (ViewPager) findViewById(R.id.graphs);

                CustomPagerAdapterPromotion mCustomPagerAdapter = new CustomPagerAdapterPromotion(context,json_length, str_promotion_id, str_promotion_text, str_promotion_image,str_promotion_description,str_catid_promotion,str_brandid_promotion);
                mViewPager.setAdapter(mCustomPagerAdapter);
                mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
                mIndicator.setViewPager(mViewPager);

                //ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);

               /* mViewPager.setAdapter(mCustomPagerAdapter);
                mViewPager.setCurrentItem(0);*/
            }


        }
    }


}
