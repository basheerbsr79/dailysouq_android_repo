package shani.netstager.com.dailysouq.GCM;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;


public class NotificationGCM extends Activity {

    TextView textView;
    String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_gcm);

        textView=(TextView)findViewById(R.id.text);

        message=getIntent().getStringExtra("message");
        textView.setText(message);

    }
}
