package shani.netstager.com.dailysouq.activity;


import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class DsSpecials extends FragmentActivity {

    ImageView footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore,cookery,quiz,recipie,partyOrder;
    ImageView profile,quickcheckout,Notification;
    TextView title;
    RelativeLayout cartcounttext;
    Button bck_btn;

    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;
    ProgressDialog mProgressDialog;

    ImageView serchgobtn;

    String srchtext;

    TextView test;
    //serch test
    SharedPreferences myprefs;
    String userVLogIn,str_json_cart_count;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ds_specials);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
//notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }






        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);


        float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsSpecials.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsSpecials.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if(isInternetPresent.equals(true)){

                    // new AsyncTaskCartcounttJson().execute();
                    new  AsyncQuickCheckOut().execute();
                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(DsSpecials.this, Notification.class);

                startActivity(in);
            }
        });

        bck_btn=(Button)findViewById(R.id.menue_btn);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        partyOrder=(ImageView)findViewById(R.id.imgparty);
       quiz=(ImageView)findViewById(R.id.imageQuiz);

        recipie=(ImageView)findViewById(R.id.imgrecipies);

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsSpecials.this,QuizView.class);

                startActivity(in);
            }
        });
        cookery=(ImageView)findViewById(R.id.imgckry);



        cookery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

         /*       AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.coming_soon));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                            }
                        });


                alertDialog.show();*/
                Intent in=new Intent(DsSpecials.this,CookeryView.class);
            //
                startActivity(in);
            }
        });
        partyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.coming_soon));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                            }
                        });


                alertDialog.show();*/
                Intent in=new Intent(DsSpecials.this,PartyOrderList.class);

                startActivity(in);
            }
        });


        footerdssp.setImageResource(R.drawable.footerdsspecialsselect);
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setText(getString(R.string.ds_special));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });




        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DsSpecials.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(DsSpecials.this,DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsSpecials.this,DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsSpecials.this,Menu_List.class);

                startActivity(in);
            }
        });
        recipie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.coming_soon));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                            }
                        });


                alertDialog.show();*/
                Intent in=new Intent(DsSpecials.this,Recipie.class);

                startActivity(in);
            }
        });


        //serch test
        catogoryspinnerinsrch=(EditText) findViewById(R.id.catSpinner);
        serchtext=(MultiAutoCompleteTextView)findViewById(R.id.Search);
        serchgobtn=(ImageView)findViewById(R.id.goButton);



        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });



        serchgobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srchtext=serchtext.getText().toString();
                if(serchtext.length()==0){
                    serchtext.setError(getString(R.string.srh_here_error));
                }
                else {



                    Intent in = new Intent(DsSpecials.this, SerchResult.class);
                    in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                    in.putExtra("text", srchtext);

                    startActivity(in);
                    //new AsyncTaskNotifCounttJson().execute();
                }
            }
        });



        //serch test




        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    //bsr new popup strt srch
    private void SerachTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
        final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
        final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
        final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(all.getText());
                popupWindow.dismiss();
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(products.getText());
                popupWindow.dismiss();
            }
        });
        catogory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(catogory.getText());
                popupWindow.dismiss();
            }
        });
        prdctcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(prdctcode.getText());
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



    }

    // bsr edit popup end srch




    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(DsSpecials.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){

            }else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }

           // Log.i("arrivedcount", str_json_cart_count);


        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(DsSpecials.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(DsSpecials.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}