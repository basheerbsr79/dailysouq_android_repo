package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.BannerDetail;
import shani.netstager.com.dailysouq.activity.Products;

/**
 * Created by prajeeshkk on 26/11/15.
 */
public class CustomPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;

   int pLength;
    String[]img;
    int[]productId,isprdtag;
    final Handler handler = new Handler();
    public Timer swipeTimer ;
    boolean enabled=true;
    int position=0;
    public static int currentpageposition;
    public static int currentPage = currentpageposition ;


    public CustomPagerAdapter(Context context,String[]image,int Blenth,int[]produ,int[]isprdtag) {
        mContext = context;
        img=image;
        pLength=Blenth;
        productId=produ;
        this.isprdtag=isprdtag;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pLength;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        this.position=position;
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.bannerImg);
        TextView text = (TextView) itemView.findViewById(R.id.test_cound);
        Picasso.with(mContext).load(img[position]).placeholder(R.drawable.placeholder).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //checked its catogory or prdct
                    if(isprdtag[position]==1){

                        if(productId[position]==0){

                        }else{
                            Intent in = new Intent(mContext, BannerDetail.class);
                            in.putExtra("ProductId", productId[position]);
                            // in.putExtra("productname","thakkali");
                            mContext.startActivity(in);
                        }
                    }else  if(isprdtag[position]==0){
                        if(productId[position]==0){

                        }else{
                            Intent in=new Intent(mContext,Products.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            String catid=String.valueOf(productId[position]);
                            in.putExtra("ID",catid);
                            mContext.startActivity(in);
                        }
                    }



            }
        });
        text.setText(position+1+".");
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setTimer(final ViewPager myPager, int time){
        final int size =pLength;


        final Runnable Update = new Runnable() {
            int NUM_PAGES =size;

            public void run() {
                if (currentPage == NUM_PAGES ) {
                    currentPage = 0;
                }
                myPager.setCurrentItem(currentPage++, true);
            }
        };

        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                if(enabled==true){
                    handler.post(Update);
                }else{
                   // Toast.makeText(mContext,"ok",Toast.LENGTH_LONG).show();
                }

            }
        }, 1000, time * 1000);
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setCurrentpage(int currentpage) {

        this.currentpageposition = currentpage;
    }
}
