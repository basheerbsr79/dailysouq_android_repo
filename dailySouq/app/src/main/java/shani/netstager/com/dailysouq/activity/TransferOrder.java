package shani.netstager.com.dailysouq.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomTimeSlot;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class TransferOrder extends AppCompatActivity {

    String strorderid="";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+",str_referemail;
    TextView txtname,txtemail,txtdeladress;
    JSONArray dataJsonArr;
    Button submit;
    ProgressDialog mProgressDialog;
    RelativeLayout fullview;
    String str_result_submit,str_paymtid_submit;
    String str_trnsorder_custid,str_trns_postcode,str_trns_landmark,str_trns_address;
    String str_trns_contctnmae,str_trns_contctnumber,str_trns_locationid;
    String str_trns_deliveradress_id,str_trns_dfltaddress,str_trns_cityid;
    ListView list;
    int productLength, y, m, d;
    String[] customerId, toTime, slotid;
    EditText ed_date;
    static final int DATE_ID = 0;
    String dtest,mtest;
    String cid, deliverydate, timeslotid="0",poscurreload;
    SharedPreferences myprefs;
    String[] imageId;

    RadioGroup paymentradio;
    RadioButton paymentoptionradio;

    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment;
    String str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment_quick;
    String userVLogIn, str_deliverydate_payment, str_json_cart_count, str_deliverytime_payment;
    String str_deliveryadresid_payment, str_opt_payment_quick;
    String str_paymentmethod_payment_name;
    int str_paymentmethod_payment;

    TextView title;
    Button Confirm, bck_btn;
    ImageView profile, quickcheckout, Notification;
    RelativeLayout cartcounttext;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;

    //variables are used for ebs
    private static String HOST_NAME = "EBS";

    ArrayList<HashMap<String, String>> custom_post_parameters;



    private static final int ACC_ID = 19241; // Provided by EBS

    private static final String SECRET_KEY = "5e95119c0b20a69f9a970dfe3aebded6";

    private static final double PER_UNIT_PRICE = 1.00;

    double totalamount;
    String total;
    String AFTER_OREDER_ID,AFTER_OREDER_NAME,AFTER_ORDER_EMAIL;

    String fulladdressoftransferuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_order);
        fullview=(RelativeLayout)findViewById(R.id.full_transfer);
        txtname=(TextView)findViewById(R.id.nametrn);
        txtemail=(TextView)findViewById(R.id.emailtrn);
        txtdeladress=(TextView)findViewById(R.id.adrestrn);
        strorderid=getIntent().getStringExtra("OrderId");
        submit=(Button)findViewById(R.id.submit_tranfrorder);
        ed_date = (EditText) findViewById(R.id.choose_date);
        paymentradio = (RadioGroup) findViewById(R.id.paymentradiogrp);
        profile = (ImageView) findViewById(R.id.profile);
        bck_btn = (Button) findViewById(R.id.menue_btn);
        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        title = (TextView) findViewById(R.id.namein_title);
        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        title.setText(getString(R.string.transfer_order));
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();}});


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TransferOrder.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TransferOrder.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(TransferOrder.this, Notification.class);

                startActivity(in);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!timeslotid.equalsIgnoreCase("0")){

                    //get radio options
                    getradiooption();



                    new AsyncSubmitJson().execute();
                }else {
                    AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_select_address_timeslot));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }
            }
        });

        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {
            //  new AsyncTaskCartcounttJson().execute();
          //  new AsyncTaskDeliveryAmount().execute();
         //   new AsyncTaskCheckOutJson().execute();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            onBackPressed();
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        tomorrowDateSet();

        showalert();
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        final Calendar calendar = Calendar.getInstance();
        y = calendar.get(Calendar.YEAR);
        m = calendar.get(Calendar.MONTH);
        d = calendar.get(Calendar.DAY_OF_WEEK);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        ed_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showDialog(DATE_ID);
                ed_date.setEnabled(false);
                return false;
            }
        });

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    void getradiooption(){
        int selectedId = paymentradio.getCheckedRadioButtonId();
        if (selectedId != 0) {
            // find the radiobutton by returned id

            paymentoptionradio = (RadioButton) findViewById(selectedId);

            str_opt_payment = paymentoptionradio.getText().toString().trim();
            if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                str_paymentmethod_payment = 1;
                str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);
            }
            else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                str_paymentmethod_payment = 2;
                str_paymentmethod_payment_name=getString(R.string.creditdebitcard);
            }
            else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                str_paymentmethod_payment = 3;
                str_paymentmethod_payment_name=getString(R.string.ewallet);

            }
            else {
                str_paymentmethod_payment = 1;
                str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);

            }

        } else {
            selectedId = R.id.codradio;
            paymentoptionradio = (RadioButton) findViewById(selectedId);
            paymentoptionradio.setError(getString(R.string.plz_select));
        }
    }
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                // set partyorderpersons picker as current partyorderpersons

                DatePickerDialog dialog = new DatePickerDialog(this, datePickerListener, y, m, d);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CheckOut.this,"clicked cancel",Toast.LENGTH_LONG).show();
                    }
                });
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, 2);
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                return dialog;



        }
        return null;
    }



    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            y = selectedYear;
            m = selectedMonth;

            if(selectedMonth<9)
            {
                m=0+selectedMonth;
                mtest="0"+String.valueOf(selectedMonth+1);

                m=Integer.parseInt(mtest);

            }
            else{
                mtest=String.valueOf(selectedMonth+1);

            }

            if(selectedDay<10)
            {
                d=0+selectedDay;
                dtest="0"+String.valueOf(selectedDay);

                d=Integer.parseInt(dtest);

            }
            else{
                dtest=String.valueOf(selectedDay);
                d=Integer.parseInt(dtest);
            }
            // set selected partyorderpersons into datepicker also
            //used to s5 issue fix
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 2);
            int testdate=c.get(Calendar.DATE);
            int curday=new Date().getDate();
            Log.i("testdate",testdate+"...."+d+"..."+curday);

            if(d>=curday&&d<=testdate){
                // Toast.makeText(CheckOut.this,"ok",Toast.LENGTH_LONG).show();
                // set selected partyorderpersons into textview
                ed_date.setText(new StringBuilder().append(dtest)
                        .append("/").append(mtest).append("/").append(y));
                deliverydate = ed_date.getText().toString();
                new AsynchTimeSlot().execute();
                // set selected partyorderpersons into datepicker also
                ed_date.setEnabled(true);
            }else{
                ed_date.setEnabled(true);
                //  Toast.makeText(CheckOut.this,"not",Toast.LENGTH_LONG).show();
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                alertDialog.setMessage("Please select current day or two days forward");
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }




        }
    };


    //method to send tomarrow partyorderpersons aand time loading
    void  tomorrowDateSet(){
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 0);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        String todayAsString = dateFormat.format(today);
        String tomorrowAsString = dateFormat.format(tomorrow);
        deliverydate=tomorrowAsString;
        ed_date.setText(deliverydate);
    }

    public  void showalert(){

        final AlertDialog.Builder alert = new AlertDialog.Builder(TransferOrder.this);
        alert.setCancelable(false);
        alert.setTitle(getString(R.string.transfer_order)); //Set Alert dialog title here
        alert.setMessage(getString(R.string.email_phone_link_user)); //Message here

        // Set an EditText view to get user input
        final EditText input = new EditText(TransferOrder.this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        input.setHint(getString(R.string.email_phone));
        alert.setView(input);

        alert.setPositiveButton(getString(R.string.transfer_order), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //You will get as string input data in this variable.
                // here we convert the input to a string and show in a toast.
                String srt = input.getEditableText().toString();


                if (srt.matches(emailPattern) && srt.length() > 0) {

                    str_referemail=srt;
                    Toast.makeText(TransferOrder.this, srt, Toast.LENGTH_LONG).show();
                    //method to confirm the user eamil ok

                    new AsyncTaskAddNewLinkedUserJson().execute();

                    new AsynchTimeSlot().execute();

                }
                else{

                    if(srt.length()==10){
                        str_referemail=srt;
                        Toast.makeText(TransferOrder.this, srt, Toast.LENGTH_LONG).show();


                        //method to confirm the user mbile ok
                        new AsyncTaskAddNewLinkedUserJson().execute();
                        new AsynchTimeSlot().execute();

                    }else{

                        input.setError(getString(R.string.invalid_email_phone));
                        Toast.makeText(TransferOrder.this,getString(R.string.invalid_email_phone), Toast.LENGTH_LONG).show();
                        onBackPressed();
                    }

                }

            } // End of onClick(DialogInterface dialog, int whichButton)
        }); //End of alert.setPositiveButton
        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                onBackPressed();
            }
        }); //End of alert.setNegativeButton
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
       /* Alert Dialog Code End*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class AsyncTaskAddNewLinkedUserJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        String str_result;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(TransferOrder.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
          //  mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.addtransferordercustomer(str_referemail);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_trns_address=dataJsonArr.getJSONObject(0).optString("Address");
                str_trns_cityid=dataJsonArr.getJSONObject(0).optString("CityId");
                str_trns_contctnmae=dataJsonArr.getJSONObject(0).optString("ContactName");
                str_trns_contctnumber=dataJsonArr.getJSONObject(0).optString("ContactNo");
                str_trns_deliveradress_id=dataJsonArr.getJSONObject(0).optString("DeliveryAddressId");
                str_trns_dfltaddress=dataJsonArr.getJSONObject(0).optString("DefaultAddress");
                str_trns_landmark=dataJsonArr.getJSONObject(0).optString("LandMark");
                str_trns_locationid=dataJsonArr.getJSONObject(0).optString("LocationId");
                str_trns_postcode=dataJsonArr.getJSONObject(0).optString("PostCode");
                str_trnsorder_custid=dataJsonArr.getJSONObject(0).optString("CustomerId");

                fulladdressoftransferuser="Name : "+str_trns_contctnmae+" ,"+"Address: "+str_trns_address+","+"Contact No: "+str_trns_contctnumber+","
                        +"Landmark: "+str_trns_landmark+","+"Postcode: "+str_trns_postcode+","+"Landmark: "+str_trns_landmark+","+"City Id: "+str_trns_cityid+",";

                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          //  mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                alertDialog.setTitle(getString(R.string.not_found));
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
                alertDialog.show();
            }else {

                fullview.setVisibility(View.VISIBLE);


                txtname.setText(str_trns_contctnmae+" ");
                txtemail.setText(str_trns_contctnumber+" ");
                txtdeladress.setText(str_trns_address+" ");

            }
        }
    }
    public class AsyncSubmitJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        String str_result;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(TransferOrder.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.submittransferorder(str_trns_deliveradress_id,str_trnsorder_custid,strorderid,deliverydate,str_paymentmethod_payment,fulladdressoftransferuser,timeslotid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_result_submit=dataJsonArr.getJSONObject(0).optString("result");
                str_paymtid_submit=dataJsonArr.getJSONObject(0).optString("PaymentMethodId");
                total=dataJsonArr.getJSONObject(0).optString("Totalamount");
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                alertDialog.setTitle(getString(R.string.not_found));
                alertDialog.show();
            }else {

                if(str_result_submit.length()>=1) {

                    if (str_paymtid_submit.trim().equalsIgnoreCase("2")) {
                        //internetbanking

                        //netbank
                        SharedPreferences.Editor editor = myprefs.edit();
                        editor.putInt("PAYMENT_GATEWAY", 3);
                        editor.apply();
                        Log.i("Arrived @","net"+"");

                        //Toast.makeText(OrderReview.this,"Need TO Add this",Toast.LENGTH_LONG).show();
                        callEbsKit(TransferOrder.this);

                    } else {

                        Toast.makeText(TransferOrder.this, "Succsessfully transferd the order", Toast.LENGTH_LONG).show();
                        Intent in=new Intent(TransferOrder.this,MainActivity.class);
                        startActivity(in);

                    }

                }else {
                    Toast.makeText(TransferOrder.this, "Failed  please  try agin", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    public class AsynchTimeSlot extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int prdlength=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(TransferOrder.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject jsonObject = jParser.time_slot(deliverydate, userVLogIn);
                Log.i("value",deliverydate+userVLogIn);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                productLength = dataJsonArr.length();
                toTime = new String[dataJsonArr.length()];
                slotid = new String[dataJsonArr.length()];
                imageId=new String[dataJsonArr.length()];
                for (int i = 0; i < productLength; i++) {
                    JSONObject c = dataJsonArr.getJSONObject(i);
                    toTime[i] = dataJsonArr.getJSONObject(i).optString("ToTime");
                    slotid[i] = c.optString("DeliverySlotId").toString();
                    imageId[i]=c.optString("SlotImage").toString();

                }
                prdlength=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            mProgressDialog.dismiss();
            if(prdlength==0){

            }else {


                CustomTimeSlot adapter = new
                        CustomTimeSlot(TransferOrder.this, productLength, slotid, toTime, imageId);
                list = (ListView) findViewById(R.id.list);
                list.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                list.setAdapter(adapter);
                //time = CustomTimeSlot.time;

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        timeslotid = slotid[position];
                    }
                });

            }
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;
        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment_quick= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(TransferOrder.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment_quick.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment_quick = 1;


                    } else if (str_opt_payment_quick.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment_quick = 2;

                    } else if (str_opt_payment_quick.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment_quick = 3;


                    } else {
                        str_paymentmethod_payment_quick = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(TransferOrder.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment_quick);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

    //method for payment gate way
    private void callEbsKit(TransferOrder buyProduct) {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */

        /** Payment Amount Details */
        // Total Amount

        String str_amnt_tot=total+"0";
        float amount=Float.parseFloat(str_amnt_tot);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        str_amnt_tot = df.format(amount);

        float flt_str=Float.parseFloat(str_amnt_tot);

        PaymentRequest.getInstance().setTransactionAmount(
                String.format("%.2f", flt_str));

        /** Mandatory */

        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        // Reference No
        PaymentRequest.getInstance().setReferenceNo(strorderid);
        /** Mandatory */

        // Email Id
        PaymentRequest.getInstance().setBillingEmail(str_trns_contctnumber);
        /** Mandatory */

        /**
         * Set failure id as 1 to display amount and reference number on failed
         * transaction page. set 0 to disable
         */
        PaymentRequest.getInstance().setFailureid("0");
        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                "Daily Souq Android App Purchase");

        /** Billing Details */
        PaymentRequest.getInstance().setBillingName(str_trns_contctnmae);
        /** Optional */
        PaymentRequest.getInstance().setBillingAddress(str_trns_contctnumber);
        /** Optional */
        PaymentRequest.getInstance().setBillingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setBillingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setBillingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setBillingCountry("IND");
        // ** Optional */
        PaymentRequest.getInstance().setBillingPhone("1 800 123 2565 ");
        /** Optional */
        /** set custom message for failed transaction */

        PaymentRequest.getInstance().setFailuremessage(
                getResources().getString(R.string.payment_failure_message));
        /** Optional */
        /** Shipping Details */
        PaymentRequest.getInstance().setShippingName(str_trns_contctnmae);
        /** Optional */
        PaymentRequest.getInstance().setShippingAddress(str_trns_contctnumber);
        /** Optional */
        PaymentRequest.getInstance().setShippingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setShippingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setShippingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setShippingCountry("IND");
        /** Optional */
        PaymentRequest.getInstance().setShippingEmail(str_trns_contctnmae);
        /** Optional */
        PaymentRequest.getInstance().setShippingPhone(str_trns_contctnumber);
        /** Optional */
		/* enable log by setting 1 and disable by setting 0 */
        PaymentRequest.getInstance().setLogEnabled("1");

        /**
         * Initialise parameters for dyanmic values sending from merchant custom
         * values from merchant
         */

        custom_post_parameters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashpostvalues = new HashMap<String, String>();
        hashpostvalues.put("account_details", "saving");
        hashpostvalues.put("merchant_type", "gold");
        custom_post_parameters.add(hashpostvalues);

        PaymentRequest.getInstance()
                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(buyProduct, ACC_ID, SECRET_KEY,
                Config.Mode.ENV_LIVE, Config.Encryption.ALGORITHM_MD5, HOST_NAME);

        // EBSPayment.getInstance().init(context, accId, secretkey, environment,
        // algorithm, host_name);

    }

//used to get failure status

}
