package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.CookeryPopupItems;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.models.CokeryModel;
import shani.netstager.com.dailysouq.support.DS_SP;


/**
 * Created by MohammedBasheer on 16-06-2016.
 */


public class CookeryItemshowPopup extends BaseAdapter {

    Context context;
    ViewHolder holder;


    String []str_prdname,str_prdid,str_prd_qty,str_subtotal,str_stockstatus,str_sizeid;
    String  []unitPrice,amount;
    int totalNo;
    LayoutInflater inflater;
    boolean outofstock;
    String cusid;
    JSONArray dataJsonArr;
    String prdid,prdsizeid,prdqty;
    String responce_json;
    String partyorderid;
    int language;
    String noofperson="0";
    ProgressDialog mProgressDialog;
    private List<CokeryModel> cookerymodel;
    DS_SP sp;
    public CookeryItemshowPopup(Context context, List<CokeryModel> cookeryModels, String[]id, String[] name, String[] qty, String []subtotal, String[] stockstatus, String cusid, String partyorderid, int language, String []prdsizeid) {
        sp=new DS_SP(context.getApplicationContext());
        setProductlist(cookeryModels);

        this.context=context;
        str_prdid=id;
        str_prdname=name;
        this.language=language;
        str_prd_qty=qty;
        this.str_sizeid=prdsizeid;
        str_subtotal=subtotal;
        str_stockstatus=stockstatus;
        this.cusid = cusid;
        this.partyorderid=partyorderid;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return str_prdid.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        TextView txtname;
        TextView txtqty;
        TextView txtrqdqty;
        ImageView imRemove;
        TextView subtotal;
        Button btnAdd;
        Button btnMinus;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.custom_item_popup_list, null);
            holder = new ViewHolder();
            holder.txtname = (TextView) vi.findViewById(R.id.name);
            holder.subtotal = (TextView) vi.findViewById(R.id.subtotal);
            holder.txtqty = (TextView) vi.findViewById(R.id.qty);
            holder.txtrqdqty = (TextView) vi.findViewById(R.id.stock);
            holder.imRemove = (ImageView) vi.findViewById(R.id.remove);

            holder.btnAdd = (Button) vi.findViewById(R.id.btn_add);
            holder.btnMinus = (Button) vi.findViewById(R.id.btn_minus);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        final CokeryModel co = (CokeryModel) cookerymodel.get(position);
            holder.txtname.setText(str_prdname[position]+"");
            // Log.i("image",image);
        String lineSep = System.getProperty("line.separator");
        String yourString= str_stockstatus[position];


        yourString= yourString.replaceAll("<br />", lineSep);

        holder.txtqty.setText(yourString+"");
            //Log.i("unitprice",unitPrice);


           /* if(str_stockstatus[position].equals("1")){
                holder.txtrqdqty.setText("In Stock");
            }else if(str_stockstatus[position].equals("2")){
                holder.txtrqdqty.setText("Limited Stock");
            }else if(str_stockstatus[position].equals("3")){
                holder.txtrqdqty.setText("Out of stock");
            }*/
        holder.txtrqdqty.setText(str_prd_qty[position]+"");


             holder.subtotal.setText(str_subtotal[position]+"");

            holder.imRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id=co.cartid;
                    new AsyncTaskRemovetJson(id).execute();

                }
            });


            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cartid=co.cartid;
                    int qty= Integer.parseInt(co.qty);

                    holder.txtqty.setText(qty+"");
                    int curqty=qty+1;

                    new AsyncTaskUpdatetJson(curqty,cartid).execute();
                }
            });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cartid=co.cartid;
                int qty= Integer.parseInt(co.qty);

                holder.txtqty.setText(qty+"");
                int curqty=qty-1;

                new AsyncTaskUpdatetJson(curqty,cartid).execute();
            }
        });

        return vi;
    }


    public void setProductlist(List<CokeryModel> productlist) {
        this.cookerymodel = productlist;
    }



    public class AsyncTaskRemovetJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        String id;

        AsyncTaskRemovetJson(String  id){
            this.id=id;
        }
        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(context);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(context.getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cookeryemovewithid(id,cusid);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                // do stufff here
                Intent in=new Intent(context,CookeryPopupItems.class);
                in.putExtra("PID",partyorderid);
                in.putExtra("LANID",language);
                in.putExtra("USERID",cusid);
                in.putExtra("NAME","Cokkery");
                sp.setcookerytemp("OK");
                ((Activity)context).finish();
                context.startActivity(in);            }

            // mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskUpdatetJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;
        String cartid;

        AsyncTaskUpdatetJson(int qty,String cartid){
            this.v=v;
            this.qty=qty;
            this.cartid=cartid;
        }

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(context);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(context.getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cookeryupdatewithqty(cusid,qty,cartid);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");
             //   noofperson = dataJsonArr.getJSONObject(0).optString("Qty");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                // do stufff here
              //  Toast.makeText(context,responce_json+"",Toast.LENGTH_LONG).show();
                Intent in=new Intent(context,CookeryPopupItems.class);
                in.putExtra("PID",partyorderid);
                in.putExtra("LANID",language);
                in.putExtra("USERID",cusid);
                in.putExtra("NAME","Cookery");
                sp.setcookerytemp("OK");

               // in.putExtra("NOPER",noofperson);
                ((Activity)context).finish();
                context.startActivity(in);
            }

            // mProgressDialog.dismiss();
        }
    }

}
