package shani.netstager.com.dailysouq.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import shani.netstager.com.dailysouq.GCM.RegistrationIntentService;
import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class EditProfile extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST =9000;
    public  static  int SPLASH_TIME_OUT=3000;
    ImageView profile,quickcheckout,Notification;
    int spinner_location_default_postion_setting;
    int spinner_city_default_postion_setting;
    Button bck_btn,update,cancel;
    TextView title;
    RelativeLayout cartcounttext;
    JSONArray dataJsonArr;
    static final  int DATE_ID=0;
    int y,m,d;
    String dtest,mtest,device;
    String userVLogIn,str_json_cart_count;
    String []comm={"Mobile Phone","Email"};
    String []comm_edit={"Mobile Phone","Email"};
    String fname_pref,lname_pref,phone_pref,email_pref,dob_pref,telephone_pref,mod_prf,address_pref,landmark_pref,pincode_pref,mail_pref,psd_pref,locationname_pref,cityname_pref;
    String fname_edit,lname_edit,phone_edit,email_edit,dob_edit,telephone_edit,mod_edit,address_edit,landmark_edit,pincode_edit,mail_edit,psd_edit,locationname_edit,cityname_edit;
    boolean checkmail;
    SharedPreferences myprefs;
    Spinner Location,sp_mode,city;
    ProgressDialog mProgressDialog;
    String []locationplace;
    static String  logInres;
    String IndSign,Indres;
    String []Locationid;
    String []cityid;

    String []citycode;
    String []cityplace;
    int citypos;
    String []Locationcode;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    int lcposid,modpoid;
    int lcposid_edit,modpoid_edit,citypos_edit;
    CheckBox mail;
    EditText ed_fname,ed_lanme,ed_email,ed_dob,ed_mobile,ed_telephone,ed_adres,ed_landmark,ed_pincode;
    RelativeLayout fullview;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    //for new progress
    int wheelstatus=0;
    ProgressWheel wheel,hwheel;
    RelativeLayout rel,relWheel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        fullview=(RelativeLayout)findViewById(R.id.edit_prfile_full) ;
        fullview.setVisibility(View.GONE);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }



        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }


        ed_fname=(EditText)findViewById(R.id.fname_edit);
        ed_lanme=(EditText)findViewById(R.id.lname_edit);
        ed_email=(EditText)findViewById(R.id.email_edit);
        ed_dob=(EditText)findViewById(R.id.dob_edit);
        ed_mobile=(EditText)findViewById(R.id.mobile_edit);
        ed_telephone=(EditText)findViewById(R.id.telephone_edit);
        ed_adres=(EditText)findViewById(R.id.address_edit);
        ed_landmark=(EditText)findViewById(R.id.landmark_edit);
        ed_pincode=(EditText)findViewById(R.id.pincode_edit);
        update=(Button)findViewById(R.id.btn_update_edit);
        cancel=(Button)findViewById(R.id.btn_cncl_edit);
        city=(Spinner)findViewById(R.id.city_edit);

        wheel = new ProgressWheel(EditProfile.this);
        hwheel= (ProgressWheel) findViewById(R.id.progress_wheel);
        rel=(RelativeLayout)findViewById(R.id.relparent);
        relWheel=(RelativeLayout)findViewById(R.id.relwheel);




        mail=(CheckBox)findViewById(R.id.checkBox_edit);
        Location=(Spinner)findViewById(R.id.location_edit);
        sp_mode=(Spinner)findViewById(R.id.modecn_edit);
        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        title.setText(getString(R.string.title_activity_edit_profile));
        device=getDeviceId();


        if (userVLogIn!="0") {

            fullview.setVisibility(View.VISIBLE);
            //internet checking
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent.equals(true)){
              //  new AsyncTaskCartcounttJson().execute();
                new AsyncTaskCarttJson().execute();

            }
            else{

                AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_net_failed));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();

            }






        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_pls_signin));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent in = new Intent(getApplicationContext(), Signin.class);

                            startActivity(in);
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();




        }











        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fname_pref = ed_fname.getText().toString().trim();
                lname_pref = ed_lanme.getText().toString().trim();
                email_pref = ed_email.getText().toString().trim();
                dob_pref = ed_dob.getText().toString().trim();
                phone_pref = ed_mobile.getText().toString().trim();
                telephone_pref = ed_telephone.getText().toString().trim();
                address_pref = ed_adres.getText().toString().trim();
                landmark_pref = ed_landmark.getText().toString().trim();
                pincode_pref = ed_pincode.getText().toString().trim();

                if (fname_pref.equals("")){
                    ed_fname.setError(getString(R.string.plz_fill));
                }else{
                    ed_fname.setError(null);
                }

                //ed_lanme.setText("");
                if (lname_pref.equals("")){
                    ed_lanme.setError(getString(R.string.plz_fill));
                }else{
                    ed_lanme.setError(null);
                }

                // ed_dob.setText("");
                if (dob_pref.equals("")){
                    ed_dob.setError(getString(R.string.plz_fill));
                }else{
                    ed_dob.setError(null);
                }

                //
                if(phone_pref.length()==0){
                    ed_mobile.setError(getString(R.string.plz_fill));
                }else{
                    ed_mobile.setError(null);
                }

                //ed_telephone.setText("");

                if(address_pref.length()<=1){
                    ed_adres.setError(getString(R.string.plz_fill));
                }else{
                    ed_adres.setError(null);
                }
                //

                // ed_landmark.setText("");

                // ed_pincode.setText("");
                if (pincode_pref.equals("")){
                    ed_pincode.setError(getString(R.string.plz_fill));
                }else
                {
                    ed_pincode.setError(null);
                }
                if (fname_pref.equals("") || lname_pref.equals("") || phone_pref.length() ==0 || dob_pref.equals("")  || address_pref.length() <= 1 || pincode_pref.equals("")) {
                   Toast.makeText(EditProfile.this, getString(R.string.plz_fill_all), Toast.LENGTH_SHORT).show();

                    //ed_fname.setText("");




                } else {

                    hwheel.setVisibility(View.VISIBLE);
                    wheel.setBarColor(Color.WHITE);
                    wheel.spin();
                    wheelstatus=1;
                    relWheel.setVisibility(View.VISIBLE);


                    mod_prf = Integer.toString(modpoid);

                    if (checkmail = mail.isChecked()) {
                        mail_pref = "true";
                    } else {
                        mail_pref = "false";
                    }





                    if (checkPlayServices()) {
                        // Start IntentService to register this application with GCM.


                        Intent intent = new Intent(EditProfile.this, RegistrationIntentService.class);
                        startService(intent);
                    }
                    // if(isConnected==true) {
                    new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                        @Override
                        public void run() {

                            new getIndividualSignUpUpdate().execute();

                        }
                    }, SPLASH_TIME_OUT);



                }

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });




        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item,comm);
        sp_mode.setPrompt(mod_prf);

        sp_mode.setAdapter(adapter);
        sp_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                modpoid = sp_mode.getSelectedItemPosition();



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        bck_btn=(Button)findViewById(R.id.menue_btn);
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(EditProfile.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(EditProfile.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(EditProfile.this, Notification.class);

                startActivity(in);
            }
        });
        final Calendar calendar=Calendar.getInstance();
        y=calendar.get(Calendar.YEAR);
        m=calendar.get(Calendar.MONTH);
        d=calendar.get(Calendar.DAY_OF_WEEK);
        ed_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showDialog(DATE_ID);
                ed_dob.setEnabled(false);

                //used to hide key board
                try  {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }


                return false;
            }
        });

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                // set partyorderpersons picker as current partyorderpersons
                DatePickerDialog da = new DatePickerDialog(this, datePickerListener,
                        y, m, d);
                da.setCanceledOnTouchOutside(false);

                da.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CheckOut.this,"clicked cancel",Toast.LENGTH_LONG).show();
                    }
                });
                da.getDatePicker().setMaxDate(new Date().getTime());
                return da;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {


        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            y = selectedYear;
            m = selectedMonth;

            if(selectedMonth<9)
            {
                m=0+selectedMonth;
                mtest="0"+String.valueOf(selectedMonth+1);

                m=Integer.parseInt(mtest);

            }
            else{
                mtest=String.valueOf(selectedMonth+1);

            }

            if(selectedDay<10)
            {
                d=0+selectedDay;
                dtest="0"+String.valueOf(selectedDay);

                d=Integer.parseInt(dtest);

            }
            else{
                dtest=String.valueOf(selectedDay);

            }




            // set selected partyorderpersons into textview
            ed_dob.setText(new StringBuilder().append(dtest)
                    .append("/").append(mtest ).append("/").append(y));
            ed_dob.setEnabled(true);
            // set selected partyorderpersons into datepicker also


        }
    };

    private void setValues() {

        ed_fname.setText(fname_edit);
        ed_lanme.setText(lname_edit);
        ed_email.setText(email_edit);
        ed_dob.setText(dob_edit);
        ed_mobile.setText(phone_edit);
        ed_telephone.setText(telephone_edit);
        ed_adres.setText(address_edit);

        if(landmark_edit!="null"){
            ed_landmark.setText(landmark_edit);
        }else{
            ed_landmark.setText("");
        }
        if(pincode_edit!="null"){
            ed_pincode.setText(pincode_edit);
        }else{
            ed_pincode.setText("");
        }



        sp_mode.setPrompt(mod_edit);



    }







    /*public void getIndividualSignUpUpdate()
    {
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_customer_update");

                    jsonn.put("LastName", lname_pref);
                    jsonn.put("FirstName", fname_pref);
                    jsonn.put("Email", email_pref);
                    jsonn.put("Phone", phone_pref);
                    jsonn.put("Address", address_pref);
                    jsonn.put("LoginType","NO");
                    jsonn.put("NewsLetterSub",mail_pref);
                    jsonn.put("ModeId",mod_prf);
                    jsonn.put("Telephone",telephone_pref);
                    jsonn.put("CompanyRegNo","");
                    jsonn.put("Dob",dob_pref);
                    jsonn.put("DeviceToken",sp.getdeviceId());
                    jsonn.put("CustomerId",userVLogIn);
                    jsonn.put("PostCode", pincode_pref);
                    jsonn.put("LandMark", landmark_pref);
                    jsonn.put("LocationId",Locationid[lcposid]);
                    jsonn.put("CityId",cityid[citypos]);
                    //its used to pass 1 android in 2 in ios
                    jsonn.put("MobType","1");



                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    IndSign = EntityUtils.toString(resEntity);
                    IndSign = IndSign.trim();


                    JSONObject jasonob = new JSONObject(IndSign);
                    dataJsonArr=jasonob.getJSONArray("d");
                    Indres = dataJsonArr.getJSONObject(0).optString("result").toString();

                    //do stuff here
                    hwheel.setVisibility(View.GONE);
                    //rel.setVisibility(View.VISIBLE);
                    wheelstatus=0;
                    relWheel.setVisibility(View.GONE);


                        if(dataJsonArr.length()==0) {



                            AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(getString(R.string.error));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent in = new Intent(EditProfile.this, MainActivity.class);

                                            startActivity(in);
                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();





                        } else {



                            showDoneAlert(getString(R.string.succsesfullyupdated));


                        }




                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();
            }
        };
        th.start();


    }*/




    public class getIndividualSignUpUpdate extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response = null;
                JSONObject jsonn = new JSONObject();
                HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_customer_update");

                jsonn.put("LastName", lname_pref);
                jsonn.put("FirstName", fname_pref);
                jsonn.put("Email", email_pref);
                jsonn.put("Phone", phone_pref);
                jsonn.put("Address", address_pref);
                jsonn.put("LoginType","NO");
                jsonn.put("NewsLetterSub",mail_pref);
                jsonn.put("ModeId",mod_prf);
                jsonn.put("Telephone",telephone_pref);
                jsonn.put("CompanyRegNo","");
                jsonn.put("Dob",dob_pref);
                jsonn.put("DeviceToken",sp.getdeviceId());
                jsonn.put("CustomerId",userVLogIn);
                jsonn.put("PostCode", pincode_pref);
                jsonn.put("LandMark", landmark_pref);
                jsonn.put("LocationId",Locationid[lcposid]);
                jsonn.put("CityId",cityid[citypos]);
                //its used to pass 1 android in 2 in ios
                jsonn.put("MobType","1");


                StringEntity se = null;
                try {
                    se = new StringEntity(jsonn.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                assert se != null;
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                assert response != null;
                HttpEntity resEntity = response.getEntity();
                try {
                    IndSign = EntityUtils.toString(resEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                IndSign = IndSign.trim();


                JSONObject jasonob = new JSONObject(IndSign);
                dataJsonArr=jasonob.getJSONArray("d");
                Indres = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //do stuff here
            hwheel.setVisibility(View.GONE);
            //rel.setVisibility(View.VISIBLE);
            wheelstatus=0;
            relWheel.setVisibility(View.GONE);

            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }else {

                showDoneAlert(getString(R.string.succsesfullyupdated));

            }
        }
    }


    public void showDoneAlert(String msg){
        final AlertDialog.Builder builder=new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                SharedPreferences.Editor editors = myprefs.edit();
                                editors.putString("bsrnme", fname_pref);
                                editors.putString("userphone", phone_pref);
                                //System.out.println("Name In Prefs" + fname_pref);
                                editors.apply();
                                Intent in=new Intent(EditProfile.this,MainActivity.class);
                                finish();
                                startActivity(in);

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskLocation extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected String doInBackground(String... arg0) {

            try {

                Json jParser = new Json();
                JSONObject json = jParser.Location(cityid[citypos]);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Locationid=new String[dataJsonArr.length()];
                Locationcode=new String[dataJsonArr.length()];
                locationplace=new String[dataJsonArr.length()];

                for(int i=0;i<dataJsonArr.length();i++){
                    Locationid[i]=dataJsonArr.getJSONObject(i).optString("LocationId");
                    locationplace[i] = dataJsonArr.getJSONObject(i).optString("LocationName");
                    Locationcode[i]=dataJsonArr.getJSONObject(i).optString("LocationCode");


                }
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {

            } else {





                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfile.this, android.
                        R.layout.simple_spinner_dropdown_item, locationplace);
                //used to the location to spinner
                int index = -1;
                for (int i=0;i<locationplace.length;i++) {
                    if (locationplace[i].equals(locationname_edit)) {
                        index = i;
                        Log.i("strposindex","ok"+index);
                        break;
                    }
                }


                Location.setSelection(index);

                Location.setAdapter(adapter);
//                if (spinner_location_default_postion_setting != 0) {
//
//                    Location.setSelection(spinner_location_default_postion_setting);
//                } else {
//                    Location.setSelection(0);
//                }
//                Location.setSelection(spinner_location_default_postion_setting - 1);

                Location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        SharedPreferences.Editor editor = myprefs.edit();
                        editor.putInt("locationidinedit", position);
                        editor.apply();
                        // Get select item
                        lcposid = Location.getSelectedItemPosition();

                    }//

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });



            }
        }
    }




    public class AsyncTaskCity extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";

    int jsonlen=0;

        @Override
        protected String doInBackground(String... arg0) {



            try {

                Json jParser = new Json();
                JSONObject json = jParser.city();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                cityid=new String[dataJsonArr.length()];
                citycode=new String[dataJsonArr.length()];
                cityplace=new String[dataJsonArr.length()];

                for(int i=0;i<dataJsonArr.length();i++){
                    cityid[i]=dataJsonArr.getJSONObject(i).optString("CityId");
                    cityplace[i] = dataJsonArr.getJSONObject(i).optString("CityName");
                    citycode[i]=dataJsonArr.getJSONObject(i).optString("CityCode");

                }
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {

            } else {


                //mProgressDialog.dismiss();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfile.this, android.
                        R.layout.simple_spinner_dropdown_item, cityplace);


                //used to the location to spinner
                int index = -1;
                for (int i=0;i<cityplace.length;i++) {
                    if (cityplace[i].equals(cityname_edit)) {
                        index = i;
                        Log.i("strposindex","ok"+index);
                        break;
                    }
                }


                Location.setSelection(index);
                city.setAdapter(adapter);
//                if (spinner_city_default_postion_setting != 0) {
//
//                    city.setSelection(spinner_city_default_postion_setting);
//                } else {
//                    city.setSelection(0);
//                }


                city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        SharedPreferences.Editor editor = myprefs.edit();
                        editor.putInt("cityidinedit", position);
                        editor.apply();

                        // Get select item
                        citypos = city.getSelectedItemPosition();

                        new AsyncTaskLocation().execute();
                    }//

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });


            }
        }
    }


    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;
        @Override
        protected String doInBackground(String... arg0) {
            try {
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                userVLogIn = myprefs.getString("shaniusrid", null);

                Json jParser = new Json();
                JSONObject json = jParser.userdetails(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                System.out.println("ssss" + productObj);
                dataJsonArr = productObj.getJSONArray("d");

                fname_edit=dataJsonArr.getJSONObject(0).optString("FirstName").toString().trim();



                email_edit=dataJsonArr.getJSONObject(0).optString("Email").toString().trim();
                phone_edit=dataJsonArr.getJSONObject(0).optString("Phone").toString().trim();
                lname_edit=dataJsonArr.getJSONObject(0).optString("LastName").toString().trim();
                telephone_edit=dataJsonArr.getJSONObject(0).optString("Telephone").toString().trim();
                dob_edit=dataJsonArr.getJSONObject(0).optString("Dob").toString().trim();
                psd_edit=dataJsonArr.getJSONObject(0).optString("Password").toString().trim();


                comm_edit[modpoid_edit]=dataJsonArr.getJSONObject(0).optString("ModeId").toString();
                address_edit=dataJsonArr.getJSONObject(0).optString("Address").toString();
             //   Locationid_edit[lcposid_edit]=dataJsonArr.getJSONObject(0).getString("LocationId").toString();
                landmark_edit=dataJsonArr.getJSONObject(0).optString("LandMark").toString();
                pincode_edit=dataJsonArr.getJSONObject(0).optString("PostCode").toString().trim();
               // cityid_edit[citypos_edit]=dataJsonArr.getJSONObject(0).getString("CityId").toString();
                locationname_edit=dataJsonArr.getJSONObject(0).optString("LocationName").toString();
                cityname_edit=dataJsonArr.getJSONObject(0).optString("CityName").toString();

                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                new AsyncTaskCity().execute();
            }else {


                setValues();

                new AsyncTaskCity().execute();

            }

        }
    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }

            //   Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }
    }



    protected String getDeviceId() {
        return Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");

                jsonlen=dataJsonArr_quick.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(EditProfile.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(EditProfile.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    startActivity(in);

                }
            }

        }
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {

                finish();
            }
            return false;
        }
        return true;
    }
}
