package shani.netstager.com.dailysouq.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.SortCustoAdapter;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.support.OnLoadMoreListener;

/**
 * Created by prajeeshkk on 16/10/15.
 */
public class Sort extends Activity {

    //used for end less

    private TextView tvEmptyView;
    private RecyclerView gridViews;
    private SortCustoAdapter mAdapter;
    int pg = 0;
    ArrayList<ProductModel> productModels;
    private GridLayoutManager lLayout;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;
    public String catrgId,sortItem;

    static String[] productId;
    static String[] productName;
    ImageView profile,quickcheckout,wishlist,Notification;
    RelativeLayout cartcounttext;
    Button back_btn;
    ImageView footeshop,footerdeal,footersp,footergft,footermore;
    public SharedPreferences myProducts;


    String userVLogIn,catresult,brandresult;
    boolean isNewtag;
    ImageView sort;

    ImageView filter;
    //its used for new scroll
    LinearLayout serch_full_view;
    int prelast,str_json_cart_count;
    int lastItem;
    LayoutInflater layoutInflater;
    View popupView;
    PopupWindow popupWindow;
    int gridview_smooth_position=0;
    ProgressDialog dialog;
    //its used for new scroll


    TextView title;
    static String[] imgPath;
    static String[] productSize;
    static String[] isNew;
    static String[] actualPrice;
    static String[] offerPrice;
    static String[] offerName;
    static int[] discount;
    static  String[]brandId;
    int sizeProduct;
    int productLength;
    SortCustoAdapter adapt;

    public SharedPreferences myprefs;


    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;

    ImageView serchgobtn;
    String []categoryName={"All","Category","Products","Product Code"};
    int pos,DEFAULT_LANGUGE;
    String srchtext;
    ProgressDialog mProgressDialog;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;

    ArrayList<ProductModel> prdtt = new ArrayList<>();
    boolean loadmore=false;

    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    TextView txt_sorttype_text_show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sort);

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }
//notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }




        //used for endless scroll
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        pgresbar_scrool_footer=(ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        productModels = new ArrayList<>();
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            new AsyncTaskCartcounttJson().execute();
            new AsyncTaskSortJson(pg).execute();

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(Sort.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        lLayout = new GridLayoutManager(Sort.this, 2);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);
//        //its used for new scroll
//        layoutInflater
//                = (LayoutInflater) getBaseContext()
//                .getSystemService(LAYOUT_INFLATER_SERVICE);
//        popupView = layoutInflater.inflate(R.layout.cust_popup_loading_product, null);
//        popupWindow = new PopupWindow(
//                popupView,
//                ActionBar.LayoutParams.FILL_PARENT,
//                ActionBar.LayoutParams.FILL_PARENT);
//        serch_full_view = (LinearLayout) findViewById(R.id.container_search);
//        //its used for new scroll
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);



        //badge

         Intent in=getIntent();



        catrgId= in.getStringExtra("CATID");

        sortItem=in.getStringExtra("SORT");
        txt_sorttype_text_show=(TextView)findViewById(R.id.sort_type);
        txt_sorttype_text_show.setText(sortItem+"");
        profile = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        //profile=(ImageView)findViewById(R.id.profile);
        //cart=(ImageView)findViewById(R.id.cart);
        // quickcheckout=(ImageView)findViewById(R.id.apload);
        //Notification=(ImageView)findViewById(R.id.notification);
        back_btn=(Button)findViewById(R.id.menue_btn);
        title=(TextView)findViewById(R.id.namein_title);




        footeshop=(ImageView)findViewById(R.id.imageView11);
        footersp=(ImageView)findViewById(R.id.imageView13);
        footermore=(ImageView)findViewById(R.id.imageView15);
        footerdeal=(ImageView)findViewById(R.id.imageView12);
        footergft=(ImageView)findViewById(R.id.imageView14);
        footermore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, Menu_List.class);

                startActivity(in);
            }
        });
        footersp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, DsSpecials.class);

                startActivity(in);
            }
        });
        footerdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, DsDeals.class);

                startActivity(in);
            }
        });
        footergft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, DsGiftCard.class);

                startActivity(in);


            }
        });

        footeshop.setImageResource(R.drawable.footershopselect);




        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent in = new Intent(Sort.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sort.this, CartView.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Sort.this, Notification.class);

                startActivity(in);
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });

        title.setText(getString(R.string.sort_rslt));
        //serch test
        catogoryspinnerinsrch=(EditText) findViewById(R.id.catSpinner);
        serchtext=(MultiAutoCompleteTextView)findViewById(R.id.Search);
        serchgobtn=(ImageView)findViewById(R.id.goButton);


        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });

        serchgobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srchtext = serchtext.getText().toString();

                if (serchtext.length() == 0) {
                    serchtext.setError(getString(R.string.srh_here_error));
                } else {


                    Intent in = new Intent(Sort.this, SerchResult.class);
                    in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                    in.putExtra("text", srchtext);

                    startActivity(in);

                }

            }
        });
        myProducts = getSharedPreferences("MYPRODUCTS",Context.MODE_PRIVATE);

        Intent ins = getIntent();
        catresult = ins.getStringExtra("CatResult");
        brandresult = ins.getStringExtra("BrandResult");
        isNewtag = ins.getBooleanExtra("isNew", false);


        //serch test
//used for sort
        //serch test
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);

        sort = (ImageView) findViewById(R.id.sort);
        filter = (ImageView) findViewById(R.id.filter);
        //btn_next= (Button) findViewById(R.id.next);
        //btn_prev= (Button) findViewById(R.id.prev);


        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PopupMenu popup = new PopupMenu(Sort.this, sort);
//                popup.getMenuInflater().inflate(R.menu.menu_popup, popup.getMenu());
//
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                        sortItem = item.getTitle().toString();
//
//
//                        Intent in = new Intent(Sort.this, Sort.class);
//                        in.putExtra("CATID", catrgId);
//                        in.putExtra("SORT", sortItem);
//
//                        startActivity(in);
//
//
//                        return true;
//                    }
//                });
//                popup.show();



                //new method

                SortTypePopup();

            }
        });


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Sort.this, Filter.class);
                in.putExtra("Catogoryid_filter",catrgId);
                startActivity(in);
            }
        });


    }



    //bsr new popup strt sort
    private void SortTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_sorttype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView pricelowtohigh = (TextView) popupView.findViewById(R.id.plh_custom_popup);
        final TextView pricehightolow = (TextView) popupView.findViewById(R.id.phl_custom_poup);
        final  TextView toprated = (TextView) popupView.findViewById(R.id.toprated_custom_popup);
        final TextView atoz = (TextView) popupView.findViewById(R.id.atoz_custom_poup);
        final TextView ztoa = (TextView) popupView.findViewById(R.id.ztoa_custom_poup);


        if(sortItem.equals("Price Low to High")){
            pricelowtohigh.setBackgroundColor(Color.GRAY);
        }else  if(sortItem.equals("Price High to Low")){
            pricehightolow.setBackgroundColor(Color.GRAY);
        }else  if(sortItem.equals("Top Rated")){
            toprated.setBackgroundColor(Color.GRAY);
        }
        else  if(sortItem.equals("A to Z")){
            atoz.setBackgroundColor(Color.GRAY);
        }else  if(sortItem.equals("Z to A")){
            ztoa.setBackgroundColor(Color.GRAY);
        }else {
            //do nothing
        }


        pricelowtohigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=pricelowtohigh.getText().toString();
                finish();
                Intent in = new Intent(Sort.this, Sort.class);
                        in.putExtra("CATID", catrgId);
                        in.putExtra("SORT", sortItem);

                        startActivity(in);

                popupWindow.dismiss();
            }
        });
        pricehightolow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=pricehightolow.getText().toString();

                Intent in = new Intent(Sort.this, Sort.class);
                in.putExtra("CATID", catrgId);
                in.putExtra("SORT", sortItem);
                startActivity(in);
                finish();
                popupWindow.dismiss();
            }
        });
        toprated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=toprated.getText().toString();

                Intent in = new Intent(Sort.this, Sort.class);
                in.putExtra("CATID", catrgId);
                in.putExtra("SORT", sortItem);
                startActivity(in);
                finish();
                popupWindow.dismiss();
            }
        });
        atoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=atoz.getText().toString();

                Intent in = new Intent(Sort.this, Sort.class);
                in.putExtra("CATID", catrgId);
                in.putExtra("SORT", sortItem);
                startActivity(in);
                finish();
                popupWindow.dismiss();
            }
        });
        ztoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=ztoa.getText().toString();

                Intent in = new Intent(Sort.this, Sort.class);
                in.putExtra("CATID", catrgId);
                in.putExtra("SORT", sortItem);
                startActivity(in);
                finish();
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(sort, 0,0);



    }

    // bsr edit popup end sort



    //bsr new popup strt srch
    private void SerachTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
        final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
        final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
        final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(all.getText());
                popupWindow.dismiss();
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(products.getText());
                popupWindow.dismiss();
            }
        });
        catogory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(catogory.getText());
                popupWindow.dismiss();
            }
        });
        prdctcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(prdctcode.getText());
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



    }

    // bsr edit popup end srch





    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskSortJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;
        public AsyncTaskSortJson(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Sort.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
           // mProgressDialog.show();
            pgresbar_scrool_footer.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();

                JSONObject jsonObject = jParser.sorting(catrgId, sortItem,userVLogIn,pg,DEFAULT_LANGUGE);
                    Log.i("catid,sorttext",catrgId+"..."+sortItem);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                productLength = dataJsonArr.length();
                imgPath = new String[dataJsonArr.length()];
                productSize = new String[dataJsonArr.length()];
                productId = new String[dataJsonArr.length()];
                productName = new String[dataJsonArr.length()];
                isNew = new String[dataJsonArr.length()];
                actualPrice = new String[dataJsonArr.length()];
                offerPrice = new String[dataJsonArr.length()];
                offerName = new String[dataJsonArr.length()];
                discount = new int[dataJsonArr.length()];
                brandId = new String[dataJsonArr.length()];


                for (int i = 0; i < productLength; i++) {

                    JSONObject c = dataJsonArr.getJSONObject(i);
//                        adapt = new productGridViewAdapter(Products.this,productLength, productId, productName, imgPath,sizeProduct,productSize,actualPrice,offerPrice,discount,isNew,brandId);
//                        //gridViews.setAdapter(adapt);
                    ProductModel product = new ProductModel();
                    product.productId=  dataJsonArr.getJSONObject(i).optString("ProductId");

                    product.productName=  c.optString("ProductShowName").toString();

                    product.imagepath=  c.optString("Imagepath").toString();
                    product.isNew=c.getString("IsNew");
                    //product.brandId=c.getString("BrandId");
                    product.category=c.getString("CategoryId");
                    product.customerId=userVLogIn;

                    ArrayList<SizeModel> sizes= new ArrayList<>();

                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
//                    System.out.println("ssss" + sizeArray.length());

                        for (int j = 0; j < sizeProduct; j++) {
                            SizeModel sizeModel = new SizeModel();
                            sizeModel.productsize = sizeArray.getJSONObject(j).optString("ProductSize");
                            sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                            sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                            sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                            sizeModel.savaPrice=sizeArray.getJSONObject(j).optString("Saveprice");
                            sizeModel.discount=sizeArray.getJSONObject(j).optInt("Discount");
                            sizeModel.stockstatus=sizeArray.getJSONObject(j).optString("Stockstatus");
                            sizes.add(sizeModel);

                        }
                    product.sizes = sizes;
                    productModels.add(product);
                   // prdt.add(productModels);
                    }
                json_length=dataJsonArr.length();

                return productModels;
                    }



            catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<ProductModel> productModels) {
            // super.onPostExecute(s);
            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(Sort.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            } else {


                pg = pg + 1;
                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }
                if (productModels.size() <= 10) {
                    mAdapter = new SortCustoAdapter(productModels, gridViews, Sort.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);
                } else {
                    mAdapter.setProductlist(productModels);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();
                }


                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {


                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                new AsyncTaskSortJson(pg).execute();


                            }
                        }, 100);

                    }
                });


                //its used for new scroll
            }
        }

            }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Sort.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }
        //   Log.i("arrivedcount", str_json_cart_count);

          //  mProgressDialog.dismiss();
        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Sort.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Sort.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(Sort.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
            }

