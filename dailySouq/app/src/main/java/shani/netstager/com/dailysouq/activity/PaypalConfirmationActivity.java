package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


import static shani.netstager.com.dailysouq.R.id.ok_button;

public class PaypalConfirmationActivity extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    String userVLogIn;
    String MerchantRefNo;
    String PaymentId;
    SharedPreferences myprefs;
    String result;
    DS_SP sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal_confirmation);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        sp=new DS_SP(getApplicationContext());

        //Getting Intent
        Intent intent = getIntent();
        userVLogIn=intent.getStringExtra("userVLogIn");
        MerchantRefNo=intent.getStringExtra("AFTER_OREDER_ID");
        Log.i("MerchantRefNo",MerchantRefNo+"");



        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));

            //Displaying payment details
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {
        //Views
        TextView textViewId = (TextView) findViewById(R.id.paymentId);
        TextView textViewStatus= (TextView) findViewById(R.id.paymentStatus);
        TextView textViewAmount = (TextView) findViewById(R.id.paymentAmount);
        Button button= (Button) findViewById(ok_button);


        //Showing the details from json object
        textViewId.setText(jsonDetails.getString("id"));
        PaymentId=jsonDetails.getString("id");
        textViewStatus.setText(jsonDetails.getString("state"));
        //MerchantRefNo=jsonDetails.getString("state");
        textViewAmount.setText(paymentAmount+" USD");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PaypalConfirmationActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        new AsyncOrderTransactioidUpdateSuccess().execute();
        new AsyncCartAllRemove().execute();

    }

    public class AsyncOrderTransactioidUpdateSuccess extends AsyncTask<String,String,String> {
        JSONArray dataJsonArr;
        String transactionid="0";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PaypalConfirmationActivity.this);
//            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
            mProgressDialog.show();

        }
        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                //JSONObject jobj = jParser.updatetransactionid(userVLogIn, MerchantRefNo, PaymentId, "Success");
                JSONObject jobj = jParser.updatetransactionid(userVLogIn, MerchantRefNo, PaymentId, "Success");
                Log.i("DADADADA",userVLogIn+" "+MerchantRefNo+" "+PaymentId);
                JSONObject productObj = new JSONObject(Json.prdetail);
                Log.i("productObj",productObj.toString());

                dataJsonArr = productObj.getJSONArray("d");
                Log.i("dataJsonArraaaaa",dataJsonArr.toString());
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaypalConfirmationActivity.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if (dataJsonArr.length() == 1) {

                    AlertDialog alertDialog = new AlertDialog.Builder(PaypalConfirmationActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    alertDialog.setMessage(getString(R.string.order_placed_payment_done));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                   /* Intent in = new Intent(PaypalConfirmationActivity.this, MainActivity.class);

                                    startActivity(in);*/

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PaypalConfirmationActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    Intent in = new Intent(PaypalConfirmationActivity.this, MainActivity.class);

                                    startActivity(in);

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }


        }
    }
    public class AsyncCartAllRemove extends AsyncTask<String,String,String> {
        JSONArray dataJsonArr;
        int prdlength = 0;

        @Override
        protected void onPreExecute() {

           /* // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaypalConfirmationActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.RemoveAllcart(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                prdlength = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //mProgressDialog.dismiss();
            if (prdlength == 0) {
                /*AlertDialog alertDialog = new AlertDialog.Builder(PaypalConfirmationActivity.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();*/
            } else {


                if (result.equals("1")) {
                    //set 0 while cart full claer
                    sp.setcartcount(0);
                    // used to set new sp based count
                    //setcartcountwihoutapi();


                    //Toast.makeText(CartView.this, getString(R.string.succsesfullyremved), Toast.LENGTH_SHORT).show();
                    //Intent in = new Intent(CartView.this, Categories.class);

                    //startActivity(in);


                } else {
                    //Toast.makeText(CartView.this, getString(R.string.failedtoremove), Toast.LENGTH_LONG).show();
                }

            }
        }
    }

}
