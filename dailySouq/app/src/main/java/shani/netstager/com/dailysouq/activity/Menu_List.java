package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class Menu_List extends AppCompatActivity {
    ImageView footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore;
    ImageView profile,quickcheckout,title,Notification;
    Button bck_btn;
    public SharedPreferences myprefs,myProducts;
    String userVLogIn,str_json_cart_count;
    TextView signup,Title,chpsd,MyOrder,userprofile,Ewallet,deliveryAddress,reqstaproduct,wishList,recwishList,settings,notificati,shoppinglist,referfriends,TransferdOrders;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    RelativeLayout cartcounttext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__list);

        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        profile=(ImageView)findViewById(R.id.profile);
        title=(ImageView)findViewById(R.id.imageView);
        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);


        MyOrder=(TextView)findViewById(R.id.textlm4);
        TransferdOrders=(TextView)findViewById(R.id.textlm4a);
        Ewallet=(TextView)findViewById(R.id.textlm5);
        deliveryAddress=(TextView)findViewById(R.id.textlm2);
        reqstaproduct=(TextView)findViewById(R.id.textlm8);
        referfriends=(TextView)findViewById(R.id.textlm9);
        notificati=(TextView)findViewById(R.id.textlm10);
        settings=(TextView)findViewById(R.id.textlm11);
        Title=(TextView)findViewById(R.id.namein_title);
        Title.setText(getString(R.string.title_activity_my_profile));
        signup=(TextView)findViewById(R.id.textlm12);
        wishList=(TextView)findViewById(R.id.textlm6);
        recwishList=(TextView)findViewById(R.id.textlm6a);
        shoppinglist=(TextView)findViewById(R.id.textlm7);
        chpsd=(TextView)findViewById(R.id.textlm3);
        userprofile=(TextView)findViewById(R.id.textlm1);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);



        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);

        if(userVLogIn !=null){
            userVLogIn =myprefs.getString("shaniusrid", userVLogIn);

        }
        else{
            userVLogIn ="0";

        }


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

         //   new AsyncTaskCartcounttJson().execute();


        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(Menu_List.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        if (userVLogIn !="0") {
            LinearLayout signup=(LinearLayout)findViewById(R.id.lm12);
            signup.setVisibility(View.VISIBLE);


        }
        else{
            LinearLayout signup=(LinearLayout)findViewById(R.id.lm12);

            signup.setVisibility(View.GONE);
        }




        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Menu_List.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Menu_List.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AsyncQuickCheckOut().execute();

            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Menu_List.this, Notification.class);

                startActivity(in);
            }
        });


        chpsd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, ChangePassword.class);

                startActivity(in);
            }
        });
        Ewallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(Menu_List.this, Ewallet.class);

                startActivity(in);
                //Toast.makeText(getApplicationContext(),"This Feature Will Come Soon..!",Toast.LENGTH_LONG).show();
            }
        });

        shoppinglist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, ShopByListActivity.class);

                startActivity(in);
            }
        });
        referfriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, ReferAndEarn.class);

                startActivity(in);
            }
        });

        notificati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, Notification.class);

                startActivity(in);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, SettingsActivity.class);

                startActivity(in);
            }
        });




        MyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, MyOrderList.class);

                startActivity(in);
            }
        });
        TransferdOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, TransferdOrders.class);

                startActivity(in);
            }
        });

        userprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, MyProfile.class);

                startActivity(in);
            }
        });
        deliveryAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, DeliveryAddress.class);

                startActivity(in);
            }
        });
        reqstaproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, RequstAProduct.class);

                startActivity(in);
            }
        });

        wishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(Menu_List.this,MyWhislist.class);

                startActivity(in);
            }
        });

        recwishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(Menu_List.this,MyRecipieWishList.class);

                startActivity(in);
            }
        });



        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(Menu_List.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_signout_confirm));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = myprefs.edit();
                                editor.remove("bsrnme");
                                editor.remove("shaniusrid");
                                editor.remove("useremail");
                                editor.remove("userphone");
                                editor.remove("cartcount");
                                editor.remove("logintype");

                                editor.remove("ORDERREVIEW_NAME");
                                editor.remove("ORDERREVIEW_ADDRESS");
                                editor.remove("ORDERREVIEW_LOCATION");
                                editor.remove("ORDERREVIEW_PHONE");
//                                editor.remove("adress");
//                                editor.remove("landmark");
//                                editor.remove("location");
//                                editor.remove("pincode");

                                editor.apply();
                               sp.clearall();


                                Toast.makeText(Menu_List.this,getString(R.string.logged_out),Toast.LENGTH_LONG).show();

                                Intent in = new Intent(Menu_List.this, MainActivity.class);

                                startActivity(in);





                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });

                alertDialog.show();




            }
        });


        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            onBackPressed();
            }
        });
        Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });



        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Menu_List.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(Menu_List.this,DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Menu_List.this,DsGiftCard.class);

                startActivity(in);;
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Menu_List.this,Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Menu_List.this,DsSpecials.class);

                startActivity(in);
            }
        });


        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Menu_List.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                    str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                json_length=dataJsonArr.length();




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Menu_List.this).create();
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.show();
            }else{

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }



//Log.i("arrivedcount",str_json_cart_count);


        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");

                jsonlen=dataJsonArr_quick.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Menu_List.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Menu_List.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(Menu_List.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}
