package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cust_list_prdct_by_srch_list;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class ShopByList extends AppCompatActivity {
    JSONArray dataJsonArr;
    ProgressDialog mProgressDialog;
    ImageView profile, quickcheckout, Notification,add_new_list;
    TextView title,txt_curent_balance;
    Button bck_btn, btn_find_all_product;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    RelativeLayout cartcounttext;
    ListView shop_list, linkedusers;
    SharedPreferences myprefs;
    int DEFAULT_LANGUGE, str_json_cart_count;
    int [] CONT_LIST_NO;
    String userVLogIn,usermasterid,accounttype,str_referemail,str_product_name_typed,str_prdname_pass_to_save;
    String[]  str_prdname,str_prd_id,str_list_item_names,str_list_item_id;
    ArrayList<String> b=new ArrayList<String>();

    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    String str_list_master_id;


    ImageView footerdssp, footerdsshop, footerdsdeal, footerdsgift, footerdsmore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_by_list);
        // this is used to get listdaily id from previos page
        Intent in=getIntent();
        str_list_master_id=in.getStringExtra("LIST_ITEM_ID");

        add_new_list=(ImageView)findViewById(R.id.add_new_list_shop_by_list);
        shop_list=(ListView)findViewById(R.id.lst_shop_bylist_items) ;
        btn_find_all_product=(Button)findViewById(R.id.btn_find_all_product);

        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        DEFAULT_LANGUGE=myprefs.getInt("DEFAULT_LANGUGE",1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);
        } else {
            userVLogIn = "0";

        }

        profile = (ImageView) findViewById(R.id.profile);
        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        title = (TextView) findViewById(R.id.namein_title);
        bck_btn = (Button) findViewById(R.id.menue_btn);

        try {


            if (userVLogIn != "0") {


                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent.equals(true)) {

                    new AsyncTaskCartcounttJson().execute();
                    new AsyncTaskListShowJson().execute();


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(getApplicationContext(), Signin.class);

                                startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();


            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        //footer delartions

        footerdsmore = (ImageView) findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        footerdssp = (ImageView) findViewById(R.id.imageView13);
        footerdsshop = (ImageView) findViewById(R.id.imageView11);
        footerdsdeal = (ImageView) findViewById(R.id.imageView12);
        footerdsgift = (ImageView) findViewById(R.id.imageView14);
        footerdsmore = (ImageView) findViewById(R.id.imageView15);

        //footer clicks

        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(ShopByList.this, DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, DsSpecials.class);

                startActivity(in);
            }
        });

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();

            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ShopByList.this, Notification.class);

                startActivity(in);

            }
        });
        title.setText(getString(R.string.my_shop_list_items));

        //click even to show all list product detail
        btn_find_all_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(ShopByList.this,ShopByListProductShow.class);
                in.putExtra("LIST_ITEM_ID",str_list_master_id);
                startActivity(in);
            }
        });
        //implimentation of add list
        add_new_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                                 /* Alert Dialog Code Start*/
                AlertDialog.Builder alert = new AlertDialog.Builder(ShopByList.this);
                alert.setTitle(getString(R.string.add_new_shop_list)); //Set Alert dialog title here

                // Set an EditText view to get user input
                final EditText input = new EditText(ShopByList.this);

                input.setHint(getString(R.string.prd_name));
                alert.setView(input);

                alert.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //You will get as string input data in this variable.
                        // here we convert the input to a string and show in a toast.
                        String srt = input.getEditableText().toString();
                        str_product_name_typed=srt;

                        //call new srch asytaskjson
                        new AsyncTaskSaveToListJson().execute();




                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                }); //End of alert.setNegativeButton
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
       /* Alert Dialog Code End*/
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                    jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {
                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if (userVLogIn.trim().equals("0")) {
                        cartcountbadge.setVisibility(View.GONE);
                    } else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }
            // Log.i("arrivedcount", str_json_cart_count);
            // mProgressDialog.dismiss();
        }
    }



    public class AsyncTaskSaveToListJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(ShopByList.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.save_list_shop(userVLogIn,str_product_name_typed,str_list_master_id);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                jsonlen=dataJsonArr.length();



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

             mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {

                new AsyncTaskListShowJson().execute();
            }
        }
    }

    public class AsyncTaskListShowJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(ShopByList.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.show_all_list_items(str_list_master_id);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                str_list_item_names=new String[dataJsonArr.length()];
                str_list_item_id=new String[dataJsonArr.length()];
                CONT_LIST_NO =new int[dataJsonArr.length()];
                for(int i=0;i<dataJsonArr.length();i++) {
                    CONT_LIST_NO[i]= i+1;
                    str_list_item_id[i] = dataJsonArr.getJSONObject(i).getString("ListDetailsId");
                    str_list_item_names[i] = dataJsonArr.getJSONObject(i).getString("ListItem");
                    Log.i("list values",str_list_item_id[i]+"   "+str_list_item_names[i]);

                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                Toast.makeText(ShopByList.this, "Currenty no item in the list", Toast.LENGTH_SHORT).show();
            }else {
                cust_list_prdct_by_srch_list adapter = new cust_list_prdct_by_srch_list(ShopByList.this, dataJsonArr.length(), str_list_item_names, str_list_item_id, CONT_LIST_NO,str_list_master_id);
                // Log.i("arrivedcount", str_json_cart_count);
                shop_list.setAdapter(adapter);
            }

        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");

                jsonlen=dataJsonArr_quick.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(ShopByList.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(ShopByList.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}
