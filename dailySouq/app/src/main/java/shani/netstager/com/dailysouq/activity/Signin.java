package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import shani.netstager.com.dailysouq.GCM.RegistrationIntentService;
import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 07/09/15.
 */
public class Signin extends Activity {
    private static final String TAG = "Sign";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST =9000;
    public  static  int SPLASH_TIME_OUT=3000;
    public LoginButton loginButton;
    public EditText ed_user, passEdit;
    public SharedPreferences myprefs;
    Button btn_signIn, btn_signUp;
    public CallbackManager callbackmanager;
    ImageView  im_individual,im_corporate;
    TextView forgoPass;
    static String  logInres;
    String  cid;
    static  String logIndetail;
    static String customerId,customername,useremail,phone;
    static  String customerlastname,telephone,dob_pref,psd_pref;
     static boolean succ;
    static String Uem;
    static String Upass;
    RelativeLayout logobtn;
    public  String findedtraindet;
    String completeURL;
    String fbEmail;
    String device,fbName,fbLastName="";
    Activity myActivity;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    int i=0;
    ProgressDialog mProgressDialog;
    DS_SP sp;

    int wheelstatus=0;
    ProgressWheel wheel,hwheel;
    RelativeLayout rel,relWheel;
    CheckBox remeberCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myprefs = getSharedPreferences("MYPREFS",Context.MODE_PRIVATE);
        FacebookSdk.sdkInitialize(Signin.this.getApplicationContext());
        callbackmanager = CallbackManager.Factory.create();
        setContentView(R.layout.sign_in);

        wheel = new ProgressWheel(Signin.this);
        hwheel= (ProgressWheel) findViewById(R.id.progress_wheel);
        rel=(RelativeLayout)findViewById(R.id.relparent);
        relWheel=(RelativeLayout)findViewById(R.id.relwheel);
        sp=new DS_SP(getApplicationContext());

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();



        logobtn=(RelativeLayout)findViewById(R.id.logo_sign_rltv);

        logobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Signin.this,MainActivity.class);

                startActivity(in);
            }
        });


        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){


        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            onBackPressed();


                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

//FBLogin
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email"));
        //loginButton.registerCallback(callbackmanager,callback);
        loginButton.setText(getString(R.string.login));

        loginButton.registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mProgressDialog = new ProgressDialog(Signin.this);
                // Set progressdialog title
                mProgressDialog.setTitle("");

                mProgressDialog.setMessage(getString(R.string.loading));
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                // Show progressdialog
                mProgressDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {


                     Profile profile=Profile.getCurrentProfile();
                                if(profile != null){
                                    fbName = profile.getFirstName().trim();
                                    fbLastName= profile.getLastName().trim();



                                    fbEmail = object.optString("email");


                                    if (checkPlayServices()) {
                                        // Start IntentService to register this application with GCM.


                                        Intent intent = new Intent(Signin.this, RegistrationIntentService.class);
                                        startService(intent);
                                    }
                                    // if(isConnected==true) {
                                    new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                                        @Override
                                        public void run() {

                                            new AsyncFbLogIn().execute();

                                        }
                                    }, SPLASH_TIME_OUT);



                                }
                                else{
                                    final AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
                                    alertDialog.setCancelable(false);
                                    alertDialog.setCanceledOnTouchOutside(false);
                                    alertDialog.setMessage(getString(R.string.thefbusercantsign));
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                        fblogout();
                                                    alertDialog.dismiss();
                                                    mProgressDialog.dismiss();
                                                }
                                            });

                                    alertDialog.show();

                                }






                          }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");


                request.setParameters(parameters);
                request.executeAsync();



            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

                Toast.makeText(Signin.this,"hi"+e.toString(),Toast.LENGTH_SHORT).show();
                Log.i("errer","e"+e.toString());

            }
        });




        //completion of FbLogin
        //starting of normal SignIn
        ed_user = (EditText) findViewById(R.id.emailEdit);
        passEdit = (EditText) findViewById(R.id.passwordEdit);
        btn_signIn = (Button) findViewById(R.id.signInBtn);
        btn_signUp = (Button) findViewById(R.id.signUpBtn);
        im_individual = (ImageView) findViewById(R.id.individual);
        im_corporate = (ImageView) findViewById(R.id.corperate);
        forgoPass=(TextView)findViewById(R.id.FpText);
        im_individual.requestFocus();
        im_individual.setFocusableInTouchMode(true);
        im_individual.setImageResource(R.drawable.individual1b);
        im_corporate.setImageResource(R.drawable.corporate1);
        ed_user.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "Email/Mobile number" + "</small>"));
        passEdit.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "Password" + "</small>"));
        remeberCheck=(CheckBox)findViewById(R.id.remeberCheck);

        //used for rmember option
            ed_user.setText(sp.getusersign());
            passEdit.setText(sp.getuserpsdsign());

        //final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.btnclick);
         final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
        //btn_signIn.setAnimation(myAnim);

        btn_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //view.startAnimation(myAnim);
                view.startAnimation(buttonClick);
                btn_signIn.setBackgroundColor(getResources().getColor(R.color.darkGreen));
                Uem = ed_user.getText().toString();
                Upass = passEdit.getText().toString();
                device = getDeviceId();


                //Intent in = new Intent(Signin.this, Categories.class);
                if (Uem.equals("") || Upass.equals("")) {

                    if (Uem.equals("")) {
                        ed_user.setError(getString(R.string.plz_fill));
                    }

                    if (Upass.equals("")) {
                        passEdit.setError(getString(R.string.plz_fill));
                    }
                    // Toast.makeText(Signin.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
                } else {

                    hwheel.setVisibility(View.VISIBLE);
                    wheel.setBarColor(Color.WHITE);
                    wheel.spin();
                    wheelstatus=1;
                    relWheel.setVisibility(View.VISIBLE);


                    if (checkPlayServices()) {
                        // Start IntentService to register this application with GCM.
                        Intent intent = new Intent(Signin.this, RegistrationIntentService.class);
                        startService(intent);
                    }
                    // if(isConnected==true) {

                           /* hwheel.setVisibility(View.VISIBLE);
                            wheel.setBarColor(Color.WHITE);
                            wheel.spin();
                            wheelstatus=1;
                            relWheel.setVisibility(View.VISIBLE);*/
                            if(remeberCheck.isChecked()){
                                sp.setusersign( ed_user.getText().toString());
                                sp.setusrpsdsign( passEdit.getText().toString());
                            }else {
                                sp.setusersign("");
                                sp.setusrpsdsign("");
                            }


                         //   getLogin();

                            new LoginAsynk().execute();









                }


            }
        });




    im_individual.setOnTouchListener(new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            im_individual.setImageResource(R.drawable.individual1b);
            im_corporate.setImageResource(R.drawable.corporate1);

            btn_signUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(Signin.this, IndividualSignUp.class);

                    startActivity(in);

                }
            });

            return false;
        }
    });
    im_corporate.setOnTouchListener(new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            im_individual.setImageResource(R.drawable.individual1);
            im_corporate.setImageResource(R.drawable.corporate1b);
            btn_signUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(Signin.this, CorperateSignUp.class);

                    startActivity(in);

                }
            });

            return false;
        }
    });


    btn_signUp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent in = new Intent(Signin.this, IndividualSignUp.class);


            startActivity(in);

        }
    });


        //forgot Password

        forgoPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                Intent in  =new Intent(Signin.this,ForgotPassword.class);
                startActivity(in);
                return false;
            }
        });





    }

    void fblogout(){
        LoginManager.getInstance().logOut();
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackmanager.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onStop() {
        super.onStop();
        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger.deactivateApp(this);
       // displayMessage(profile);
    }






    public  void getLogin() {




        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_customerlogin_only");

                    jsonn.put("Username", Uem);
                    jsonn.put("LoginType", "NO");
                    jsonn.put("Password", Upass);
                    jsonn.put("DeviceToken", sp.getdeviceId());

                    Log.i("DeviceTokenGcm",sp.getdeviceId()+"  "+"8");
                    //its used to pass 1 android in 2 in ios
                    jsonn.put("MobType","1");

                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    logIndetail = EntityUtils.toString(resEntity);
                    logIndetail = logIndetail.trim();


                    JSONObject jasonob;

                    jasonob = new JSONObject(logIndetail);

                    hwheel.setVisibility(View.GONE);
                    //rel.setVisibility(View.VISIBLE);
                    wheelstatus=0;
                    relWheel.setVisibility(View.GONE);

                    logInres = jasonob.getJSONArray("d").getJSONObject(0).optString("result").toString();
                    if(!logInres.equals("success"))

                    {


                        succ=false;
                       // Toast.makeText(Signin.this, "Invalid Username or Password", Toast.LENGTH_SHORT).show();

                        AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.invld_usr_psd));
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();



                    }

                    else{

                        customername=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("FirstName").toString();
                        customerId=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("CustomerId").toString();
                        useremail=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Email").toString();
                        phone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Phone").toString();
                        customerlastname=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("LastName").toString();
                        telephone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Telephone").toString();
                        dob_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Dob").toString();
                        psd_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Password").toString();

                        SharedPreferences.Editor editor=myprefs.edit();
                        editor.putString("shaniusrid", customerId);
                        editor.putString("bsrnme", customername);
                        editor.putString("useremail", useremail);
                        editor.putString("userphone",phone);
                        editor.putString("lastname", customerlastname);

                        editor.putString("logintype","NO");
                        editor.putString("telephone", telephone);
                        editor.putString("password", psd_pref);
                        editor.apply();
                        editor.apply();

                        succ=true;




                       Toast.makeText(getApplication().getBaseContext(),"Success",Toast.LENGTH_LONG).show();

                        Intent in = new Intent(Signin.this, MainActivity.class);


                        startActivity(in);



                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();

            }

        };
            th.start();

        }



    protected String getDeviceId() {
        return Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class AsyncFbLogIn extends AsyncTask<String,String,String>{
       JSONArray dataJsonArray;
       int prd_length=0;
       // Create a progressdialog


       @Override
       protected void onPreExecute() {
           super.onPreExecute();

       }

       @Override
        protected String doInBackground(String... strings) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.FBLogin_new(fbEmail,fbName,fbLastName);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArray = productObj.getJSONArray("d");
                cid=dataJsonArray.getJSONObject(0).getString("CustomerId");
                prd_length=dataJsonArray.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

       @Override
       protected void onPostExecute(String s) {
           super.onPostExecute(s);
           mProgressDialog.dismiss();
            if(prd_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.show();
            }else{
                SharedPreferences.Editor editor=myprefs.edit();
                editor.putString("shaniusrid",cid);
                editor.putString("bsrnme",fbName);
                editor.putString("useremail",fbEmail);

                editor.putString("logintype","FA");


                editor.apply();
                Toast.makeText(Signin.this,"succsess",Toast.LENGTH_LONG).show();


                Intent in = new Intent(Signin.this, MainActivity.class);


                startActivity(in);
            }

       }
   }

    public class LoginAsynk extends AsyncTask<String,String,String> {
        int json_length=0;
        JSONArray dataJsonArr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.login(Uem,"NO",Upass,sp.getdeviceId(),"1");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                logInres = dataJsonArr.getJSONObject(0).optString("result").toString();
                json_length=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(json_length==0){
                //do stuff here
                hwheel.setVisibility(View.GONE);
                //rel.setVisibility(View.VISIBLE);
                wheelstatus=0;
                relWheel.setVisibility(View.GONE);

                AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }else {


               //do stuff here
                hwheel.setVisibility(View.GONE);
                //rel.setVisibility(View.VISIBLE);
                wheelstatus=0;
                relWheel.setVisibility(View.GONE);
                if(!logInres.equals("success"))

                {
                    succ=false;
                    // Toast.makeText(Signin.this, "Invalid Username or Password", Toast.LENGTH_SHORT).show();

                    AlertDialog alertDialog = new AlertDialog.Builder(Signin.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.invld_usr_psd));
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }

                else{

                    try {
                        customername=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("FirstName").toString();
                    customerId=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("CustomerId").toString();
                    useremail=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Email").toString();
                    phone=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Phone").toString();
                    customerlastname=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("LastName").toString();
                    telephone=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Telephone").toString();
                    dob_pref=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Dob").toString();
                    psd_pref=dataJsonArr.getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Password").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPreferences.Editor editor=myprefs.edit();
                    editor.putString("shaniusrid", customerId);
                    editor.putString("bsrnme", customername);
                    editor.putString("useremail", useremail);
                    editor.putString("userphone",phone);
                    editor.putString("lastname", customerlastname);
                    editor.putString("logintype","NO");
                    editor.putString("telephone", telephone);
                    editor.putString("password", psd_pref);
                    editor.apply();
                    editor.apply();
                    succ=true;

                  //  Toast.makeText(getApplication().getBaseContext(),"Success",Toast.LENGTH_LONG).show();
                    Intent in = new Intent(Signin.this, MainActivity.class);

                    startActivity(in);



                }


            }


        }
    }




    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {

                finish();
            }
            return false;
        }
        return true;
    }

}


