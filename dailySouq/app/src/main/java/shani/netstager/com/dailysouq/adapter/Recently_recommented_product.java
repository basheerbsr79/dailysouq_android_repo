package shani.netstager.com.dailysouq.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.ProductDetail;
import shani.netstager.com.dailysouq.activity.Signin;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.DS_SP;


/**
 * Created by prajeeshkk on 09/10/15.
 */
public class Recently_recommented_product extends BaseAdapter {

    String product, category,disc;
    ViewHolder holder;
    ProgressDialog mProgressDialog;
    static ArrayAdapter<String> adapter;
    SharedPreferences myprefs;
    int cartcount;
    Context context;

    LayoutInflater inflater;
    int count = 1, pos;
    String cusId, pid, qun, result,res,bid,cid,addPid,wishPid;
    TextView cartcountbadge;
    RelativeLayout cartcounttext;
    JSONArray dataJsonArr;
    int str_json_cart_count;




    private ArrayList<ProductModel> productModels;
    DS_SP sp;

    public Recently_recommented_product(Context products, ArrayList<ProductModel> productModels, RelativeLayout cartcounttext, TextView cartcountbadge) {
        this.context = products;
        this.productModels = productModels;
        inflater = LayoutInflater.from(this.context);
        //badge
        this.cartcounttext=cartcounttext;
        this.cartcountbadge=cartcountbadge;
        sp=new DS_SP(context.getApplicationContext());

    }


    @Override
    public int getCount() {
        return productModels.size();
    }

    @Override
    public Object getItem(int i) {
        return productModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class ViewHolder {
        ImageView isnew;
        //newly added for hide
        LinearLayout actual_rate_full;
        ImageButton prdimg;
        TextView prdname;
        TextView actualprice;
        TextView offprice;
        TextView discont,save;
        Spinner prdSize;
        ImageButton valuePlus, valueMinus, addButton;
        TextView showValue;
        CustomSpinnerAdapter adapter;
        ProductModel productModel;
        SizeModel sizeModel;
        ImageView wishList;
        TextView sizeid;
        TextView stockstatus;
        //TextView nn;
    }

    public View getView(int position, View arg1, ViewGroup viewGroup) {


        View vi = arg1;
        int rollnocount = position;

        if (arg1 == null) {

            vi = inflater.inflate(R.layout.recent_products, null);

            holder = new ViewHolder();
            //newly added for hide
            holder.stockstatus=(TextView)vi.findViewById(R.id.txt_recent_stockstatus);
            holder.actual_rate_full=(LinearLayout)vi.findViewById(R.id.prd_actuel_rate_full);
            holder.isnew = (ImageView) vi.findViewById(R.id.new_product);
            holder.discont = (TextView) vi.findViewById(R.id.offer);
            holder.prdimg = (ImageButton) vi.findViewById(R.id.product_image);
            holder.prdname = (TextView) vi.findViewById(R.id.product_name);
            holder.actualprice = (TextView) vi.findViewById(R.id.mrpPrice);
            holder.offprice = (TextView) vi.findViewById(R.id.offPrice);
            holder.prdSize = (Spinner) vi.findViewById(R.id.productSpinner);
            holder.valuePlus = (ImageButton) vi.findViewById(R.id.valuePlus);
            holder.valueMinus = (ImageButton) vi.findViewById(R.id.valueMinus);
            holder.showValue = (TextView) vi.findViewById(R.id.valueText);
            holder.addButton = (ImageButton) vi.findViewById(R.id.addButton);
            holder.wishList = (ImageView) vi.findViewById(R.id.wishlist);
            holder.save=(TextView)vi.findViewById(R.id.save);
            holder.adapter = new CustomSpinnerAdapter(context);
            holder.productModel = productModels.get(position);
            holder.sizeid=(TextView)vi.findViewById(R.id.textid);


            vi.setTag(holder);

        } else {
            holder = (ViewHolder) arg1.getTag();
        }
        try {
            final ProductModel pro = productModels.get(position);

            holder.prdname.setText(pro.productName);

            holder.adapter.setdata(pro.sizes);

            holder.adapter.notifyDataSetChanged();
            holder.prdSize.setFocusable(true);
            holder.prdSize.setAdapter(holder.adapter);

            final View finalVi = vi;
            holder.prdSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    ViewHolder myViewHolder = (ViewHolder) finalVi.getTag();

                    SizeModel sizeModel = myViewHolder.adapter.getItem(i);
                    //hiding repeat rate tage save tag
                    if(sizeModel.actualprize.equals(sizeModel.offerprize)){
                        holder.actual_rate_full.setVisibility(View.GONE);
                        holder.save.setVisibility(View.GONE);

                    }
                    else{
                        holder.actual_rate_full.setVisibility(View.VISIBLE);
                        holder.save.setVisibility(View.VISIBLE);

                    }
                    if(sizeModel.stockstatus.trim().equals("0")){
                        holder.stockstatus.setTextColor(Color.RED);
                       holder.stockstatus.setText(context.getString(R.string.out_stock));
                    } else if (sizeModel.stockstatus.trim().equals("1")) {
                        holder.stockstatus.setTextColor(Color.WHITE);
                        holder.stockstatus.setText(context.getString(R.string.in_stock));
                    }else if (sizeModel.stockstatus.trim().equals("2")) {
                        holder.stockstatus.setTextColor(Color.WHITE);
                        holder.stockstatus.setText(context.getString(R.string.limted_stock));
                    }
                    else{
                        holder.stockstatus.setVisibility(View.GONE);
                    }



                   // pos=sizeModel.productSizeId;
                    myViewHolder.sizeid.setText(String.valueOf(sizeModel.productSizeId));
                    myViewHolder.actualprice.setText(sizeModel.actualprize+"0");
                    myViewHolder.actualprice.setPaintFlags(myViewHolder.actualprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                    Typeface face=Typeface.createFromAsset(context.getAssets(),
                            "fonts/ExoBold.otf");
                    //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                    myViewHolder.actualprice.setTypeface(face);
                    myViewHolder.offprice.setText(sizeModel.offerprize+"0");
                    Typeface face1=Typeface.createFromAsset(context.getAssets(),
                            "fonts/Bold.otf");
                    //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                    myViewHolder.offprice.setTypeface(face1);
                    myViewHolder.save.setText(context.getString(R.string.you_save)+" "+sizeModel.savaPrice+"0");
                    myViewHolder.discont.setText(sizeModel.discount+"%");
                    disc=myViewHolder.discont.getText().toString().trim();


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            Picasso.with(context).load(pro.imagepath).into(holder.prdimg);
            final ViewHolder myViewHolder = (ViewHolder) finalVi.getTag();
            myViewHolder.showValue.setText(Integer.toString(count));

            //stepper

            holder.prdimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    pid = pro.productId;
                    bid = pro.brandId;
                    cid = pro.category;

                    Intent in = new Intent(context, ProductDetail.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("Product", pid);
                    in.putExtra("Brand", bid);
                    in.putExtra("CATEGORY", cid);
                    context.startActivity(in);
                }
            });

            myViewHolder.showValue.setText(Integer.toString(count));

            //stepper
            qun=myViewHolder.showValue.getText().toString();
            myViewHolder.valuePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vplus=1;

                    vplus= Integer.parseInt(myViewHolder.showValue.getText().toString());

                    myViewHolder.showValue.setText(Integer.toString(vplus+1));
                    qun = myViewHolder.showValue.getText().toString();
                }
            });


            myViewHolder.valueMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vminus=1;
                    vminus= Integer.parseInt(myViewHolder.showValue.getText().toString());

                    myViewHolder.showValue.setText(Integer.toString(vminus-1));
                    qun = myViewHolder.showValue.getText().toString();

                }
            });

            if (pro.isNew == "true") {
                holder.isnew.setVisibility(View.VISIBLE);
            } else {
                holder.isnew.setVisibility(View.GONE);
            }

            cusId = pro.customerId;

            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    View prdd=myViewHolder.prdSize.getSelectedView();
                    TextView vv=(TextView)prdd;
                    String aiwa=vv.getText().toString();
                    Log.i("addd",aiwa);
                    View ids=  myViewHolder.sizeid.getRootView();
                    //TextView bb=(TextView)ids;
                    String sid=myViewHolder.sizeid.getText().toString();

                    pos=Integer.parseInt(sid);
                    Log.i("asss",sid);

                    if(cusId!="0"){
                        qun = myViewHolder.showValue.getText().toString();
                        int lst_qun=Integer.parseInt(qun);
                        if(lst_qun<=0){

                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(context.getString(R.string.alert_pls_quntit));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();


                        }else{


                            pid = pro.productId;
                            bid = pro.brandId;
                            cid = pro.category;

//                            Intent in = new Intent(context, ProductDetail.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            in.putExtra("Product", pid);
//                            in.putExtra("Brand", bid);
//                            in.putExtra("CATEGORY", cid);
//                            context.startActivity(in);

                            //need cahngue
                           new AsyncTaskAddCartJson().execute();
                        }

                    }
                    else{
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_signin));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in=new Intent(context,Signin.class);

                                        context.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }
                    addPid=pro.productId;
                }

            });

            holder.wishList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cusId!="0"){

                        new  AsyncTaskAddWishListJson().execute();

                    }
                    else{
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_signin));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in=new Intent(context,Signin.class);

                                        context.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }


                    wishPid=pro.productId;
                }
            });



        }



        catch (Exception e) {

        }
        return vi;
    }

    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addCart(0,cusId, addPid, pos, qun);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            } else {
                if (result.equals("success")) {
                   new AsyncTaskCartcounttJson().execute();
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("updated")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("Out of Stock")) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                    TextView ok = (TextView) layout.findViewById(R.id.textok);


                    builder.setView(layout);


                    final android.app.AlertDialog alertDialog = builder.create();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                } else {

                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));

                    alertDialog.show();


                }


            }

        }
    }

    public class AsyncTaskAddWishListJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addWishList(cusId, wishPid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                res = dataJsonArr.getJSONObject(0).optString("result");
                    jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                if (res.equals("success")) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_wishlist, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (res.equals("Alredy exist")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_exist, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));


                    alertDialog.show();


                }

            }
        }


    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {

                sp.setcartcount(str_json_cart_count);
                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(cusId.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }

            }
            // mProgressDialog.dismiss();
        }
    }
}




