package shani.netstager.com.dailysouq.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;

public class LangugeSelection extends AppCompatActivity {
       ImageView malayalam,english;
    SharedPreferences myprefs;
    TextView submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languge_selection);
        malayalam=(ImageView)findViewById(R.id.malnonselect);
        english=(ImageView)findViewById(R.id.engnonselect);
        submit=(TextView)findViewById(R.id.submit) ;
        english.setImageResource(R.drawable.englishselect);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myprefs.edit();
        editor.remove("DEFAULT_LANGUGE");
        editor.apply();

        english.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                english.setImageResource(R.drawable.englishselect);
                malayalam.setImageResource(R.drawable.malnonselect);
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                SharedPreferences.Editor editor=myprefs.edit();
                editor.putInt("DEFAULT_LANGUGE", 1);
                    editor.apply();
                return false;
            }


        });

        malayalam.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                malayalam.setImageResource(R.drawable.malayalamselect);
                english.setImageResource(R.drawable.engnonselect);
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                 SharedPreferences.Editor editor=myprefs.edit();
                    editor.putInt("DEFAULT_LANGUGE", 2);
                   editor.apply();
                return false;
            }

        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor=myprefs.edit();
                editor.putBoolean("HIDE_LANGUGUE_PAGE",true);
                editor.apply();


                    Intent in=new Intent(LangugeSelection.this,MainActivity.class);

                   startActivity(in);



            }
        });




    }
}
