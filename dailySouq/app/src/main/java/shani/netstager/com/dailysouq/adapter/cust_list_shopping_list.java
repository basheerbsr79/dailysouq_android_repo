package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.Ewallet;
import shani.netstager.com.dailysouq.activity.ShopByList;
import shani.netstager.com.dailysouq.activity.ShopByListProductShow;


public class cust_list_shopping_list extends BaseAdapter{

    String[] str_user_status,id;
    private Activity activity;


    LayoutInflater inf;
    ViewHolder holder;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;

    public cust_list_shopping_list(Activity context,  String[] str_satatus_linked_user,String[] id) {

        this.activity=context;

        this.str_user_status=str_satatus_linked_user;
        this.id=id;


        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);




    }

    @Override
    public int getCount() {
        return str_user_status.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static class ViewHolder{

        public TextView name;
        public ImageView additems;


    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.cust__shop_list, null);
            holder = new ViewHolder();


            holder.name = (TextView) vi.findViewById(R.id.name);
            holder.additems = (ImageView) vi.findViewById(R.id.txtAddList);

            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }

        holder.name.setText(str_user_status[position]+" ");
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(activity,ShopByListProductShow.class);
                in.putExtra("LIST_ITEM_ID",id[position]);
                activity.startActivity(in);

            }
        });
        holder.additems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in =new Intent(activity,ShopByList.class);
                in.putExtra("LIST_ITEM_ID",id[position]);
                activity.startActivity(in);
            }
        });

        return vi;
    }





}

