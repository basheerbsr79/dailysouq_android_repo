package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import shani.netstager.com.dailysouq.R;

/**
 * Created by prajeeshkk on 08/11/15.
 */
public class CustomTimeSlot extends BaseAdapter {
    ViewHolder holder;
    Context context;
    String[]timeid,timeSlot;
    String[]slotImage;
    public static String time;
    int timeLength;
    LayoutInflater inflater;

    public CustomTimeSlot(Context con, int timeLen, String[] timeId, String[] time, String[] image){
        context=con;
        timeid=timeId;
        timeSlot=time;
        slotImage=image;
        timeLength=timeLen;
       inflater = LayoutInflater.from(this.context);
    }
    @Override
    public int getCount() {
        return timeLength;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
class ViewHolder
{
    TextView txtTitle;
    ImageView imageView;
}
    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        View vi = view;
        if (view == null) {
            vi = inflater.inflate(R.layout.custom_time_slot, null);
            holder = new ViewHolder();
            holder.txtTitle=(TextView)vi.findViewById(R.id.txt);
            holder.imageView=(ImageView)vi.findViewById(R.id.img);
            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }


        try {
            if(timeSlot[position].equalsIgnoreCase("12:00AM - 12:00AM"))
            {
                holder.txtTitle.setText("Express Delivery");
            }else {
                holder.txtTitle.setText(timeSlot[position]);
            }


                Picasso.with(context).load(slotImage[position]).into(holder.imageView);


            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


        return vi;
    }
}
