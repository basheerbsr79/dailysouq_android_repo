package shani.netstager.com.dailysouq.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cust_list_quiz_winner_list;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class QuizView extends Activity {

    ImageView qustion,footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore,Notification;
    RelativeLayout cartcounttext;
    ArrayList <String> answerStatus;
    TextView qustiontext,opt1text,opt2text,opt3text,opt4text,othertext,title;
    String answr="",opt1str,opt2str,opt3str,opt4str,otherstr;
    Button submit,result,bck_btn;
    ProgressDialog mProgressDialog;
    ImageView profile,quickcheckout;
    int qstn_count=0;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    String[]options;
    String []option,qustionstr,quizid;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;
    String userVLogIn,nameofwinner,winningdate,winningquizname,str_json_cart_count;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    int []json_user_quiz_valid;


    String[] arrqstname, arrqstdate, arrwinname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_view);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge =(TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }


        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(QuizView.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(QuizView.this,CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(QuizView.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(QuizView.this, Notification.class);

                startActivity(in);
            }
        });





        bck_btn=(Button)findViewById(R.id.menue_btn);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);


        qustion=(ImageView)findViewById(R.id.qstnquizimg);


        submit=(Button)findViewById(R.id.button2);
        result=(Button)findViewById(R.id.button);

        qustiontext=(TextView)findViewById(R.id.qstntext);
        opt1text=(TextView)findViewById(R.id.opt1txt);
        opt2text=(TextView)findViewById(R.id.opt2txt);
        opt3text=(TextView)findViewById(R.id.opt3txt);
        opt4text=(TextView)findViewById(R.id.opt4txt);
        othertext=(TextView)findViewById(R.id.othertxt);


        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setText(R.string.title_activity_quiz_view);
        footerdssp.setImageResource(R.drawable.footerdsspecialsselect);



        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(QuizView.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(QuizView.this,DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(QuizView.this,DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(QuizView.this, Menu_List.class);

                startActivity(in);
            }
        });

        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

          //  new AsyncTaskCartcounttJson().execute();

            if(!userVLogIn.equalsIgnoreCase("0")){
                new AsyncTaskCarttJson().execute();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setTitle("");
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(QuizView.this, Signin.class);

                                QuizView.this.startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }

        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        opt1text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opt1text.setBackgroundResource(R.drawable.btn_active);
                opt2text.setBackgroundResource(R.drawable.btn);
                opt3text.setBackgroundResource(R.drawable.btn);
                opt4text.setBackgroundResource(R.drawable.btn);
                othertext.setBackgroundResource(R.drawable.btn);

                answr = opt1text.getText().toString();

            }
        });
        opt2text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opt1text.setBackgroundResource(R.drawable.btn);
                opt2text.setBackgroundResource(R.drawable.btn_active);
                opt3text.setBackgroundResource(R.drawable.btn);
                opt4text.setBackgroundResource(R.drawable.btn);
                othertext.setBackgroundResource(R.drawable.btn);
                answr=opt2text.getText().toString();
            }
        });
        opt3text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opt1text.setBackgroundResource(R.drawable.btn);
                opt2text.setBackgroundResource(R.drawable.btn);
                opt3text.setBackgroundResource(R.drawable.btn_active);
                opt4text.setBackgroundResource(R.drawable.btn);
                othertext.setBackgroundResource(R.drawable.btn);
                answr=opt3text.getText().toString();
            }
        });
        opt4text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opt1text.setBackgroundResource(R.drawable.btn);
                opt2text.setBackgroundResource(R.drawable.btn);
                opt3text.setBackgroundResource(R.drawable.btn);
                opt4text.setBackgroundResource(R.drawable.btn_active);
                othertext.setBackgroundResource(R.drawable.btn);
                answr=opt4text.getText().toString();
            }
        });
        othertext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opt1text.setBackgroundResource(R.drawable.btn);
                opt2text.setBackgroundResource(R.drawable.btn);
                opt3text.setBackgroundResource(R.drawable.btn);
                opt4text.setBackgroundResource(R.drawable.btn);
                othertext.setBackgroundResource(R.drawable.btn_active);
                answr=othertext.getText().toString();
            }
        });
            result.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     new Asynwinnerjson().execute();


                 }
            });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(answr.length()>1) {

                      new Asyncresultsubmit().execute();

                }

                else
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.plz_select),Toast.LENGTH_LONG).show();

                    AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.plz_select));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();


                    alertDialog.dismiss();

                }




            }
        });


/*if(sp.getQuizuserfirst()) {
    submit.setVisibility(View.GONE);
    qustiontext.setText(getString(R.string.thnku));
    opt1text.setVisibility(View.GONE);
    opt2text.setVisibility(View.GONE);
    opt3text.setVisibility(View.GONE);
    opt4text.setVisibility(View.GONE);
    othertext.setVisibility(View.GONE);
    result.setVisibility(View.VISIBLE);

}else {
    result.setVisibility(View.GONE);
}*/

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen = 0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(QuizView.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                JSONObject json = jParser.quiz(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Log.i("respnce of quiz", dataJsonArr.toString());
                quizid = new String[dataJsonArr.length()];
                qustionstr = new String[dataJsonArr.length()];
                option = new String[dataJsonArr.length()];
                json_user_quiz_valid = new int[dataJsonArr.length()];
                for (int i = 0; i < dataJsonArr.length(); i++) {
                    quizid[i] = dataJsonArr.getJSONObject(i).optString("QuizMasterId");
                    qustionstr[i] = dataJsonArr.getJSONObject(i).optString("QuizName");
                    option[i] = dataJsonArr.getJSONObject(i).optString("Options");
                    json_user_quiz_valid[i] = dataJsonArr.getJSONObject(i).optInt("status");

                }

                jsonlen = dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            answerStatus = new ArrayList<>();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                alertDialog.setTitle(getString(R.string.no_qst_found));
                alertDialog.show();

                submit.setVisibility(View.GONE);
                qustiontext.setText(getString(R.string.thnku));
                opt1text.setVisibility(View.GONE);
                opt2text.setVisibility(View.GONE);
                opt3text.setVisibility(View.GONE);
                opt4text.setVisibility(View.GONE);
                othertext.setVisibility(View.GONE);
                sp.setQuizuserfirst(true);
                result.setVisibility(View.VISIBLE);


            } else {

                for (int i = 0; i < dataJsonArr.length(); i++) {

                    if (json_user_quiz_valid[i] == 0) {

                        options = option[i].split(",");

                        qstn_count = i;
                        //not  answerd
                        submit.setVisibility(View.VISIBLE);

                        opt1text.setVisibility(View.VISIBLE);
                        opt2text.setVisibility(View.VISIBLE);
                        opt3text.setVisibility(View.VISIBLE);
                        opt4text.setVisibility(View.VISIBLE);
                        othertext.setVisibility(View.VISIBLE);
                        sp.setQuizuserfirst(true);
                        result.setVisibility(View.GONE);

                        qustiontext.setText(qustionstr[i]);
                        if (options.length == 1) {
                            opt1text.setText(options[0]);
                            opt2text.setVisibility(View.GONE);
                            opt3text.setVisibility(View.GONE);
                            opt4text.setVisibility(View.GONE);
                            othertext.setVisibility(View.GONE);

                        } else if (options.length == 2) {
                            opt1text.setText(options[0]);
                            opt2text.setText(options[1]);
                            opt3text.setVisibility(View.GONE);
                            opt4text.setVisibility(View.GONE);
                            othertext.setVisibility(View.GONE);

                        } else if (options.length == 3) {
                            opt1text.setText(options[0]);
                            opt2text.setText(options[1]);
                            opt3text.setText(options[2]);
                            opt4text.setVisibility(View.GONE);
                            othertext.setVisibility(View.GONE);
                        } else if (options.length == 4) {
                            opt1text.setText(options[0]);
                            opt2text.setText(options[1]);
                            opt3text.setText(options[2]);
                            opt4text.setText(options[3]);
                            othertext.setVisibility(View.GONE);

                        } else {
                            opt1text.setText(options[0]);
                            opt2text.setText(options[1]);
                            opt3text.setText(options[2]);
                            opt4text.setText(options[3]);
                            othertext.setText(options[4]);
                        }
                        break;
                    }


                }
                for (int i = 0; i < dataJsonArr.length(); i++) {
                    if (json_user_quiz_valid[i] == 1) {

                        answerStatus.add("true");
                    } else {
                        answerStatus.add("false");
                    }
                  /*  if(i+1>=dataJsonArr.length()){
                        Log.i("working","inside all answerd loop");
                        //alredy answerd
                        submit.setVisibility(View.GONE);
                        qustiontext.setText(getString(R.string.thnku));
                        opt1text.setVisibility(View.GONE);
                        opt2text.setVisibility(View.GONE);
                        opt3text.setVisibility(View.GONE);
                        opt4text.setVisibility(View.GONE);
                        othertext.setVisibility(View.GONE);
                        sp.setQuizuserfirst(true);
                        result.setVisibility(View.VISIBLE);
                    }else{

                        Log.i("working","inside  loop first cntions");

                        // continue;
                    }

                }*/


            /*if(json_user_quiz_valid==0){
                //user not answerd

                qustiontext.setText(qustionstr[qstn_count]);

                if (options.length == 1) {
                    opt1text.setText(options[0]);
                    opt2text.setVisibility(View.GONE);
                    opt3text.setVisibility(View.GONE);
                    opt4text.setVisibility(View.GONE);
                    othertext.setVisibility(View.GONE);

                } else if (options.length == 2) {
                    opt1text.setText(options[0]);
                    opt2text.setText(options[1]);
                    opt3text.setVisibility(View.GONE);
                    opt4text.setVisibility(View.GONE);
                    othertext.setVisibility(View.GONE);

                } else if (options.length == 3) {
                    opt1text.setText(options[0]);
                    opt2text.setText(options[1]);
                    opt3text.setText(options[2]);
                    opt4text.setVisibility(View.GONE);
                    othertext.setVisibility(View.GONE);
                } else if (options.length == 4) {
                    opt1text.setText(options[0]);
                    opt2text.setText(options[1]);
                    opt3text.setText(options[2]);
                    opt4text.setText(options[3]);
                    othertext.setVisibility(View.GONE);

                } else {
                    opt1text.setText(options[0]);
                    opt2text.setText(options[1]);
                    opt3text.setText(options[2]);
                    opt4text.setText(options[3]);
                    othertext.setText(options[4]);
                }
            }else {
                //alredy answerd
                submit.setVisibility(View.GONE);
                qustiontext.setText(getString(R.string.thnku));
                opt1text.setVisibility(View.GONE);
                opt2text.setVisibility(View.GONE);
                opt3text.setVisibility(View.GONE);
                opt4text.setVisibility(View.GONE);
                othertext.setVisibility(View.GONE);
                sp.setQuizuserfirst(true);
                result.setVisibility(View.VISIBLE);

            }*/


                }
                Log.i("answer", answerStatus + "");
                if (!answerStatus.contains("false")){
                    submit.setVisibility(View.GONE);
                    qustiontext.setText(getString(R.string.thnku));
                    opt1text.setVisibility(View.GONE);
                    opt2text.setVisibility(View.GONE);
                    opt3text.setVisibility(View.GONE);
                    opt4text.setVisibility(View.GONE);
                    othertext.setVisibility(View.GONE);
                    sp.setQuizuserfirst(true);
                    result.setVisibility(View.VISIBLE);
                }
            }
        }

    }


    public class Asynwinnerjson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(QuizView.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                JSONObject json = jParser.quizwinerlist();
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                arrqstname=new String [dataJsonArr.length()];
                arrqstdate=new String [dataJsonArr.length()];
                arrwinname=new String [dataJsonArr.length()];
                for(int i=0;i<dataJsonArr.length();i++) {
                    arrqstname[i] = dataJsonArr.getJSONObject(i).optString("QuizName");
                    arrqstdate[i] = dataJsonArr.getJSONObject(i).optString("ConductedDate");
                    arrwinname[i] = dataJsonArr.getJSONObject(i).optString("name");
                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
                if(jsonlen==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                    alertDialog.setTitle(getString(R.string.resultnotdeclaired));
                    alertDialog.show();
                }else {

                    resultPopup();

                    /*AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(getString(R.string.winners));
                    alertDialog.setMessage(getString(R.string.quiz_name) + winningquizname
                            + "\n" + "\n" + "\n" + getString(R.string.win_date) + winningdate
                            + "\n" + "\n" + getString(R.string.win_name) + nameofwinner);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mProgressDialog.dismiss();
                                }
                            });
                    alertDialog.show();*/
                }
        }


        private void resultPopup() {
            LayoutInflater layoutInflater
                    = (LayoutInflater)getBaseContext()
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.custom_quiz_result_popup, null);
            final PopupWindow popupWindow = new PopupWindow(
                    popupView,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);

            final ListView prdctcode = (ListView) popupView.findViewById(R.id.list);
            final Button dismiss = (Button) popupView.findViewById(R.id.btnDismiss);

            cust_list_quiz_winner_list ad =new cust_list_quiz_winner_list(QuizView.this,arrqstname,arrqstdate,arrwinname);
            prdctcode.setAdapter(ad);

            dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

            popupWindow.setBackgroundDrawable(null);
            popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));

            popupWindow.showAsDropDown(submit, 0,0);



        }


        }

    public class Asyncresultsubmit extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(QuizView.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                Log.i("quioz id",quizid[qstn_count]);
                JSONObject json = jParser.saveresultquiz(userVLogIn, quizid[qstn_count], answr);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                Log.i("respnce of su",dataJsonArr.toString());
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                Toast.makeText(getApplicationContext(),getString(R.string.error),Toast.LENGTH_LONG).show();

                onBackPressed();
            }
            else {

                    Intent in=new Intent(QuizView.this,QuizView.class);
                            QuizView.this.finish();
                        startActivity(in);

               // new AsyncTaskCarttJson().execute();
            }
               // Toast.makeText(getApplicationContext(),"Succsessfully submitted "+answr,Toast.LENGTH_LONG).show();
               // qstn_count++;


               /* if(qstn_count==dataJsonArr.length())*/{
                   /* AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.quiz_cmplete));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {*/
                                   /* submit.setVisibility(View.GONE);
                                    qustiontext.setText(getString(R.string.thnku));
                                    opt1text.setVisibility(View.GONE);
                                    opt2text.setVisibility(View.GONE);
                                    opt3text.setVisibility(View.GONE);
                                    opt4text.setVisibility(View.GONE);
                                    othertext.setVisibility(View.GONE);
                                    sp.setQuizuserfirst(true);
                                    result.setVisibility(View.VISIBLE);*/

                               /*     dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }*/
               /* else{
                    new AsyncTaskCarttJson().execute();
                }*/

            }


        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }
          //  Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
            int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(QuizView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(QuizView.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
    }

