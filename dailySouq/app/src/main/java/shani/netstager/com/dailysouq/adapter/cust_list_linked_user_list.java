package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.Ewallet;
import shani.netstager.com.dailysouq.support.Json;


public class cust_list_linked_user_list extends BaseAdapter{

    String[] str_user_status,str_fanme,str_ref_cus_id,str_address,str_emailorphone;
    private Activity activity;
    String str_amtpaid, str_responce,usermasterid,accounttype,str_description,userVLogIn;
    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;

    public cust_list_linked_user_list(Activity context, int length, String[] str_satatus_linked_user, String[] str_fname_linked_user, String[] str_ref_cust_id,String[]str_eamilorphone_user,String[]str_address_user) {

        this.activity=context;
        this.rec_count=length;
        this.str_user_status=str_satatus_linked_user;
        this.str_fanme=str_fname_linked_user;
        this.str_ref_cus_id=str_ref_cust_id;
        this.str_emailorphone=str_eamilorphone_user;
        this.str_address=str_address_user;


        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myprefs = activity.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }


    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static class ViewHolder{
        public TextView name;
        public TextView status;
        public Button transfer;

    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.cust_list_linked_user, null);
            holder = new ViewHolder();

            holder.name = (TextView) vi.findViewById(R.id.txt_name_linkeduser);
            holder.status = (TextView) vi.findViewById(R.id.txt_status_linkeduser);
            holder.transfer = (Button) vi.findViewById(R.id.btn_transfer_linkeduser);
            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{    holder.transfer.setVisibility(View.GONE);
                holder.name.setText(str_fanme[position]);
                holder.status.setText(str_user_status[position]);

                String test=holder.status.getText().toString();
                if(test.trim().equals("Accepted")||test.trim().equals("Verified")){
                    holder.status.setVisibility(View.GONE);
                    holder.transfer.setVisibility(View.VISIBLE);
                    holder.transfer.setText(activity.getString(R.string.transfer_found));
                    holder.transfer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(activity,str_ref_cus_id[position],Toast.LENGTH_LONG).show();

                               /* Alert Dialog Code Start*/
                            AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                            alert.setTitle(activity.getString(R.string.trsfer_ewallet_amnt)); //Set Alert dialog title here
                            // Set an EditText view to get user input
                            final EditText input = new EditText(activity);
                            input.setHint(activity.getString(R.string.amnt_in_rs));
                            alert.setView(input);

                            alert.setPositiveButton(activity.getString(R.string.transfer_money), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    usermasterid=str_ref_cus_id[position];
                                    accounttype=activity.getString(R.string.credit);
                                    str_description=activity.getString(R.string.money_trnfer);

                                    //You will get as string input data in this variable.
                                    // here we convert the input to a string and show in a toast.
                                    str_amtpaid = input.getEditableText().toString();
                                    Toast.makeText(activity, str_amtpaid, Toast.LENGTH_LONG).show();

                                    //alert to confirm the user

                                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                                    alertDialog.setTitle(activity.getString(R.string.confirm_user));
                                    String emailsrt = activity.getString(R.string.email_phone) + ": " + str_emailorphone[position];
                                    String alert2 = activity.getString(R.string.name) + ": " + str_fanme[position];
                                    String alert3 = activity.getString(R.string.adrs) + ": " + str_address[position];
                                    alertDialog.setMessage(emailsrt + "\n" + alert2 + "\n" + alert3);

                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, activity.getString(R.string.confirm),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    //call to transfer process

                                                    new AsyncTaskEwalletTransferJson().execute();


                                                    dialog.dismiss();
                                                }
                                            });

                                    alertDialog.show();




                                } // End of onClick(DialogInterface dialog, int whichButton)
                            }); //End of alert.setPositiveButton
                            alert.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // Canceled.
                                    dialog.cancel();
                                }
                            }); //End of alert.setNegativeButton
                            AlertDialog alertDialog = alert.create();
                            alertDialog.show();
       /* Alert Dialog Code End*/

                        }
                    });


                }

        }
        catch(Exception e){

        }
        return vi;
    }

    public class AsyncTaskEwalletTransferJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.ewalletfoundsave(usermasterid,accounttype,str_amtpaid, userVLogIn,str_description, "0");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_responce = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                if(jsonlen==0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(activity.getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, activity.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent in = new Intent(activity, Ewallet.class);
                                    ((Activity) activity).finish();
                                    activity.startActivity(in);

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else{

                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(str_responce);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, activity.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent in = new Intent(activity, Ewallet.class);
                                    ((Activity) activity).finish();
                                    activity.startActivity(in);

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }


        }
    }



}

