package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.ShopByList;


/**
 * Created by user on 02/02/16.
 */

public class cust_list_prdct_by_srch_list extends BaseAdapter {


    private Activity activity;
    String[]name,id;
    int rec_count;
    int []count;
    LayoutInflater inf;
    ViewHolder holder;
    String userVLogIn,str_id_delete;
    SharedPreferences myprefs;
    String str_list_master_id;

    public cust_list_prdct_by_srch_list(Activity context, int length, String[] name, String[] id,int []count, String str_list_master_id){
        this.activity=context;
        this.rec_count=length;
        this.name=name;
        this.id=id;
        this.count=count;
        this.str_list_master_id=str_list_master_id;

        myprefs = activity.getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder{
        public TextView prdname;
        public  TextView counttxt;
        public ImageView add;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_prdct_by_srch_list, null);
            holder = new ViewHolder();
            holder.counttxt=(TextView)vi.findViewById(R.id.cust_list_shop_count);
            holder.prdname = (TextView) vi.findViewById(R.id.txt_cust_list_item_srch_prdct);
            holder.add=(ImageView)vi.findViewById(R.id.btn_add_cust_prdct_shop_by_srch);

            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{
            holder.prdname.setText(name[position]);
            holder.counttxt.setText(count[position]+" .");

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    AlertDialog alertDialog=new AlertDialog.Builder(activity).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(activity.getString(R.string.remove_alert));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,activity.getString(R.string.yes),new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {



                            str_id_delete=id[position];

                            new AsyncTaskRemoveFromListJson().execute();
                            dialogInterface.dismiss();
                        }
                    });


                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,activity.getString(R.string.no),new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });
                    alertDialog.show();




                }
            });


            holder.prdname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Toast.makeText(activity,"Need to show Products",Toast.LENGTH_LONG).show();







                }
            });

        }catch(Exception e){

        }
        return vi;
    }


    public class AsyncTaskRemoveFromListJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        ProgressDialog mProgressDialog;
        JSONArray dataJsonArr;
        int jsonlen=0;


        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(activity);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(activity.getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.remove_from_shopinglist(userVLogIn,str_id_delete);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                jsonlen=dataJsonArr.length();



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen>0) {


                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(activity).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(activity.getString(R.string.succsesfullyremved));
                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, activity.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(activity, ShopByList.class);
                                in.putExtra("LIST_ITEM_ID",str_list_master_id);
                                ((Activity) activity).finish();
                                activity.startActivity(in);
                            }
                        });

                alertDialog.show();
            }else{
                final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(activity).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(activity.getString(R.string.error));
                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, activity.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                            }
                        });

                alertDialog.show();
            }



        }
    }
}