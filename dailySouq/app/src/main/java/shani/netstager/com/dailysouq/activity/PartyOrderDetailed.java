package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.partyoredritemsGridViewAdapter;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.support.OnLoadMoreListener;

public class PartyOrderDetailed extends AppCompatActivity {
    private RecyclerView gridViews;
    private partyoredritemsGridViewAdapter mAdapter;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;

    View footer;
    ProgressDialog dialog;
    //its used for new scroll
    int id = 0;
    ImageView profiles, quickcheckout;
    ProgressDialog mProgressDialog;
    int sizeProduct;
    int productLength;
    int DEFAULT_LANGUGE;
    public SharedPreferences myProducts;
    Button back_btn;
    String catId, userVLogIn;
    SharedPreferences myprefs;
    TextView title;

    JSONArray dataJsonArr;
    ImageView Notification;
    RelativeLayout cartcounttext;
    int str_json_cart_count;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout, str_contact_name_quick_checkout, str_opt_payment, str_location_quick_checkout, str_phone_quick_checkout;
    int str_paymentmethod_payment;
    String[] str_partyorder_id, str_partyorder_name, str_partyorder_crdate, str_partyorder_desc, str_partyorder_persons;

    String partyorderid,partyorderpersons;
    private GridLayoutManager lLayout;
    private TextView tvEmptyView;
    ArrayList<ProductModel> productModels;


    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_order_detailed);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }

        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        productModels = new ArrayList<>();
        lLayout = new GridLayoutManager(PartyOrderDetailed.this, 1);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);

        title = (TextView) findViewById(R.id.namein_title);
        partyorderid=getIntent().getStringExtra("PartyOrderid");
        partyorderpersons=getIntent().getStringExtra("PartyOrderPersons");
        title.setText(getIntent().getStringExtra("PartyOrderName")+"");
        tvEmptyView = (TextView) findViewById(R.id.empty_view);

        pgresbar_scrool_footer = (ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            new AsyncTaskCartcounttJson().execute();

            new AsyncTaskTempJson().execute();

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView) findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        profiles = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);

        back_btn = (Button) findViewById(R.id.menue_btn);



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PartyOrderDetailed.this, EditProfile.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(PartyOrderDetailed.this, Notification.class);

                startActivity(in);
            }
        });




        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(PartyOrderDetailed.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PartyOrderDetailed.this, CartView.class);

                startActivity(in);
            }
        });



        /*add_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userVLogIn != "0") {

                    //call add method
                    new AsyncTaskAddCartJson().execute();

                }else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(PartyOrderDetailed.this, Signin.class);

                                    PartyOrderDetailed.this.startActivity(in);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }
            }
        });*/


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }



    public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;

        int json_length=0;



        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.partorderdetails(partyorderid,userVLogIn,DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);


                    //Shakeeb code
                    ProductModel product = new ProductModel();
                    product.partorderid = c.optString("PartyOrderId");
                    product.partyordername = c.optString("PartyOrderName").toString();
                    product.partyorderdesc = c.optString("PartyDescription").toString();
                    product.partyodernoofperson = c.optString("NumberofPersons");
                    product.partordertotal = c.optString("Total");
                    product.imagepath=c.getString("Imagepath");
                    product.customerId=userVLogIn;
                    productModels.add(product);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);



                }



                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<ProductModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(PartyOrderDetailed.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            }else {



                if (productModels.isEmpty()) {
                    Toast.makeText(PartyOrderDetailed.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                    /*add_all.setVisibility(View.VISIBLE);*/

                    mAdapter = new partyoredritemsGridViewAdapter(productModels, gridViews, PartyOrderDetailed.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);

                    mAdapter.setProductlist(productModels);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();


                }
                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {


                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                new AsyncTaskParseJson().execute();


                            }
                        }, 100);

                    }
                });


            }


        }
    }

    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prd_length=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PartyOrderDetailed.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addpartyordertocart(partyorderid,userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).getString("result");
                prd_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (prd_length == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();

                alertDialog.setMessage(getString(R.string.error));


                alertDialog.show();
            } else {


                if (result.equals("success")) {
                    new AsyncTaskCartcounttJson().execute();
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PartyOrderDetailed.this);
                    LayoutInflater inflater1 = (LayoutInflater) PartyOrderDetailed.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);

//                    new AsyncTaskCartcounttJson().execute();
//                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//                    alertDialog.setMessage(getString(R.string.alert_added_cart));
//
//                    alertDialog.show();


                } else if (result.equals("updated")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PartyOrderDetailed.this);
                    LayoutInflater inflater1 = (LayoutInflater) PartyOrderDetailed.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("Out of Stock")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PartyOrderDetailed.this);
                    LayoutInflater inflater1 = (LayoutInflater) PartyOrderDetailed.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                    TextView ok = (TextView) layout.findViewById(R.id.textok);


                    builder.setView(layout);


                    final android.app.AlertDialog alertDialog = builder.create();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();


                        }
                    });
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();

                    alertDialog.setMessage(getString(R.string.error));


                    alertDialog.show();


                }
            }
        }
    }

    public class AsyncTaskTempJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                //first time pass 0 fo rthe api
                JSONObject json = jParser.temppartyordercart(partyorderid,userVLogIn,"0");
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new AsyncTaskParseJson().execute();
        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PartyOrderDetailed.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            // mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();

                alertDialog.setMessage(getString(R.string.error));

                alertDialog.show();
            }else {

                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }

                // Log.i("arrivedcount", str_json_cart_count);

            } //  mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderDetailed.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(PartyOrderDetailed.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}
