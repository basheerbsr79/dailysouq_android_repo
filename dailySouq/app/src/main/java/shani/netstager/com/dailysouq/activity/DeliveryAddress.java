package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cust_list_delivery;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class DeliveryAddress extends AppCompatActivity {
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    TextView title;
    Button bck_btn,add_new;
    ListView list;
    public SharedPreferences myprefs,myProducts;
    String userVLogIn,str_json_cart_count;
    int handlercount=10000;
    Handler mHandler;
    RelativeLayout fullview;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    String[]address,postcode,landmark,contactname,contactno,deliveryaddressid,cityname,locationname;
    Boolean[] defaultaddress;
    int[] locationid,cityid;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_address);

        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }


        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        Log.i("usr_ida",userVLogIn+"f");
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);
            Log.i("usr_idb",userVLogIn+"f");
        }
        else{
            Log.i("usr_idc",userVLogIn+"f");
            userVLogIn="0";
            Log.i("usr_idd",userVLogIn+"f");
        }


        fullview=(RelativeLayout)findViewById(R.id.delivery_full) ;
        fullview.setVisibility(View.GONE);
//        this.mHandler = new Handler();
//
//        this.mHandler.postDelayed(m_Runnable, handlercount);

        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        list=(ListView)findViewById(R.id.ldel1);
        add_new=(Button)findViewById(R.id.btn_add_del);
        myprefs = getSharedPreferences("MYPREFS",Context.MODE_PRIVATE);
        try {


            if (userVLogIn!="0") {
                fullview.setVisibility(View.VISIBLE);
                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if(isInternetPresent.equals(true)){

                   // new AsyncTaskCartcounttJson().execute();
                    new AsyncTaskCarttJson().execute();

                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }





            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in=new Intent(getApplicationContext(),Signin.class);

                                startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();




            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ins = new Intent(DeliveryAddress.this, AddDeliveryAddress.class);
                ins.putExtra("arrivedfrom",4);
                startActivity(ins);

            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DeliveryAddress.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DeliveryAddress.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(DeliveryAddress.this, Notification.class);

                startActivity(in);
            }
        });
        title.setText(getString(R.string.delivery_adress));

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            Log.i("usr_id2",userVLogIn+"f");
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

      //  mHandler.removeCallbacks(m_Runnable);
    }






    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(DeliveryAddress.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                userVLogIn = myprefs.getString("shaniusrid", null);

                Json jParser = new Json();
                JSONObject json = jParser.delivery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                address=new String[dataJsonArr.length()];
                postcode=new String[dataJsonArr.length()];
                landmark=new String[dataJsonArr.length()];
                contactname=new String[dataJsonArr.length()];
                contactno=new String[dataJsonArr.length()];
                locationname=new String[dataJsonArr.length()];
                cityname=new String[dataJsonArr.length()];
                locationid=new int[dataJsonArr.length()];
                cityid=new int[dataJsonArr.length()];
                deliveryaddressid=new String[dataJsonArr.length()];
                defaultaddress=new Boolean[dataJsonArr.length()];

                for(int i=0;i<dataJsonArr.length();i++) {


                    address[i] = dataJsonArr.getJSONObject(i).optString("Address");
                    postcode[i] = dataJsonArr.getJSONObject(i).optString("PostCode");
                    landmark[i] = dataJsonArr.getJSONObject(i).optString("LandMark");
                    contactname[i] = dataJsonArr.getJSONObject(i).optString("ContactName");
                    contactno[i] = dataJsonArr.getJSONObject(i).optString("ContactNo");
                    locationid[i] = dataJsonArr.getJSONObject(i).optInt("LocationId");
                    locationname[i] = dataJsonArr.getJSONObject(i).optString("LocationName");
                    cityname[i] = dataJsonArr.getJSONObject(i).optString("CityName");
                    deliveryaddressid[i] = dataJsonArr.getJSONObject(i).optString("DeliveryAddressId");
                    defaultaddress[i] = dataJsonArr.getJSONObject(i).optBoolean("DefaultAddress");
                    cityid[i] = dataJsonArr.getJSONObject(i).optInt("CityId");

                }
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.delnoalert));
                alertDialog.show();
            } else {

            cust_list_delivery adapter = new cust_list_delivery(DeliveryAddress.this, dataJsonArr.length(), defaultaddress, cityname, locationname, contactname, address, landmark, locationid, cityid, deliveryaddressid, postcode, contactno);
            list = (ListView) findViewById(R.id.ldel1);
            list.setAdapter(adapter);

        }
        }
    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {


                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }

            }
            //  Log.i("arrivedcount", str_json_cart_count);

            //  mProgressDialog.dismiss();
        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
            jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(DeliveryAddress.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(DeliveryAddress.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}
