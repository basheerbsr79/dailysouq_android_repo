package shani.netstager.com.dailysouq.support;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by prajeeshkk on 21/09/15.
 */
public class jSonfiles{

    public static String[] stationCode;
    public static String[] stationName;
    public static String[] age;
    public static int stationLen;

    static String[] productId;
    static String[] productName;
    static String[] categoryId;
    static String[] imgPath;
    static String[] productExpire;
    static String[] isNew;
    static String[] actualPrice;
    static String[] offerPrice;
    static String[] offerName;

    ArrayList<String> nw = new ArrayList<String>();
  static   int catLength;
    static String []spinCatName;



    static int productLength;
        public static void parseStationlist(String stationData) throws Exception{


           JSONObject stationArray=new JSONObject(stationData);

            JSONArray samplearray=stationArray.getJSONArray("d");

                stationLen=samplearray.length();

                stationCode=new String[stationLen];
                stationName=new String[stationLen];
            age=new String[stationLen];

              //st  System.out.println("companyLen====="+stationLen);

                for(int i = 0;i<stationLen;i++){
                    stationCode [i]=samplearray.getJSONObject(i).getString("CategoryId").toString();
                    stationName [i]=samplearray.getJSONObject(i).getString("CategoryName").toString();
                    age [i]=samplearray.getJSONObject(i).getString("CategoryMobImg").toString();

                }
            }

    public  static  void parseProductList(String productData)throws  Exception
    {

      JSONObject productObj=new JSONObject(productData);
        JSONArray productArray=productObj.getJSONArray("d");

        productLength=productArray.length();

        productId=new String[productLength];
        productName=new String[productLength];
        categoryId=new String[productLength];
        imgPath=new String[productLength];
        productExpire=new String[productLength];
        isNew=new String[productLength];
        actualPrice=new String[productLength];
        offerPrice=new String[productLength];
        offerName=new String[productLength];

        for(int i=0;i<productLength;i++)
        {
            productId[i]=productArray.getJSONObject(i).getString("ProductId").toString();
            productName[i]=productArray.getJSONObject(i).getString("ProductName").toString();
            categoryId[i]=productArray.getJSONObject(i).getString("CategoryId").toString();
            imgPath[i]=productArray.getJSONObject(i).getString("Imagepath").toString();
            productExpire[i]=productArray.getJSONObject(i).getString("ProductExpire").toString();
            isNew[i]=productArray.getJSONObject(i).getString("IsNew").toString();
            actualPrice[i]=productArray.getJSONObject(i).getString("ActualPrice").toString();
            offerPrice[i]=productArray.getJSONObject(i).getString("OfferPrice").toString();
            offerName[i]=productArray.getJSONObject(i).getString("offername").toString();
        }

    }

}
