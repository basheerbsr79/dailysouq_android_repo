package shani.netstager.com.dailysouq.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.PartyOrderDetailed;
import shani.netstager.com.dailysouq.activity.PartyOrderPopupItemsShow;
import shani.netstager.com.dailysouq.activity.Signin;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.OnLoadMoreListener;

public class partyoredritemsGridViewAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    int str_json_cart_count;
    RelativeLayout cartcounttext;
    TextView cartcountbadge;

    //item qntity
    int count = 1;
    int pos=0;
    //intent passing datas
    String cusId, pid, qun, result,res,bid,cid,addPid,wishPid;
    //size list
    SizeModel sizeModel;


    private List<ProductModel> productlist;
    private ArrayList<SizeModel> sizeModels;
    static Context context;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    public boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    int language;
    DS_SP sp;
    String str_prty_oredr_id;

    public void setProductlist(List<ProductModel> productlist) {
        this.productlist = productlist;
    }

   /* public partyoredritemsGridViewAdapter(View t,String cusId,String partorderid){
        //here to show poup

        str_prty_oredr_id =partorderid;
        this.cusId=cusId;
        View v=t;

        new AsyncTaskAddCartJson(v).execute();
    }*/

    public partyoredritemsGridViewAdapter(List<ProductModel> productModels, RecyclerView recyclerView, PartyOrderDetailed products, RelativeLayout cartcounttext, TextView cartcountbadge, int DEFAULT_LANGUGE) {
        setProductlist(productModels);

        this.context=products;
        this.cartcounttext= cartcounttext;
        this.cartcountbadge= cartcountbadge;
        this.language=DEFAULT_LANGUGE;
        sp=new DS_SP(context.getApplicationContext());


        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {

            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return productlist.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.partorder_item_grid, parent, false);

        vh = new MainViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MainViewHolder) {


            final ProductModel pro = (ProductModel) productlist.get(position);

            cusId = pro.customerId;
            str_prty_oredr_id =pro.partorderid;

            ((MainViewHolder) holder).prdname.setText(pro.partyordername+"");
            ((MainViewHolder) holder).qty_partorder.setText("No of persons : "+pro.partyodernoofperson);

            ((MainViewHolder) holder).txt_mrp.setText("MRP : "+pro.partordertotal);

            ((MainViewHolder) holder).txt_desc.setText(pro.partyorderdesc+"");

            ((MainViewHolder) holder).btn_addcart.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    //here to show poup
                    if(cusId.equalsIgnoreCase("0")){
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_signin));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,context. getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in = new Intent(context, Signin.class);

                                       context.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }else {

                        // new AsyncTaskAddCartJson(v).execute();
                        Intent in = new Intent(context, PartyOrderPopupItemsShow.class);
                        in.putExtra("PID", str_prty_oredr_id);
                        in.putExtra("LANID", language);
                        in.putExtra("USERID", cusId);
                        in.putExtra("NOPER", pro.partyodernoofperson);
                        in.putExtra("NAME", pro.partyordername);
                        context.startActivity(in);
                    }

                }
            });

            Picasso.with(context).load(pro.imagepath).placeholder(R.drawable.placeholder).into(((MainViewHolder) holder).prdimg);
            //getting user id



            ((MainViewHolder) holder).prdimg.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    // do nothing

                }
            });



            ((MainViewHolder) holder).student = pro;

        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


   /* public void  mypopup(String id,View v){
        LayoutInflater layoutInflater =
                (LayoutInflater)context
                        .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custompopupforpartyorder, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView, GridLayoutManager.LayoutParams.WRAP_CONTENT, GridLayoutManager.LayoutParams.WRAP_CONTENT);

        //Update TextView in PopupWindow dynamically
        final EditText edQty = (EditText) popupView.findViewById(R.id.ed_count_poup);

        Button btnAdd = (Button)popupView.findViewById(R.id.btn_add);

        Button btnMinus = (Button)popupView.findViewById(R.id.btnMinus);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty= Integer.parseInt(edQty.getText().toString());
                qty++;
                edQty.setText(qty+"");
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty= Integer.parseInt(edQty.getText().toString());
                qty--;
                edQty.setText(qty+"");
            }
        });

        Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);

        ListView popupSpinner = (ListView)popupView.findViewById(R.id.popupspinner);


         popadpter =
                new partyOrderItemshowPopup( context, str_prdid, str_prdname, str_prdsizeid, str_prd_qty, str_subtotal, str_stockstatus,cusId,str_prty_oredr_id);

        popupSpinner.setAdapter(popadpter);

        btnDismiss.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }});



        popupWindow.showAtLocation( v, Gravity.CENTER,0,0);
    }*/


    //
    public static class MainViewHolder extends RecyclerView.ViewHolder {

        ImageButton prdimg;
        TextView prdname;

        TextView qty_partorder;
        TextView txt_mrp;
        TextView txt_desc;
        Button btn_addcart;

        public ProductModel student;

        public MainViewHolder(View v) {
            super(v);
            //newly added for hide
            qty_partorder=(TextView)v.findViewById(R.id.prtyqnty) ;

            prdimg = (ImageButton) v.findViewById(R.id.product_image);
            prdname = (TextView) v.findViewById(R.id.product_name);
            txt_mrp=(TextView)v.findViewById(R.id.prtyrs);
            txt_desc=(TextView)v.findViewById(R.id.prtydesc);
            btn_addcart=(Button)v.findViewById(R.id.prtyaddcart);

        }
    }

   /* public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;
        View v;
        AsyncTaskAddCartJson(View v){
            this.v=v;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.toviewprtoderepoupitems(str_prty_oredr_id,cusId, language);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_prdid =new String[dataJsonArr.length()];
                str_prdname =new String[dataJsonArr.length()];
                str_prdsizeid =new String[dataJsonArr.length()];
                str_prd_qty =new String[dataJsonArr.length()];
                str_subtotal =new String[dataJsonArr.length()];
                str_stockstatus =new String[dataJsonArr.length()];


                for(int i=0;i<dataJsonArr.length();i++) {
                    str_prdid[i] = dataJsonArr.getJSONObject(i).optString("ProductId");
                    str_prdname[i] = dataJsonArr.getJSONObject(i).optString("ProductShowName");
                    str_prdsizeid[i] = dataJsonArr.getJSONObject(i).optString("ProductSizeId");
                    str_prd_qty[i] = dataJsonArr.getJSONObject(i).optString("Quantity");
                    str_subtotal[i] = dataJsonArr.getJSONObject(i).optString("Subtotal");
                    str_stockstatus[i] = dataJsonArr.getJSONObject(i).optString("Stockstatus");
                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.not_found));
                alertDialog.show();
            } else {

                mypopup(str_prty_oredr_id,v);

                Intent in=new Intent(context, PartyOrderPopupItemsShow.class);
                context.startActivity(in);
            }

        }
    }

    public class AsyncTaskAddWishListJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();

                JSONObject json = jParser.addWishList(cusId, wishPid);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                res = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                if (res.equals("success")) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_wishlist, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;


                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });

                    handler.postDelayed(runnable, 2000);


                } else if (res.equals("Alredy exist")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_exist, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();
                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });

                    handler.postDelayed(runnable, 2000);


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));


                    alertDialog.show();


                }
            }

        }


    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                sp.setcartcount(str_json_cart_count);
                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(cusId.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }

            // mProgressDialog.dismiss();
        }
    }*/
}