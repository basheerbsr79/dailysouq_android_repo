package shani.netstager.com.dailysouq.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomOrderReview;
import shani.netstager.com.dailysouq.paypal.PayPalConfig;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 28/10/15.
 */
public class OrderReview extends Activity {

    String deliverDate, deliveryTime, discount, usermasterid, accounttype, str_amtpaid, str_description;
    String[] quantity, productName, stockstatus;
    String[] amount, unitprice;
    String subTotal, total;
    int deliveryCharges;
    int totalNoItems, detailsLength, STR_PAYMENT_METHOD_ID;
    ProgressDialog mprogressDialog, mprogressDialog2, mProgressDialog3;
    TextView deliveryDate, deliveryTme, totalItems, subTot, discoun, deliveryCharge, tot, title;
    ListView orderProductList;
    ImageView profile, quickcheckout, Notification;
    RelativeLayout cartcounttext;
    TextView cartcountbadge, notifbadge;
    DS_SP sp;
    Button back_btn;
    SharedPreferences myprefs;
    String userVLogIn, str_json_cart_count;
    JSONArray dataJsonArr;
    //ProgressDialog mProgressDialog;
    int count = 0;
    TextView txt_avil_free_amnt, txt_payment_method;
    String AFTER_OREDER_ID, AFTER_OREDER_NAME, AFTER_ORDER_EMAIL;
    LinearLayout lnr_delivery_layout;
    LinearLayout lnr_delivery_layout2;
    int str_del_amnt;
    //variables are used for ebs
    private static String HOST_NAME = "EBS";
    ArrayList<HashMap<String, String>> custom_post_parameters;
    private static final int ACC_ID = 19241; // Provided by EBS
    private static final String SECRET_KEY = "5e95119c0b20a69f9a970dfe3aebded6";
    private static final double PER_UNIT_PRICE = 1.00;
    double totalamount;
    String ORDERREVIEW_NAME, ORDERREVIEW_ADDRESS, ORDERREVIEW_PHONE, ORDERREVIEW_LOCATION, STR_QUICK_ARRIVED = "";
    TextView txt_order_name, txt_order_address, txt_order_location, txt_order_phone;
    // its used for quick check out
    String str_address_quick_checkout, str_contact_name_quick_checkout, str_opt_payment, str_location_quick_checkout, str_phone_quick_checkout, STR_PAYMENT_METHOD_NAME;
    int str_paymentmethod_payment;
    //its used to changue step image
    ImageView lnr_step_image;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+", str_referemail;


    String str_trnsorder_custid, str_trns_postcode, str_trns_landmark, str_trns_address, str_trns_contctnmae, str_trns_contctnumber, str_trns_locationid, str_trns_deliveradress_id, str_trns_dfltaddress, str_trns_cityid;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;


    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    String INR_TO_USD;
    Float USD_RATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_review);

        //setting delivery address in review
        txt_order_name = (TextView) findViewById(R.id.orderreview_username);
        txt_order_address = (TextView) findViewById(R.id.orderreview_useraddress);
        txt_order_location = (TextView) findViewById(R.id.orderreview_userlocation);
        txt_order_phone = (TextView) findViewById(R.id.orderreview_userphone);
        //GETTING DFORM ADDRESS

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        ORDERREVIEW_NAME = myprefs.getString("ORDERREVIEW_NAME", "");
        ORDERREVIEW_ADDRESS = myprefs.getString("ORDERREVIEW_ADDRESS", "");
        ORDERREVIEW_PHONE = myprefs.getString("ORDERREVIEW_PHONE", "");
        ORDERREVIEW_LOCATION = myprefs.getString("ORDERREVIEW_LOCATION", "");
        txt_order_name.setText(ORDERREVIEW_NAME);
        txt_order_address.setText(ORDERREVIEW_ADDRESS);
        txt_order_location.setText(ORDERREVIEW_LOCATION);
        txt_order_phone.setText(ORDERREVIEW_PHONE);


        lnr_step_image = (ImageView) findViewById(R.id.step_four);

        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);

        //badge
        cartcountbadge = (TextView) findViewById(R.id.badge);
        //notif badge full
        sp = new DS_SP(getApplicationContext());
        notifbadge = (TextView) findViewById(R.id.badgenotif);
        if (sp.getnotcount() != 0) {
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount() + "");
        }
//getting paymenth method id using intent
        Intent in = getIntent();
        STR_PAYMENT_METHOD_ID = in.getIntExtra("PAYMENT_METHOD_TYPE_ID", 1);
        STR_PAYMENT_METHOD_NAME = in.getStringExtra("PAYMENT_METHOD_TYPE_NAME");
        STR_QUICK_ARRIVED = in.getStringExtra("TAG_QUICK_CHECKOUT_INTENT");
        txt_payment_method = (TextView) findViewById(R.id.txt_payment_type_ordeer_review);
        txt_payment_method.setText(STR_PAYMENT_METHOD_NAME);
        //used to avoid intent null point
        if (STR_QUICK_ARRIVED != null) {
            STR_QUICK_ARRIVED = in.getStringExtra("TAG_QUICK_CHECKOUT_INTENT");
        } else {
            STR_QUICK_ARRIVED = "0";
        }
        //getting which type checkout if quick

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }


        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            // new AsyncTaskCartcounttJson().execute();
            new AsyncTaskCurrencyRate().execute();
            new AsyncTaskDeliveryAmount().execute();
            new AsyncOrderReview().execute();

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        deliveryDate = (TextView) findViewById(R.id.date);
        deliveryTme = (TextView) findViewById(R.id.time);


        orderProductList = (ListView) findViewById(R.id.listorder);
        title = (TextView) findViewById(R.id.namein_title);
        title.setText(getString(R.string.order_review));
        back_btn = (Button) findViewById(R.id.menue_btn);
        profile = (ImageView) findViewById(R.id.profile);


        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(!STR_QUICK_ARRIVED.trim().equals("back_finish")){
                    Intent in = new Intent(OrderReview.this, CheckOut.class);
                    OrderReview.this.finish();
                    startActivity(in);

                }else if(STR_QUICK_ARRIVED.trim().equals("back_finish")){

                    onBackPressed();

                }*/
                //new code
                onBackPressed();
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderReview.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderReview.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderReview.this, CartView.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(OrderReview.this, Notification.class);

                startActivity(in);
            }
        });


//        orderProductList.setOnTouchListener(new View.OnTouchListener() {
//            // Setting on Touch Listener for handling the touch inside ScrollView
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // Disallow the touch request for parent scroll on touch of child view
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

        // used to set new sp based count
        setcartcountwihoutapi();
        setnewProgressbarClicks();
    }

    private void setnewProgressbarClicks() {
        ImageView ivstpcart, ivstpdelivery, ivstpPayment, ivstpConfirm;
        ivstpcart = (ImageView) findViewById(R.id.ivStpCart);
        ivstpdelivery = (ImageView) findViewById(R.id.ivStpDelivery);
        ivstpPayment = (ImageView) findViewById(R.id.ivStpPayment);
        ivstpConfirm = (ImageView) findViewById(R.id.ivStpConfirm);

        ivstpcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderReview.this, CartView.class);
                startActivity(in);
            }
        });
        ivstpdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderReview.this, CheckOut.class);
                startActivity(in);
            }
        });
        ivstpPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        ivstpConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    public void showProgressAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderReview.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    void setcartcountwihoutapi() {
        if (sp.getcartcount() == 0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if (userVLogIn.trim().equals("0")) {
                cartcountbadge.setVisibility(View.GONE);
            } else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount() + "");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //old codde
        /*if(!STR_QUICK_ARRIVED.trim().equals("back_finish")){
            Intent in = new Intent(OrderReview.this, CheckOut.class);
            OrderReview.this.finish();
            startActivity(in);

        }else if(STR_QUICK_ARRIVED.trim().equals("back_finish")){

            OrderReview.this.finish();

        }*/
    }


    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ActionBar.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int dens = dm.densityDpi;
        double wi = (double) width / (double) dens;
        double hi = (double) height / (double) dens;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);
        Log.i("screen inch", screenInches + "height" + height);

        if (screenInches > 4.7) {
            //new code by me
            int tconsize = height / 10;
            int ttenper = tconsize / 3;
            int atsize = tconsize - ttenper;

            Log.i("screen consize", tconsize + "height" + height + "persize" + ttenper + "finalsize" + atsize);
            params.height = atsize * listAdapter.getCount();
        } else {
            //new code by me
            int tconsize = height / 10;
            int ttenper = tconsize / 3;
            int atsize = tconsize - ttenper;

            Log.i("screen consize", tconsize + "height" + height + "persize" + ttenper + "finalsize" + atsize);
            params.height = atsize * listAdapter.getCount();

        }


        // }


        listView.setLayoutParams(params);

        listView.requestLayout();

    }

    public static void setListViewHeightBasedOnChildrenforcommon(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ActionBar.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            if (listView.getCount() > 6) {
                totalHeight += view.getMeasuredHeight() / 2.6;
            } else {
                totalHeight += view.getMeasuredHeight() / 2;
            }


        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));


        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public void setMyAlert() {

        count = count + 1;
        if (count == 1) {
            AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.out_of_stock_removed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.dismiss();
                        }
                    });

            alertDialog.show();
            count++;
        }

    }

    //method for payment gate way
    private void callEbsKit(OrderReview buyProduct) {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */

        /** Payment Amount Details */
        // Total Amount

        String str_amnt_tot=total+"0";
        //String str_amnt_tot="0.5";


       // String str_amnt_tot = "1";

//        String str_amnt_tot="1";
        float amount = Float.parseFloat(str_amnt_tot);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        str_amnt_tot = df.format(amount);

        float flt_str = Float.parseFloat(str_amnt_tot);

        PaymentRequest.getInstance().setTransactionAmount(
                String.format("%.2f", flt_str));

        /** Mandatory */
        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        // Reference No
        PaymentRequest.getInstance().setReferenceNo(AFTER_OREDER_ID);
        /** Mandatory */

        // Email Id
        PaymentRequest.getInstance().setBillingEmail(AFTER_ORDER_EMAIL);
        /** Mandatory */

        /**
         * Set failure id as 1 to display amount and reference number on failed
         * transaction page. set 0 to disable
         */
        PaymentRequest.getInstance().setFailureid("1");
        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                "Daily Souq Android App Purchase");

        /** Billing Details */
        PaymentRequest.getInstance().setBillingName(AFTER_OREDER_NAME);
        /** Optional */
        PaymentRequest.getInstance().setBillingAddress(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setBillingCity("Tirur");
        /** Optional */

        PaymentRequest.getInstance().setBillingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setBillingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setBillingCountry("IND");
        // ** Optional */
        PaymentRequest.getInstance().setBillingPhone("1 800 123 2565");
        /** Optional */
        /** set custom message for failed transaction */

        PaymentRequest.getInstance().setFailuremessage(
                getResources().getString(R.string.payment_failure_message));
        /** Optional */
        /** Shipping Details */
        PaymentRequest.getInstance().setShippingName(AFTER_OREDER_NAME);
        /** Optional */
        PaymentRequest.getInstance().setShippingAddress(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setShippingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setShippingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setShippingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setShippingCountry("IND");
        /** Optional */
        PaymentRequest.getInstance().setShippingEmail(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setShippingPhone("01234567890");
        /** Optional */
        /* enable log by setting 1 and disable by setting 0 */
        PaymentRequest.getInstance().setLogEnabled("1");


        /**
         * Payment option configuration
         */

        /** Optional */
        PaymentRequest.getInstance().setHidePaymentOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideCashCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideCreditCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideDebitCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideNetBankingOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideStoredCardOption(false);
        /**
         * Initialise parameters for dyanmic values sending from merchant custom
         * values from merchant
         */

        custom_post_parameters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashpostvalues = new HashMap<String, String>();
        hashpostvalues.put("account_details", "saving");
        hashpostvalues.put("merchant_type", "gold");
        custom_post_parameters.add(hashpostvalues);

        PaymentRequest.getInstance()
                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(buyProduct, ACC_ID, SECRET_KEY,
                Config.Mode.ENV_LIVE, Config.Encryption.ALGORITHM_MD5, HOST_NAME);

        // EBSPayment.getInstance().init(context, accId, secretkey, environment,
        // algorithm, host_name);

    }

    //used to get failure status
    @Override
    public void onResume() {
        super.onResume();

        if (null != mprogressDialog) {
            if (mprogressDialog.isShowing()) {
                mprogressDialog.dismiss();
            }
        }
        if (null != mprogressDialog2) {
            if (mprogressDialog2.isShowing()) {
                mprogressDialog2.dismiss();
            }
        }
        if (null != mProgressDialog3) {
            if (mProgressDialog3.isShowing()) {
                mProgressDialog3.dismiss();
            }
        }

 /*   Intent intent =new Intent(OrderReview.this,PaymentSuccessActivity.class);
    startActivity(intent);*/

        Log.i("PaymentResponseeee", PaymentRequest.getInstance()
                .getPaymentResponse());

        Log.i("PaymentIDDD", PaymentRequest.getInstance().getPaymentId());

        new AsyncOrderTransactioidUpdateJsson().execute();

        Log.i("PaymentResponse", PaymentRequest.getInstance()
                .getPaymentResponse());
        /*
         * To get Payment id alone use
         * "PaymentRequest.getInstance().getPaymentId()"
         * To get Entire response use
         * "PaymentRequest.getInstance().getPaymentResponse()"
         */

        if (!PaymentRequest.getInstance().getPaymentId().equals("")) {
            Toast.makeText(
                    getApplicationContext(),
                    "Your Payment Id :"
                            + PaymentRequest.getInstance().getPaymentId(),
                    Toast.LENGTH_SHORT).show();

            Log.i("Payment Response", PaymentRequest.getInstance()
                    .getPaymentResponse());
            String array = PaymentRequest.getInstance()
                    .getPaymentResponse();
            //test for payment

            String[] kvPairs = array.split(",");
            for (String kvPair : kvPairs) {
                String[] kv = kvPair.split(":");
                String key = kv[0];
                String value = kv[1];
                String t = "ResponseCod";

                key = key.substring(1, 12);
                // Now do with key whatever you want with key and value...
                if (key.equals("" + t + "")) {
                    Log.i("val", value + "ok");
                   // new AsyncOrderTransactioidUpdateJsson().execute();
                    break;
                    // Do something with value if the key is "specialvalue"...
                }
            }


            Log.i("failed", PaymentRequest.getInstance().getFailuremessage());
            Log.i("failedid", PaymentRequest.getInstance().getFailureid());

        }

    }

    public class AsyncOrderReview extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int jsonlen = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mprogressDialog = new ProgressDialog(OrderReview.this);
//            // Set progressdialog title
            mprogressDialog.setTitle("");
            mprogressDialog.setCancelable(true);
            mprogressDialog.setCanceledOnTouchOutside(true);
            mprogressDialog.setMessage(getString(R.string.loading));
            mprogressDialog.setIndeterminate(false);
//            // Show progressdialog
            mprogressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Json jParser = new Json();
                JSONObject jobj = jParser.orderReview(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                deliverDate = dataJsonArr.getJSONObject(0).optString("DeliveyDate");
                deliveryTime = dataJsonArr.getJSONObject(0).optString("DeliverySlot");
                JSONArray detail = dataJsonArr.getJSONObject(0).getJSONArray("Details");
                detailsLength = detail.length();
                totalNoItems = detail.getJSONObject(0).optInt("TotalItems");
                subTotal = detail.getJSONObject(0).optString("OrderAmount");
                discount = detail.getJSONObject(0).optString("Discount");
                deliveryCharges = detail.getJSONObject(0).optInt("Deliverycharge");
                total = detail.getJSONObject(0).optString("Nettotal");
                amount = new String[detailsLength];
                productName = new String[detailsLength];
                quantity = new String[detailsLength];
                unitprice = new String[detailsLength];
                stockstatus = new String[detailsLength];

                for (int i = 0; i < detailsLength; i++) {
                    amount[i] = detail.getJSONObject(i).optString("Subtotal");
                    productName[i] = detail.getJSONObject(i).optString("ProductShowName");
                    quantity[i] = detail.getJSONObject(i).optString("Quantity");
                    unitprice[i] = detail.getJSONObject(i).optString("unitprice");
                    stockstatus[i] = detail.getJSONObject(i).optString("Stockstatus");

                    //  Toast.makeText(OrderReview.this,"status: "+stockstatus[i],Toast.LENGTH_LONG).show();
                }
                jsonlen = dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mprogressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                CustomOrderReview adapter = new CustomOrderReview(OrderReview.this, productName, unitprice, quantity, amount, detailsLength, stockstatus, STR_PAYMENT_METHOD_ID, USD_RATE);
                orderProductList.setAdapter(adapter);
                if (Build.VERSION.SDK_INT >= 21) {
                    // Do some stuff  for s5 only
                    setListViewHeightBasedOnChildren(orderProductList);
                } else {
                    setListViewHeightBasedOnChildrenforcommon(orderProductList);
                }


                deliveryDate.setText(deliverDate);
                deliveryTme.setText(deliveryTime);
                // View footerView = ((LayoutInflater) OrderReview.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.order_review_footer, null, false);
                // orderProductList.addFooterView(footerView);
                totalItems = (TextView) findViewById(R.id.items);
                subTot = (TextView) findViewById(R.id.subTotal);
                discoun = (TextView) findViewById(R.id.discount);
                deliveryCharge = (TextView) findViewById(R.id.deliveryCharge);
                tot = (TextView) findViewById(R.id.total);
                lnr_delivery_layout = (LinearLayout) findViewById(R.id.lnr_del_order_review_footer);
                lnr_delivery_layout2 = (LinearLayout) findViewById(R.id.lnr_deltext_order_review_footer);
                txt_avil_free_amnt = (TextView) findViewById(R.id.txt_shop_amnt_delivery);
                txt_avil_free_amnt.setText(getString(R.string.alert_delivery_free1) + str_del_amnt + ".00 " + getString(R.string.alert_delivery_free2));

                String tItems = Integer.toString(totalNoItems);
                totalItems.setText(tItems);
                if (STR_PAYMENT_METHOD_ID == 5) {
                    subTot.setText("$" + String.format("%.2f", (Float.parseFloat(subTotal) / USD_RATE)));
                    discoun.setText("$" + String.format("%.2f", (Float.parseFloat(discount) / USD_RATE)));
                    int delvalue = Integer.valueOf(deliveryCharges);
                    if (delvalue == 0) {
                        lnr_delivery_layout.setVisibility(View.GONE);
                        lnr_delivery_layout2.setVisibility(View.GONE);
                    }
                    deliveryCharge.setText("$" + String.format("%.2f", ((deliveryCharges) / USD_RATE)));
                    tot.setText("$" + String.format("%.2f", (Float.parseFloat(total) / USD_RATE)));
                    INR_TO_USD = String.format("%.2f", (Float.parseFloat(total) / USD_RATE));
                } else {
                    subTot.setText(subTotal + "0");
                    discoun.setText(discount + "0");
                    int delvalue = Integer.valueOf(deliveryCharges);
                    if (delvalue == 0) {
                        lnr_delivery_layout.setVisibility(View.GONE);
                        lnr_delivery_layout2.setVisibility(View.GONE);
                    }
                    deliveryCharge.setText(deliveryCharges + ".00");
                    tot.setText(total + "0");
                }


                //used for footer events in 4 button like place order edit order ..
                Button trnsferOrder, editOrder, placeOrder, continueShop;


                //  trnsferOrder=(Button)findViewById(R.id.transferOrder);
                editOrder = (Button) findViewById(R.id.editOrder);
                placeOrder = (Button) findViewById(R.id.placeorder);
                //  continueShop= (Button) findViewById(R.id.continueshopping);


                editOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent in = new Intent(OrderReview.this, CartView.class);

                        startActivity(in);
                    }
                });
                placeOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.confirm_place_order));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        if (STR_PAYMENT_METHOD_ID == 1) {
                                            //cash on delivery
                                            new AsyncOrderSaveJsson().execute();
                                        } else if (STR_PAYMENT_METHOD_ID == 2) {
                                            //netbank
                                            new AsyncOrderSaveJsson().execute();
                                            //Toast.makeText(OrderReview.this,"Need TO Add this",Toast.LENGTH_LONG).show();


                                        } else if (STR_PAYMENT_METHOD_ID == 3) {
                                            //ewallet
                                            str_amtpaid = String.valueOf(total);
                                            str_description = "purchase";
                                            accounttype = getString(R.string.credit);
                                            usermasterid = "0";


                                            new AsyncTaskEwalletSaveJson().execute();

                                        } else if (STR_PAYMENT_METHOD_ID == 5) {
                                            //paypal
                                           /* str_amtpaid=String.valueOf(total);
                                            str_description="purchase";
                                            accounttype=getString(R.string.credit);
                                            usermasterid="0";*/

                                            new AsyncOrderSaveJsson().execute();

                                            /*Intent intent=new Intent(OrderReview.this,Paypal.class);
                                            startActivity(intent);*/

                                        } else {
                                            AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                                            alertDialog.setCancelable(false);
                                            alertDialog.setCanceledOnTouchOutside(false);
                                            alertDialog.setMessage(getString(R.string.error));
                                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {

                                                            dialog.dismiss();
                                                        }
                                                    });
                                            alertDialog.show();

                                        }


                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();


                    }
                });
//                continueShop.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        clearspdet();
//
//                        Intent in=new Intent(OrderReview.this,Categories.class);
//                        OrderReview.this.finish();
//                        startActivity(in);
//                    }
//                });


            }
        }


    }

    public class AsyncOrderSaveJsson extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        String transactionid = "0";
        int jsonlen = 0;
        String result = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mprogressDialog2 = new ProgressDialog(OrderReview.this);
//            // Set progressdialog title
            mprogressDialog2.setTitle("");
            mprogressDialog2.setCancelable(true);
            mprogressDialog2.setCanceledOnTouchOutside(true);
            mprogressDialog2.setMessage(getString(R.string.loading));
            mprogressDialog2.setIndeterminate(false);
//            // Show progressdialog
            mprogressDialog2.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Json jParser = new Json();
                JSONObject jobj = jParser.ordersave(userVLogIn, transactionid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                Log.i("resultsave ", dataJsonArr.toString() + "");
                result = dataJsonArr.getJSONObject(0).optString("result");
                AFTER_OREDER_ID = dataJsonArr.getJSONObject(0).optString("OrderId");
                AFTER_OREDER_NAME = dataJsonArr.getJSONObject(0).optString("FirstName");
                AFTER_ORDER_EMAIL = dataJsonArr.getJSONObject(0).optString("Email");

                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mprogressDialog2.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {

                // {"d":[{"result":"failed"}]}
                if (dataJsonArr.length() == 1) {

                    //ceck fail or not

                    if (result.equalsIgnoreCase("failed")) {

                    } else {
                        //check this net bank
                        if (!AFTER_OREDER_ID.equalsIgnoreCase("0")) {
                            if (STR_PAYMENT_METHOD_ID == 2) {
                                //netbank
                                SharedPreferences.Editor editor = myprefs.edit();
                                editor.putInt("PAYMENT_GATEWAY", 2);
                                editor.apply();
                                //Toast.makeText(OrderReview.this,"Need TO Add this",Toast.LENGTH_LONG).show();
                                callEbsKit(OrderReview.this);
                            } else if (STR_PAYMENT_METHOD_ID == 5) {
                                SharedPreferences.Editor editor = myprefs.edit();
                                editor.putInt("PAYMENT_GATEWAY", 5);
                                editor.apply();
                                /*//removing cart count
                                sp.setcartcount(0);
                                clearspdet();*/

                                getPayment();

                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                                alertDialog.setCancelable(false);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.setMessage(getString(R.string.order_placed));
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            public void onClick(DialogInterface dialog, int which) {
                                                //used to set step image
                                                lnr_step_image.setBackground(getResources().getDrawable(R.drawable.step_five));
                                                //removing dflt addtress clar
                                                sp.setcartcount(0);
                                                clearspdet();
                                                dialog.dismiss();
                                                Intent in = new Intent(OrderReview.this, MainActivity.class);


                                                startActivity(in);


                                            }
                                        });
                                alertDialog.show();

                            }
                        }
                    }
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    sp.setcartcount(0);
                                    Intent in = new Intent(OrderReview.this, MainActivity.class);

                                    startActivity(in);

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }


            }
        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen = 0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if (userVLogIn.trim().equals("0")) {
                        cartcountbadge.setVisibility(View.GONE);
                    } else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }
            // Log.i("arrivedcount", str_json_cart_count);

            //  mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskDeliveryAmount extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen = 0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.bal_amt_to_avil_freedelivery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_del_amnt = dataJsonArr.getJSONObject(0).optInt("result");
                jsonlen = dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }

        }
    }

    public class AsyncTaskCurrencyRate extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";


        @Override
        protected String doInBackground(String... arg0) {
            try {

                //String url="https://openexchangerates.org/api/latest.json?app_id=2227622a54074dd4bcb6bcbbdac5ec8b";

                Json jParser = new Json();
                JSONObject json = jParser.getCurrencyRate();
                JSONObject productObj = new JSONObject(Json.prdetail);
                Log.i("productObj", productObj + "");
                JSONArray jsonArray = productObj.getJSONArray("d");
                USD_RATE = Float.parseFloat(jsonArray.getJSONObject(0).optString("Amount"));
                /*JSONObject jsonObject=productObj.getJSONObject("rates");
                String s=jsonObject.optString("INR");
                Log.i("INR",s+"");*/
                //INR_TO_USD=(Float.parseFloat(total)/USD_RATE);
                //Log.i("INR_TO_USD",INR_TO_USD+"");


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }

    public class AsyncTaskEwalletSaveJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        String ewallettransferresponce;
        int jsonlen = 0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.ewalletfoundsave(usermasterid, accounttype, str_amtpaid, userVLogIn, str_description, "0");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                ewallettransferresponce = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // mProgressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent in = new Intent(OrderReview.this, MainActivity.class);

                                startActivity(in);

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            } else {


                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(ewallettransferresponce);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                if (ewallettransferresponce.trim().equals("success")) {
                                    new AsyncOrderSaveJsson().execute();
                                } else if (ewallettransferresponce.trim().equals("No more balance to credit")) {

                                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                                    alertDialog.setCancelable(false);
                                    alertDialog.setCanceledOnTouchOutside(false);
                                    alertDialog.setMessage(getString(R.string.nomore_balance_to_credit));
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    onBackPressed();

                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.show();


                                } else {
                                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                                    alertDialog.setCancelable(false);
                                    alertDialog.setCanceledOnTouchOutside(false);
                                    alertDialog.setMessage(getString(R.string.error));
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                    Intent in = new Intent(OrderReview.this, MainActivity.class);

                                                    startActivity(in);

                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.show();
                                }

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                // Log.i("arrivedcount", str_json_cart_count);
                //  new AsyncTaskLinkeduserJson().execute();
            }
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen = 0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout = dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment = dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout = dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout = dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout = dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen = dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            } else {


                if (sp.getcartcount() == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                } else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else if (str_opt_payment.equals(getString(R.string.paypal))) {
                        str_paymentmethod_payment = 5;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(OrderReview.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME", str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT", "back_finish");
                    startActivity(in);

                }
            }

        }
    }

    //need to move og cod e
    public class AsyncOrderTransactioidUpdateJsson extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        String transactionid = "0";
        int jsonlen = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog3 = new ProgressDialog(OrderReview.this);
//            // Set progressdialog title
            mProgressDialog3.setTitle("");

            mProgressDialog3.setMessage(getString(R.string.loading));
            mProgressDialog3.setIndeterminate(false);
            mProgressDialog3.setCancelable(false);
            mProgressDialog3.setCanceledOnTouchOutside(false);
//            // Show progressdialog
            mProgressDialog3.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                Log.i("AFTER_OREDER_ID", AFTER_OREDER_ID + "->");
                JSONObject jobj = jParser.updatetransactionid(userVLogIn, AFTER_OREDER_ID, "0", "Failed");
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                jsonlen = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            mProgressDialog3.dismiss();
            if (jsonlen == 0) {
//                AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
//                alertDialog.setTitle(getString(R.string.error));
//                alertDialog.show();
            } else {
                if (dataJsonArr.length() == 1) {

                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    alertDialog.setMessage(getString(R.string.payment_failure));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(OrderReview.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }


        }
    }

    void clearspdet() {
        //remove the address from check out
        SharedPreferences.Editor editor = myprefs.edit();
        editor.remove("ORDERREVIEW_NAME");
        editor.remove("ORDERREVIEW_ADDRESS");
        editor.remove("ORDERREVIEW_LOCATION");
        editor.remove("ORDERREVIEW_PHONE");

        editor.apply();
    }

    private void getPayment() {

        //Getting the amount from editText
        //paymentAmount = editTextAmount.getText().toString();

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(INR_TO_USD)), "USD", "Daily Souq",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }


    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {

                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.e("Show", confirm.getPayment().toJSONObject().toString(4));

                        Log.i("paymentExample", paymentDetails);


                        //Starting a new activity for the payment details and also putting the payment details with intent
                        startActivity(new Intent(this, PaypalConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", INR_TO_USD + "")
                                .putExtra("userVLogIn", userVLogIn)
                                .putExtra("AFTER_OREDER_ID", AFTER_OREDER_ID));


                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }


        }
    }


}
