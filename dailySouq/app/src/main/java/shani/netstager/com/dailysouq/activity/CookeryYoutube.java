package shani.netstager.com.dailysouq.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cookery_youtube_adapter;

public class CookeryYoutube extends AppCompatActivity {
    String []CookeryId, CookeryTitle, CookeryVideoUrl, CookeryDetails, CookeryImage, CookeryList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cookery_youtube);
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        //to use RecycleView, you need a layout manager. default is LinearLayoutManager
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        cookery_youtube_adapter adapter=new cookery_youtube_adapter(CookeryYoutube.this,CookeryId, CookeryTitle, CookeryVideoUrl, CookeryDetails, CookeryImage, CookeryList);
        recyclerView.setAdapter(adapter);
    }
}
