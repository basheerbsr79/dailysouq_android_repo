package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.AddDeliveryAddress;

/**
 * Created by prajeeshkk on 19/10/15.
 */
public class CustomChooseAddress extends BaseAdapter {
    ViewHolder holder;
    int product_length;
    String[]customer_name;
    String[]location;
    String[]contact_number;
    String[]address,deliveryAddressId;
    boolean []deliveryaddress;
    Context context;
    int test;
   public static String addressId;
    LayoutInflater inf;
    SharedPreferences myprefs;
    Integer selected_position = 0;


    // a field in the adapter
    private int mSelectedPosition = -1;

    public CustomChooseAddress(Context con, int plength, String[] cust_name, String[] addr, String[] locate, String[] phone, String[] addressid, boolean[] deliveryAddress)
    {
        product_length=plength;
        customer_name=cust_name;
        location=locate;
        contact_number=phone;
        address=addr;
        deliveryaddress=deliveryAddress;
        deliveryAddressId=addressid;
        context=con;
        myprefs = con.getSharedPreferences("MYPREFS",Context.MODE_PRIVATE);
        inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    // getter and setter methods for the field above
    public void setSelectedPosition(int selectedPosition) {
        mSelectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return mSelectedPosition;
    }


    @Override
    public int getCount() {
        return  product_length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {

        return product_length;
    }

    class ViewHolder
    {

        TextView cust_Name;
        TextView address;
        TextView location;
        TextView phone;
        Button add;

        TextView addDefault;
        TextView select;
        LinearLayout add_new_address;


    }

    @Override
    public View getView(final int position, View arg1, ViewGroup viewGroup) {
        //int rollnocount = arg0;
        View vi = arg1;

        if (arg1 == null) {




                vi = inf.inflate(R.layout.custom_chhose_address, null);
                holder = new ViewHolder();


                holder.cust_Name = (TextView) vi.findViewById(R.id.customer_name);
                holder.address = (TextView) vi.findViewById(R.id.address);
                holder.location = (TextView) vi.findViewById(R.id.location);
                holder.phone = (TextView) vi.findViewById(R.id.phone);
                holder.add = (Button) vi.findViewById(R.id.add);
                holder.addDefault=(TextView)vi.findViewById(R.id.adddefualt);
                holder.select=(TextView)vi.findViewById(R.id.adddefualt2);




            vi.setTag(holder);

        } else {
            holder = (ViewHolder) vi.getTag();

        }




        if (mSelectedPosition == position) {
            holder.addDefault.setVisibility(View.VISIBLE);
            holder.select.setVisibility(View.GONE);
        } else {

            holder.addDefault.setVisibility(View.GONE);
            holder.select.setVisibility(View.VISIBLE);
        }






//        holder.addDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked)
//                {
//                    selected_position =  position;
//                    //settings addres to set in order review
//
//                    SharedPreferences.Editor editor=myprefs.edit();
//                    editor.putString("ORDERREVIEW_NAME", customer_name[selected_position]);
//                    editor.putString("ORDERREVIEW_ADDRESS", address[selected_position]);
//                    editor.putString("ORDERREVIEW_LOCATION", location[selected_position]);
//                    editor.putString("ORDERREVIEW_PHONE",contact_number[selected_position]);
//
//                    editor.apply();
//                    addressId=deliveryAddressId[selected_position];
//                }
//                else{
//                    selected_position = -1;
//                }
//                notifyDataSetChanged();
//            }
//        });





            if(position==0)
            {

               // holder.addDefault.setChecked(true);
                holder.add.setVisibility(View.VISIBLE);
            }
            else
            {
              //  holder.addDefault.setChecked(false);
                holder.add.setVisibility(View.GONE);

            }
            holder.cust_Name.setText(customer_name[position]);
            holder.address.setText(address[position]);
            holder.location.setText(location[position]);
            holder.phone.setText(contact_number[position]);
//            if (position == 0) {
//
//
//                if (holder.addDefault.isChecked() == false) {
//                    holder.addDefault.setChecked(true);
//                }
//            }
            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ins = new Intent(context, AddDeliveryAddress.class);

                    test = 1;
                    ins.putExtra("arrivedfrom", test);
                    context.startActivity(ins);
                }
            });

//            holder.addNew.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent ins = new Intent(context, AddDeliveryAddress.class);
//
//                    test = 1;
//                    ins.putExtra("arrivedfrom", test);
//                    context.startActivity(ins);
//                }
//            });

//            holder.addDefault.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    notifyDataSetChanged();
//                    addressId = deliveryAddressId[selected_position];
//
//                }
//            });






        return vi;

    }
}
