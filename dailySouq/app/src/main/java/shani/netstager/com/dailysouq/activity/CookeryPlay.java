package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class CookeryPlay extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

        TextView title,cookerytitle,cookerydetails;
        Button back_btn;
        ImageView profile,quickcheckout,Notification;
        String CookeryIds="",CookeryTitles="",CookeryVideoUrls="",CookeryDetailss="";
        String userVLogIn;
        RelativeLayout cartcounttext;
        SharedPreferences myprefs;
        ProgressDialog mProgressDialog;
        JSONArray dataJsonArr;
        String str_json_cart_count,str_add_responce;

        public static final String API_KEY = "AIzaSyBCkZGHdBZbTe3cyn-oEKpRpfHquUTnsiU";
        // its used for quick check out
        String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
        int str_paymentmethod_payment;
        TextView cartcountbadge,notifbadge;
        DS_SP sp;
        Button addingrmnts;
        int languge=1;
        String responce_json="";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            /** attaching layout xml **/
            setContentView(R.layout.activity_cookery_play);
            profile=(ImageView)findViewById(R.id.profile);

            quickcheckout=(ImageView)findViewById(R.id.apload);
            Notification=(ImageView) findViewById(R.id.notification);
            back_btn=(Button)findViewById(R.id.menue_btn);
            title=(TextView)findViewById(R.id.namein_title);
            cookerytitle=(TextView)findViewById(R.id.cookerytitle_cookery);
            cookerydetails=(TextView)findViewById(R.id.descriptionid_cookery);
            addingrmnts=(Button)findViewById(R.id.add_cook_ingr);
            //notif badge full
            sp=new DS_SP(getApplicationContext());
            notifbadge=(TextView)findViewById(R.id.badgenotif);
            if(sp.getnotcount()!=0){
                notifbadge.setVisibility(View.VISIBLE);
                notifbadge.setText(sp.getnotcount()+"");
            }

            cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
            //badge
           cartcountbadge=(TextView)findViewById(R.id.badge);

            //badge
            myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
            languge=myprefs.getInt("DEFAULT_LANGUGE",1);
            userVLogIn = myprefs.getString("shaniusrid", null);
            if(userVLogIn!=null){
                userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

            }
            else{
                userVLogIn="0";

            }


            Intent in =getIntent();
            CookeryIds=in.getStringExtra("cookery id");
            CookeryVideoUrls=in.getStringExtra("cookery url");

            CookeryDetailss=in.getStringExtra("cookery details");
            CookeryTitles=in.getStringExtra("cookery name");

            title.setText(CookeryTitles+"");
            cookerytitle.setText(getString(R.string.dish_name)+CookeryTitles);
            cookerydetails.setText(getString(R.string.dish_desc)+CookeryDetailss);

            /** Initializing YouTube player view **/
            YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
            youTubePlayerView.initialize(API_KEY, this);






            back_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     onBackPressed();
                }
            });

            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in=new Intent(CookeryPlay.this,EditProfile.class);
                    startActivity(in);
                }
            });
            cartcounttext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in=new Intent(CookeryPlay.this,CartView.class);
                    startActivity(in);
                }
            });
            quickcheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //internet checking
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent.equals(true)) {


                          new AsyncQuickCheckOut().execute();

                    } else {

                        AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_net_failed));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        onBackPressed();

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }


                }
            });
            Notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent in = new Intent(CookeryPlay.this, Notification.class);

                    startActivity(in);
                }
            });


            addingrmnts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  new AsyncTaskAddIngrJson().execute();
                    if(userVLogIn.equalsIgnoreCase("0")){
                        AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_pls_signin));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in = new Intent(CookeryPlay.this, Signin.class);

                                        CookeryPlay.this.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }else {


                        new AsyncTaskTemptJson().execute();
                    }


                }
            });


            // used to set new sp based count
            setcartcountwihoutapi();
        }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
            Toast.makeText(this, getString(R.string.failed_intilise), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
            /** add listeners to YouTubePlayer instance **/
            player.setPlayerStateChangeListener(playerStateChangeListener);
            player.setPlaybackEventListener(playbackEventListener);

            /** Start buffering **/
            if (!wasRestored) {
                player.cueVideo(CookeryVideoUrls);
            }
        }

        private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

            @Override
            public void onBuffering(boolean arg0) {
            }

            @Override
            public void onPaused() {
            }

            @Override
            public void onPlaying() {
            }

            @Override
            public void onSeekTo(int arg0) {
            }

            @Override
            public void onStopped() {
            }

        };


        private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {


            private YouTubePlayer.ErrorReason arg0;

            @Override
            public void onAdStarted() {
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason arg0) {
                this.arg0 = arg0;
            }

            @Override
            public void onLoaded(String arg0) {
            }

            @Override
            public void onLoading() {
            }

            @Override
            public void onVideoEnded() {
            }



            @Override
            public void onVideoStarted() {
            }
        };


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int countcart=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                if (userVLogIn.equalsIgnoreCase("0")) {

                } else{
                    countcart = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                    sp.setcartcount(countcart);
                   // setcartcountwihoutapi();
                }



                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


    }
    public class AsyncTaskAddIngrJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(CookeryPlay.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.addcookeryingrmentstocart(userVLogIn,CookeryIds);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_add_responce = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                    if(str_add_responce.equalsIgnoreCase("success")){

                        AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_added_cart));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //load to cague cart count
                                        new AsyncTaskCartcounttJson().execute();
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }

            }
        }
    }

    public class AsyncTaskTemptJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cookerytempsave(userVLogIn,CookeryIds,"0");
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");
                //  String noofperson = dataJsonArr.getJSONObject(0).optString("Qty");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                alertDialog.setTitle(CookeryPlay.this.getString(R.string.error));
                alertDialog.show();
            }else {
                Intent in = new Intent(CookeryPlay.this, CookeryPopupItems.class);
                in.putExtra("PID", CookeryIds);
                in.putExtra("USERID", userVLogIn);
                in.putExtra("LANID", languge);
                in.putExtra("NAME", CookeryTitles);
                finish();
                startActivity(in);

               // new  AsyncTaskAddCartJson().execute();
            }

            // mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(CookeryPlay.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(CookeryPlay.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
    }