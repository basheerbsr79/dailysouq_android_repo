package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class ChangePassword extends AppCompatActivity {

    TextView title;
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    Button bck_btn,submit,cancel;
    ProgressDialog mProgressDialog;
    String option,qustionstr;
    JSONArray dataJsonArr;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    SharedPreferences myprefs;
    String str_curnt_psd,str_new_psd;
    EditText currntpsd,newpsd,cnfrmpsd;
    String userVLogIn,str_json_cart_count,logintype;
    RelativeLayout fullview;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
//notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        fullview=(RelativeLayout)findViewById(R.id.changu_psd_full) ;
        fullview.setVisibility(View.GONE);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        logintype = myprefs.getString("logintype", null);

        if(logintype!=null){
            logintype=myprefs.getString("logintype",logintype);

        }
        else{
            logintype="NO";

        }


        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        submit=(Button)findViewById(R.id.btn_upd_psd);
        submit.setVisibility(View.GONE);

        try {
            if(logintype.trim().equals("FA")){
                AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.fb_psd_changue_error));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{

            }

            if(userVLogIn!="0"){


                    submit.setVisibility(View.VISIBLE);
                    fullview.setVisibility(View.VISIBLE);



            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in=new Intent(getApplicationContext(),Signin.class);
                                startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();




            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

        title=(TextView)findViewById(R.id.namein_title);

        cancel=(Button)findViewById(R.id.btn_cncl_psd);
        currntpsd=(EditText)findViewById(R.id.curentpas);
        newpsd=(EditText)findViewById(R.id.new_psd);
        cnfrmpsd=(EditText)findViewById(R.id.cnfrm_new_psd);

        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);




        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(ChangePassword.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(ChangePassword.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(ChangePassword.this, Notification.class);

                startActivity(in);
            }
        });



        title.setText(getString(R.string.changepassword));
        bck_btn=(Button)findViewById(R.id.menue_btn);
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (newpsd.getText().toString().equals("") || cnfrmpsd.getText().toString() .equals("") || currntpsd.getText().toString().equals("")) {


                    newpsd.setError(getString(R.string.plz_fill));

                    cnfrmpsd.setError(getString(R.string.plz_fill));

                    currntpsd.setError(getString(R.string.plz_fill));



                } else {


                    if (newpsd.getText().toString().equals(cnfrmpsd.getText().toString())) {
                        str_curnt_psd = currntpsd.getText().toString();
                        str_new_psd = cnfrmpsd.getText().toString();



                            if (newpsd.length() < 6)

                            {

                                newpsd.setError(getString(R.string.mustbe6chrtr));

                                cnfrmpsd.setError(getString(R.string.mustbe6chrtr));
                            }
                            else{



                                //internet checking
                                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                                Boolean isInternetPresent = cd.isConnectingToInternet();
                                if (isInternetPresent.equals(true)) {

                                    new AsyncTaskCarttJson().execute();
                                } else {

                                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                                    alertDialog.setCancelable(false);
                                    alertDialog.setCanceledOnTouchOutside(false);
                                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    onBackPressed();
                                                    dialog.dismiss();
                                                }
                                            });

                                    alertDialog.show();

                                }


                            }



                    } else {


                        newpsd.setError(getString(R.string.psd_mismatch));
                        cnfrmpsd.setError(getString(R.string.psd_mismatch));

                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(ChangePassword.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {

                Json jParser = new Json();
                JSONObject json = jParser.ChangePassword(userVLogIn,str_curnt_psd, str_new_psd);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                qustionstr = dataJsonArr.getJSONObject(0).optString("result");

                prdlength=dataJsonArr.length();




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
            if (prdlength == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                if (qustionstr.equals("update password successfully")) {


                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(qustionstr);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent in = new Intent(ChangePassword.this, MainActivity.class);
                                    startActivity(in);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                } else if (qustionstr.equals("current password does not exist")) {


                    currntpsd.setError(qustionstr);

                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }
            }
        }
    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                prdlength=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (prdlength == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }

            }
        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int prdlength=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                prdlength=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(prdlength==0){
                AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(ChangePassword.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}






