package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import shani.netstager.com.dailysouq.R;

/**
 * Created by Mohamed Shani on 09-02-2016.
 */
public class TermsandConditions extends Activity {

    WebView conditon;

    AlertDialog alertDialog;
    Button btn_dismiss;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.termsandcondition);
        btn_dismiss=(Button)findViewById(R.id.btn_dimiss_webviewin_terms) ;
        progressBar = (ProgressBar) findViewById(R.id.progrss_addmonew_webview);
        conditon=(WebView)findViewById(R.id.condionwebView);

        conditon.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);




        conditon.setWebViewClient(new myWebClient());


        conditon.getSettings().setJavaScriptEnabled(true);


        conditon.loadUrl("http://dailysouq.in/termsandcondition.aspx");

        btn_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        progressBar = ProgressDialog.show(TermsandConditions.this, "WebView Example", "Loading...");
//
//        conditon.setWebViewClient(new WebViewClient() {
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.i("Here it is...", "Processing webview url click...");
//                view.loadUrl(url);
//                return true;
//            }
//
//            public void onPageFinished(WebView view, String url) {
//                Log.i("TAG", "Finished loading URL: " + url);
//                if (progressBar.isShowing()) {
//                    progressBar.dismiss();
//                }
//            }
//
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Log.e("TAG", "Error: " + description);
//                Toast.makeText(TermsandConditions.this, "Hi", Toast.LENGTH_SHORT).show();
//                alertDialog.setTitle("Error");
//                alertDialog.setMessage(description);
//                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        return;
//                    }
//                });
//                alertDialog.show();
//            }
//        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            conditon.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progressBar.setVisibility(View.GONE);
            conditon.setVisibility(View.VISIBLE);
        }
    }
}
