package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.MyWhislist;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.DS_SP;


/**
 * Created by prajeeshkk on 05/11/15.
 */
public class CustomWhislist extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ViewHolder holder;
    private ArrayList<ProductModel> productModels;
    int pos, count=1;
    String qun, custom, pid, result,wishId;
    SharedPreferences myprefs;
    int cartcount;
    TextView cartcountbadge;
    RelativeLayout cartcounttext;
    JSONArray dataJsonArr;
    int str_json_cart_count;
    ProgressDialog mProgressDialog;
    DS_SP sp;
    //android.os.Handler mHandler;

    public CustomWhislist(MyWhislist products, ArrayList<ProductModel> productModels, RelativeLayout cartcounttext, TextView cartcountbadge) {
        this.context = products;
        this.productModels = productModels;

        //badge
        this.cartcounttext=cartcounttext;
        this.cartcountbadge=cartcountbadge;

        sp=new DS_SP(context.getApplicationContext());
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return productModels.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {

        ImageView productImg;
        TextView productName, productPrice;
        Spinner prdSize;
        TextView showValue;
        ImageButton valuePlus, valueMinus, add, remove;

        CustomSpinnerAdapter adapter;
        ProductModel productModel;
        SizeModel sizeModel;
        TextView sizeP;

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;
        if (view == null) {


            vi = inflater.inflate(R.layout.customwishlist, null);

            holder = new ViewHolder();

            holder.productPrice = (TextView) vi.findViewById(R.id.whisMrp);
            holder.productName = (TextView) vi.findViewById(R.id.whisProduct);
            holder.productImg = (ImageView) vi.findViewById(R.id.whisImage);
            holder.prdSize = (Spinner) vi.findViewById(R.id.productSpinner);
            holder.valuePlus = (ImageButton) vi.findViewById(R.id.valuePlus);
            holder.valueMinus = (ImageButton) vi.findViewById(R.id.valueMinus);
            holder.showValue = (TextView) vi.findViewById(R.id.valueText);
            holder.adapter = new CustomSpinnerAdapter(context);
            holder.productModel = productModels.get(position);
            holder.add = (ImageButton) vi.findViewById(R.id.add);
            holder.remove=(ImageButton)vi.findViewById(R.id.remove);
            holder.sizeP=(TextView)vi.findViewById(R.id.textid);

            vi.setTag(holder);
        }
        try {


            final ProductModel pro = productModels.get(position);
            holder.productName.setText(pro.productName);

            Picasso.with(context).load(pro.imagepath).into(holder.productImg);

            holder.adapter.setdata(pro.sizes);
            holder.adapter.notifyDataSetChanged();
            holder.prdSize.setFocusable(true);
            holder.prdSize.setAdapter(holder.adapter);

            final View finalVi = vi;
            holder.prdSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    ViewHolder myViewHolder = (ViewHolder) finalVi.getTag();
                    SizeModel sizeModel = myViewHolder.adapter.getItem(i);
                    myViewHolder.sizeP.setText(String.valueOf(sizeModel.productSizeId));
                    myViewHolder.productPrice.setText(sizeModel.actualprize+"0");
                    Typeface face=Typeface.createFromAsset(context.getAssets(),
                            "fonts/ExoBold.otf");
                    myViewHolder.productPrice.setTypeface(face);

                    pos = i + 1;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            final ViewHolder myViewHolder = (ViewHolder) finalVi.getTag();
            myViewHolder.showValue.setText(Integer.toString(count));
            //stepper
            qun=myViewHolder.showValue.getText().toString();
            myViewHolder.valuePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vplus=1;
                    //count++;
                    vplus= Integer.parseInt(myViewHolder.showValue.getText().toString());

                    myViewHolder.showValue.setText(Integer.toString(vplus+1));
                    qun = myViewHolder.showValue.getText().toString();
                }
            });


            myViewHolder.valueMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vminus=1;
                    vminus= Integer.parseInt(myViewHolder.showValue.getText().toString());
                    myViewHolder.showValue.setText(Integer.toString(vminus-1));

                    qun = myViewHolder.showValue.getText().toString();

                }
            });

            holder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    custom = pro.customerId;
                    pid = pro.productId;
                    qun = myViewHolder.showValue.getText().toString();

                    View prdd=myViewHolder.prdSize.getSelectedView();
                    TextView vv=(TextView)prdd;
                    String aiwa=vv.getText().toString();
                    Log.i("addd",aiwa);
                    View ids=  myViewHolder.sizeP.getRootView();
                    //TextView bb=(TextView)ids;
                    String sid=myViewHolder.sizeP.getText().toString();
                    pos=Integer.parseInt(sid);

                    int lst_qun=Integer.parseInt(qun);
                    if(lst_qun<=0){

                        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_quntit));
                        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();


                    }else{
                        new AsyncTaskAddCartJson().execute();
                    }

                }
            });

            holder.remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                 wishId=pro.wishId;
                    new AsyncRemoveWishList().execute();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vi;
    }

    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addCart(0,custom, pid, pos, qun);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                if (result.equals("success")) {
                    new AsyncTaskCartcounttJson().execute();

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;


                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("updated")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();
                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("Out of Stock")) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                    TextView ok = (TextView) layout.findViewById(R.id.textok);


                    builder.setView(layout);


                    final android.app.AlertDialog alertDialog = builder.create();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();
                } else {

                    android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));


                    alertDialog.show();

                }
            }
        }

    }

    public class AsyncRemoveWishList extends AsyncTask<String,String,String>
    {
        JSONArray dataJsonArray;
        int jsonlen=0;
        @Override
        protected String doInBackground(String... strings) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.wishListRemove(wishId);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArray = productObj.getJSONArray("d");
                result = dataJsonArray.getJSONObject(0).optString("result");
                jsonlen=dataJsonArray.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                if (result.equals("1")) {

                    final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle(context.getString(R.string.alert));
                    alertDialog.setMessage(context.getString(R.string.remove_alert_wishlist));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent in = new Intent(context, MyWhislist.class);
                            ((Activity) context).finish();
                            context.startActivity(in);
                            Toast.makeText(context, context.getString(R.string.succsesfullyremved), Toast.LENGTH_SHORT).show();

                            dialogInterface.dismiss();
                            alertDialog.dismiss();
                        }
                    });


                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.no), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            alertDialog.dismiss();

                        }
                    });
                    alertDialog.show();


                }
            }

        }

    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(custom);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(custom.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }
        }
    }
}
