package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;


public class partyordertGridViewAdapter extends BaseAdapter {


    private Context activity;


    LayoutInflater inf;
    ViewHolder holder;
    String []str_partyorder_id,str_partyorder_name,str_partyorder_crdate,str_partyorder_desc,str_partyorder_persons;
    public partyordertGridViewAdapter(Context context, String[] str_partyorder_id, String[] str_partyorder_name, String[] str_partyorder_crdate, String[] str_partyorder_desc, String[] str_partyorder_persons) {
        this.activity=context;

        this.str_partyorder_id=str_partyorder_id;
        this.str_partyorder_name=str_partyorder_name;
        this.str_partyorder_crdate=str_partyorder_crdate;
        this.str_partyorder_desc=str_partyorder_desc;
        this.str_partyorder_persons=str_partyorder_persons;
        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }




    @Override
    public int getCount() {
        return str_partyorder_id.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.partyorder_grid, null);
            holder = new ViewHolder();


            holder.partordername = (TextView) vi.findViewById(R.id.partordername);
            holder.partyoderdescrption = (TextView) vi.findViewById(R.id.partorderdesc);
            holder.partyorderpersons = (TextView) vi.findViewById(R.id.partorderpepls);



            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();


        }

        try {


            holder.partordername.setText(activity.getString(R.string.prt_ord_name)+" "+str_partyorder_name[position]);
            holder.partyoderdescrption.setText(activity.getString(R.string.description)+" : "+str_partyorder_desc[position]);
            holder.partyorderpersons.setText(activity.getString(R.string.party_persons)+" "+str_partyorder_persons[position]);


        }catch (Exception e){
            e.printStackTrace();
        }

        return vi;
    }


    public static class ViewHolder{

        public TextView partordername;
        public TextView partyoderdescrption;
        public TextView partyorderpersons;




    }


//    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {
//
//        final String TAG = "AsyncTaskParseJson.java";
//        JSONArray dataJsonArr;
//        int jsonlen=0;
//
//        @Override
//        protected void onPreExecute() {
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            try {
//                Json jParser = new Json();
//                JSONObject json = jParser.addCart(0,cusId, pid,pos, qun);
//                Log.i("posss",pos+qun);
//                JSONObject productObj = new JSONObject(Json.prdetail);
//
//                dataJsonArr = productObj.getJSONArray("d");
//                result = dataJsonArr.getJSONObject(0).optString("result");
//                jsonlen=dataJsonArr.length();
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (jsonlen == 0) {
//                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                alertDialog.setTitle(context.getString(R.string.error));
//                alertDialog.show();
//            } else {
//                if (result.equals("success")) {
//
//                    new AsyncTaskCartcounttJson().execute();
//
//                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    View layout = inflater1.inflate(R.layout.custom_alert, null);
//
//
//                    builder.setView(layout);
//
//                    final android.app.AlertDialog alertDialog = builder.create();
//                    alertDialog.getWindow().getAttributes().windowAnimations =
//
//
//                            R.style.Bounce;
//
//                    alertDialog.show();
//                    final Handler handler = new Handler();
//                    final Runnable runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (alertDialog.isShowing()) {
//                                alertDialog.dismiss();
//                            }
//                        }
//                    };
//
//                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            handler.removeCallbacks(runnable);
//                        }
//                    });
//
//                    handler.postDelayed(runnable, 2000);
//
//
//                } else if (result.equals("updated")) {
//
//                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);
//
//
//                    builder.setView(layout);
//
//                    final android.app.AlertDialog alertDialog = builder.create();
//                    alertDialog.getWindow().getAttributes().windowAnimations =
//
//
//                            R.style.Bounce;
//
//                    alertDialog.show();
//
//                    final Handler handler = new Handler();
//                    final Runnable runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (alertDialog.isShowing()) {
//                                alertDialog.dismiss();
//                            }
//                        }
//                    };
//
//                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            handler.removeCallbacks(runnable);
//                        }
//                    });
//
//                    handler.postDelayed(runnable, 2000);
//
//
//                } else if (result.equals("Out of Stock")) {
//
//
//                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
//                    TextView ok = (TextView) layout.findViewById(R.id.textok);
//
//
//                    builder.setView(layout);
//
//                    final android.app.AlertDialog alertDialog = builder.create();
//
//                    ok.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            alertDialog.dismiss();
//                        }
//                    });
//                    alertDialog.getWindow().getAttributes().windowAnimations =
//
//
//                            R.style.Bounce;
//
//                    alertDialog.show();
//
//
//                } else {
//
//                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//                    alertDialog.setMessage(context.getString(R.string.error));
//
//
//                    alertDialog.show();
//
//
//                }
//
//
//            }
//
//        }
//    }
//
//    public class AsyncTaskAddWishListJson extends AsyncTask<String, String, String> {
//
//        final String TAG = "AsyncTaskParseJson.java";
//        JSONArray dataJsonArr;
//        int jsonlen=0;
//
//        @Override
//        protected void onPreExecute() {
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            try {
//                Json jParser = new Json();
//
//                JSONObject json = jParser.addWishList(cusId, wishPid);
//                JSONObject productObj = new JSONObject(Json.prdetail);
//
//                dataJsonArr = productObj.getJSONArray("d");
//                res = dataJsonArr.getJSONObject(0).optString("result");
//                jsonlen=dataJsonArr.length();
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(jsonlen==0){
//                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                alertDialog.setTitle(context.getString(R.string.error));
//                alertDialog.show();
//            }else {
//                if (res.equals("success")) {
//
//
//                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    View layout = inflater1.inflate(R.layout.custom_alert_wishlist, null);
//
//
//                    builder.setView(layout);
//
//                    final android.app.AlertDialog alertDialog = builder.create();
//                    alertDialog.getWindow().getAttributes().windowAnimations =
//
//
//                            R.style.Bounce;
//
//
//                    alertDialog.show();
//
//                    final Handler handler = new Handler();
//                    final Runnable runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (alertDialog.isShowing()) {
//                                alertDialog.dismiss();
//                            }
//                        }
//                    };
//
//                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            handler.removeCallbacks(runnable);
//                        }
//                    });
//
//                    handler.postDelayed(runnable, 2000);
//
//
//                } else if (res.equals("Alredy exist")) {
//
//                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
//                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    View layout = inflater1.inflate(R.layout.custom_alert_exist, null);
//
//
//                    builder.setView(layout);
//
//                    final android.app.AlertDialog alertDialog = builder.create();
//                    alertDialog.getWindow().getAttributes().windowAnimations =
//
//
//                            R.style.Bounce;
//
//                    alertDialog.show();
//                    final Handler handler = new Handler();
//                    final Runnable runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (alertDialog.isShowing()) {
//                                alertDialog.dismiss();
//                            }
//                        }
//                    };
//
//                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            handler.removeCallbacks(runnable);
//                        }
//                    });
//
//                    handler.postDelayed(runnable, 2000);
//
//
//                } else {
//
//                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//                    alertDialog.setMessage(context.getString(R.string.error));
//
//
//                    alertDialog.show();
//
//
//                }
//            }
//
//        }
//
//
//    }
//    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {
//
//        final String TAG = "AsyncTaskParseJson.java";
//        int jsonlen=0;
//
//
//
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            try {
//
//
//                Json jParser = new Json();
//                JSONObject json = jParser.cartcount(cusId);
//                JSONObject productObj = new JSONObject(Json.prdetail);
//
//                dataJsonArr = productObj.getJSONArray("d");
//
//                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
//
//                jsonlen=dataJsonArr.length();
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(jsonlen==0){
//                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                alertDialog.setTitle(context.getString(R.string.error));
//                alertDialog.show();
//            }else {
//                if (str_json_cart_count.trim().equals("0")) {
//                    cartcountbadge.setVisibility(View.GONE);
//                } else {
//                    if(cusId.trim().equals("0")){
//                        cartcountbadge.setVisibility(View.GONE);
//                    }else {
//                        cartcountbadge.setVisibility(View.VISIBLE);
//                        cartcountbadge.setText(str_json_cart_count);
//                    }
//                }
//            }
//
//            // mProgressDialog.dismiss();
//        }
//    }
}