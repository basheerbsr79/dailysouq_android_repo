package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.Notification;


public class cust_list_notification extends BaseAdapter{


    private Activity activity;
    String[] notif_id, notif_title, notif_desc, notif_date;

    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;
    String seleted_id="0";

    public cust_list_notification(Activity context, int length, String[] notif_id, String[] notif_title, String[] notif_desc, String[] notif_date) {


        this.activity=context;
        this.rec_count=length;
        this.notif_id = notif_id;
        this.notif_title = notif_title;
        this.notif_desc =notif_desc;
        this.notif_date =notif_date;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public static class ViewHolder{
        public TextView notiftitle;
        public TextView notifdesc;
        public TextView notifdate;
        public ImageView notifremove;




    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_notification, null);
            holder = new ViewHolder();

            holder.notiftitle = (TextView) vi.findViewById(R.id.notiftiltle);
            holder.notifdesc = (TextView) vi.findViewById(R.id.notifdesc);
            holder.notifdate = (TextView) vi.findViewById(R.id.notifdate);
            holder.notifremove = (ImageView) vi.findViewById(R.id.notifremove);


            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{


            holder.notiftitle.setText( notif_title[position]);
            holder.notifdesc.setText( notif_desc[position]);
            holder.notifdate.setText( notif_date[position]);
            holder.notifremove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seleted_id=notif_id[position];
                   //Toast.makeText(activity,"Deleted id"+seleted_id,Toast.LENGTH_LONG).show();
                    new AsyncTaskCartcounttJson().execute();

                }
            });





        }catch(Exception e){

        }
        return vi;
    }




        public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
            JSONArray dataJsonArr;
            String responce;




        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.notificationdelete(seleted_id);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce = dataJsonArr.getJSONObject(0).optString("result");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle(activity.getString(R.string.error));
                alertDialog.show();
            }else {
                if (responce.trim().equals("success")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                    alertDialog.setTitle(activity.getString(R.string.succsesfullyremved));
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.finish();
                            Intent in=new Intent(activity, Notification.class);
                            activity.startActivity(in);
                        }
                    });
                    alertDialog.show();
                } else{
                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                    alertDialog.setTitle(activity.getString(R.string.failedtoremove));
                    alertDialog.show();
                }
            }

            // mProgressDialog.dismiss();
        }
    }
}
