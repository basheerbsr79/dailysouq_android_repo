package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class FileAClaim extends AppCompatActivity {
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    TextView title,claimno_current,desc_current,date_current,status_current,cur_claim;
    Button bck_btn,btn_claim,cancel_claim;
    EditText ed_subject,ed_orderno,ed_productname,ed_description;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;

    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    SharedPreferences myprefs;
    ImageView footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore;
    String userVLogIn,str_clmid,str_desc,str_date,str_stus,str_sub,str_prdnme,str_descclaim,str_orderno,result,str_json_cart_count;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_aclaim);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);


        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }



        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        cur_claim=(TextView)findViewById(R.id.cur_claim_text);
        claimno_current=(TextView)findViewById(R.id.claimno_claim);
        desc_current=(TextView)findViewById(R.id.desc_claim_current);
        date_current=(TextView)findViewById(R.id.date_claim);
        status_current=(TextView)findViewById(R.id.status_claim);
        btn_claim=(Button)findViewById(R.id.btn_claim_claim);
        cancel_claim=(Button)findViewById(R.id.btn_cncl_claim);
        ed_subject=(EditText)findViewById(R.id.subject_claim);
        ed_orderno=(EditText)findViewById(R.id.orderno_claim);
        ed_productname=(EditText)findViewById(R.id.prdname_claim);
        ed_description=(EditText)findViewById(R.id.desc_claim);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

        Intent in=getIntent();
        str_orderno=in.getStringExtra("notiftitle");
        ed_orderno.setText("DSQORD"+str_orderno);
        userVLogIn = myprefs.getString("shaniusrid", null);

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(FileAClaim.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FileAClaim.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(FileAClaim.this, Notification.class);

                startActivity(in);
            }
        });
        title.setText(getString(R.string.title_activity_file_aclaim));
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
           // new AsyncTaskCartcounttJson().execute();
            new AsyncTRepeattJson().execute();

        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }



        btn_claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_sub = ed_subject.getText().toString();
                str_descclaim = ed_description.getText().toString();
              //  str_orderno = ed_orderno.getText().toString();

                str_prdnme = ed_productname.getText().toString();
                if(str_orderno.equals("")||str_descclaim.equals("")||str_prdnme.equals("")){

                    ed_productname.setError(getString(R.string.plz_fill));
                    ed_description.setError(getString(R.string.plz_fill));
                  //  ed_orderno.setError("Required");


                   // Toast.makeText(getApplicationContext(),"Please fill all the field",Toast.LENGTH_LONG).show();
                }else {



                    new AsyncClaimtJson().execute();
                }
            }
        });
        cancel_claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FileAClaim.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(FileAClaim.this,DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(FileAClaim.this,DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FileAClaim.this, Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(FileAClaim.this,DsSpecials.class);

                startActivity(in);
            }
        });


        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTRepeattJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(FileAClaim.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.currentclaim(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_clmid = dataJsonArr.getJSONObject(0).optString("ClaimId");
                str_desc = dataJsonArr.getJSONObject(0).optString("Description");
                str_date = dataJsonArr.getJSONObject(0).optString("ClaimDate");
                str_stus = dataJsonArr.getJSONObject(0).optString("ClaimStatusName");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();

            if(jsonlen==0){

                cur_claim.setText(getString(R.string.youhave__no_currnt_claim));
                desc_current.setVisibility(View.GONE);
                date_current.setVisibility(View.GONE);
                status_current.setVisibility(View.GONE);
                claimno_current.setVisibility(View.GONE);

            }
            else {


                claimno_current.setText("Claim No :" + str_clmid);
                desc_current.setText("Description :" + str_desc);
                date_current.setText("Date :" + str_date);
                status_current.setText("Status:" + str_stus);
            }


        }
    }









    public class AsyncClaimtJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(FileAClaim.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.fileaclaim(userVLogIn, str_orderno, str_descclaim, str_prdnme);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {


    if (!result.equals(null)) {

        AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();

        alertDialog.setMessage(getString(R.string.succsess));
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        // Intent in = new Intent(FileAClaim.this, Menu_List.class);
                        onBackPressed();
                        // startActivity(in);

                    }
                });


        alertDialog.show();

    } else {
        AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();

        alertDialog.setMessage(getString(R.string.error));
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        // Intent in = new Intent(FileAClaim.this, Menu_List.class);
                        onBackPressed();
                        // startActivity(in);

                    }
                });


        alertDialog.show();
    }


}


        }
    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(FileAClaim.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            //mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");

                jsonlen=dataJsonArr.length();



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {



    if (str_json_cart_count.trim().equals("0")) {
        cartcountbadge.setVisibility(View.GONE);
    } else {
        if(userVLogIn.trim().equals("0")){
            cartcountbadge.setVisibility(View.GONE);
        }else {
            cartcountbadge.setVisibility(View.VISIBLE);
            cartcountbadge.setText(str_json_cart_count);
        }
    }


    // Log.i("arrivedcount", str_json_cart_count);
}
          //  mProgressDialog.dismiss();
        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(FileAClaim.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(FileAClaim.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}
