package shani.netstager.com.dailysouq.activity;

/**
 * Created by prajeeshkk on 16/09/15.
 */

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.productGridViewAdapter;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.support.OnLoadMoreListener;




public class Products extends Activity {


    //used for end less

    private TextView tvEmptyView;
    private RecyclerView gridViews;
    private productGridViewAdapter mAdapter;

    int pg = 0;
    ArrayList<ProductModel> productModels;
    private GridLayoutManager lLayout;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;




    ImageView footeshop,footerdeal,footersp,footergft,footermore;

    View footer;
    ProgressDialog dialog;
    //its used for new scroll

    int id=0;


    ImageView profiles,quickcheckout;
    ProgressDialog mProgressDialog;




    int sizeProduct;
    int productLength;
    ImageView sort;

    int DEFAULT_LANGUGE;
    ImageView filter;

    public SharedPreferences myProducts;
    Button back_btn;


    String catId,userVLogIn,qun;

    String sortItem;
    SharedPreferences myprefs;

    TextView title;

    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;

    ImageView serchgobtn,Notification;
    RelativeLayout cartcounttext;


    String srchtext;
    int str_json_cart_count;;



    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.products);

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }


        //used for endless scroll
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        pgresbar_scrool_footer=(ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        productModels = new ArrayList<>();
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            new AsyncTaskCartcounttJson().execute();
            new AsyncTaskParseJson(pg).execute();

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(Products.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        lLayout = new GridLayoutManager(Products.this, 2);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);


        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);



        //badge

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }



        footeshop = (ImageView) findViewById(R.id.imageView11);
        footersp = (ImageView) findViewById(R.id.imageView13);
        footermore = (ImageView) findViewById(R.id.imageView15);
        footerdeal = (ImageView) findViewById(R.id.imageView12);
        footergft = (ImageView) findViewById(R.id.imageView14);
        footermore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, Menu_List.class);

                startActivity(in);
            }
        });
        footersp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, DsSpecials.class);

                startActivity(in);
            }
        });
        footerdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, DsDeals.class);

                startActivity(in);
            }
        });
        footergft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, DsGiftCard.class);

                startActivity(in);
            }
        });

        footeshop.setImageResource(R.drawable.footershopselect);
        profiles = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);

        back_btn = (Button) findViewById(R.id.menue_btn);
        title = (TextView) findViewById(R.id.namein_title);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });

        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, EditProfile.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Products.this, Notification.class);

                startActivity(in);
            }
        });
        //serch test
        catogoryspinnerinsrch = (EditText) findViewById(R.id.catSpinner);
        serchtext = (MultiAutoCompleteTextView) findViewById(R.id.Search);
        serchgobtn = (ImageView) findViewById(R.id.goButton);


        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });

        serchgobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srchtext = serchtext.getText().toString();
                if (serchtext.length() == 0) {
                    serchtext.setError(getString(R.string.srh_here_error));
                } else {

//    Toast.makeText(getBaseContext(), "You have selected: " + categoryName[pos],
//            Toast.LENGTH_SHORT).show();
//    Toast.makeText(getBaseContext(), "You have : " + srchtext,
//            Toast.LENGTH_SHORT).show();

                    Intent in = new Intent(Products.this, SerchResult.class);
                    in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                    in.putExtra("text", srchtext);

                    startActivity(in);
                    //new AsyncTaskNotifCounttJson().execute();
                }
            }
        });


        //serch test
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);

        sort = (ImageView) findViewById(R.id.sort);
        filter = (ImageView) findViewById(R.id.filter);
        //btn_next= (Button) findViewById(R.id.next);
        //btn_prev= (Button) findViewById(R.id.prev);
        Intent in = getIntent();
        catId = in.getStringExtra("ID");
        title.setText(in.getStringExtra("Categoryname"));





        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Products.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Products.this, CartView.class);

                startActivity(in);
            }
        });


        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SortTypePopup();
            }
        });


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Products.this, Filter.class);
                in.putExtra("Catogoryid_filter",catId);
                startActivity(in);
            }
        });
    }



    //bsr new popup strt sort
    private void SortTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_sorttype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView pricelowtohigh = (TextView) popupView.findViewById(R.id.plh_custom_popup);
        final TextView pricehightolow = (TextView) popupView.findViewById(R.id.phl_custom_poup);
        final  TextView toprated = (TextView) popupView.findViewById(R.id.toprated_custom_popup);
        final TextView atoz = (TextView) popupView.findViewById(R.id.atoz_custom_poup);
        final TextView ztoa = (TextView) popupView.findViewById(R.id.ztoa_custom_poup);
        pricelowtohigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=pricelowtohigh.getText().toString();

                Intent in = new Intent(Products.this, Sort.class);
                in.putExtra("CATID", catId);
                in.putExtra("SORT", sortItem);
                startActivity(in);

                popupWindow.dismiss();
            }
        });
        pricehightolow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=pricehightolow.getText().toString();

                Intent in = new Intent(Products.this, Sort.class);
                in.putExtra("CATID", catId);
                in.putExtra("SORT", sortItem);
                startActivity(in);

                popupWindow.dismiss();
            }
        });
        toprated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=toprated.getText().toString();

                Intent in = new Intent(Products.this, Sort.class);
                in.putExtra("CATID", catId);
                in.putExtra("SORT", sortItem);
                startActivity(in);

                popupWindow.dismiss();
            }
        });
        atoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=atoz.getText().toString();

                Intent in = new Intent(Products.this, Sort.class);
                in.putExtra("CATID", catId);
                in.putExtra("SORT", sortItem);
                startActivity(in);

                popupWindow.dismiss();
            }
        });
        ztoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortItem=ztoa.getText().toString();

                Intent in = new Intent(Products.this, Sort.class);
                in.putExtra("CATID", catId);
                in.putExtra("SORT", sortItem);
                startActivity(in);

                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(sort, 0,0);



    }

    // bsr edit popup end sort


    //bsr new popup strt srch
    private void SerachTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
        final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
        final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
        final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(all.getText());
                popupWindow.dismiss();
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(products.getText());
                popupWindow.dismiss();
            }
        });
        catogory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(catogory.getText());
                popupWindow.dismiss();
            }
        });
        prdctcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(prdctcode.getText());
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



    }

    // bsr edit popup end srch


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }



    public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;

        public AsyncTaskParseJson(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.products(catId, userVLogIn, pg,DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);


                    //Shakeeb code
                    ProductModel product = new ProductModel();
                    product.productId = dataJsonArr.getJSONObject(i).getString("ProductId");

                    product.productName = c.getString("ProductShowName").toString();
                    product.imagepath = c.getString("Imagepath").toString();
                    product.isNew = c.getString("IsNew");
                    product.brandId = c.getString("BrandId");
                    product.category = c.getString("CategoryId");
                    product.customerId = userVLogIn;

                    ArrayList<SizeModel> sizes = new ArrayList<>();

                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
                    if (sizeProduct > 0) {
                        for (int j = 0; j < sizeProduct; j++) {
                            SizeModel sizeModel = new SizeModel();
                            sizeModel.productsize = sizeArray.getJSONObject(j).getString("ProductSize");
                            sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                            sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                            sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                            sizeModel.savaPrice = sizeArray.getJSONObject(j).optString("Saveprice");
                            sizeModel.discount = sizeArray.getJSONObject(j).optInt("Discount");
                            sizeModel.stockstatus=sizeArray.getJSONObject(j).optString("Stockstatus");
                            sizes.add(sizeModel);

                        }
                    }
                    product.sizes = sizes;
                    productModels.add(product);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);


                    SharedPreferences.Editor editor = myProducts.edit();


                    editor.putString("myCatid", product.category);
                    editor.putString("mypid", product.productId);
                    editor.apply();

                }



                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<ProductModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(Products.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            }else {


                pg = pg + 1;
                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }
                if (productModels.size() <= 10) {
                    mAdapter = new productGridViewAdapter(productModels, gridViews, Products.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);
                } else {
                    mAdapter.setProductlist(productModels);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();
                }


                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {


                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                new AsyncTaskParseJson(pg).execute();


                            }
                        }, 100);

                    }
                });


            }


        }
    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Products.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            // mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(Products.this).create();

    alertDialog.setMessage(getString(R.string.error));

    alertDialog.show();
}else {
    sp.setcartcount(str_json_cart_count);

    if (sp.getcartcount()==0) {
        cartcountbadge.setVisibility(View.GONE);
    } else {
        if(userVLogIn.trim().equals("0")){
            cartcountbadge.setVisibility(View.GONE);
        }else {
            cartcountbadge.setVisibility(View.VISIBLE);
            cartcountbadge.setText(sp.getcartcount()+"");
        }
    }

    // Log.i("arrivedcount", str_json_cart_count);

} //  mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Products.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Products.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(Products.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}


