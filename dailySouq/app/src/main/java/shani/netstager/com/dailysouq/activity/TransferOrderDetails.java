package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cust_list_order_details;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class TransferOrderDetails extends AppCompatActivity {
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    TextView title;
    Button bck_btn;
    String str_orderno,str_deliverdby,str_totalitem,str_orderdate,str_status;
    String str_amtpaid,str_discount,str_deliverycharge,str_nettotal="0",str_orderitem;
    int int_pyameth_id;
    TextView orderno,amtpaid,deliverdby,subtotal,discounts,deliverycharge,nettotal,unitprice,qntity,orderitem,totalitem,orderdate,status;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    SharedPreferences myprefs;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    String userVLogIn,result,str_json_cart_count;
    String[] str_imgpath,str_qntity,str_prdname;
    String []str_unitprice,str_subtotal;
    ImageView imgpaths;
    ListView orderlistdet;
    //Button cancelorder,fileaclaim,repeatorder;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    //Button transferorder;
    //for new transfer buy now
    Button btnPaynow;

    //variables are used for ebs
    private static String HOST_NAME = "EBS";
    ArrayList<HashMap<String, String>> custom_post_parameters;
    private static final int ACC_ID = 19241; // Provided by EBS
    private static final String SECRET_KEY = "5e95119c0b20a69f9a970dfe3aebded6";
    private static final double PER_UNIT_PRICE = 1.00;
    double totalamount;
    String ORDERREVIEW_NAME,ORDERREVIEW_ADDRESS,ORDERREVIEW_PHONE,ORDERREVIEW_LOCATION,STR_QUICK_ARRIVED="";
    String AFTER_ORDER_EMAIL,AFTER_OREDER_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_order_details);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        Intent in=getIntent();
        str_orderno=in.getStringExtra("notif_id");
       // transferorder=(Button)findViewById(R.id.transfer_order);
        //cancelorder=(Button)findViewById(R.id.cancel_order);
       // fileaclaim=(Button)findViewById(R.id.fileaclaim_order);
       // repeatorder=(Button)findViewById(R.id.rpeat_order);
        orderno=(TextView)findViewById(R.id.txt_orderno_det);
        amtpaid=(TextView)findViewById(R.id.txt_amtpaid_det);
        deliverdby=(TextView)findViewById(R.id.txt_deliverdby_det);
        subtotal=(TextView)findViewById(R.id.txt_subtotal_det);
        discounts=(TextView)findViewById(R.id.txt_discount_det);
        deliverycharge=(TextView)findViewById(R.id.txt_deliverycharge_det);
        nettotal=(TextView)findViewById(R.id.t5b);
        orderlistdet = (ListView) findViewById(R.id.orderdetlist);
        btnPaynow=(Button)findViewById(R.id.btnPaynow);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);


            userVLogIn = myprefs.getString("shaniusrid", null);


        orderlistdet.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        /*transferorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(TransferOrderDetails.this,TransferOrder.class);
                in.putExtra("OrderId",str_orderno);
                startActivity(in);
            }
        });*/

        btnPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //need impliment call the apis....



                SharedPreferences.Editor editor = myprefs.edit();
                editor.putInt("PAYMENT_GATEWAY", 3);
                editor.apply();

                callEbsKit(TransferOrderDetails.this);

               // new AsyncBuyJson().execute();


                /*final AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage("need impliment call the apis...");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                               // new AsyncTRepeattJson().execute();

                            }
                        });
                     *//*alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();

                    }
                });*//*


                alertDialog.show();*/
            }
        });


        /*cancelorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.confirm_cancel_order));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                new AsyncCancelJson().execute();

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.no),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();

                    }
                });


                alertDialog.show();


            }
        });*/
        /*repeatorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                final AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.confirm_repeat_order));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                new AsyncTRepeattJson().execute();

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();

                    }
                });


                alertDialog.show();

            }
        });*/
        /*fileaclaim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TransferOrderDetails.this, FileAClaim.class);
                in.putExtra("notiftitle",str_orderno);
                startActivity(in);
            }
        });*/


        totalitem=(TextView)findViewById(R.id.txt_totalitem_det);
        orderdate=(TextView)findViewById(R.id.orderdate_det);
        status=(TextView)findViewById(R.id.txt_status_det);


        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        bck_btn=(Button)findViewById(R.id.menue_btn);

       /* //footer delartions

        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);

        //footer clicks

        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OrderDetails.this, Categories.class);
                OrderDetails.this.finish();
                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(OrderDetails.this,DsDeals.class);
                OrderDetails.this.finish();
                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(OrderDetails.this,DsGiftCard.class);
                OrderDetails.this.finish();
                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(OrderDetails.this,Menu_List.class);
                OrderDetails.this.finish();
                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(OrderDetails.this,DsSpecials.class);
                OrderDetails.this.finish();
                startActivity(in);
            }
        });*/

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(TransferOrderDetails.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TransferOrderDetails.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TransferOrderDetails.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(TransferOrderDetails.this, Notification.class);

                startActivity(in);
            }
        });
        title.setText(getString(R.string.title_activity_order_details));





        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

         //   new AsyncTaskCartcounttJson().execute();
            new AsyncTaskCarttJson().execute();
        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }



        // used to set new sp based count
        setcartcountwihoutapi();
        //set email and fname using SP
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        AFTER_OREDER_NAME = myprefs.getString("bsrnme", null);
        AFTER_ORDER_EMAIL = myprefs.getString("useremail", null);



    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(TransferOrderDetails.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.orderdetail(str_orderno,userVLogIn);

                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_subtotal=new String[dataJsonArr.length()];
                str_imgpath=new String[dataJsonArr.length()];
                str_unitprice=new String[dataJsonArr.length()];
                str_qntity=new String[dataJsonArr.length()];
                str_prdname=new String[dataJsonArr.length()];
                str_amtpaid = dataJsonArr.getJSONObject(0).optString("Nettotal");
                str_deliverdby = dataJsonArr.getJSONObject(0).optString("DeliverdBy");
                str_discount = dataJsonArr.getJSONObject(0).optString("Discount");
                str_deliverycharge = dataJsonArr.getJSONObject(0).optString("ShippingCharge");
                str_nettotal = dataJsonArr.getJSONObject(0).optString("Nettotal");
                str_orderitem = dataJsonArr.getJSONObject(0).optString("OrderAmount");
                str_totalitem = dataJsonArr.getJSONObject(0).optString("Totalitems");
                str_orderdate = dataJsonArr.getJSONObject(0).optString("OrderDate");
                str_status = dataJsonArr.getJSONObject(0).optString("OrderStatusName");
                int_pyameth_id = dataJsonArr.getJSONObject(0).optInt("PaymentMethodId");

                    jsonlen=dataJsonArr.length();


                for(int i=0;i<dataJsonArr.length();i++) {

                    str_subtotal[i] = dataJsonArr.getJSONObject(i).optString("subtotal");
                    str_imgpath[i] = dataJsonArr.getJSONObject(i).optString("ProductShowName")+"                      ";
                    str_prdname[i]=str_imgpath[i].substring(0,18);
                    str_unitprice[i] = dataJsonArr.getJSONObject(i).optString("UnitPrice");
                    str_qntity[i] = dataJsonArr.getJSONObject(i).optString("Quantity");


                }




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {


                cust_list_order_details adapter = new cust_list_order_details(TransferOrderDetails.this, dataJsonArr.length(), str_prdname, str_unitprice, str_qntity, str_subtotal);
                orderlistdet = (ListView) findViewById(R.id.orderdetlist);
                orderlistdet.setAdapter(adapter);


                orderno.setText("Order No: DSQORD" + str_orderno);
                if(int_pyameth_id==1){
                    //cod
                    amtpaid.setText("Payable Amount : " + str_amtpaid + "0");
                }else {
                    //other
                    amtpaid.setText(getString(R.string.paid_amount) + str_amtpaid + "0");

                }


                if(str_deliverdby.equalsIgnoreCase("0")||str_deliverdby.equals(null)||str_deliverdby.length()<=2){
                    deliverdby.setVisibility(View.GONE);
                }else {
                    deliverdby.setText(getString(R.string.deliverd_by) + " " + str_deliverdby);
                }

                subtotal.setText(getString(R.string.rs) + str_orderitem + "0 ");
                discounts.setText(getString(R.string.rs) + str_discount + "0 ");
                deliverycharge.setText(getString(R.string.rs) + str_deliverycharge + "0");
                nettotal.setText(getString(R.string.rs) + str_nettotal + "0");

                totalitem.setText(str_totalitem + " " + getString(R.string.items));
                orderdate.setText(getString(R.string.date) + str_orderdate);
                if (str_status.equals("Delivered")) {
                    status.setTextColor(getResources().getColor(R.color.green));
                } else if (str_status.equals("In Transit")) {
                    status.setTextColor(Color.BLUE);
                } else if (str_status.equals("Not Delivered")) {
                    status.setTextColor(Color.RED);
                }
                else if(str_status.equalsIgnoreCase("Confirmed")){
                    status.setTextColor(getResources().getColor(R.color.green));
                }
                status.setText(getString(R.string.status) + str_status);


            }


        }
    }














    public class AsyncTRepeattJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(TransferOrderDetails.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.repeatoreder(str_orderno, userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                json_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.show();

            }else{
                Intent in = new Intent(TransferOrderDetails.this, CartView.class);

                startActivity(in);
            }

        }
    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {


    if (str_json_cart_count.trim().equals("0")) {
        cartcountbadge.setVisibility(View.GONE);
    } else {
        if(userVLogIn.trim().equals("0")){
            cartcountbadge.setVisibility(View.GONE);
        }else {
            cartcountbadge.setVisibility(View.VISIBLE);
            cartcountbadge.setText(str_json_cart_count);
        }
    }

}
            //Log.i("arrivedcount", str_json_cart_count);

            //  mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(TransferOrderDetails.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(TransferOrderDetails.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }


    //method for payment gate way
    private void callEbsKit(TransferOrderDetails buyProduct) {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */

        /** Payment Amount Details */
        // Total Amount

        String str_amnt_tot=str_nettotal+"0";
        float amount=Float.parseFloat(str_amnt_tot);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        str_amnt_tot = df.format(amount);

        float flt_str=Float.parseFloat(str_amnt_tot);

        PaymentRequest.getInstance().setTransactionAmount(
                String.format("%.2f", flt_str));

        /** Mandatory */

        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        Log.i("Order id", str_orderno + "");
        // Reference No
        PaymentRequest.getInstance().setReferenceNo(str_orderno);
        /** Mandatory */

        // Email Id
        PaymentRequest.getInstance().setBillingEmail(AFTER_ORDER_EMAIL);
        /** Mandatory */

        /**
         * Set failure id as 1 to display amount and reference number on failed
         * transaction page. set 0 to disable
         */
        PaymentRequest.getInstance().setFailureid("0");
        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                "Daily Souq Android App Purchase");

        /** Billing Details */
        PaymentRequest.getInstance().setBillingName(AFTER_OREDER_NAME);
        /** Optional */
        PaymentRequest.getInstance().setBillingAddress(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setBillingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setBillingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setBillingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setBillingCountry("IND");
        // ** Optional */
        PaymentRequest.getInstance().setBillingPhone("1 800 123 2565 ");
        /** Optional */
        /** set custom message for failed transaction */

        PaymentRequest.getInstance().setFailuremessage(
                getResources().getString(R.string.payment_failure_message));
        /** Optional */
        /** Shipping Details */
        PaymentRequest.getInstance().setShippingName(AFTER_OREDER_NAME);
        /** Optional */
        PaymentRequest.getInstance().setShippingAddress(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setShippingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setShippingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setShippingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setShippingCountry("IND");
        /** Optional */
        PaymentRequest.getInstance().setShippingEmail(AFTER_ORDER_EMAIL);
        /** Optional */
        PaymentRequest.getInstance().setShippingPhone(AFTER_ORDER_EMAIL);
        /** Optional */
		/* enable log by setting 1 and disable by setting 0 */
        PaymentRequest.getInstance().setLogEnabled("1");

        /**
         * Initialise parameters for dyanmic values sending from merchant custom
         * values from merchant
         */

        custom_post_parameters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashpostvalues = new HashMap<String, String>();
        hashpostvalues.put("account_details", "saving");
        hashpostvalues.put("merchant_type", "gold");
        custom_post_parameters.add(hashpostvalues);

        PaymentRequest.getInstance()
                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(buyProduct, ACC_ID, SECRET_KEY,
                Config.Mode.ENV_LIVE, Config.Encryption.ALGORITHM_MD5, HOST_NAME);

        // EBSPayment.getInstance().init(context, accId, secretkey, environment,
        // algorithm, host_name);

    }



}
