package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.OrderDetails;

public class cust_list_bundle_product_items extends BaseAdapter{


    private Activity activity;
    String[]orderno,orderitem;
    String[] totalitem;

    LayoutInflater inf;
    ViewHolder holder;

    public cust_list_bundle_product_items(Activity context,  String[] orderno, String[] orderitem, String[] status) {


        this.activity=context;

        this.orderno=orderno;
        this.orderitem=orderitem;
        this.totalitem=status;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return orderno.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public static class ViewHolder{
        public TextView name;
        public TextView qty;
        public TextView mrp;




    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.cust_list_bundle_items, null);
            holder = new ViewHolder();

            holder.name = (TextView) vi.findViewById(R.id.name);
            holder.qty = (TextView) vi.findViewById(R.id.qty);
            holder.mrp = (TextView) vi.findViewById(R.id.mrp);


            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{


            holder.name.setText(orderno[position]);
            holder.qty.setText(orderitem[position]);
            holder.mrp.setText(totalitem[position]);


        }catch(Exception e){

        }
        return vi;
    }
}
