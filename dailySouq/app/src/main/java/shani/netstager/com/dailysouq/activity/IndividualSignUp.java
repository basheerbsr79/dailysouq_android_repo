package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.Calendar;

import shani.netstager.com.dailysouq.GCM.RegistrationIntentService;
import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 09/09/15.
 */
public class IndividualSignUp extends Activity{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST =9000;
    public  static  int SPLASH_TIME_OUT=3000;
EditText  ed_firstName,ed_lastName,ed_dob,ed_telno,ed_email,ed_mobile,ed_password,ed_rferal,ed_confirm;
  Spinner sp_mode;
    String []comm={"Mobile Phone","Email"};
    String modeId;
    CheckBox c_condtions,c_mail;

    String mail_Check,termsCheck,customertypeid="1",logintype="NO",device;
    Button btn_Submit,btn_cancel;
    String firstN,lastN,dob,telno,email,mobile,password,refferal="",confpass,LoginUserIdType="1";
    static final  int DATE_ID=0;
    int y,m,d;
    String dtest,mtest;
    public SharedPreferences myprefs;
    static String customername,useremail,phone,customerId;
    static  String customerlastname,telephone,dob_pref,psd_pref;
    int sid;
    ImageView Indback;
    String IndSign,Indres;
    static boolean succ;
    ProgressDialog mProgressDialog;
    boolean eamilnotneedfalg=true;
    TextView txt_link_terms;
    RadioButton myuserid_phone,myuserid_email;
    DS_SP sp;



//    final EditText ed_email = (EditText)findViewById(R.id.textMessage);
//
//
//
//    String email = ed_email.getText().toString().trim();

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

// onClick of button perform this simplest code.





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.individual_signup);
        sp=new DS_SP(getApplicationContext());
        txt_link_terms=(TextView)findViewById(R.id.txt_link_terms);
        myuserid_email=(RadioButton)findViewById(R.id.email_radio);
        myuserid_phone=(RadioButton)findViewById(R.id.phone_radio);
        ed_firstName= (EditText) findViewById(R.id.IndFirstName);
        ed_lastName= (EditText) findViewById(R.id.IndLastName);
        ed_dob= (EditText) findViewById(R.id.IndDOB);
        //ed_dob.setEnabled(false);
        ed_telno=(EditText) findViewById(R.id.IndTelephone);
        ed_email=(EditText) findViewById(R.id.IndEmail);
        ed_mobile=(EditText) findViewById(R.id.IndMobile);
        ed_password=(EditText) findViewById(R.id.IndPassword);
        ed_confirm=(EditText)findViewById(R.id.IndConfirmPassword);
        ed_rferal=(EditText) findViewById(R.id.IndRefferal);
        sp_mode=(Spinner)findViewById(R.id.IndSpinner);
        c_condtions=(CheckBox)findViewById(R.id.IndcheckBox);
        c_mail=(CheckBox)findViewById(R.id.IndcheckBox2);
        btn_Submit=(Button)findViewById(R.id.IndSubmit);
        btn_cancel=(Button)findViewById(R.id.IndCancel);
        Indback=(ImageView)findViewById(R.id.Indback);

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

        ed_telno.setText("");
        ed_email.setText("");
        ed_rferal.setText("");

        device= getDeviceId();

        final Calendar calendar=Calendar.getInstance();
        y=calendar.get(Calendar.YEAR);
        m=calendar.get(Calendar.MONTH);
        d=calendar.get(Calendar.DAY_OF_WEEK);




        ed_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showDialog(DATE_ID);
                ed_dob.setEnabled(false);
                return false;
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item,comm);

        sp_mode.setAdapter(adapter);
        sp_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sid = sp_mode.getSelectedItemPosition();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firstN = ed_firstName.getText().toString();
                lastN = ed_lastName.getText().toString();
                dob = ed_dob.getText().toString();
                telno = ed_telno.getText().toString();
                email = ed_email.getText().toString().trim();
                mobile = ed_mobile.getText().toString();
                password = ed_password.getText().toString();
                refferal = ed_rferal.getText().toString();
                confpass = ed_confirm.getText().toString();
                modeId = Integer.toString(sid);

                if (c_mail.isChecked()) {
                    mail_Check = "true";
                } else {
                    mail_Check = "false";
                }


                if (firstN.equals("")) {

                    ed_firstName.setError(getString(R.string.enter_first_name));
                }
                if (lastN.equals("")) {

                    ed_lastName.setError(getString(R.string.enter_last_name));
                }
//                if (dob.equals("")) {
//
//                    ed_dob.setError(getString(R.string.plz_fill));
//                } else {
//                    ed_dob.setError(null);
//                }


//                if (mobile.length() != 10)
//
//                {
//
//                    ed_mobile.setError(getString(R.string.digit10need));
//                }

                if (password.length() < 6)

                {

                    ed_password.setError(getString(R.string.mustbe6chrtr));
                }


                if (!c_condtions.isChecked())

                {
                    c_condtions.setError(getString(R.string.accpt_terms_condition));


                    // Toast.makeText(IndividualSignUp.this, "please accept the terms and conditions.", Toast.LENGTH_SHORT).show();


                } else {
                    c_condtions.setError(null);
                }


                if (firstN.equals("") || lastN.equals("")  || password.equals("") || confpass.equals("") || !c_condtions.isChecked())

                {


                } else

                {


                    //internet checking
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent.equals(true)) {
                        if (password.equals(confpass)) {


//                            if (mobile.length() <= 2)
//
//                            {
//
//                                ed_mobile.setError(getString(R.string.digit10need));
//                            }
//                            else{
                            if (email.length() > 1) {
                                if (email.matches(emailPattern) && email.length() > 0) {

                                    eamilnotneedfalg = false;
                                    // Create a progressdialog
                                    mProgressDialog = new ProgressDialog(IndividualSignUp.this);
                                    // Set progressdialog title
                                    mProgressDialog.setTitle("");

                                    mProgressDialog.setMessage(getString(R.string.loading));
                                    mProgressDialog.setIndeterminate(false);
                                    mProgressDialog.setCancelable(false);
                                    mProgressDialog.setCanceledOnTouchOutside(false);
                                    // Show progressdialog
                                    mProgressDialog.show();
                                    if(myuserid_email.isChecked()==true){
                                        LoginUserIdType = "1";
                                    }else if(myuserid_phone.isChecked()==true){

                                        LoginUserIdType = "2";
                                    }
                                    else {
                                        LoginUserIdType = "1";

                                    }


                                    if (checkPlayServices()) {
                                        // Start IntentService to register this application with GCM.


                                        Intent intent = new Intent(IndividualSignUp.this, RegistrationIntentService.class);
                                        startService(intent);
                                    }
                                    // if(isConnected==true) {
                                    new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                                        @Override
                                        public void run() {

                                            getIndividualSignUp();

                                        }
                                    }, SPLASH_TIME_OUT);




                                } else {

                                    ed_email.setError(getString(R.string.invalid_email));

                                    eamilnotneedfalg = false;

                                }
                            } else {



                                if (eamilnotneedfalg == false) {

                                } else {
                                    // Create a progressdialog
                                    mProgressDialog = new ProgressDialog(IndividualSignUp.this);
                                    // Set progressdialog title
                                    mProgressDialog.setTitle("");

                                    mProgressDialog.setMessage(getString(R.string.loading));
                                    mProgressDialog.setIndeterminate(false);
                                    mProgressDialog.setCancelable(false);
                                    mProgressDialog.setCanceledOnTouchOutside(false);
                                    // Show progressdialog
                                    mProgressDialog.show();
                                    if(myuserid_email.isChecked()==true){
                                        LoginUserIdType = "1";
                                    }else if(myuserid_phone.isChecked()==true){
                                        LoginUserIdType = "2";
                                    }
                                    else {
                                        LoginUserIdType = "1";

                                    }
                                    if (checkPlayServices()) {
                                        // Start IntentService to register this application with GCM.


                                        Intent intent = new Intent(IndividualSignUp.this, RegistrationIntentService.class);
                                        startService(intent);
                                    }
                                    // if(isConnected==true) {
                                    new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                                        @Override
                                        public void run() {

                                            getIndividualSignUp();

                                        }
                                    }, SPLASH_TIME_OUT);
                                }

                            }


                        } else {


                            ed_password.setError(getString(R.string.psd_mismatch));

                            ed_confirm.setError(getString(R.string.psd_mismatch));
                        }


                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(IndividualSignUp.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_net_failed));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        onBackPressed();

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }


                }


            }
        });

    btn_cancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        onBackPressed();
    }
});
     Indback.setOnTouchListener(new View.OnTouchListener() {
         @Override
         public boolean onTouch(View view, MotionEvent motionEvent) {
             Intent bin = new Intent(IndividualSignUp.this, Signin.class);
             startActivity(bin);
             return false;
         }
     });
        txt_link_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(IndividualSignUp.this, TermsandConditions.class);
                startActivity(in);
            }
        });


    }
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                // set partyorderpersons picker as current partyorderpersons
                DatePickerDialog da = new DatePickerDialog(this, datePickerListener,
                        y, m, d);
                da.setCanceledOnTouchOutside(false);
                da.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CheckOut.this,"clicked cancel",Toast.LENGTH_LONG).show();
                    }
                });
                return da;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            y = selectedYear;
            m = selectedMonth;

            if(selectedMonth<9)
            {
                m=0+selectedMonth;
                mtest="0"+String.valueOf(selectedMonth+1);

                m=Integer.parseInt(mtest);

            }
            else{
                mtest=String.valueOf(selectedMonth+1);

            }

            if(selectedDay<10)
            {
                d=0+selectedDay;
                dtest="0"+String.valueOf(selectedDay);

                d=Integer.parseInt(dtest);

            }
            else{
                dtest=String.valueOf(selectedDay);

            }


            // set selected partyorderpersons into textview
            ed_dob.setText(new StringBuilder().append(dtest)
                    .append("/").append(mtest ).append("/").append(y));
            ed_dob.setEnabled(true);
            // set selected partyorderpersons into datepicker also


        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void getIndividualSignUp()
    {
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();

                // Create a progressdialog
                mProgressDialog = new ProgressDialog(IndividualSignUp.this);
                // Set progressdialog title
                mProgressDialog.setTitle("");

                mProgressDialog.setMessage(getString(R.string.loading));
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                // Show progressdialog
                mProgressDialog.show();


                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_signup");
                    jsonn.put("LastName",lastN);
                    jsonn.put("FirstName", firstN);
                    jsonn.put("Email", email);
                    jsonn.put("LoginUserIdType", LoginUserIdType);
                    jsonn.put("Phone",mobile);
                    jsonn.put("Address", " ");
                    jsonn.put("LoginType",logintype);
                    jsonn.put("NewsLetterSub",mail_Check);
                    jsonn.put("Password",password);
                    jsonn.put("CustomerTypeId",customertypeid);
                    jsonn.put("ModeId",modeId);
                    jsonn.put("Telephone",telno);
                    jsonn.put("CompanyRegNo"," ");
                    jsonn.put("Dob",dob);
                    jsonn.put("DeviceToken",sp.getdeviceId());
                    jsonn.put("ReferalCode",refferal);
                    jsonn.put("PostCode", " ");
                    jsonn.put("LandMark", " ");
                    jsonn.put("LocationId","1");
                    jsonn.put("CityId", "1");
                    //its used to pass 1 android in 2 in ios
                    jsonn.put("MobType","1");


                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    IndSign = EntityUtils.toString(resEntity);
                    IndSign = IndSign.trim();
                    JSONObject jasonob = new JSONObject(IndSign);
                     Indres = jasonob.getJSONArray("d").getJSONObject(0).optString("result").toString();




                if(Indres.length()>0) {
    if (Indres.equals("success")) {
        customername=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("FirstName").toString();
        customerId=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("CustomerId").toString();
        useremail=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("Email").toString();
        phone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("Phone").toString();

        customerlastname=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("LastName").toString();
        telephone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("Telephone").toString();
        dob_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("Dob").toString();
        psd_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).optString("Password").toString();


        SharedPreferences.Editor editor = myprefs.edit();
        editor.remove("adress");
        editor.remove("landmark");
        editor.remove("location");
        editor.remove("pincode");
        editor.putString("shaniusrid", customerId);
        editor.putString("bsrnme", customername);
        editor.putString("useremail", useremail);
        editor.putString("userphone", phone);
        editor.putString("lastname", customerlastname);
        editor.putString("dob", dob_pref);
        editor.putString("telephone", telephone);
        editor.putString("password", psd_pref);
        editor.apply();
        succ = true;
        mProgressDialog.dismiss();
        AlertDialog alertDialog = new AlertDialog.Builder(IndividualSignUp.this).create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage(getString(R.string.reg_success));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        ;

                        Intent in = new Intent(IndividualSignUp.this, MainActivity.class);


                        startActivity(in);
                        dialog.dismiss();
                    }
                });

        alertDialog.show();

    } else if (Indres.equals("Already Exist")) {
        mProgressDialog.dismiss();


        AlertDialog alertDialog = new AlertDialog.Builder(IndividualSignUp.this).create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage(getString(R.string.alert_user_exist));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        ;



                        Intent in = new Intent(IndividualSignUp.this, Signin.class);
                        startActivity(in);


                        dialog.dismiss();
                    }
                });

        alertDialog.show();


    } else {
        AlertDialog alertDialog = new AlertDialog.Builder(IndividualSignUp.this).create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage(getString(R.string.error));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        ;
                        dialog.dismiss();
                    }
                });

        alertDialog.show();

    }
}
else{
                    AlertDialog alertDialog = new AlertDialog.Builder(IndividualSignUp.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage( getString(R.string.alert_no_exist));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,  getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    ;
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();
            }
        };
        th.start();


    }
    protected String getDeviceId() {
        return Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {

                finish();
            }
            return false;
        }
        return true;
    }

    }

