package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.OrderReview;

/**
 * Created by basheer on 30/10/15.
 */
public class CustomOrderReview extends BaseAdapter{
    ViewHolder holder;

    Context context;
    String[] productName,stockstatuss,quantity;
    String  []unitPrice,amount;
    int totalNo;
    LayoutInflater inflater;
    boolean outofstock;
    int STR_PAYMENT_METHOD_ID_ID;
    Float USD_RATE_RATE;


   public  CustomOrderReview(Context con, String[] pName, String[] unit, String[] qty, String[] amoun, int totlNo, String[] stockstatus, int STR_PAYMENT_METHOD_ID, Float USD_RATE)
    {
        context=con;
        productName=pName;
        unitPrice=unit;
        quantity=qty;
        amount=amoun;
        totalNo=totlNo;
        stockstatuss=stockstatus;
        STR_PAYMENT_METHOD_ID_ID=STR_PAYMENT_METHOD_ID;
        USD_RATE_RATE=USD_RATE;
        inflater = LayoutInflater.from(this.context);

    }

    @Override
    public int getCount() {

        return totalNo;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class ViewHolder
    {
        TextView pName;
        TextView orderUnit;
        TextView orderQuantity;
        TextView orderAmount;
        TextView stockstatus;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;
        if (view == null) {
            vi = inflater.inflate(R.layout.custom_order_review, null);
            holder = new ViewHolder();
            holder.pName = (TextView) vi.findViewById(R.id.pNmae);
            holder.orderUnit = (TextView) vi.findViewById(R.id.unitPrice);
            holder.orderQuantity = (TextView) vi.findViewById(R.id.quantity);
            holder.orderAmount = (TextView) vi.findViewById(R.id.amount);
            holder.stockstatus = (TextView) vi.findViewById(R.id.txt_id_outofstock);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

            for(int i=0;i<totalNo;i++) {

                if (STR_PAYMENT_METHOD_ID_ID==5){
                    holder.pName.setText(productName[position]);
                    // Log.i("image",image);
                    //holder.orderUnit.setText(unitPrice[position]+"0");
                    holder.orderUnit.setText("$"+String.format("%.2f",Float.parseFloat(unitPrice[position])/USD_RATE_RATE));

                    //Log.i("unitprice",unitPrice);
                    holder.orderQuantity.setText(quantity[position]);
                    holder.orderAmount.setText("$"+String.format("%.2f",Float.parseFloat(amount[position])/USD_RATE_RATE));
                    if (stockstatuss[position].equals("Out of Stock")) {
                        holder.stockstatus.setVisibility(View.VISIBLE);
                        outofstock = true;

                    }
                }else {
                    holder.pName.setText(productName[position]);
                    // Log.i("image",image);
                    holder.orderUnit.setText(unitPrice[position]+"0");
                    //Log.i("unitprice",unitPrice);
                    holder.orderQuantity.setText(quantity[position]);
                    holder.orderAmount.setText(amount[position]+"0");
                    if (stockstatuss[position].equals("Out of Stock")) {
                        holder.stockstatus.setVisibility(View.VISIBLE);
                        outofstock = true;

                    }
                }


            }

        if(outofstock==true){

            ((OrderReview)context).setMyAlert();

        }

        return vi;
    }

}




