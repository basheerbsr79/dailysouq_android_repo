

 package shani.netstager.com.dailysouq.activity;

/**
 * Created by prajeeshkk on 16/09/15.
 */

 import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomGridViewAdapter;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
 import shani.netstager.com.dailysouq.support.JSONfunctions;
 import shani.netstager.com.dailysouq.support.Json;
 import shani.netstager.com.dailysouq.support.jSonfiles;


 public class Categories extends Activity {
    ImageView footeshop,footerdeal,footersp,footergft,footermore;
    String[] trno;
    String[] trnm;
    String[] rlno;

    ImageView profile,quickcheckout,Notification;

    RelativeLayout cartcounttext;
    TextView title;
    Button back_btn;
    String details = "";
    GridView gridView;
    JSONObject jsonobject;
    JSONArray jsonarray;
    int  str_json_cart_count;

    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;

    ImageView serchgobtn;

    String srchtext;

    TextView test;
    //serch test
    String userVLogin;
    SharedPreferences myprefs;

    CustomGridViewAdapter adapter;
    ProgressDialog mProgressDialog;
     TextView cartcountbadge,notifbadge;
     DS_SP sp;
    int DEFAULT_LANGUGE;
     // its used for quick check out
     String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
     int str_paymentmethod_payment;

    public Categories() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);


        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogin = myprefs.getString("shaniusrid", null);

        DEFAULT_LANGUGE=myprefs.getInt("DEFAULT_LANGUGE",1);
        if(userVLogin !=null){
            userVLogin =myprefs.getString("shaniusrid", userVLogin);

        }
        else{
            userVLogin ="0";

        }


        //serch test
        catogoryspinnerinsrch=(EditText)findViewById(R.id.catSpinner);
        serchtext=(MultiAutoCompleteTextView)findViewById(R.id.Search);
        serchgobtn=(ImageView)findViewById(R.id.goButton);


        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });

            serchgobtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    srchtext=serchtext.getText().toString();
                    if(serchtext.length()==0){
                        serchtext.setError(getString(R.string.srh_here_error));
                    }
                    else {

                        Intent in = new Intent(Categories.this, SerchResult.class);
                        in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                        in.putExtra("text", srchtext);

                        startActivity(in);

                    }
                }
            });



        //serch test



        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        back_btn=(Button)findViewById(R.id.menue_btn);
        title=(TextView)findViewById(R.id.namein_title);
        title.setText(getString(R.string.catogories));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(Categories.this, Notification.class);

                startActivity(in);

            }
        });

        footeshop=(ImageView)findViewById(R.id.imageView11);
        footersp=(ImageView)findViewById(R.id.imageView13);
        footermore=(ImageView)findViewById(R.id.imageView15);
        footerdeal=(ImageView)findViewById(R.id.imageView12);
        footergft=(ImageView)findViewById(R.id.imageView14);
        footermore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,Menu_List.class);

                startActivity(in);
            }
        });
        footersp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,DsSpecials.class);

                startActivity(in);
            }
        });
        footerdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,DsDeals.class);

                startActivity(in);

            }
        });
        footergft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Categories.this,DsGiftCard.class);

                startActivity(in);

            }
        });

        footeshop.setImageResource(R.drawable.footershopselect);
        //setContentView(R.layout.searchbar);



        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
            new AsyncTaskCartcounttJson().execute();
            new DownloadJSON().execute();
        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(Categories.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        gridView = (GridView)findViewById(R.id.gridView);


        // used to set new sp based count
       // setcartcountwihoutapi();
    }

     void setcartcountwihoutapi(){

        Log.i("countcart",sp.getcartcount()+"");
         if (sp.getcartcount()==0) {
             cartcountbadge.setVisibility(View.GONE);
         } else {
             if(userVLogin.trim().equals("0")){
                 cartcountbadge.setVisibility(View.GONE);
             }else {
                 Log.i("countcartwork",sp.getcartcount()+"");
                 cartcountbadge.setVisibility(View.VISIBLE);
                 cartcountbadge.setText(sp.getcartcount()+"");
             }
         }
     }

     //bsr new popup strt srch
     private void SerachTypePopup() {
         LayoutInflater layoutInflater
                 = (LayoutInflater)getBaseContext()
                 .getSystemService(LAYOUT_INFLATER_SERVICE);
         View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
         final PopupWindow popupWindow = new PopupWindow(
                 popupView,
                 ActionBar.LayoutParams.WRAP_CONTENT,
                 ActionBar.LayoutParams.WRAP_CONTENT);
         final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
         final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
         final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
         final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
         all.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 catogoryspinnerinsrch.setText(all.getText());
                 popupWindow.dismiss();
             }
         });
         products.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 catogoryspinnerinsrch.setText(products.getText());
                 popupWindow.dismiss();
             }
         });
         catogory.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 catogoryspinnerinsrch.setText(catogory.getText());
                 popupWindow.dismiss();
             }
         });
         prdctcode.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 catogoryspinnerinsrch.setText(prdctcode.getText());
                 popupWindow.dismiss();
             }
         });




         popupWindow.setOutsideTouchable(true);
         popupWindow.setBackgroundDrawable(null);
         popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
         popupWindow.setOutsideTouchable(true);
         popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



     }



     // bsr edit popup end srch

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    private class DownloadJSON extends AsyncTask<Void, Void, Void> {
        int prdlegth=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Categories.this);
            // Set progressdialog title

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            // arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            details = JSONfunctions.getJSONfromURL(DEFAULT_LANGUGE);
           // prdlegth=details.length();
            try {
                // Locate the array name in JSON
                jSonfiles.parseStationlist(details);


                int count1 = jSonfiles.stationLen;
                prdlegth=count1;


                trno = new String[count1];
                trnm = new String[count1];
                rlno = new String[count1];


                for (int i = 0; i < count1; i++) {


                        rlno[i] = jSonfiles.stationName[i].toString();

                    trnm[i] = jSonfiles.age[i];

                    trno[i] = jSonfiles.stationCode[i];


                }




            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void args) {
            mProgressDialog.dismiss();
            if(prdlegth==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Categories.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {


                adapter = new CustomGridViewAdapter(Categories.this, rlno, trnm, trno,cartcounttext,cartcountbadge,userVLogin);
                // Set the adapter to the ListView
                gridView.setAdapter(adapter);
                // Close the progressdial

            }

        }


    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogin);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                 str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                sp.setcartcount(str_json_cart_count);
                prdlength=dataJsonArr.length();




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                if(prdlength==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Categories.this).create();
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.show();
                }else {
                    sp.setcartcount(str_json_cart_count);
                    if (sp.getcartcount()==0) {
                        cartcountbadge.setVisibility(View.GONE);
                    } else {
                        if(userVLogin.trim().equals("0")){
                            cartcountbadge.setVisibility(View.GONE);
                        }else {
                            cartcountbadge.setVisibility(View.VISIBLE);
                            cartcountbadge.setText(sp.getcartcount()+"");
                        }
                    }
                }
                }

            //    Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }


     public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

         final String TAG = "AsyncTaskParseJson.java";
         JSONArray dataJsonArr_quick;
        int prdlegth=0;



         @Override
         protected String doInBackground(String... arg0) {
             try {


                 Json jParser = new Json();
                 JSONObject json = jParser.quick_checkout(userVLogin);
                 JSONObject productObj = new JSONObject(Json.prdetail);

                 dataJsonArr_quick = productObj.getJSONArray("d");

                 str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                 str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                 str_contact_name_quick_checkout= dataJsonArr_quick.optJSONObject(0).optString("ContactName");
                 str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                 str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                    prdlegth=dataJsonArr_quick.length();

             } catch (JSONException e) {
                 e.printStackTrace();
             }
             return null;
         }

         @Override
         protected void onPostExecute(String s) {
             super.onPostExecute(s);
//
             if(prdlegth==0){
                 AlertDialog alertDialog = new AlertDialog.Builder(Categories.this).create();
                 alertDialog.setCancelable(false);
                 alertDialog.setCanceledOnTouchOutside(false);
                 alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                 alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int which) {
                                 dialog.dismiss();
                             }
                         });

                 alertDialog.show();
             }
             else{


                 if(sp.getcartcount()==0){
                     AlertDialog alertDialog = new AlertDialog.Builder(Categories.this).create();
                     alertDialog.setCancelable(false);
                     alertDialog.setCanceledOnTouchOutside(false);
                     alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                     alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog, int which) {
                                     dialog.dismiss();
                                 }
                             });

                     alertDialog.show();
                 }else {


                     if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                         str_paymentmethod_payment = 1;


                     } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                         str_paymentmethod_payment = 2;

                     } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                         str_paymentmethod_payment = 3;


                     } else {
                         str_paymentmethod_payment = 1;


                     }
                     SharedPreferences.Editor editor = myprefs.edit();
                     editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                     editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                     editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                     editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                     editor.apply();


                     Intent in = new Intent(Categories.this, OrderReview.class);
                     in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                     in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                     in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                     startActivity(in);

                 }
             }

         }
     }


}