package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.PartyOrderPopupItemsShow;


/**
 * Created by MohammedBasheer on 16-06-2016.
 */


public class partyOrderItemshowPopup extends BaseAdapter {

    Context context;
    ViewHolder holder;


    String []str_prdname,str_prdid,str_prd_qty,str_subtotal,str_stockstatus,str_need_act_qty;
    String  []unitPrice,amount;
    int totalNo;
    LayoutInflater inflater;
    boolean outofstock;
    String cusid;
    JSONArray dataJsonArr;
    String prdid,prdsizeid,prdqty;
    String noofperson;
    String responce_json;
    String partyorderid;
    int language;
    ProgressDialog mProgressDialog;
    public partyOrderItemshowPopup(Context context,String[] id,String[] name,String[] qty,String []subtotal,String[] stockstatus,String cusid,String partyorderid,int language,String noofperson,String []str_need_act_qty) {

        this.context=context;
        str_prdid=id;
        str_prdname=name;
        this.language=language;
        str_prd_qty=qty;
        this.str_need_act_qty=str_need_act_qty;
        this.noofperson=noofperson;
        str_subtotal=subtotal;
        str_stockstatus=stockstatus;
        this.cusid = cusid;
        this.partyorderid=partyorderid;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return str_prdid.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder
    {
        TextView txtname;
        TextView txtqty;
        TextView txtrqdqty;
        ImageView imRemove;
        TextView subtotal;
        Button btnAdd,btnMinus;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.custom_item_popup_list, null);
            holder = new ViewHolder();
            holder.txtname = (TextView) vi.findViewById(R.id.name);
            holder.subtotal = (TextView) vi.findViewById(R.id.subtotal);
            holder.txtqty = (TextView) vi.findViewById(R.id.qty);
            holder.txtrqdqty = (TextView) vi.findViewById(R.id.stock);
            holder.imRemove = (ImageView) vi.findViewById(R.id.remove);

            holder.btnMinus = (Button) vi.findViewById(R.id.btn_minus);
            holder.btnAdd = (Button) vi.findViewById(R.id.btn_add);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

            holder.txtname.setText(str_prdname[position]+"");
            // Log.i("image",image);
        String lineSep = System.getProperty("line.separator");
        String yourString= str_prd_qty[position];


        yourString= yourString.replaceAll("<br />", lineSep);


            holder.txtqty.setText(yourString+"");
            //Log.i("unitprice",unitPrice);

            holder.txtrqdqty.setText(str_stockstatus[position]+"");



             holder.subtotal.setText(str_subtotal[position]+"");

            holder.imRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id=str_prdid[position];
                    new AsyncTaskRemovetJson(id).execute();

                }
            });

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //  Toast.makeText(context,"Need to add a qty",Toast.LENGTH_LONG).show();
                    int qty= Integer.parseInt(str_need_act_qty[position]);
                    qty++;
                    // holder.txtqty.setText(qty+"");


                    String id=str_prdid[position];
                    new AsyncTaskUpdatetJson(qty,id).execute();

                }
            });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(context,"Need to minus a qty",Toast.LENGTH_LONG).show();
                int qty= Integer.parseInt(str_need_act_qty[position]);
                if(qty>=1) {
                    qty--;
                }
                // holder.txtqty.setText(qty+"");


                String id=str_prdid[position];
                new AsyncTaskUpdatetJson(qty,id).execute();

            }
        });

        return vi;
    }

    public class AsyncTaskRemovetJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        String id;

        AsyncTaskRemovetJson(String  id){
            this.id=id;
        }

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(context);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(context.getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.partyorderremovewithid(id,cusid);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                // do stufff here
                Intent in=new Intent(context,PartyOrderPopupItemsShow.class);
                in.putExtra("PID",partyorderid);
                in.putExtra("LANID",language);
                in.putExtra("USERID",cusid);
                in.putExtra("NAME","Party Order");
                in.putExtra("NOPER",noofperson);
                ((Activity)context).finish();
                context.startActivity(in);            }

            // mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskUpdatetJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;
        String cartid;

        AsyncTaskUpdatetJson(int qty,String cartid){

            this.qty=qty;
            this.cartid=cartid;
        }
        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(context);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(context.getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.partryorderupdatewithindivituelqty(cusid,qty,cartid);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");
                //   noofperson = dataJsonArr.getJSONObject(0).optString("Qty");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                // do stufff here
                Intent in=new Intent(context,PartyOrderPopupItemsShow.class);
                in.putExtra("PID",partyorderid);
                in.putExtra("LANID",language);
                in.putExtra("USERID",cusid);
                in.putExtra("NAME","Party Order");
                in.putExtra("NOPER",noofperson);
                ((Activity)context).finish();
                context.startActivity(in);
            }

            // mProgressDialog.dismiss();
        }
    }

}
