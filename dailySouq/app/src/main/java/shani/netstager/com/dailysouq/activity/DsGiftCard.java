package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.productGridViewAdapter;
import shani.netstager.com.dailysouq.models.GiftCardModel;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.Json;

public class DsGiftCard extends AppCompatActivity {
    //used for end less

    private TextView tvEmptyView;
    private RecyclerView gridViews;
    private productGridViewAdapter mAdapter;
    ArrayList<ProductModel> productModels;
    int pg = 0;

    ArrayList<GiftCardModel> giftCardModels;
    private GridLayoutManager lLayout;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;


    int productLength;
    ImageView footeshop,footerdeal,footersp,footergft,footermore;

    View footer;
    ProgressDialog dialog;
    //its used for new scroll

    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    ImageView profile,quickcheckout,Notification;
    Button back_btn;
    TextView title;
    RelativeLayout cartcounttext;
    String str_json_cart_count,str_address_quick_checkout,str_opt_payment,str_contact_name_quick_checkout,str_location_quick_checkout,str_phone_quick_checkout;
    int sizeProduct,DEFAULT_LANGUGE,str_paymentmethod_payment;
    public SharedPreferences myprefs,myProducts;
    ProgressDialog mProgressDialog;
    String userVLogIn;
    JSONArray dataJsonArr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ds_gift_card);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }


        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        //used for endless scroll
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        pgresbar_scrool_footer=(ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        giftCardModels = new ArrayList<>();
        productModels = new ArrayList<>();
        lLayout = new GridLayoutManager(DsGiftCard.this, 2);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);

        back_btn=(Button)findViewById(R.id.menue_btn);
        profile=(ImageView)findViewById(R.id.profile);
        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        title.setText("Gift Cards");
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        cartcountbadge =(TextView)findViewById(R.id.badge);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent in=new Intent(CartView.this,MainActivity.class);
                onBackPressed();
                //startActivity(in);
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent in = new Intent(CartView.this, MainActivity.class);
                onBackPressed();
                //startActivity(in);
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsGiftCard.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(DsGiftCard.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(DsGiftCard.this, Notification.class);

                startActivity(in);

            }
        });

        if (userVLogIn!="0") {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent.equals(true)) {
                new AsyncTaskCartcounttJson().execute();
                new AsyncTaskParseJson(pg).execute();

            } else {

                AlertDialog alertDialog = new AlertDialog.Builder(DsGiftCard.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_net_failed));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();

            }
        }else {
            AlertDialog alertDialog = new AlertDialog.Builder(DsGiftCard.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_pls_signin));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent in=new Intent(DsGiftCard.this,Signin.class);

                            startActivity(in);
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlegth=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr.optJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactNo");
                prdlegth=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prdlegth==0){
                AlertDialog alertDialog = new AlertDialog.Builder(DsGiftCard.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{

                if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                    str_paymentmethod_payment = 1;


                }
                else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                    str_paymentmethod_payment = 2;

                }
                else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                    str_paymentmethod_payment = 3;


                }
                else {
                    str_paymentmethod_payment = 1;


                }
                SharedPreferences.Editor editor=myprefs.edit();
                editor.putString("ORDERREVIEW_NAME",str_contact_name_quick_checkout );
                editor.putString("ORDERREVIEW_ADDRESS",str_address_quick_checkout );
                editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                editor.apply();


                Intent in =new Intent( DsGiftCard.this,OrderReview.class);
                in.putExtra("PAYMENT_METHOD_TYPE_ID",str_paymentmethod_payment);
                in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                startActivity(in);


            }

        }
    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                prdlength=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prdlength==0){
                AlertDialog alertDialog = new AlertDialog.Builder(DsGiftCard.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }
            else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }

            }

            //  mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;

        public AsyncTaskParseJson(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.giftcardall(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);


                    //Shakeeb code
                    ProductModel product = new ProductModel();
                    product.productId = dataJsonArr.getJSONObject(i).getString("ProductId");

                    product.productName = c.getString("ProductShowName").toString();
                    product.imagepath = c.getString("Imagepath").toString();
                    product.isNew = c.getString("IsNew");
                    product.brandId = c.getString("BrandId");
                    product.category = c.getString("CategoryId");
                    product.customerId = userVLogIn;

                    ArrayList<SizeModel> sizes = new ArrayList<>();

                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
                    if (sizeProduct > 0) {
                        for (int j = 0; j < sizeProduct; j++) {
                            SizeModel sizeModel = new SizeModel();
                            sizeModel.productsize = sizeArray.getJSONObject(j).getString("ProductSize");
                            sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                            sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                            sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                            sizeModel.savaPrice = sizeArray.getJSONObject(j).optString("Saveprice");
                            sizeModel.discount = sizeArray.getJSONObject(j).optInt("Discount");
                            sizeModel.stockstatus=sizeArray.getJSONObject(j).optString("Stockstatus");
                            sizes.add(sizeModel);

                        }
                    }
                    product.sizes = sizes;
                    productModels.add(product);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);


                    SharedPreferences.Editor editor = myProducts.edit();


                    editor.putString("myCatid", product.category);
                    editor.putString("mypid", product.productId);
                    editor.apply();

                }


                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<ProductModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(DsGiftCard.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            }else {


                pg = pg + 1;
                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }
                if (productModels.size() <= 10) {
                    mAdapter = new productGridViewAdapter(productModels, gridViews, DsGiftCard.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);
                } else {
                    mAdapter.setProductlist(productModels);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();
                }


                /*mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {


                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                new AsyncTaskParseJson(pg).execute();


                            }
                        }, 100);

                    }
                });*/


            }


        }
    }


        //importent its used for the ui based gift card
    /*public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<GiftCardModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;


        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<GiftCardModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.giftcardall();
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Log.i("responce",dataJsonArr.toString());
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);

                    GiftCardModel giftmodel=new GiftCardModel();

                    giftmodel.giftcardid=c.getString("GiftCardId").toString();
                    giftmodel.giftcardname=c.getString("GiftCardName").toString();
                    giftmodel.giftcardnamemal=c.getString("GiftCardNameMal").toString();
                    giftmodel.giftcartdno=c.getString("GiftCardNo").toString();
                    giftmodel.giftcardamount=c.getString("GiftCardAmt").toString();
                    giftmodel.giftcardoldamount=c.getString("GiftCardOldAmt").toString();
                    giftmodel.isshow=c.getString("IsShow").toString();
                    giftmodel.expiredate=c.getString("ExpiryDate").toString();

                    giftCardModels.add(giftmodel);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);


                }



                return giftCardModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<GiftCardModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(DsGiftCard.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            }else {

                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }

                    mAdapter = new giftCardViewAdapter(productModels, gridViews, DsGiftCard.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);

                    mAdapter.setProductlist(productModels);
                    //mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();






            }


        }
    }*/
}
