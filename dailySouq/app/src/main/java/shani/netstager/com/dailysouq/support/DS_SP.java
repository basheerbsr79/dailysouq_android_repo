package shani.netstager.com.dailysouq.support;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by MohammedBasheer on 25-05-2016.
 */
public class DS_SP {

    final  static int notifcount=1;
    final  static int testcount=2;
    final  static int cartcount=0;
    final  static  boolean blnfirsttimeonly=false;
    final  static  boolean lanselection=true;
    final  static  String cookeryfirttime="not";
    final  static String promoentred="5";
    final  static  String gcmdevicetoken="15";
    final  static  String usersign="16";
    final  static  String usrpsdsign="17";
    final  static  boolean quizuserfirst=false;
    private final static String SHARED_PREFS = "settings";
    private SharedPreferences prefs;
    private SharedPreferences.Editor edit;
    public DS_SP(Context context) {
        prefs = context
                .getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public void setnotcount(int Text) {

        edit = prefs.edit();
        edit.putInt(String.valueOf(notifcount), Text);

        edit.commit();
    }
    public int getnotcount() {

        return prefs.getInt(String.valueOf(notifcount), 0);
    }

    public  void  setcartcount(int s){
        edit = prefs.edit();
        edit.putInt(String.valueOf(cartcount), s);

        edit.commit();
    }
    public  int getcartcount(){
        return  prefs.getInt(String.valueOf(cartcount),0);
    }

    public  void setbolfirsttimeonly(boolean b){
        edit=prefs.edit();
        edit.putBoolean(String.valueOf(blnfirsttimeonly),b);
        edit.commit();
    }
    public boolean getbolfirsttimeonly(){
        return prefs.getBoolean(String.valueOf(blnfirsttimeonly),false);
    }

    public  void setbolpromoentrd(String b){
        edit=prefs.edit();
        edit.putString(String.valueOf(promoentred),b);
        edit.commit();
    }
    public String getbolpromoentred(){
        return prefs.getString(String.valueOf(promoentred),"");
    }

    public void settestpromo(int Text) {
        edit = prefs.edit();
        edit.putInt(String.valueOf(testcount), Text);
        edit.commit();
    }
    public int gettestpromo() {

        return prefs.getInt(String.valueOf(testcount), 0);
    }

    public  void setcookerytemp(String b){
        edit=prefs.edit();
        edit.putString(String.valueOf(cookeryfirttime),b);
        edit.commit();
    }
    public String getcookerytemp(){

        return prefs.getString(String.valueOf(cookeryfirttime),"");
    }

    public void setdeviceId(String token) {
        edit=prefs.edit();
        edit.putString(gcmdevicetoken,token);
        edit.commit();
    }
    public String getdeviceId(){

        return prefs.getString(gcmdevicetoken,"");
    }

    public  void setQuizuserfirst(boolean b){
        edit=prefs.edit();
        edit.putBoolean(String.valueOf(quizuserfirst),b);
        edit.commit();
    }
    public boolean getQuizuserfirst(){
        return prefs.getBoolean(String.valueOf(quizuserfirst),false);
    }

    public void setusersign(String token) {
        edit=prefs.edit();
        edit.putString(usersign,token);
        edit.commit();
    }
    public String getusersign(){

        return prefs.getString(usersign,"");
    }

    public void setusrpsdsign(String token) {
        edit=prefs.edit();
        edit.putString(usrpsdsign,token);
        edit.commit();
    }
    public String getuserpsdsign(){

        return prefs.getString(usrpsdsign,"");
    }


    public  void setlanselection(boolean b){
        edit=prefs.edit();
        edit.putBoolean(String.valueOf(lanselection),b);
        edit.commit();
    }
    public boolean getlanselection(){
        return prefs.getBoolean(String.valueOf(lanselection),false);
    }


    public void clearall(){
        edit=prefs.edit();
        edit.putBoolean(String.valueOf(false),false);
        edit.putString("","");
        edit.putString(String.valueOf(""),"");
        edit.putInt(String.valueOf(0), 0);
        edit.putString(String.valueOf(""),"");
        edit.putBoolean(String.valueOf(false),false);
        edit.putInt(String.valueOf(0), 0);
        edit.putInt(String.valueOf(0), 0);
       /* edit.putString(usrpsdsign,"");
        edit.putString(usersign,"");*/
        edit.commit();
    }

}
