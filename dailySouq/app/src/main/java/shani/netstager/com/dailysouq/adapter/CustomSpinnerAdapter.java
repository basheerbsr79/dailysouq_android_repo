package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.models.SizeModel;

/**
 * Created by prajeeshkk on 26/10/15.
 */
public class CustomSpinnerAdapter extends BaseAdapter {

    ArrayList<SizeModel> myList = new ArrayList<SizeModel>();
    LayoutInflater inflater;
    Context context;


    public CustomSpinnerAdapter(Context context, ArrayList<SizeModel> myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    public CustomSpinnerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);

    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public SizeModel getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        SizeModel currentListData = getItem(position);
       // Log.i("model",currentListData.productsize);
        mViewHolder.tvTitle.setText(currentListData.productsize);


        return convertView;
    }

    public void setdata(ArrayList<SizeModel> sizes) {
        this.myList = sizes;
    }

    private class MyViewHolder {
        TextView tvTitle, tvDesc;

        public MyViewHolder(View item) {
            tvTitle = (TextView) item.findViewById(android.R.id.text1);

        }
    }
}