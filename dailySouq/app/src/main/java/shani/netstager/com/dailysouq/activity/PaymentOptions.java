package shani.netstager.com.dailysouq.activity;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomChooseAddress;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class  PaymentOptions extends AppCompatActivity {
    TextView title;
    Button Confirm, bck_btn;
    ImageView profile, quickcheckout, Notification;
    RelativeLayout cartcounttext;

    JSONArray dataJsonArr;
    RadioGroup paymentradio;
    RadioButton paymentoptionradio;
    public SharedPreferences myprefs, myProducts;
    //RadioButton radioButton_cod,radioButton_ewallet,radioButton_netbank,radioButton_crordr;
    String userVLogIn, str_deliverydate_payment, str_json_cart_count, str_deliverytime_payment, str_deliveryadresid_payment, str_opt_payment_quick;
    ProgressDialog mProgressDialog;
    int str_paymentmethod_payment;
    String str_paymentmethod_payment_name;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    int DEFAULT_LANGUGE;
    String categoryId, prid;

    TwoWayView recoomendedList,recentlist;

    LinearLayout recmnded_layout,recnt_layout;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment_quick;

    //for payment summery
    TextView txtItems,txtSubtotal,txtDiscount,txtYousave,txtDeliverycharge,txtDeliveryalert,txtNettotal;
    int str_del_amnt;

    //for transfer order
    Button btnTransferorder;
    TextView txtTransfertext;
    int linkedusercount=0;
    String [] arrlinkedusername,arrlinkeduserid;
    String strOrderIdAfterSave="0",strTransferCusId="0",strSelectedTransfercusName="";
    LinearLayout lndeltext,lndelalert;
    //for gift card
    LinearLayout lnGiftFull;
    LinearLayout lnGiftFullApplied;
    Button btnGiftSubmit;
    EditText edGiftCoupon;
    Spinner edGiftDropDown;
    String strGiftCoupen;
    ArrayList arGiftDropDownVoucher;
    ArrayList arGiftDropDownCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);
        recmnded_layout=(LinearLayout)findViewById(R.id.linearLayout33);
        recnt_layout=(LinearLayout)findViewById(R.id.linearLayout36);
        paymentradio = (RadioGroup) findViewById(R.id.paymentradiogrp);
        Confirm = (Button) findViewById(R.id.confirm_order_payment);
        //for payment summery
        txtItems=(TextView)findViewById(R.id.items);
        txtSubtotal=(TextView)findViewById(R.id.subTotal);
        txtDiscount=(TextView)findViewById(R.id.discount);
        txtYousave=(TextView)findViewById(R.id.yousave);
        txtDeliverycharge=(TextView)findViewById(R.id.deliveryCharge);
        txtDeliveryalert=(TextView)findViewById(R.id.txtdelalert);
        txtNettotal=(TextView)findViewById(R.id.total);
        //for hide del while no amt
        lndelalert=(LinearLayout)findViewById(R.id.linearLayout60);
        lndeltext=(LinearLayout)findViewById(R.id.linearLayout59) ;
        //for transfer order
        btnTransferorder=(Button)findViewById(R.id.trnsfer_order_payment);
        txtTransfertext =(TextView)findViewById(R.id.txttrnsfer_text) ;
        //for gift card
        lnGiftFull=(LinearLayout)findViewById(R.id.ln_promo_full_one);
        lnGiftFullApplied=(LinearLayout)findViewById(R.id.ln_promo_full_two);
        btnGiftSubmit=(Button)findViewById(R.id.btn_giftcard);
        edGiftCoupon=(EditText)findViewById(R.id.ed_gift_enter) ;
        edGiftDropDown=(Spinner) findViewById(R.id.edgiftDropDown) ;

        profile = (ImageView) findViewById(R.id.profile);
        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        title = (TextView) findViewById(R.id.namein_title);
        recoomendedList = (TwoWayView) findViewById(R.id.recommendedList);
        recentlist = (TwoWayView) findViewById(R.id.recentList);
        bck_btn = (Button) findViewById(R.id.menue_btn);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE",1);
        userVLogIn = myprefs.getString("shaniusrid", null);
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);
        categoryId = myProducts.getString("myCatid", "1");
        prid = myProducts.getString("mypid", "1");
        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }





paymentradio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int selectedId = paymentradio.getCheckedRadioButtonId();
        if (selectedId != 0) {
            // find the radiobutton by returned id

            paymentoptionradio = (RadioButton) findViewById(selectedId);

            str_opt_payment = paymentoptionradio.getText().toString().trim();
            if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                /*str_paymentmethod_payment = 1;
                str_paymentmethod_payment_name = getString(R.string.cash_on_delivery);
                new AsyncTaskCheckJson().execute();*/

            } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
               /* str_paymentmethod_payment = 2;
                str_paymentmethod_payment_name = getString(R.string.creditdebitcard);
                new AsyncTaskCheckJson().execute();*/

            } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                /*str_paymentmethod_payment = 3;
                str_paymentmethod_payment_name = getString(R.string.ewallet);
                new AsyncTaskCheckJson().execute();*/


            } else if (str_opt_payment.equals(getString(R.string.giftcard))) {
                /*str_paymentmethod_payment = 4;
                str_paymentmethod_payment_name = getString(R.string.giftcard);
                new AsyncTaskCheckJson().execute();*/


            } else if (str_opt_payment.equals(getString(R.string.paypal))) {

                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentOptions.this);
                builder.setTitle("DailySouq");
                builder.setMessage("The amount will be converted to USD, Do you need to continue ?")
                        .setInverseBackgroundForced(false)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        /*str_paymentmethod_payment = 5;
                                        str_paymentmethod_payment_name = getString(R.string.paypal);
                                        new AsyncTaskCheckJson().execute();*/

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        paymentoptionradio = (RadioButton) findViewById(R.id.codradio);

                                        paymentoptionradio.setChecked(true);
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();


            }
        }
    }
});















        //badge
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {
            Intent in = getIntent();
            str_deliverydate_payment = in.getStringExtra("DELIVERYDATE_CHECKOUT");
            str_deliverytime_payment = in.getStringExtra("DELIVERYTIME_CHECKOUT");
            str_deliveryadresid_payment = in.getStringExtra("DELIVERYADDRESS_CHECKOUT");
            new AsyncTaskDeliveryAmountAlert().execute();
            new AsyncTaskGiftVouchers().execute();
        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        title.setText(getString(R.string.title_activity_payment_options));
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();}
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PaymentOptions.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PaymentOptions.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(PaymentOptions.this, Notification.class);

                startActivity(in);
            }
        });
        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = paymentradio.getCheckedRadioButtonId();
                if (selectedId != 0) {
                    // find the radiobutton by returned id

                    paymentoptionradio = (RadioButton) findViewById(selectedId);

                    str_opt_payment = paymentoptionradio.getText().toString().trim();
                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;
                        str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);
                        new AsyncTaskCheckJson().execute();

                    }
                    else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;
                        str_paymentmethod_payment_name=getString(R.string.creditdebitcard);
                        new AsyncTaskCheckJson().execute();

                    }
                    else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;
                        str_paymentmethod_payment_name=getString(R.string.ewallet);
                        new AsyncTaskCheckJson().execute();


                    }
                    else if (str_opt_payment.equals(getString(R.string.giftcard))) {
                        str_paymentmethod_payment = 4;
                        str_paymentmethod_payment_name=getString(R.string.giftcard);
                        new AsyncTaskCheckJson().execute();


                    }else if (str_opt_payment.equals(getString(R.string.paypal))) {
                        str_paymentmethod_payment = 5;
                        str_paymentmethod_payment_name=getString(R.string.paypal);
                        new AsyncTaskCheckJson().execute();
                       /* AlertDialog.Builder builder=new AlertDialog.Builder(PaymentOptions.this);
                        builder.setTitle("DailySouq");
                        builder.setMessage("The amount will be converted to USD, Do you need to continue ?")
                                .setInverseBackgroundForced(false)
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int id) {
                                                str_paymentmethod_payment = 5;
                                                str_paymentmethod_payment_name=getString(R.string.paypal);
                                                new AsyncTaskCheckJson().execute();

                                            }
                                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();*/
                    }
                    else {
                        str_paymentmethod_payment = 1;
                        str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);
                        new AsyncTaskCheckJson().execute();

                    }
                } else {
                    selectedId = R.id.codradio;
                    paymentoptionradio = (RadioButton) findViewById(selectedId);
                    paymentoptionradio.setError(getString(R.string.plz_select));
                }


            }
        });


        /*//for transfer ordr


        if(linkedusercount!=0) {
            btnTransferorder.setVisibility(View.VISIBLE);
            txtTransfertext.setVisibility(View.VISIBLE);
            btnTransferorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgressAlert("Need to impliment");
                }
            });
        }*/



        // used to set new sp based count
        setcartcountwihoutapi();
        setnewProgressbarClicks();
        setGiftCardOption();
    }

    private void setGiftCardOption() {

        /*paymentradio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                    if(checkedId== R.id.giftcardradio) {
                        // do operations specific to this selection
                        //showProgressAlert("Clicked gift cart show events");

                        lnGiftFull.setVisibility(View.VISIBLE);
                    }else {

                        lnGiftFull.setVisibility(View.GONE);

                    }
            }
        });*/

      /*  edGiftDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });*/

       // showGiftDropDown();

        //for gift clicks
        btnGiftSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 strGiftCoupen=edGiftCoupon.getText().toString();
               // showProgressAlert(strGiftCoupen+" is valid coupon");
               new  AsyncTaskSaveGift().execute();
            }
        });


    }

    private void showGiftDropDown() {

        ArrayAdapter ad=new ArrayAdapter(PaymentOptions.this,R.layout.support_simple_spinner_dropdown_item,arGiftDropDownVoucher);
        edGiftDropDown.setAdapter(ad);
        edGiftDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                edGiftCoupon.setText(arGiftDropDownCode.get(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                edGiftDropDown.setPrompt("select  Gift Voucher Type");
                edGiftCoupon.setText("");
            }
        });



    }

    private void setnewProgressbarClicks() {
        ImageView ivstpcart,ivstpdelivery,ivstpPayment,ivstpConfirm;
        ivstpcart=(ImageView)findViewById(R.id.ivStpCart);
        ivstpdelivery=(ImageView)findViewById(R.id.ivStpDelivery);
        ivstpPayment=(ImageView)findViewById(R.id.ivStpPayment);
        ivstpConfirm=(ImageView)findViewById(R.id.ivStpConfirm);

        ivstpcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(PaymentOptions.this,CartView.class);
                startActivity(in);
            }
        });
        ivstpdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ivstpPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        ivstpConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPaymentok();
            }
        });

    }

    public void showProgressAlert(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(PaymentOptions.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showProgressAlertDone(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(PaymentOptions.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();
                                new AsyncTaskDeliveryAmountAlert().execute();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showProgressAlertforLinked(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(PaymentOptions.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public  void  checkPaymentok(){
        int selectedId = paymentradio.getCheckedRadioButtonId();
        if (selectedId != 0) {
            // find the radiobutton by returned id

            paymentoptionradio = (RadioButton) findViewById(selectedId);

            str_opt_payment = paymentoptionradio.getText().toString().trim();
            if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                str_paymentmethod_payment = 1;
                str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);
                new AsyncTaskCheckJson().execute();

            }
            else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                str_paymentmethod_payment = 2;
                str_paymentmethod_payment_name=getString(R.string.creditdebitcard);
                new AsyncTaskCheckJson().execute();

            }
            else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                str_paymentmethod_payment = 3;
                str_paymentmethod_payment_name=getString(R.string.ewallet);
                new AsyncTaskCheckJson().execute();


            }
            else if (str_opt_payment.equals(getString(R.string.giftcard))) {
                str_paymentmethod_payment = 4;
                str_paymentmethod_payment_name=getString(R.string.giftcard);
                new AsyncTaskCheckJson().execute();


            }else if (str_opt_payment.equals(getString(R.string.paypal))) {

                str_paymentmethod_payment = 5;
                str_paymentmethod_payment_name=getString(R.string.paypal);
                new AsyncTaskCheckJson().execute();


               /* AlertDialog.Builder builder=new AlertDialog.Builder(PaymentOptions.this);
                builder.setTitle("DailySouq");
                builder.setMessage("The amount will be converted to USD, Do you need to continue ?")
                        .setInverseBackgroundForced(false)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {


                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();*/

            }
            else {
                str_paymentmethod_payment = 1;
                str_paymentmethod_payment_name=getString(R.string.cash_on_delivery);
                new AsyncTaskCheckJson().execute();

            }
        } else {
            selectedId = R.id.codradio;
            paymentoptionradio = (RadioButton) findViewById(selectedId);
            paymentoptionradio.setError(getString(R.string.plz_select));
        }
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    public class AsyncTaskCheckJson extends AsyncTask<String, String, String> {
            JSONArray dataJsonArr;
            int jsonlen=0;

            @Override
            protected String doInBackground(String... strings) {

                try {


                    Json jParser = new Json();

                    JSONObject jsonObject = jParser.checkout(userVLogIn, str_deliverydate_payment, str_deliverytime_payment, str_deliveryadresid_payment, str_paymentmethod_payment);
                    JSONObject productOb = new JSONObject(Json.prdetail);
                    dataJsonArr = productOb.getJSONArray("d");
                    jsonlen=dataJsonArr.length();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(jsonlen==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                    alertDialog.setTitle(getString(R.string.error));
                    alertDialog.show();
                }else {

                    CustomChooseAddress.addressId = null;
                    Intent in = new Intent(PaymentOptions.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME", str_paymentmethod_payment_name);

                    startActivity(in);
                }
            }
        }


    public class AsyncTaskCheckJsonforTransfer extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected String doInBackground(String... strings) {

            try {


                Json jParser = new Json();
                //2 for transfer
                JSONObject jsonObject = jParser.checkout(userVLogIn, str_deliverydate_payment, str_deliverytime_payment, str_deliveryadresid_payment, 2);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {

                new AsyncOrderSaveJsson().execute();
            }
        }
    }



    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
          //  mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                json_length=dataJsonArr.length();




            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           // mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setMessage(getString(R.string.error));
                alertDialog.show();
            }else{

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }



//Log.i("arrivedcount",str_json_cart_count);


        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;
        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment_quick= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment_quick.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment_quick = 1;


                    } else if (str_opt_payment_quick.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment_quick = 2;

                    } else if (str_opt_payment_quick.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment_quick = 3;


                    } else {
                        str_paymentmethod_payment_quick = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(PaymentOptions.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment_quick);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }



    public class AsyncTaskDeliveryAmountAlert extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlegth=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.bal_amt_to_avil_freedelivery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_del_amnt= dataJsonArr.getJSONObject(0).optInt("result");
                prdlegth=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new AsyncPaymentSummary().execute();
        }
    }

    public class AsyncLinkedusercount extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlegth=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.linkedusercount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                linkedusercount= dataJsonArr.getJSONObject(0).optInt("Numberlinkedusers");
                prdlegth=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            settransferbuttonactive();
        }
    }

    public class AsyncTaskSaveGift extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int jsonlen=0;
        String responcejson="";

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
              mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {


                Json jParser = new Json();
                //2 for transfer
                JSONObject jsonObject = jParser.savegiftcoupen(userVLogIn,strGiftCoupen);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                responcejson=dataJsonArr.getJSONObject(0).getString("status");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if(responcejson.equals("success")){
                    showProgressAlertDone(responcejson);
                    lnGiftFull.setVisibility(View.GONE);
                    lnGiftFullApplied.setVisibility(View.VISIBLE);
                }else if(responcejson.equals("Invalid")){
                    showProgressAlert(responcejson);

                }else {
                    showProgressAlert(responcejson);

                }

            }
        }
    }

    public class AsyncTaskGiftVouchers extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int jsonlen=0;
        String responcejson="";

        @Override
        protected String doInBackground(String... strings) {

            try {


                Json jParser = new Json();
                //2 for transfer
                JSONObject jsonObject = jParser.showgiftcoupen(userVLogIn);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                arGiftDropDownCode=new ArrayList();
                arGiftDropDownVoucher=new ArrayList();
                arGiftDropDownVoucher.add(0,"Select  Gift Voucher Type");
                arGiftDropDownCode.add(0,"");
                for(int i=0;i<dataJsonArr.length();i++){
                    arGiftDropDownCode.add(dataJsonArr.getJSONObject(i).getString("VoucherCode"));
                    arGiftDropDownVoucher.add(dataJsonArr.getJSONObject(i).getString("VoucherName"));
                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                //no value found make actions
                edGiftDropDown.setVisibility(View.GONE);
            }else {

                showGiftDropDown();

            }
        }
    }

    private void settransferbuttonactive() {


           // btnTransferorder.setVisibility(View.VISIBLE);
           // txtTransfertext.setVisibility(View.VISIBLE);
            btnTransferorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showProgressAlert("Need to impliment");
                    if(linkedusercount!=0) {

                        AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage("In transfer order you can only made credit / debit card or net banking Do you want procceed..?");


                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        new AsyncLinkeduserlist().execute();
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }else {
                        showProgressAlertforLinked("This option will only be available if you have linked users in your account");
                    }

                  //  new AsyncLinkeduserlist().execute();
                }
            });


    }


    public class AsyncPaymentSummary extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;
        int  totitems,subtotral,discountamt,deliverychrge,totsave,nettotal;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.getpaymentsummery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                totitems = dataJsonArr.getJSONObject(0).optInt("Totalitem");
                discountamt = dataJsonArr.getJSONObject(0).optInt("Discount");
                deliverychrge = dataJsonArr.getJSONObject(0).optInt("Deliverycharge");
                subtotral = dataJsonArr.getJSONObject(0).optInt("subtotal");
                nettotal = dataJsonArr.getJSONObject(0).optInt("nettotal");
               // result = dataJsonArr.getJSONObject(0).optString("result");
                json_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setMessage(getString(R.string.nodatafound));
                alertDialog.show();

            }else{

                //used for the new payment summery
                setPaymentsummery();
                new AsyncLinkedusercount().execute();
            }


        }
        public void  setPaymentsummery(){
            txtItems.setText(totitems+".00");
            txtSubtotal.setText(subtotral+".00");
            txtDiscount.setText(discountamt+".00");
            txtYousave.setText(totsave+".00");
            if(deliverychrge==0){
                lndelalert.setVisibility(View.GONE);
                lndeltext.setVisibility(View.GONE);
            }else {
                txtDeliverycharge.setText(deliverychrge+".00");
                txtDeliveryalert.setText(getString(R.string.ad_rs)+" "+str_del_amnt+".00 "+getString(R.string.alert_sure_checkout2));
            }

            txtNettotal.setText(nettotal+".00");
        }
    }

    public class AsyncLinkeduserlist extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;
        int  totitems,subtotral,discountamt,deliverychrge,totsave,nettotal;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.linkedusersfortransfer(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                arrlinkedusername=new String[dataJsonArr.length()];
                arrlinkeduserid=new String[dataJsonArr.length()];
                for(int i=0;i<dataJsonArr.length();i++) {
                    arrlinkedusername[i] = dataJsonArr.getJSONObject(i).optString("Name");
                    arrlinkeduserid[i] = dataJsonArr.getJSONObject(i).optString("CustomerId");
                }
               // deliverychrge = dataJsonArr.getJSONObject(0).optInt("Deliverycharge");
               // subtotral = dataJsonArr.getJSONObject(0).optInt("subtotal");
                //nettotal = dataJsonArr.getJSONObject(0).optInt("nettotal");
                // result = dataJsonArr.getJSONObject(0).optString("result");
                json_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setMessage(getString(R.string.nodatafound));
                alertDialog.show();

            }else{
                    // do stuff here


                popupforlinkeduserlist();

            }


        }

    }

    private void popupforlinkeduserlist() {

        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.cus_popup_linkdlist_transferorder, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        ListView lstUserlist = (ListView) popupView.findViewById(R.id.lstlinkedusers);

        ArrayAdapter ad=new ArrayAdapter(PaymentOptions.this,R.layout.select_dialog_singlechoice_material,arrlinkedusername);
        lstUserlist.setAdapter(ad);
        lstUserlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

              //  Toast.makeText(PaymentOptions.this,"Cliked  "+arrlinkedusername[position]+" id  "+arrlinkeduserid[position],Toast.LENGTH_LONG).show();
                strSelectedTransfercusName=arrlinkedusername[position];
                strTransferCusId=arrlinkeduserid[position];

                new AsyncTaskCheckJsonforTransfer().execute();

              //  new AsyncOrderSaveJsson().execute();

                popupWindow.dismiss();

            }

            });



        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAtLocation(btnTransferorder, Gravity.CENTER,0,0);

    }


    public class AsyncOrderSaveJsson extends AsyncTask<String,String,String> {
        JSONArray dataJsonArr;
        String transactionid="0";
        int jsonlen=0;
        String result="";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
//            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
//            // Show progressdialog
            mProgressDialog.show();

        }
        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject jobj = jParser.ordersave(userVLogIn,transactionid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                Log.i("resultsave ",dataJsonArr.toString()+"");
                result=dataJsonArr.getJSONObject(0).optString("result");
                strOrderIdAfterSave=dataJsonArr.getJSONObject(0).optString("OrderId");
                //AFTER_OREDER_NAME=dataJsonArr.getJSONObject(0).optString("FirstName");
                //AFTER_ORDER_EMAIL=dataJsonArr.getJSONObject(0).optString("Email");

                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {

                // {"d":[{"result":"failed"}]}
                if (dataJsonArr.length() == 1) {

                    //ceck fail or not

                    if(result.equalsIgnoreCase("failed")){

                    }else {

                                new AsyncTransferOrderSubmit().execute();




                    }

                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    sp.setcartcount(0);
                                    Intent in = new Intent(PaymentOptions.this, MainActivity.class);

                                    startActivity(in);

                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }


            }
        }
    }

    void clearspdet(){
        //remove the address from check out
        SharedPreferences.Editor editor=myprefs.edit();
        editor.remove("ORDERREVIEW_NAME");
        editor.remove("ORDERREVIEW_ADDRESS");
        editor.remove("ORDERREVIEW_LOCATION");
        editor.remove("ORDERREVIEW_PHONE");

        editor.apply();
    }



    public class AsyncTransferOrderSubmit extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int json_length=0;
        String responce="";



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PaymentOptions.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.transferordersave(strTransferCusId,strOrderIdAfterSave);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");


                    responce = dataJsonArr.getJSONObject(0).optString("result");

                // deliverychrge = dataJsonArr.getJSONObject(0).optInt("Deliverycharge");
                // subtotral = dataJsonArr.getJSONObject(0).optInt("subtotal");
                //nettotal = dataJsonArr.getJSONObject(0).optInt("nettotal");
                // result = dataJsonArr.getJSONObject(0).optString("result");
                json_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(json_length==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                alertDialog.setMessage(getString(R.string.nodatafound));
                alertDialog.show();

            }else{
                // do stuff here

                if(responce.equalsIgnoreCase("Success")) {
                    if (!strOrderIdAfterSave.equalsIgnoreCase("0")) {

                        //do stuff code here

                        AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage("Your oder has been successfully transferred to "+strSelectedTransfercusName+" for payment");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(DialogInterface dialog, int which) {

                                        ;
                                        //removing dflt addtress clar
                                        sp.setcartcount(0);
                                        clearspdet();
                                        dialog.dismiss();
                                        Intent in = new Intent(PaymentOptions.this, MainActivity.class);


                                        startActivity(in);


                                    }
                                });
                        alertDialog.show();


                    }
                }
                else {
                    AlertDialog alertDialog = new AlertDialog.Builder(PaymentOptions.this).create();
                    alertDialog.setMessage(getString(R.string.somethingwentwrong));
                    alertDialog.show();
                }



            }


        }

    }


}

