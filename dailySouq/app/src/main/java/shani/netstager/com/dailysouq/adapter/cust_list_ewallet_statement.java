package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;

public class cust_list_ewallet_statement extends BaseAdapter{

    String[] str_date,str_desc,str_ac_type,str_amont,str_balance;
    private Activity activity;

    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;
    public cust_list_ewallet_statement(Activity context, int length, String[] str_dates, String[] str_descs, String[] str_ac_types, String[] str_amonts, String[] str_balances) {

        this.activity=context;
        this.rec_count=length;
        this.str_date=str_dates;
        this.str_desc=str_descs;
        this.str_ac_type=str_ac_types;
        this.str_amont=str_amonts;
        this.str_balance=str_balances;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static class ViewHolder{
        public TextView dates;
        public TextView descriptions;
        public TextView amount;
        public TextView dborcr;
        public TextView balance;



    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.cust_list_ewllet_statement, null);
            holder = new ViewHolder();

            holder.dates = (TextView) vi.findViewById(R.id.txt_date_ewaallet_sytatement);
            holder.descriptions = (TextView) vi.findViewById(R.id.txt_description_ewaallet_sytatement);
            holder.dborcr = (TextView) vi.findViewById(R.id.txt_dborcr_ewaallet_sytatement);
            holder.amount = (TextView) vi.findViewById(R.id.txt_amont_ewaallet_sytatement);
            holder.balance = (TextView) vi.findViewById(R.id.txt_balance_ewaallet_sytatement);



            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{


            holder.dates.setText(str_date[position]);
            holder.descriptions.setText(str_desc[position]);
            holder.dborcr.setText(str_ac_type[position]);
            holder.amount.setText(str_amont[position]+"");
            holder.balance.setText(str_balance[position]+"");


        }catch(Exception e){

        }
        return vi;
    }
}
