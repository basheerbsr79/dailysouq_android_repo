package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class RequstAProduct extends AppCompatActivity {
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    TextView title;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    Button submit,cancel,bck_btn;
    EditText prddesc,prdname,custname,custadres,custphone,custemail;
    LinearLayout lncusname,lncusaddress,lncusphone,lncusemail;
    String strprdname,strcustname,strcustadres,strcustphone,strcustemail,strprddsec;
    String resultdata;
    SharedPreferences myprefs;
    String userVLogIn,str_json_cart_count;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    String fname_edit,email_edit,address_edit,cityname_edit,phone_edit,lname_edit,landmark_edit,pincode_edit,locationname_edit;
    RelativeLayout fullview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requst_aproduct);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        fullview=(RelativeLayout)findViewById(R.id.full) ;
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }


        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){
                if(userVLogIn.equalsIgnoreCase("0")){
                    fullview.setVisibility(View.VISIBLE);
                }else {
                    new AsyncUserDetailsJson().execute();
                }

        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        profile=(ImageView)findViewById(R.id.profile);

        title=(TextView)findViewById(R.id.namein_title);
        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        submit=(Button)findViewById(R.id.btn_sub_reqprd);
        cancel=(Button)findViewById(R.id.btn_cncl_reqprd);
        prdname=(EditText)findViewById(R.id.ed_prdname_rqsprd);
        prddesc=(EditText)findViewById(R.id.ed_prddesc_rqsprd);
        custname=(EditText)findViewById(R.id.ed_name_rqsprd);
        custadres=(EditText)findViewById(R.id.ed_adres_reqprd);
        custemail=(EditText)findViewById(R.id.ed_email_rqsprd);
        custphone=(EditText)findViewById(R.id.ed_phn_rqsprd);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        lncusname=(LinearLayout)findViewById(R.id.lnname);
        lncusaddress=(LinearLayout)findViewById(R.id.lnaddress);
        lncusemail=(LinearLayout)findViewById(R.id.lnemail);
        lncusphone=(LinearLayout)findViewById(R.id.lnmobile);


        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });





        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(RequstAProduct.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(RequstAProduct.this,CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(RequstAProduct.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(RequstAProduct.this, Notification.class);

                startActivity(in);
            }
        });


        title.setText(getString(R.string.title_activity_requst_aproduct));


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!userVLogIn.equalsIgnoreCase("0")) {
                    strprddsec = prddesc.getText().toString();
                    strprdname = prdname.getText().toString();
                    if (strprdname.equals("")){
                        prdname.setError(getString(R.string.plz_fill));
                    }else {

                        new AsyncTaskCarttJson().execute();
                    }
                } else {
                    strprddsec = prddesc.getText().toString();
                    strprdname = prdname.getText().toString();
                    strcustname = custname.getText().toString();
                    strcustemail = custemail.getText().toString();
                    strcustadres = custadres.getText().toString();
                    strcustphone = custphone.getText().toString();

                if (strprdname.equals("") || strcustemail.equals("")) {


                    custemail.setError(getString(R.string.plz_fill));

                    prdname.setError(getString(R.string.plz_fill));

                } else {
                    if (strcustemail.matches(emailPattern) && strcustemail.length() > 0) {

                        new AsyncTaskCarttJson().execute();
                    } else {

                        custemail.setError(getString(R.string.invalid_email));

                    }


                }


            }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    public class AsyncUserDetailsJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(RequstAProduct.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                userVLogIn = myprefs.getString("shaniusrid", null);

                Json jParser = new Json();
                JSONObject json = jParser.userdetails(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                System.out.println("ssss" + productObj);
                dataJsonArr = productObj.getJSONArray("d");

                fname_edit=dataJsonArr.getJSONObject(0).optString("FirstName").toString().trim();

                lname_edit=dataJsonArr.getJSONObject(0).optString("LastName").toString().trim();
                email_edit=dataJsonArr.getJSONObject(0).optString("Email").toString().trim();
                phone_edit=dataJsonArr.getJSONObject(0).optString("Phone").toString().trim();

                address_edit=dataJsonArr.getJSONObject(0).optString("Address").toString();
                //   Locationid_edit[lcposid_edit]=dataJsonArr.getJSONObject(0).getString("LocationId").toString();
                landmark_edit=dataJsonArr.getJSONObject(0).optString("LandMark").toString();
                pincode_edit=dataJsonArr.getJSONObject(0).optString("PostCode").toString().trim();
                // cityid_edit[citypos_edit]=dataJsonArr.getJSONObject(0).getString("CityId").toString();
                locationname_edit=dataJsonArr.getJSONObject(0).optString("LocationName").toString();
                cityname_edit=dataJsonArr.getJSONObject(0).optString("CityName").toString();

                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            mProgressDialog.dismiss();
            fullview.setVisibility(View.VISIBLE);
            if(jsonlen==0){
             //   new AsyncTaskCity().execute();
            }else {


                setValues();

              //  new AsyncTaskCity().execute();

            }

        }
    }

    private void setValues() {


        strcustname = fname_edit+lname_edit+" ";
        strcustemail = email_edit;
        strcustadres = address_edit+" , "+landmark_edit+" , "+locationname_edit+" , "+cityname_edit+" , "+"PIN: "+pincode_edit;
        strcustphone = phone_edit;
        custname.setVisibility(View.GONE);
        custphone.setVisibility(View.GONE);
        custemail.setVisibility(View.GONE);
        custadres.setVisibility(View.GONE);
        lncusname.setVisibility(View.GONE);
        lncusaddress.setVisibility(View.GONE);
        lncusphone.setVisibility(View.GONE);
        lncusemail.setVisibility(View.GONE);

    }


    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(RequstAProduct.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.requstaproduct(strprdname,strcustname,strcustemail,strcustadres,strcustphone,strprddsec);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                resultdata=dataJsonArr.getJSONObject(0).optString("result").toString();
                jsonlen= dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if (resultdata.equals("success")) {

                    AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
                    alertDialog.setMessage(getString(R.string.succsess));
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();



                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                }


            }



        }
    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {
                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }
           // Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                    jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(RequstAProduct.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(RequstAProduct.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}





