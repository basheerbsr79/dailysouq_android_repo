package shani.netstager.com.dailysouq.support;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Netstager on 18-Apr-17.
 */
public class Json {

    public static String prdetail;
    static JSONObject jObj = null;
    // this is used for stage you have changue this when live..
    //public static String BASE_URL="http://servicedailysouq.netstager.com/";
    // live url
    public static String BASE_URL="http://service.dailysouq.in/";



    public JSONObject products(final String categoryId, final String customerid, int pg,int lang) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_product_all");

            jObj.put("CategoryId", categoryId);
            jObj.put("CustomerId", customerid);
            jObj.put("PageNo", pg);
            jObj.put("LangId",lang);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject detailProduct(final String productId,final String customerid,int lang) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_productby_id_for_Android");

            jObj.put("ProductId", productId);
            jObj.put("CustomerId", customerid);
            jObj.put("LangId",lang);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject brandProduct(final String brandId,final String customerid,int lang) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_product_by_brandid");

            jObj.put("BrandId", brandId);
            jObj.put("CustomerId", customerid);
            jObj.put("LangId",lang);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject filterProduct(String str_catogory_id, int lang) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/view_filtering");
            jObj.put("LangId",lang);
            jObj.put("CategoryId",str_catogory_id);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject sorting(final String categId, final String sort, final String customerid, int pg,int lang) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_sorting");

            jObj.put("search", sort);
            jObj.put("CategoryId", categId);
            jObj.put("PageNo", pg);
            jObj.put("CustomerId", customerid);
            jObj.put("LangId",lang);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject showCart(final String customerId,int lang) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_show_cart");

            jObj.put("CustomerId", customerId);
            jObj.put("LangId",lang);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject quiz(String id) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_quiz_all");
            ;jObj.put("CustomerId", id);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject recommentedProduct(final String categoryId, final String productId, final String customerid,int lang) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recomended_product");

            jObj.put("CategoryId", categoryId);
            jObj.put("ProductId", productId);
            jObj.put("CustomerId", customerid);
            jObj.put("LangId",lang);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

//


    }


    public JSONObject ChangePassword(final String UserId, final String CurrentPsd, final String NewPsd) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_changepassword");

            jObj.put("CustomerId", UserId);
            jObj.put("Password", CurrentPsd);
            jObj.put("NewPassword", NewPsd);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


    }


    public JSONObject AddNewAddress(final String UserId, final String Name, final String Contactno, final String adress, final String landmark, final String pincode,final String dfltste,final String cityid, final String locationid,final String deliveryaddressid) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_deliveryaddress_save");



            jObj.put("CustomerId", UserId);
            jObj.put("Address", adress);
            jObj.put("PostCode", pincode);
            jObj.put("LandMark", landmark);
            jObj.put("ContactName", Name);
            jObj.put("ContactNo", Contactno);
            jObj.put("LocationId", locationid);
            jObj.put("CityId", cityid);
            jObj.put("DefaultAddress", dfltste);

            if(deliveryaddressid!=null){
                jObj.put("DeliveryAddressId", deliveryaddressid);
            }
            else{
                jObj.put("DeliveryAddressId", "0");
            }




            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;



    }


    public JSONObject Location(final String cityid) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_location_all_byid");
            jObj.put("CityId",cityid);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject recipie() {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipie_all");
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return jObj;


    }


    public JSONObject orderlist(String userid) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_vieworder_recent_order");
            jObj.put("CustomerId", userid);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();
            Log.i("orderlistIva",response.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject transferorderlist(String userid) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_transfer_getlist");
            jObj.put("CustomerId", userid);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject chooseAddress(final String custID) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_deliveryaddress_list_bycustid");
            jObj.put("CustomerId", custID);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject addCart(int cartid,String customerId,String productId,int productsizeId,String quantity) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_car_save_client");

            jObj.put("CartId", cartid);
            jObj.put("CustomerId",customerId);
            jObj.put("ProductId",productId);
            jObj.put("ProductSizeId",productsizeId);
            jObj.put("Quantity",quantity);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject recentProducts(String productId,String customerID,int lang) {






        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_prodcutby_other_customer");

            jObj.put("CustomerId",customerID);
            jObj.put("productid",productId);
            jObj.put("LangId",lang);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject orderReview(String customId) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_review_details");

            jObj.put("CustomerId",customId);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }


        return jObj;


    }


    public JSONObject delivery(final String Userid) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_deliveryaddress_list_bycustid");
            jObj.put("CustomerId", Userid);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject city() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_city_all");
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject defaultaddress(final String Userid) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_deliveryaddress_bycustid");
            jObj.put("CustomerId", Userid);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject userdetails(final String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_customer_details");
            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject myWhislist(final String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_wishlist_by_id");
            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject addWishList(final String userVLogIn,final String product){

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_add_to_wishlist");
            jObj.put("CustomerId", userVLogIn);
            jObj.put("ProductId", product);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject requstaproduct(String prdname,String custname,String custemail,String custAddress,String custphone,String prddesc) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_requestproduct_save");
            jObj.put("ProducutName", prdname);
            jObj.put("CustomerName", custname);
            jObj.put("Email", custemail);
            jObj.put("Address", custAddress);
            jObj.put("Phone", custphone);
            jObj.put("ProductDescription", prddesc);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;

    }


    public JSONObject orderdetail(String str_orderno,String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_view_order_details");
            jObj.put("OrderId", str_orderno);
            jObj.put("CustomerId", userVLogIn);
            jObj.put("LangId","2");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;

    }


    public JSONObject recipiedetail(String str_id) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_byid");
            jObj.put("recipeid", str_id);
            jObj.put("LangId","2");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject time_slot(String delivery_date,String customerID) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_get_time_sloat_client");

            jObj.put("DeliveryDate", delivery_date);
            jObj.put("CustomerId", customerID);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject checkout( String customerID, String deliveryDate, String deliverySlot, String deliveryAddress,int paymenthmethod) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_updatetemp_order");
            // Log.i("timeDate",delivery_date+customerID);

            jObj.put("CustomerId", customerID);
            jObj.put("DeviceType", "Mob");
            jObj.put("DeliveyDate", deliveryDate);
            jObj.put("DeliverySlotId",deliverySlot );
            jObj.put("PaymentMethodId",paymenthmethod );
            jObj.put("GiftCardId", "1");
            jObj.put("PromoCode", "");
            jObj.put("DeliveryAddressId", deliveryAddress);




            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject srchnew(String type, String srchtext, int DEFAULT_LANGUGE, String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_search");
            jObj.put("Type", type);
            jObj.put("text", srchtext);
            jObj.put("LangId",DEFAULT_LANGUGE);
            jObj.put("CustomerId",userVLogIn);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject cancelorder(String str_orderno) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_cancel");
            jObj.put("OrderId", str_orderno);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject repeatoreder(String str_orderno, String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_repeat");
            jObj.put("OrderId", str_orderno);
            jObj.put("CustomerId", userVLogIn);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject currentclaim(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_claim_bycustmid");

            jObj.put("CustomerId", userVLogIn);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject fileaclaim(String userVLogIn, String str_orderno, String str_desc, String str_prdnme) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_claim_save");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("OrderId", str_orderno);
            jObj.put("ProductName", str_prdnme);
            jObj.put("Description", str_desc);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject ordersave(String userVLogIn, String transactionid) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_save");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("TransactionId", transactionid);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;


    }


    public JSONObject referalsave(String userVLogIn, String str_name, String str_email, String str_message) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_refferal_save");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("RefName", str_name);
            jObj.put("RefEmail", str_email);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject referlist(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_referral_listbycustomer");
            jObj.put("CustomerId", userVLogIn);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;

    }


    public JSONObject cartRemove(String CustomerID,int cartID) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cart_delete_page");
            jObj.put("CustomerId", CustomerID);
            jObj.put("CartId", cartID);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject wishListRemove(String wishId) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_wishlist_delete");

            jObj.put("WishListId", wishId);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }


    public JSONObject filterresult(String userVLogIn, String catresult, String brandresult, String refineresult, int pg, int lan,String price,String stock,String offer) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_filtering");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("PageNo", pg);
            jObj.put("category", catresult);
            jObj.put("brand", brandresult);
            jObj.put("isnew", refineresult);
            jObj.put("LangId",lan);
            jObj.put("Price",price);
            jObj.put("Stock",stock);
            jObj.put("Offer",offer);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;





    }


    public JSONObject cookery() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookery_all");
            jObj.put("LangId","2");
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject FBLogin(final String Fbemail) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_customerlogin_only");

            jObj.put("LoginType", "FA");
            jObj.put("Username", Fbemail);
            //its used to pass 1 android in 2 in ios
            jObj.put("MobType","1");

            jObj.put("Password", "");
            jObj.put("DeviceToken", "");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject quizwinerlist() {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_result_all");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return jObj;

    }


    public JSONObject RemoveAllcart(String customerId) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cart_delete_bycustid");

            jObj.put("CustomerId",customerId);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject saveresultquiz(String userVLogIn, String quizid, String answr) {


        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_quiz_winner_save");

            jObj.put("CustomerId",userVLogIn);
            jObj.put("QuizmasterId",quizid);
            jObj.put("Options", answr);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject cartcount(String customerId) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cart_count_bycustid");
            //  Log.i("ID",customerId);
            jObj.put("CustomerId", customerId);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject banner() {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_banner_all");

            //jObj.put("CustomerId", customerId);
            jObj.put("LangId","2");
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject deliveryfreecount(String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_check_deliveryfree");

            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject ewalletfoundsave(String usermasterid, String accounttype, String str_amtpaid, String userVLogIn, String description, String paymentId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_ewallet_save");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("AccountType", accounttype);
            jObj.put("Amount", str_amtpaid+"0");
            jObj.put("RefCustomerId", usermasterid);
            jObj.put("Description", description);
            jObj.put("TransactionId", paymentId);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject ewalletaddnewlinkeduser(String userVLogIn, String str_referemail) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_ewallet_referral");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("ReferedEmail", str_referemail);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;



    }


    public JSONObject linkedusers(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_ewallet_linkedusers");

            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject linkedusersfortransfer(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_Ewallet_Userlist");

            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject ewalletsummary(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_ewallet_summary");

            jObj.put("CustomerId",userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject bal_amt_to_avil_freedelivery(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_freedelivery_check");

            jObj.put("CustomerId",userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject search_shoplist_product(String str_product_name, int str_languge_id) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_product_alllist");

            jObj.put("ProductName",str_product_name);
            jObj.put("LangId",str_languge_id);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;


    }


    public JSONObject save_list_shop(String userVLogIn, String str_prdname_pass_to_save,String listDailyid   ) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_listitem_save");

            jObj.put("CustomerId",userVLogIn);
            jObj.put("ListItem",str_prdname_pass_to_save);
            jObj.put("DailyListId",listDailyid);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject shop_by_list_full_list(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_list_get_DailylistId");

            jObj.put("CustomerId",userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject find_all_by_shop_list(String userVLogIn,String dailylistid, int default_languge) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_listt_findproducts");

            jObj.put("CustomerId",userVLogIn);
            jObj.put("DailyListId",dailylistid);

            jObj.put("LangId",default_languge);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject remove_from_shopinglist(String userVLogIn, String str_id_delete) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_shoaplist_delete");

            jObj.put("CustomerId",userVLogIn);
            jObj.put("ListDetailsId",str_id_delete);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public JSONObject linked_user_details(String str_referemail) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_ewallet_customer_details");

            jObj.put("Email",str_referemail);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject newsupdations() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_settings_bind");
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
//

        }
        return jObj;

    }

    public JSONObject quick_checkout(String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_quickcheckout");

            jObj.put("CustomerId",userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject add_new_shopList(String userVLogIn,String listname,String dailylistid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_list_create");

            jObj.put("CustomerId",userVLogIn);
            jObj.put("ListName",listname);
            jObj.put("DailyListId",dailylistid);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject show_all_list_items(String listdailyid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_listget_unsavedlist");

            jObj.put("DailyListId",listdailyid);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject updatetransactionid(String userVLogIn, String orderid, String trnsactionid,String status) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_update_TransactionId");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("TransactionId", trnsactionid);
            jObj.put("OrderId", orderid);
            jObj.put("Status", status);
            //jObj.put("Status", 10);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

            Log.i("prdetail",prdetail);




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;



    }

    public JSONObject forgotpassword(String forEmail) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_forgotpassword_verificationcode");


            jObj.put("Email", forEmail);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;

    }


    public JSONObject partorderall() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_all");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }


        return jObj;
    }

    public JSONObject partorderdetails(String prtyid, String cusid, int lanid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_details");


            jObj.put("PartyOrderId", prtyid);
            jObj.put("CustomerId",cusid);
            jObj.put("LangId", lanid);




            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject addpartyordertocart(String partyorderid, String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partorder_save_cart");



            jObj.put("PartyOrderId", partyorderid);
            jObj.put("CustomerId",userVLogIn);





            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject notiflist() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_notification_all");


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }


        return jObj;
    }

    public JSONObject notificationdelete(String seleted_id) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_notification_delete");


            jObj.put("NotificationId", seleted_id);






            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject bundleallproduct(String id) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_bundleproduct_all");


            jObj.put("CustomerId", id);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject promocodesave(String userVLogIn, String str_promocde) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {



            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_promovouchercart_save");


            jObj.put("CustomerId", userVLogIn);
            jObj.put("VoucherCode", str_promocde);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();



        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jObj;
    }

    public JSONObject promotiondailysoq() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_promotion_all");


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return jObj;

    }

    public JSONObject discoundalldeals(String customerId, int pg, int default_languge) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_discont_all");


            jObj.put("CustomerId", customerId);
            jObj.put("PageNo", pg);
            jObj.put("LangId",default_languge);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject offeralldeals(String customerId, int pg, int default_languge) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_offer_all");


            jObj.put("CustomerId", customerId);
            jObj.put("PageNo", pg);
            jObj.put("LangId",default_languge);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject giftcardall(String id) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_giftproducts_all");


            jObj.put("CustomerId", id);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject addtransferordercustomer(String str_referemail) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_customer_details_byemail");


            jObj.put("Email", str_referemail);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject submittransferorder(String delid, String custid, String strorderid, String deldate, int paymethid, String deladdrss, String slotid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_order_save_transfer");


            jObj.put("DeliveryAddressId", delid);
            jObj.put("CustomerId", custid);
            jObj.put("OrderId", strorderid);
            jObj.put("DeliveyDate", deldate);
            jObj.put("PaymentMethodId", paymethid);
            jObj.put("DeliveryAddress", deladdrss);
            jObj.put("DeliverySlotId", slotid);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject addcookeryingrmentstocart(String userVLogIn, String cookeryIds) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookeryshow_save_cart");


            jObj.put("CookeryId", cookeryIds);
            jObj.put("CustomerId", userVLogIn);




            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject temppartyordercart(String partyorderid, String userVLogIn, String partyorderperson) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_cart_save");


            jObj.put("PartyOrderId", partyorderid);
            jObj.put("CustomerId", userVLogIn);
            jObj.put("Quantity", partyorderperson);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject toviewprtoderepoupitems( String cusId,String partyorderId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_get_partyorder_cart");



            jObj.put("CustomerId", cusId);
            jObj.put("PartyOrderId", partyorderId);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();
            Log.i("prtyitemdet",prdetail.toString()+"ok");


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject partyorderupdatewithqty( String cusid, int str_prd_qty,String orderid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_updatecart");



            jObj.put("CustomerId", cusid);
            jObj.put("Qty", str_prd_qty);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipieitemupdatewithqty( String cusid, int str_prd_qty,String orderid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_cart_update");



            jObj.put("CustomerId", cusid);
            jObj.put("Qty", str_prd_qty);
            jObj.put("recipeid", orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject partyorderremovewithid(String partyordercartid,String cusid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_deletecart");



            jObj.put("PartOrderCartId", partyordercartid);
            jObj.put("CustomerId", cusid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject partyordersubmittocart(String cusId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_cartsave_all");



            jObj.put("CustomerId", cusId);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }


    public JSONObject recipieitemssubmittocart(String cusId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_cartsave_alll");



            jObj.put("CustomerId", cusId);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject toviewcookerypoupitems(String cookid,String cusId,int lanid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_get_cookery_cartt");



            jObj.put("CookeryId", cookid);
            jObj.put("LangId", lanid);
            jObj.put("CustomerId", cusId);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject cookerytempsave(String cookid,String cusId,String qty) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookery_cart_savee");



            jObj.put("CookeryId", cusId);
            jObj.put("CustomerId", cookid);
            jObj.put("Quantity", qty);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject cookerysubmittocart(String cusId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookery_cartsave_alll");

            jObj.put("CustomerId", cusId);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject cookeryemovewithid(String id, String cusid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookery_deletecartt");

            jObj.put("CustomerId", cusid);
            jObj.put("CookeryCartId", id);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject cookeryupdatewithqty(String cusid, int qty, String partyorderid) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_cookery_updatecart");



            jObj.put("CustomerId", cusid);
            jObj.put("CookeryCartId", partyorderid);
            jObj.put("Quantity", qty);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipietempsave(String cusId, String partyoderid,String qty) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_cart_savee");



            jObj.put("CustomerId", cusId);
            jObj.put("recipeid", partyoderid);
            jObj.put("Quantity", qty);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject toviewrecpiepoupitems(String partyoderid, String cusId, int language) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_get_recipe_cartt");

            // Log.i("det","id"+partyoderid+"lanid"+language+"cusid"+cusId);

            jObj.put("recipeid", partyoderid);
            jObj.put("LangId", language);
            jObj.put("CustomerId", cusId);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipiesubmittocart(String cusId) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_cartsave_all");

            jObj.put("CustomerId", cusId);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipieremovewithid(String id, String cusid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_deletecartt");

            jObj.put("CustomerId", cusid);
            jObj.put("RecipeCartId", id);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipieupdatewithqty(String cusid, int qty, String cartid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_updatecart");



            jObj.put("CustomerId", cusid);
            jObj.put("RecipeCartId", cartid);
            jObj.put("Quantity", qty);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject notifupdate(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_notification_update");



            jObj.put("NotificationId", userVLogIn);



            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject notifcount() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_notification_count");


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject bundleProductitems(String pid, String userVLogIn, int default_languge) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_bundleproduct_byId");

            jObj.put("BundleProductId", pid);
            jObj.put("CustomerId", userVLogIn);
            jObj.put("LangId",default_languge);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject login(String uem, String no, String upass, String s, String s1) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_customerlogin_only");

            jObj.put("Username", uem);
            jObj.put("LoginType", "NO");
            jObj.put("Password", upass);
            jObj.put("DeviceToken", s);
            Log.i("DeviceTokenGcm",s+"  "+"8");
            //its used to pass 1 android in 2 in ios
            jObj.put("MobType","1");

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject refertext() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_pages_all");


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject getpaymentsummery(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_paymentsummary");

            jObj.put("CustomerId", userVLogIn);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipiefaveratesave(String userVLogIn, String str_id) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipefavorite_save");

            jObj.put("CustomerId", userVLogIn);
            jObj.put("RecipeId", str_id);
            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject linkedusercount(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_EwalletNumber_linkedusers");

            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipiewishlist(String userVLogIn) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipefavorite_byId");

            jObj.put("CustomerId", userVLogIn);

            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject transferordersave(String trcusid,String orderid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_transfer_order");

            jObj.put("TransferCustomerId", trcusid);
            jObj.put("OrderId", orderid);
            jObj.put("PaymentMethodId", 2);


            Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

            Log.i("trdet",prdetail.toString());


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject maketransferorderpayment(String str_orderno) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_transfer_order_status");

            jObj.put("OrderId", str_orderno);

            // Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

            Log.i("trdet",prdetail.toString());


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject recipieupdatewithindivituelqty(String cusid, int qty, String cartid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_recipe_updatecart_indivual");

            jObj.put("RecipeCartId", cartid);
            jObj.put("CustomerId", cusid);
            jObj.put("Qty", qty);


            // Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

            Log.i("trdet",prdetail.toString());


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject partryorderupdatewithindivituelqty(String cusid, int qty, String cartid) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_partyorder_updatecart_indivual");

            jObj.put("PartOrderCartId", cartid);
            jObj.put("CustomerId", cusid);
            jObj.put("Qty", qty);


            // Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();

            Log.i("trdet",prdetail.toString());


        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject savegiftcoupen(String userVLogIn,String coupen) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_promovouchercart_save_gift");

            jObj.put("VoucherCode", coupen);
            jObj.put("CustomerId", userVLogIn);



            // Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

    public JSONObject showgiftcoupen(String userVLogIn) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_OrderGiftVoucher_customer");


            jObj.put("CustomerId", userVLogIn );



            // Log.i("ids",trcusid+"ij"+orderid);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject getCurrencyRate() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();
        try {

            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_convertamount");

            // Log.i("ids",trcusid+"ij"+orderid);
            jObj.put("amount", "0" );


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        }  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }

    public JSONObject FBLogin_new(String fbEmail, String fbName, String fbLastName) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost(BASE_URL+"jsonwebservice.asmx/sdk_api_customerlogin_only_new");

            jObj.put("LoginType", "FA");
            jObj.put("Username", fbEmail);
            //its used to pass 1 android in 2 in ios
            jObj.put("MobType","1");

            jObj.put("Password", "");
            jObj.put("DeviceToken", "");
            jObj.put("firstname", fbName);
            jObj.put("lastname", fbLastName);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            prdetail = EntityUtils.toString(resEntity);
            prdetail = prdetail.trim();




        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }
}
