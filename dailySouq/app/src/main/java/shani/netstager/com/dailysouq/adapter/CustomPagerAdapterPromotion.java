package shani.netstager.com.dailysouq.adapter;

/**
 * Created by User on 3/22/2016.
 */
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.ProductDetail;


public class CustomPagerAdapterPromotion extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int pLength;
    String[]promo_id,promo_text,promo_desc,promo_img,promo_catid,promo_branid;

    public CustomPagerAdapterPromotion(Context context, int json_length, String[] str_promotion_id,
                                       String[] str_promotion_text, String[] str_promotion_image, String[] str_promotion_description, String[] str_promotion_catid, String[] str_promotion_brandid) {
        mContext = context;
        pLength=json_length;
        promo_id=str_promotion_id;
        promo_text=str_promotion_text;
        promo_img=str_promotion_image;
        promo_desc=str_promotion_description;
        promo_branid=str_promotion_brandid;
        promo_catid=str_promotion_catid;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pLength;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.custom_pager_promotion, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_promotion);
        TextView texttitle = (TextView) itemView.findViewById(R.id.txt_title);
        TextView textdesc = (TextView) itemView.findViewById(R.id.txt_desc);
        Picasso.with(mContext).load(promo_img[position]).placeholder(R.drawable.placeholder).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(mContext, ProductDetail.class);
                in.putExtra("Product",promo_id[position]);
                in.putExtra("Brand",promo_branid[position]);
                in.putExtra("CATEGORY",promo_catid[position]);
                mContext.startActivity(in);
            }
        });

        texttitle.setText(promo_text[position]);
        textdesc.setText(promo_desc[position]);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
