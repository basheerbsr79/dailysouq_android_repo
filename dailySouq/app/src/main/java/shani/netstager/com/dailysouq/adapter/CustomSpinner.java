package shani.netstager.com.dailysouq.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.MainActivity;


/**
 * Created by prajeeshkk on 05/10/15.
 */
public class CustomSpinner extends BaseAdapter {
    static String detail;
    Viewholder holder;
    String[] catogeryList;
    String[]categoryId;
    Context con;
    int catnameLength;
    int resource;

    public CustomSpinner(Context context,String[] catList) {

        this.con = context;

        this.catogeryList = catList;
        spinnerCategory();


    }
    @Override
    public int getCount() {
        return catnameLength;

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    class Viewholder
    {
        TextView tvName;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        int rollnocount=i;
        LayoutInflater inflater = (LayoutInflater) con
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi=view;
        if (view == null) {


            vi = inflater.inflate(R.layout.custom_spinner, null);
            holder = new Viewholder();

            holder.tvName = (TextView) vi.findViewById(R.id.tvLanguage);
            vi.setTag(holder);
        }
        else
        {
            holder = (Viewholder) view.getTag();
        }
        try {


            holder.tvName.setText(catogeryList[rollnocount]);
            Intent in=new Intent(con,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            in.putExtra("List",catogeryList[rollnocount]);


        }catch (Exception e)
        {

        }


        return vi;

    }


    public   void spinnerCategory()
    {
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {
                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_category_all");
                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    detail = EntityUtils.toString(resEntity);
                    detail = detail.trim();

                    JSONObject jasonob = new JSONObject(detail);
                    JSONArray catName=jasonob.getJSONArray("d");
                    catnameLength=catName.length();
                    catogeryList=new String[catnameLength];
                    categoryId=new String[catnameLength];
                    for(int i=0;i<catnameLength;i++) {
                        categoryId[i]=catName.getJSONObject(i).optString("CategoryId").toString();
                        catogeryList[i] = catName.getJSONObject(i).optString("CategoryName").toString();

                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }
                Looper.loop();
            }
        };
        th.start();

    }
}
