package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.AddDeliveryAddress;

public class cust_list_delivery extends BaseAdapter {
    String[]address,postcode,landmark,contactname,contactno,deliveryaddressid,locationname,cityname;
    Boolean[] defaultaddress;
    int[] locationid,cityid;
    int length;
    private Activity activity;

    LayoutInflater inf;
    ViewHolder holder;


    public cust_list_delivery( Activity acti,int len, Boolean[] defaultaddress, String[] cityname, String[] locationname, String[] contactname, String[] address, String[] landmark, int[] locationid,int[] cityid, String[] deliveryaddressid, String[] postcode, String[] contactno) {
        this.activity=acti;
        this.length=len;
        this.address=address;
        this.postcode=postcode;
        this.landmark=landmark;
        this.contactname=contactname;
        this.contactno=contactno;
        this.defaultaddress=defaultaddress;
        this.locationid=locationid;
        this.cityid=cityid;
        this.locationname=locationname;
        this.cityname=cityname;
        this.deliveryaddressid=deliveryaddressid;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder{
        public TextView txt_name;
        public TextView txt_adres;
        public TextView txt_landmark;
        public TextView txt_default;
        public TextView txt_phone;

        public ImageView img_dflt;
        public ImageView img_edit;
        public ImageView img_phone;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_delivery, null);
            holder = new ViewHolder();

            holder.img_dflt=(ImageView)vi.findViewById(R.id.dflt_img);
            holder.img_phone=(ImageView)vi.findViewById(R.id.imageView5);
            holder.img_edit=(ImageView)vi.findViewById(R.id.btn_edit);

            holder.txt_name = (TextView) vi.findViewById(R.id.nameindel);
            holder.txt_adres = (TextView) vi.findViewById(R.id.adindel);
            holder.txt_landmark = (TextView) vi.findViewById(R.id.plc_del);
            holder.txt_default = (TextView) vi.findViewById(R.id.txt_dflt);
            holder.txt_phone = (TextView) vi.findViewById(R.id.phneindel);


            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }


        if(defaultaddress[position]!=false){

            holder.txt_default.setVisibility(View.VISIBLE);
            holder.img_dflt.setVisibility(View.VISIBLE);

        }
        else {

            holder.txt_default.setVisibility(View.GONE);
            holder.img_dflt.setVisibility(View.GONE);

        }

        try{
            holder.txt_name.setText(contactname[position]);
            holder.txt_adres.setText(address[position]+" , "+landmark[position]);
            holder.txt_landmark.setText(locationname[position] + " , " + cityname[position] + " - "+activity.getString(R.string.pin) + postcode[position]);
            holder.txt_phone.setText(contactno[position]);


            holder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent ins =new Intent(activity.getBaseContext(),AddDeliveryAddress.class);
                    ins.putExtra("name",contactname[position]);
                    ins.putExtra("adress",address[position]);
                    ins.putExtra("landmark",landmark[position]);
                    ins.putExtra("postcode", postcode[position]);
                    ins.putExtra("1",deliveryaddressid[position]);
                    ins.putExtra("locationid",locationid[position]);
                    ins.putExtra("cityid",cityid[position]);
                    ins.putExtra("arrivedfrom",2);
                    ins.putExtra("phone",contactno[position]);

                    Toast.makeText(activity.getApplicationContext(),activity.getString(R.string.alert_re_select_city_location),Toast.LENGTH_LONG).show();


                    activity.startActivity(ins);
                }
            });




        }catch(Exception e){

        }
        return vi;
    }
}
