package shani.netstager.com.dailysouq.support;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 21/09/15.
 */
public class JSONfunctions {

    public static String getJSONfromURL(int lang) {

        String detail = "";


        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        HttpResponse response;
        JSONObject jsonn = new JSONObject();
        try {
            HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_category_all_by_feature");
            jsonn.put("PageNo","0");
            jsonn.put("LangId",lang);
            System.out.println("language"+lang);
            StringEntity se = new StringEntity(jsonn.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);

            HttpEntity resEntity = response.getEntity();
            detail = EntityUtils.toString(resEntity);
            detail = detail.trim();
            // String details=detail.toString();

        } catch (Exception e) {
            e.printStackTrace();
            //createDialog("Error", "Cannot Estabilish Connection");
        }
        return detail;
    }


}


