package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomWhislist;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 05/11/15.
 */
public class MyWhislist extends Activity {

    GridView whisGrid;
    ProgressDialog mProgressDialog;
    public SharedPreferences myprefs;

    int productLength, sizeProduct;
    CustomWhislist adapt;
    JSONArray dataJsonArr;
    //Handler mHandler;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    ImageView footerdssp,footerdsshop,footerdsdeal,footerdsgift,footerdsmore,Notification;
    RelativeLayout cartcounttext;
    ImageView profile,quickcheckout;
    TextView title;
    Button bck_btn;
    String userVLogIn;
    RelativeLayout fullview;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment,str_json_cart_count;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_whislist);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        fullview=(RelativeLayout)findViewById(R.id.my_wishlist_full) ;
        fullview.setVisibility(View.GONE);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        bck_btn=(Button)findViewById(R.id.menue_btn);
        //footer delartions

        footerdsmore=(ImageView)findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        footerdssp=(ImageView)findViewById(R.id.imageView13);
        footerdsshop=(ImageView)findViewById(R.id.imageView11);
        footerdsdeal=(ImageView)findViewById(R.id.imageView12);
        footerdsgift=(ImageView)findViewById(R.id.imageView14);
        footerdsmore=(ImageView)findViewById(R.id.imageView15);

        //footer clicks

        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MyWhislist.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(MyWhislist.this,DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyWhislist.this,DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyWhislist.this,Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyWhislist.this,DsSpecials.class);

                startActivity(in);
            }
        });

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();

            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyWhislist.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MyWhislist.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(MyWhislist.this, Notification.class);

                startActivity(in);
            }
        });
        title.setText(getString(R.string.my_wishlist));

        whisGrid = (GridView) findViewById(R.id.gridView2);


        if (userVLogIn!="0") {

            //internet checking
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent.equals(true)){
                new AsyncTaskCartcounttJson().execute();
                new AsynchWhisList().execute();

            }
            else{
                AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_net_failed));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();

            }






        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_pls_signin));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent in=new Intent(getApplicationContext(),Signin.class);

                            startActivity(in);
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();




        }



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsynchWhisList extends AsyncTask<String, String, ArrayList<ProductModel>> {
        int jsonlen=0;
        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(MyWhislist.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
             mProgressDialog.show();
        }


        @Override
        protected ArrayList<ProductModel> doInBackground(String... strings) {

            try {
                JSONArray dataJsonArr;
                Json jParser = new Json();
                JSONObject json = jParser.myWhislist(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();
                ArrayList<ProductModel> productModels = new ArrayList<>();
                for (int i = 0; i < productLength; i++) {
                    JSONObject c = dataJsonArr.getJSONObject(i);
                    ProductModel product = new ProductModel();
                    product.productId = dataJsonArr.getJSONObject(i).optString("ProductId");
                    product.productName = c.optString("ProductShowName").toString();
                    product.imagepath = c.optString("Imagepath").toString();
                    product.customerId = userVLogIn;
                    product.wishId=c.getString("WishListId");
                    ArrayList<SizeModel> sizes = new ArrayList<>();
                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
                    for (int j = 0; j < sizeProduct; j++) {
                        SizeModel sizeModel = new SizeModel();
                        sizeModel.productsize = sizeArray.getJSONObject(j).optString("ProductSize");
                        sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                        sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                        sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                        sizes.add(sizeModel);
                    }
                    jsonlen=dataJsonArr.length();
                    product.sizes = sizes;
                    productModels.add(product);

                }
                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<ProductModel> productModels) {
//            super.onPostExecute(s);
            //data = new ArrayList<String>();
            mProgressDialog.dismiss();
            if(jsonlen==0){


                AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_no_item_wishlist));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                onBackPressed();
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();



            }else{
                fullview.setVisibility(View.VISIBLE);
                adapt = new CustomWhislist(MyWhislist.this, productModels,cartcounttext,cartcountbadge);
                whisGrid.setAdapter(adapt);
                mProgressDialog.dismiss();

            }

        }

    }

    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {

                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if (userVLogIn.trim().equals("0")) {
                        cartcountbadge.setVisibility(View.GONE);
                    } else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }
            }
         //   Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                    jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(MyWhislist.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(MyWhislist.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}


