package shani.netstager.com.dailysouq.activity;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.productGridSearchAdapter;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class SerchResult extends AppCompatActivity {

    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    TextView title;
    TextView cartcountbadge,notifbadge;
    Button bck_btn;

    ImageView footeshop,footerdeal,footersp,footergft,footermore;

    int sizeProduct;
    int productLength;
    //serch test
    EditText catogoryspinnerinsrch;
    MultiAutoCompleteTextView serchtext;
    JSONArray dataJsonArr;
    String []prdnamesrch,prdidsrch,catnamesrch,catidsrch,bramdID;
    public SharedPreferences myProducts;
    ImageView serchgobtn;
    String []categoryName={"All","Category","Products","Product Code"};
    int pos;
    String result;
    String srchtext;
    String typetext;
    ListView srchlist;

    ProgressDialog mProgressDialog;
    //serch test;
    SharedPreferences myprefs;
    String userVLogIn,str_json_cart_count;
    String []imgurl;
    int DEFAULT_LANGUGE;

    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    //used for end less

    private TextView tvEmptyView;
    private RecyclerView gridViews;
    private productGridSearchAdapter mAdapter;

    int pg = 0;
    ArrayList<ProductModel> productModels;
    private GridLayoutManager lLayout;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;


    View footer;
    ProgressDialog dialog;
    //its used for new scroll

    //its for filter sort and serach
    ImageView sort;
    ImageView filter;

    String catId,qun;
    String str_cat_id_usedtofilter;
    String sortItem;
    DS_SP sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serch_result);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);
        DEFAULT_LANGUGE=myprefs.getInt("DEFAULT_LANGUGE",1);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }
        //used for endless scroll
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        pgresbar_scrool_footer=(ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        productModels = new ArrayList<>();
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

          //  new AsyncTaskCartcounttJson().execute();


            Intent in=getIntent();
            typetext=in.getStringExtra("type");
            srchtext=in.getStringExtra("text");
            new AsyncTaskParseJson().execute();
        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(SerchResult.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        lLayout = new GridLayoutManager(SerchResult.this, 2);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);

        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        bck_btn=(Button)findViewById(R.id.menue_btn);




        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SerchResult.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(SerchResult.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(SerchResult.this, Notification.class);

                startActivity(in);
            }
        });


        title.setText(getString(R.string.title_activity_serch_result));
        //serch test
        catogoryspinnerinsrch = (EditText) findViewById(R.id.catSpinner);
        serchtext = (MultiAutoCompleteTextView) findViewById(R.id.Search);
        serchgobtn = (ImageView) findViewById(R.id.goButton);


        catogoryspinnerinsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SerachTypePopup();
            }
        });

        serchgobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srchtext = serchtext.getText().toString();
                if (serchtext.length() == 0) {
                    serchtext.setError(getString(R.string.srh_here_error));
                } else {

//    Toast.makeText(getBaseContext(), "You have selected: " + categoryName[pos],
//            Toast.LENGTH_SHORT).show();
//    Toast.makeText(getBaseContext(), "You have : " + srchtext,
//            Toast.LENGTH_SHORT).show();

                    Intent in = new Intent(SerchResult.this, SerchResult.class);
                    in.putExtra("type", catogoryspinnerinsrch.getText().toString());
                    in.putExtra("text", srchtext);

                    startActivity(in);
                    //new AsyncTaskNotifCounttJson().execute();
                }
            }
        });


        //serch test
//its for filter and sort
        //serch test
        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);

        sort = (ImageView) findViewById(R.id.sort);
        filter = (ImageView) findViewById(R.id.filter);
        //btn_next= (Button) findViewById(R.id.next);
        //btn_prev= (Button) findViewById(R.id.prev);
        if (str_cat_id_usedtofilter != null) {
            catId=str_cat_id_usedtofilter;
        } else {
            catId = "1";
        }

        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(SerchResult.this, sort);
                popup.getMenuInflater().inflate(R.menu.menu_popup, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        sortItem = item.getTitle().toString();


                        Intent in = new Intent(SerchResult.this, Sort.class);
                        in.putExtra("CATID", catId);
                        in.putExtra("SORT", sortItem);

                        startActivity(in);


                        return true;
                    }
                });
                popup.show();
            }
        });


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(SerchResult.this, Filter.class);
                in.putExtra("Catogoryid_filter",catId);
                startActivity(in);
            }
        });



        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    //bsr new popup strt srch
    private void SerachTypePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.custom_popup_searchtype, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        final TextView all = (TextView) popupView.findViewById(R.id.all_custom_popup);
        final TextView products = (TextView) popupView.findViewById(R.id.prd_custom_popup);
        final  TextView catogory = (TextView) popupView.findViewById(R.id.cat_custom_poup);
        final TextView prdctcode = (TextView) popupView.findViewById(R.id.prdcode_custom_poup);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(all.getText());
                popupWindow.dismiss();
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(products.getText());
                popupWindow.dismiss();
            }
        });
        catogory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(catogory.getText());
                popupWindow.dismiss();
            }
        });
        prdctcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catogoryspinnerinsrch.setText(prdctcode.getText());
                popupWindow.dismiss();
            }
        });




        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setBackgroundDrawable(new BitmapDrawable(null,""));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(catogoryspinnerinsrch, 0,0);



    }

    // bsr edit popup end srch




    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }









    public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;


        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.srchnew(typetext, srchtext,DEFAULT_LANGUGE,userVLogIn);

                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);


                    //Shakeeb code
                    ProductModel product = new ProductModel();
                    product.productId = dataJsonArr.getJSONObject(i).getString("ProductId");

                    product.productName = c.getString("ProductShowName").toString();
                    product.imagepath = c.getString("Imagepath").toString();
//                    product.isNew = c.getString("IsNew");
                    product.brandId = c.getString("BrandId");
                    product.category = c.getString("CategoryId");
                    str_cat_id_usedtofilter=product.category;
                    product.customerId = userVLogIn;

                    ArrayList<SizeModel> sizes = new ArrayList<>();

                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
                    if (sizeProduct > 0) {
                        for (int j = 0; j < sizeProduct; j++) {
                            SizeModel sizeModel = new SizeModel();
                            sizeModel.productsize = sizeArray.getJSONObject(j).getString("ProductSize");
                            sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                            sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                            sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                            sizeModel.savaPrice = sizeArray.getJSONObject(j).optString("Saveprice");
                            sizeModel.discount = sizeArray.getJSONObject(j).optInt("Discount");
                            sizeModel.stockstatus=sizeArray.getJSONObject(j).optString("Stockstatus");
                            sizes.add(sizeModel);

                        }
                    }
                    product.sizes = sizes;
                    productModels.add(product);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);


                    SharedPreferences.Editor editor = myProducts.edit();


                    editor.putString("myCatid", product.category);
                    editor.putString("mypid", product.productId);
                    editor.apply();

                }



                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<ProductModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Intent in = new Intent(getApplicationContext(), NoResult.class);

                startActivity(in);
            }else {


                pg = pg + 1;
                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }

                    mAdapter = new productGridSearchAdapter(productModels, gridViews,SerchResult.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);

                    mAdapter.setProductlist(productModels);
//                    mAdapter.notifyDataSetChanged();
//                    mAdapter.setLoaded();
//
//
//
//                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//                    @Override
//                    public void onLoadMore() {
//
//
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//
//
//
//
//                            }
//                        }, 100);
//
//                    }
//                });


            }


        }
    }



    /*public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(SerchResult.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {
                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.hide();
                } else {
                    cartcountbadge.show();
                    cartcountbadge.setText(str_json_cart_count);
                }
            }
           // Log.i("arrivedcount", str_json_cart_count);

           // mProgressDialog.dismiss();
        }
    }*/

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;

        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(SerchResult.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(SerchResult.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(SerchResult.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
}


