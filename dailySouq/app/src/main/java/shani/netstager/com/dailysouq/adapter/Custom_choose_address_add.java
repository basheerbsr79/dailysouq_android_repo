package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.AddDeliveryAddress;

/**
 * Created by prajeeshkk on 06/11/15.
 */
public class Custom_choose_address_add extends BaseAdapter {

    ViewHolder holder;
    LayoutInflater inflater;
    int test=1;
Activity context;

    public Custom_choose_address_add(Activity con)
    {
        context=con;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
class ViewHolder
{
    Button addNew;
}
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (view == null) {

            vi=inflater.inflate(R.layout.custom_add,null);
            holder = new ViewHolder();
            holder.addNew=(Button)vi.findViewById(R.id.addNew);
            vi.setTag(holder);

        }else
        {
            holder = (ViewHolder) vi.getTag();

        }
        try
       {
           holder.addNew.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   test=1;
                   Intent ins=new Intent(context.getApplicationContext() ,AddDeliveryAddress.class);
                   ins.putExtra("arrivedfrom",test);
                   context.startActivity(ins);
               }
           });


       }
       catch (Exception e)
       {

       }

            return vi;
    }


}
