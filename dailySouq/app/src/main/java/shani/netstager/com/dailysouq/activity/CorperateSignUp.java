package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.GCM.RegistrationIntentService;
import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 02/10/15.
 */
public class CorperateSignUp extends Activity
{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST =9000;
    public  static  int SPLASH_TIME_OUT=3000;
    EditText ed_CfirstName,ed_ClastName,ed_Cdob,ed_Ctelno,ed_Cemail,ed_Cmobile,ed_Cpassword,ed_Cconfirm;
    Spinner copsp_mode;
    String []comm={"Mobile Phone","Email"};
    ProgressDialog mProgressDialog;
    String copmodeId;
    CheckBox c_condtions,c_mail;
    public SharedPreferences myprefs;
    static String customername,useremail,phone,customerId;
    static  String customerlastname,telephone,dob_pref,psd_pref;
    String copmail_Check;
    Button btn_CSubmit,btn_Ccancel;
    String firstN,lastN,dob,telno,email,mobile,password,confpass,customertypeid="2",logintype="NO",device,LoginUserIdType="1";
    static boolean succ;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String copSign,copRes;
    int sid;
    boolean eamilnotneedfalg=true;
    ImageView copback;
    TextView txt_link_terms;
    RadioButton myuserid_phone,myuserid_email;
    DS_SP sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.corporate_signin);
        sp=new DS_SP(getApplicationContext());

        txt_link_terms=(TextView)findViewById(R.id.txt_link_terms);
        myuserid_email=(RadioButton)findViewById(R.id.email_radio);
        myuserid_phone=(RadioButton)findViewById(R.id.phone_radio);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        ed_CfirstName= (EditText) findViewById(R.id.CopCompanyName);
        ed_ClastName= (EditText) findViewById(R.id.copPersonName);
        ed_Cdob= (EditText) findViewById(R.id.copRegistraion);
        ed_Ctelno=(EditText) findViewById(R.id.CopTelephone);
        ed_Cemail=(EditText) findViewById(R.id.copEmail);
        ed_Cmobile=(EditText) findViewById(R.id.copMobile);
        ed_Cpassword=(EditText) findViewById(R.id.copPassword);
        ed_Cconfirm=(EditText)findViewById(R.id.copConfirmPassword);

        copsp_mode=(Spinner)findViewById(R.id.copSpinner);
        c_condtions=(CheckBox)findViewById(R.id.copcheckBox);
        c_mail=(CheckBox)findViewById(R.id.copcheckBox2);
        btn_CSubmit=(Button)findViewById(R.id.copSubmit);
        btn_Ccancel=(Button)findViewById(R.id.copCancel);
        copback=(ImageView)findViewById(R.id.Copback);


        ed_Ctelno.setText("");
        ed_Cemail.setText("");

        device= getDeviceId();




        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.
                R.layout.simple_spinner_dropdown_item,comm);

        copsp_mode.setAdapter(adapter);
        copsp_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sid= copsp_mode.getSelectedItemPosition();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btn_CSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firstN=ed_CfirstName.getText().toString();
                lastN=ed_ClastName.getText().toString();
                dob=ed_Cdob.getText().toString();
                telno=ed_Ctelno.getText().toString();
                email=ed_Cemail.getText().toString();
                mobile=ed_Cmobile.getText().toString();
                password=ed_Cpassword.getText().toString();

                confpass=ed_Cconfirm.getText().toString();
                copmodeId=Integer.toString(sid);


//                if(c_condtions.isChecked())
//                {
//                    termsCheck="TRUE";
//                }



                if(c_mail.isChecked())
                {
                    copmail_Check="true";
                }
                else
                {
                    copmail_Check="false";
                }




                if (firstN.equals("")) {

                    ed_CfirstName.setError(getString(R.string.plz_fill));
                }
                else{
                    ed_CfirstName.setError(null);
                }
                if (lastN.equals("")) {

                    ed_ClastName.setError(getString(R.string.plz_fill));
                }
                else{
                    ed_ClastName.setError(null);
                }
                if (dob.equals("")) {

                    ed_Cdob.setError(getString(R.string.plz_fill));
                }
                else{
                    ed_Cdob.setError(null);
                }




//                if (mobile.length() != 10)
//
//                {
//
//
//                    ed_Cmobile.setError(getString(R.string.digit10need));
//                }

                if (password.length() < 6)

                {

                    ed_Cpassword.setError(getString(R.string.mustbe6chrtr));


                    ed_Cconfirm.setError(getString(R.string.mustbe6chrtr));
                }
                if (!c_condtions.isChecked())

                {
                    c_condtions.setError(getString(R.string.accpt_terms_condition));
                   // Toast.makeText(CorperateSignUp.this, "please accept the terms and conditions.", Toast.LENGTH_SHORT).show();


                }
                else{
                    c_condtions.setError(null);
                }








                if(firstN.equals("")||lastN.equals("")||dob.equals("")||mobile.equals("")||password.equals("")||confpass.equals("")||!c_condtions.isChecked())
                {
                    Toast.makeText(CorperateSignUp.this, getString(R.string.plz_fill_all), Toast.LENGTH_SHORT).show();



                }
                else
                {




                    //internet checking
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if(isInternetPresent.equals(true)){

                        if (password.equals(confpass)) {



//                            if (mobile.length() != 10)
//
//                            {
//
//                                ed_Cmobile.setError(getString(R.string.digit10need));
//                            }
                           // else{

                                if(email.length()>1) {
                                    if (email.matches(emailPattern) && email.length() > 0) {
                                        ed_Cemail.setError(null);
                                        LoginUserIdType="1";
                                        eamilnotneedfalg=false;

                                        // Create a progressdialog
                                        mProgressDialog = new ProgressDialog(CorperateSignUp.this);
                                        // Set progressdialog title
                                        mProgressDialog.setTitle("");

                                        mProgressDialog.setMessage(getString(R.string.loading));
                                        mProgressDialog.setIndeterminate(false);
                                        mProgressDialog.setCancelable(false);
                                        mProgressDialog.setCanceledOnTouchOutside(false);
                                        // Show progressdialog
                                        mProgressDialog.show();
                                        if(myuserid_email.isChecked()==true){
                                            LoginUserIdType = "1";
                                        }else if(myuserid_phone.isChecked()==true){
                                            LoginUserIdType = "2";
                                        }
                                        else {
                                            LoginUserIdType = "1";

                                        }

                                        if (checkPlayServices()) {
                                            // Start IntentService to register this application with GCM.


                                            Intent intent = new Intent(CorperateSignUp.this, RegistrationIntentService.class);
                                            startService(intent);
                                        }
                                        // if(isConnected==true) {
                                        new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                                            @Override
                                            public void run() {

                                                getCorperateSignUp();

                                            }
                                        }, SPLASH_TIME_OUT);

                                    } else {

                                        ed_Cemail.setError(getString(R.string.invalid_email));

                                        eamilnotneedfalg=false;

                                    }
                                }
                                else{
                                    LoginUserIdType="2";


                                    if(eamilnotneedfalg==false){

                                    }else{
                                        // Create a progressdialog
                                        mProgressDialog = new ProgressDialog(CorperateSignUp.this);
                                        // Set progressdialog title
                                        mProgressDialog.setTitle("");

                                        mProgressDialog.setMessage(getString(R.string.loading));
                                        mProgressDialog.setIndeterminate(false);
                                        mProgressDialog.setCancelable(false);
                                        mProgressDialog.setCanceledOnTouchOutside(false);
                                        // Show progressdialog
                                        mProgressDialog.show();
                                        if(myuserid_email.isChecked()==true){
                                            LoginUserIdType = "1";
                                        }else if(myuserid_phone.isChecked()==true){
                                            LoginUserIdType = "2";
                                        }
                                        else {
                                            LoginUserIdType = "1";

                                        }
                                        if (checkPlayServices()) {
                                            // Start IntentService to register this application with GCM.


                                            Intent intent = new Intent(CorperateSignUp.this, RegistrationIntentService.class);
                                            startService(intent);
                                        }
                                        // if(isConnected==true) {
                                        new android.os.Handler().postDelayed(new Runnable() {

   /*
    * Showing splash screen with a timer. This will be useful when you
    * want to show case your app logo / company
    */

                                            @Override
                                            public void run() {

                                                getCorperateSignUp();

                                            }
                                        }, SPLASH_TIME_OUT);
                                    }

                                }


                            //}


                        } else {

                            ed_Cpassword.setError(getString(R.string.psd_mismatch));

                            ed_Cconfirm.setError(getString(R.string.psd_mismatch));
                        }

                    }
                    else{

                        AlertDialog alertDialog = new AlertDialog.Builder(CorperateSignUp.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_net_failed));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        onBackPressed();

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }





                }



            }
        });

        btn_Ccancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        copback.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent bin=new Intent(CorperateSignUp.this,Signin.class);
                startActivity(bin);
                return false;
            }
        });
        txt_link_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(CorperateSignUp.this, TermsandConditions.class);
                startActivity(in);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void getCorperateSignUp(){
        Thread th = new Thread() {
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                // Create a progressdialog
                mProgressDialog = new ProgressDialog(CorperateSignUp.this);
                // Set progressdialog title
                mProgressDialog.setTitle("");

                mProgressDialog.setMessage(getString(R.string.loading));
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                // Show progressdialog
                mProgressDialog.show();
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject jsonn = new JSONObject();
                try {


                    HttpPost post = new HttpPost(Json.BASE_URL+"jsonwebservice.asmx/sdk_api_signup");

                    jsonn.put("LastName", lastN);
                    jsonn.put("FirstName", firstN);
                    jsonn.put("Email", email);
                    jsonn.put("LoginUserIdType", LoginUserIdType);
                    jsonn.put("Phone",mobile);
                    jsonn.put("Address", " ");
                    jsonn.put("LoginType",logintype);
                    jsonn.put("NewsLetterSub",copmail_Check);
                    jsonn.put("Password",password);
                    jsonn.put("CustomerTypeId",customertypeid);
                    jsonn.put("ModeId", copmodeId);
                    jsonn.put("Telephone", telno);
                    jsonn.put("CompanyRegNo",dob);
                    jsonn.put("Dob","");
                    jsonn.put("DeviceToken",sp.getdeviceId());
                    jsonn.put("ReferalCode", " ");
                    jsonn.put("PostCode", " ");
                    jsonn.put("LandMark", " ");
                    jsonn.put("LocationId"," ");
                    jsonn.put("CityId"," ");
                    //its used to pass 1 android in 2 in ios
                    jsonn.put("MobType","1");




                    StringEntity se = new StringEntity(jsonn.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    HttpEntity resEntity = response.getEntity();
                    copSign = EntityUtils.toString(resEntity);
                    copSign = copSign.trim();


                    JSONObject jasonob = new JSONObject(copSign);
                    copRes = jasonob.getJSONArray("d").getJSONObject(0).optString("result").toString();





                         if(copRes.equals("success")) {
                             customername=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("FirstName").toString();
                             customerId=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("CustomerId").toString();
                             useremail=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Email").toString();
                             phone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Phone").toString();

                             customerlastname=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("LastName").toString();
                             telephone=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Telephone").toString();
                             dob_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Dob").toString();
                             psd_pref=jasonob.getJSONArray("d").getJSONObject(0).getJSONArray("Details").getJSONObject(0).getString("Password").toString();





                             SharedPreferences.Editor editor=myprefs.edit();
                             editor.putString("shaniusrid", customerId);
                             editor.putString("bsrnme", customername);
                             editor.putString("useremail", useremail);
                             editor.putString("userphone", phone);
                             editor.putString("lastname",customerlastname);
                             editor.putString("dob",dob_pref);
                             editor.putString("telephone", telephone);
                             editor.putString("password",psd_pref);
                             editor.apply();
                             succ = true;
                             mProgressDialog.dismiss();
                             AlertDialog alertDialog = new AlertDialog.Builder(CorperateSignUp.this).create();
                             alertDialog.setCancelable(false);
                             alertDialog.setCanceledOnTouchOutside(false);
                             alertDialog.setMessage(getString(R.string.reg_success));
                             alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.ok),
                                     new DialogInterface.OnClickListener() {
                                         public void onClick(DialogInterface dialog, int which) {

                                             ;

                                             Intent in = new Intent(CorperateSignUp.this, MainActivity.class);


                                             startActivity(in);
                                             dialog.dismiss();
                                         }
                                     });

                             alertDialog.show();

                         }
                        else if(copRes.equals("Already Exist")) {
                             mProgressDialog.dismiss();
                             succ = false;


                             AlertDialog alertDialog = new AlertDialog.Builder(CorperateSignUp.this).create();
                             alertDialog.setCancelable(false);
                             alertDialog.setCanceledOnTouchOutside(false);
                             alertDialog.setMessage(getString(R.string.alert_user_exist));
                             alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                     new DialogInterface.OnClickListener() {
                                         public void onClick(DialogInterface dialog, int which) {

                                             ;

                                             Intent in = new Intent(CorperateSignUp.this, Signin.class);


                                             startActivity(in);

                                             dialog.dismiss();
                                         }
                                     });

                             alertDialog.show();


                         }
                    else{
                             AlertDialog alertDialog = new AlertDialog.Builder(CorperateSignUp.this).create();
                             alertDialog.setTitle(getString(R.string.error));
                             alertDialog.show();
                         }
                } catch (Exception e) {
                    e.printStackTrace();
                    //createDialog("Error", "Cannot Estabilish Connection");
                }
                Looper.loop();
            }
        };
        th.start();


    }

    protected String getDeviceId() {
        return Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {

                finish();
            }
            return false;
        }
        return true;
    }
    }


