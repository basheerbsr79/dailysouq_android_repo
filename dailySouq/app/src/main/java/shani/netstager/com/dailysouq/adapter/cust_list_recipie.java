package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.RecipieDetail;

public class cust_list_recipie extends BaseAdapter{


    private Activity activity;
    String[]recipieid,recipiename,recipieimagepath,recipiechefname;
    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;

    public cust_list_recipie(Activity context, int length, String[] recpid, String[] recpnme, String[] recpimg, String[] recpchef){
        this.activity=context;
        this.rec_count=length;
        this.recipieid=recpid;
        this.recipiename=recpnme;
        this.recipieimagepath=recpimg;
        this.recipiechefname=recpchef;
        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder{
        public TextView trno;
        public ImageView img;
        public ImageView share;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_recipie, null);
            holder = new ViewHolder();

            holder.trno = (TextView) vi.findViewById(R.id.textcust_recipie);
            holder.img=(ImageView)vi.findViewById(R.id.img_recipi);
            holder.share=(ImageView)vi.findViewById(R.id.shaer);

            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{
            holder.trno.setText(recipiename[position]);

            Picasso.with(activity).load(recipieimagepath[position]).into(holder.img);
          // Log.i("Recipieurl",recipieimagepath[position]);
            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in =new Intent(activity.getBaseContext(),RecipieDetail.class);
                    in.putExtra("recipie_id",recipieid[position]);
                    activity.startActivity(in);
                }
            });

            holder.trno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in =new Intent(activity.getBaseContext(),RecipieDetail.class);
                    in.putExtra("recipie_id",recipieid[position]);
                    activity.startActivity(in);
                }
            });

            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                    // Add data to the intent, the receiving app will decide
                    // what to do with it.
                    share.putExtra(Intent.EXTRA_SUBJECT,activity. getString(R.string.app_name));
                    share.putExtra(Intent.EXTRA_TEXT,"Please click the link to get the varieties of recipies.. http://dailysouq.in/recipes.aspx");

                    activity.startActivity(Intent.createChooser(share, "Share the  recipie to your friends.."));
                }
            });

        }catch(Exception e){

        }
        return vi;
    }
}
