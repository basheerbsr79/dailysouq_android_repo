package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.cust_list_ewallet_statement;
import shani.netstager.com.dailysouq.adapter.cust_list_linked_user_list;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class Ewallet extends AppCompatActivity {
    JSONArray dataJsonArr;
    ProgressDialog mProgressDialog;
    ImageView profile, quickcheckout, Notification;
    RelativeLayout cartcounttext;
    TextView title,txt_curent_balance;
    Button bck_btn, btn_fond, btn_add_new_linked_user;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    ListView orderlist, linkedusers;

    SharedPreferences myprefs;
    String userVLogIn, str_json_cart_count,usermasterid,accounttype,str_referemail;
    String[] str_address_linked_user,str_eamilorphone_linked_user,str_ref_cust_id,str_satatus_linked_user,str_fname_linked_user,str_date_summary,str_desc_summary,str_ac_type_summary,str_amount_summary,str_balance_summary;
    String  str_amtpaid,str_description,str_url,str_linked_user_fname,str_linked_user_address;


    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    ImageView footerdssp, footerdsshop, footerdsdeal, footerdsgift, footerdsmore;

    TextView title_summary;
    RelativeLayout fullview;
    //variables are used for ebs
    private static String HOST_NAME = "EBS";

    ArrayList<HashMap<String, String>> custom_post_parameters;



    private static final int ACC_ID = 19241; // Provided by EBS

    private static final String SECRET_KEY = "5e95119c0b20a69f9a970dfe3aebded6";

    private static final double PER_UNIT_PRICE = 1.00;

    double totalamount;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    LinearLayout fullsummary;

    String fname_edit,email_edit="",phone_edit,lname_edit,telephone_edit,address_edit,pincode_edit,locationname_edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ewallet);
        fullview=(RelativeLayout)findViewById(R.id.ewallet_full) ;
        fullsummary=(LinearLayout)findViewById(R.id.ewalln2);
        fullview.setVisibility(View.GONE);

        // Create a progressdialog
        mProgressDialog = new ProgressDialog(Ewallet.this);
        mProgressDialog.setMessage(getString(R.string.loading));
        // Show progressdialog
          mProgressDialog.show();


        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }


        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);
        } else {
            userVLogIn = "0";

        }

        profile = (ImageView) findViewById(R.id.profile);
        btn_fond = (Button) findViewById(R.id.btn_wallet_fund);
        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        title = (TextView) findViewById(R.id.namein_title);
        bck_btn = (Button) findViewById(R.id.menue_btn);
        btn_add_new_linked_user = (Button) findViewById(R.id.add_new_linked_custom);
        linkedusers = (ListView) findViewById(R.id.lst_linked_users);
        orderlist = (ListView) findViewById(R.id.lst_ewall_summery);
        btn_add_new_linked_user.setVisibility(View.GONE);
        linkedusers.setVisibility(View.GONE);
        title_summary=(TextView)findViewById(R.id.txtsum);



        txt_curent_balance=(TextView)findViewById(R.id.txt_current_balance_ewallet) ;


        try {


            if (userVLogIn != "0") {

                fullview.setVisibility(View.VISIBLE);
                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent.equals(true)) {

                  //  new AsyncTaskCartcounttJson().execute();
                    new AsyncTaskAccountStatementCarttJson().execute();
                    new AsyncTaskLinkeduserJson().execute();
                    new AsyncTaskUserDetails().execute();

                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(getApplicationContext(), Signin.class);

                                startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();


            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        //footer delartions

        footerdsmore = (ImageView) findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        footerdssp = (ImageView) findViewById(R.id.imageView13);
        footerdsshop = (ImageView) findViewById(R.id.imageView11);
        footerdsdeal = (ImageView) findViewById(R.id.imageView12);
        footerdsgift = (ImageView) findViewById(R.id.imageView14);
        footerdsmore = (ImageView) findViewById(R.id.imageView15);

        //footer clicks

        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Ewallet.this, DsDeals.class);

                startActivity(in);
            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, DsGiftCard.class);

                startActivity(in);
            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, DsSpecials.class);

                startActivity(in);
            }
        });

        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Ewallet.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Ewallet.this, Notification.class);

                startActivity(in);
            }
        });
        btn_fond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  /* Alert Dialog Code Start*/
                AlertDialog.Builder alert = new AlertDialog.Builder(Ewallet.this);
                alert.setTitle(getString(R.string.found_cash_to_wallet)); //Set Alert dialog title here
                alert.setMessage(getString(R.string.enter_amt_here)); //Message here

                // Set an EditText view to get user input
                final EditText input = new EditText(Ewallet.this);
                input.setHint(getString(R.string.amnt_in_rs));
                alert.setView(input);

                alert.setPositiveButton(getString(R.string.add_money), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        usermasterid="0";
                        accounttype="Debit";
                        str_description="money adding";

                        //You will get as string input data in this variable.
                        // here we convert the input to a string and show in a toast.
                        str_amtpaid = input.getEditableText().toString();
                        Toast.makeText(Ewallet.this, str_amtpaid, Toast.LENGTH_LONG).show();


                        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor=myprefs.edit();
                        editor.putInt("PAYMENT_GATEWAY",1);
                        editor.apply();

                        //use ebs payment
                        callEbsKit(Ewallet.this);





                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                }); //End of alert.setNegativeButton
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
       /* Alert Dialog Code End*/
            }
        });

        title.setText(getString(R.string.ewallet));


        linkedusers.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        orderlist.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });





        btn_add_new_linked_user.setVisibility(View.VISIBLE);


        btn_add_new_linked_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                            /* Alert Dialog Code Start*/
                final AlertDialog.Builder alert = new AlertDialog.Builder(Ewallet.this);
                alert.setTitle(getString(R.string.add_new_link_user)); //Set Alert dialog title here
                alert.setMessage(getString(R.string.email_phone_link_user)); //Message here

                // Set an EditText view to get user input
                final EditText input = new EditText(Ewallet.this);
                input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                input.setHint(getString(R.string.email_phone));
                alert.setView(input);

                alert.setPositiveButton(getString(R.string.link), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //You will get as string input data in this variable.
                        // here we convert the input to a string and show in a toast.
                        String srt = input.getEditableText().toString();


                        if (srt.matches(emailPattern) && srt.length() > 0) {

                            str_referemail=srt;
                            Toast.makeText(Ewallet.this, srt, Toast.LENGTH_LONG).show();
                            //method to confirm the user eamil ok

                            new AsyncTaskLinkedUserDeailsJson().execute();



                        }
                        else{

                            if(srt.length()==10){
                                str_referemail=srt;
                                Toast.makeText(Ewallet.this, srt, Toast.LENGTH_LONG).show();


                                //method to confirm the user mbile ok
                                new AsyncTaskLinkedUserDeailsJson().execute();


                            }else{

                                input.setError(getString(R.string.invalid_email_phone));
                                Toast.makeText(Ewallet.this,getString(R.string.invalid_email_phone), Toast.LENGTH_LONG).show();
                            }

                        }





                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                }); //End of alert.setNegativeButton
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
       /* Alert Dialog Code End*/
            }
        });

        mProgressDialog.dismiss();
        // used to set new sp based count
        setcartcountwihoutapi();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }





    //method for payment gate way



    private void callEbsKit(Ewallet buyProduct) {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */

        /** Payment Amount Details */
        // Total Amount


        float amount=Integer.valueOf(str_amtpaid);
        PaymentRequest.getInstance().setTransactionAmount(
                String.format("%.2f", amount));

        /** Mandatory */

        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        // Reference No
        PaymentRequest.getInstance().setReferenceNo("223");
        /** Mandatory */

        // Email Id
        PaymentRequest.getInstance().setBillingEmail(email_edit);
        /** Mandatory */

        /**
         * Set failure id as 1 to display amount and reference number on failed
         * transaction page. set 0 to disable
         */
        PaymentRequest.getInstance().setFailureid("0");
        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                "Daily Souq Android App Purchase");

        /** Billing Details */
        PaymentRequest.getInstance().setBillingName(email_edit);
        /** Optional */
        PaymentRequest.getInstance().setBillingAddress(email_edit);
        /** Optional */
        PaymentRequest.getInstance().setBillingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setBillingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setBillingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setBillingCountry("IND");
        // ** Optional */
        PaymentRequest.getInstance().setBillingPhone("1 800 123 2565");
        /** Optional */
        /** set custom message for failed transaction */

        PaymentRequest.getInstance().setFailuremessage(
                getResources().getString(R.string.payment_failure_message));
        /** Optional */
        /** Shipping Details */
        PaymentRequest.getInstance().setShippingName(email_edit);
        /** Optional */
        PaymentRequest.getInstance().setShippingAddress(email_edit);
        /** Optional */
        PaymentRequest.getInstance().setShippingCity("Tirur");
        /** Optional */
        PaymentRequest.getInstance().setShippingPostalCode("676101");
        /** Optional */
        PaymentRequest.getInstance().setShippingState("kerala");
        /** Optional */
        PaymentRequest.getInstance().setShippingCountry("IND");
        /** Optional */
        PaymentRequest.getInstance().setShippingEmail(email_edit);
        /** Optional */
        PaymentRequest.getInstance().setShippingPhone("01234567890");
        /** Optional */
		/* enable log by setting 1 and disable by setting 0 */
        PaymentRequest.getInstance().setLogEnabled("1");

        /**
         * Initialise parameters for dyanmic values sending from merchant custom
         * values from merchant
         */

        custom_post_parameters = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> hashpostvalues = new HashMap<String, String>();
        hashpostvalues.put("account_details", "saving");
        hashpostvalues.put("merchant_type", "gold");
        custom_post_parameters.add(hashpostvalues);

        PaymentRequest.getInstance()
                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(buyProduct, ACC_ID, SECRET_KEY,
                Config.Mode.ENV_LIVE, Config.Encryption.ALGORITHM_MD5, HOST_NAME);

        // EBSPayment.getInstance().init(context, accId, secretkey, environment,
        // algorithm, host_name);

    }



    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {


                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
              //  new AsyncTaskLinkeduserJson().execute();
                // mProgressDialog.dismiss();
            }
        }
    }

    public class AsyncTaskLinkeduserJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.linkedusers(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");


                str_ref_cust_id = new String[dataJsonArr.length()];
                str_satatus_linked_user = new String[dataJsonArr.length()];
                str_fname_linked_user = new String[dataJsonArr.length()];
                str_eamilorphone_linked_user = new String[dataJsonArr.length()];
                str_address_linked_user = new String[dataJsonArr.length()];

                for (int i = 0; i < dataJsonArr.length(); i++) {


                str_ref_cust_id[i] = dataJsonArr.getJSONObject(i).optString("RefCustomerId");
                str_satatus_linked_user[i] = dataJsonArr.getJSONObject(i).optString("Status");
                str_fname_linked_user [i]= dataJsonArr.getJSONObject(i).optString("FirstName");
                    str_eamilorphone_linked_user [i]= dataJsonArr.getJSONObject(i).optString("Email");
                    str_address_linked_user [i]= dataJsonArr.getJSONObject(i).optString("Address");
            }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           // mProgressDialog.dismiss();
                if(jsonlen==0){

                }else {


                    linkedusers.setVisibility(View.VISIBLE);

                    cust_list_linked_user_list adapter = new cust_list_linked_user_list(Ewallet.this, dataJsonArr.length(), str_satatus_linked_user, str_fname_linked_user, str_ref_cust_id, str_eamilorphone_linked_user, str_address_linked_user);
                    linkedusers = (ListView) findViewById(R.id.lst_linked_users);
                    linkedusers.setAdapter(adapter);


                   // new AsyncTaskAccountStatementCarttJson().execute();


                }

        }
    }

    public class AsyncTaskAccountStatementCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.ewalletsummary(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_date_summary = new String[dataJsonArr.length()];
                str_desc_summary = new String[dataJsonArr.length()];
                str_ac_type_summary = new String[dataJsonArr.length()];
                str_amount_summary = new String[dataJsonArr.length()];
                str_balance_summary = new String[dataJsonArr.length()];



                for (int i = 0; i < dataJsonArr.length(); i++) {

                    str_date_summary[i] = dataJsonArr.getJSONObject(i).optString("Date");
                    str_desc_summary[i] = dataJsonArr.getJSONObject(i).optString("Description");
                    str_ac_type_summary[i] = dataJsonArr.getJSONObject(i).optString("AccountType");
                    str_amount_summary[i] = dataJsonArr.getJSONObject(i).optString("Amount");
                    str_balance_summary[i] = dataJsonArr.getJSONObject(i).optString("Balance");
                    //Log.i("amounttt",str_amount_summary[i]);

                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (jsonlen == 0) {
               title_summary.setVisibility(View.VISIBLE);
                title_summary.setText(getString(R.string.summery_not_found)+"");
            } else {

                fullsummary.setVisibility(View.VISIBLE);
                title_summary.setVisibility(View.VISIBLE);
                cust_list_ewallet_statement adapter = new cust_list_ewallet_statement(Ewallet.this, dataJsonArr.length(), str_date_summary, str_desc_summary, str_ac_type_summary, str_amount_summary, str_balance_summary);
                orderlist = (ListView) findViewById(R.id.lst_ewall_summery);
                orderlist.setAdapter(adapter);
                if (dataJsonArr.length() == 0) {
                    String text = "Current Balance : Rs 0.00";
                    txt_curent_balance.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                } else {


                    String text = "Current Balance :Rs " + str_balance_summary[0] + "";
                    txt_curent_balance.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                    // txt_curent_balance.setText("Current balance :"+ Html.fromHtml("<i><small><font color=\"#ff0000\">"+str_balance_summary[dataJsonArr.length()-1]+"</font"));

                }

                // mProgressDialog.dismiss();
            }
        }
    }


    public class AsyncTaskAddNewLinkedUserJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        String str_result;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Ewallet.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
             mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.ewalletaddnewlinkeduser(userVLogIn,str_referemail);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_result = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mProgressDialog.dismiss();
if(jsonlen==0){
    AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {


    AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();

    alertDialog.setMessage("Linked user request sent");

    alertDialog.show();


    new AsyncTaskLinkeduserJson().execute();
}
        }
    }


    public class AsyncTaskLinkedUserDeailsJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Ewallet.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.linked_user_details(str_referemail);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                 str_linked_user_address= dataJsonArr.getJSONObject(0).optString("Address");
                 str_linked_user_fname= dataJsonArr.getJSONObject(0).optString("FirstName");
                    jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.invalid_email_phone));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }else {


                AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                alertDialog.setTitle(getString(R.string.confirm_user));


                String emailsrt = getString(R.string.email_phone) + ": " + str_referemail;
                String alert2 = getString(R.string.name) + ": " + str_linked_user_fname;
                String alert3 = getString(R.string.adrs) + ": " + str_linked_user_address;
                alertDialog.setMessage(emailsrt + "\n" + alert2 + "\n" + alert3);

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                //call new linked user toadd the linked user

                                new AsyncTaskAddNewLinkedUserJson().execute();


                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(Ewallet.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            //  mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                    jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(Ewallet.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(Ewallet.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
    public class AsyncTaskUserDetails extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        @Override
        protected String doInBackground(String... arg0) {
            try {
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                userVLogIn = myprefs.getString("shaniusrid", null);

                Json jParser = new Json();
                JSONObject json = jParser.userdetails(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                System.out.println("ssss" + productObj+"");
                dataJsonArr = productObj.getJSONArray("d");

                //fname_edit=dataJsonArr.getJSONObject(0).optString("FirstName").toString().trim();



                email_edit=dataJsonArr.getJSONObject(0).optString("Email").toString().trim();
                //phone_edit=dataJsonArr.getJSONObject(0).optString("Phone").toString().trim();
                //lname_edit=dataJsonArr.getJSONObject(0).optString("LastName").toString().trim();
                //telephone_edit=dataJsonArr.getJSONObject(0).optString("Telephone").toString().trim();
                //dob_edit=dataJsonArr.getJSONObject(0).optString("Dob").toString().trim();
                //psd_edit=dataJsonArr.getJSONObject(0).optString("Password").toString().trim();


                //comm_edit[modpoid_edit]=dataJsonArr.getJSONObject(0).optString("ModeId").toString();
                //address_edit=dataJsonArr.getJSONObject(0).optString("Address").toString();
                //   Locationid_edit[lcposid_edit]=dataJsonArr.getJSONObject(0).getString("LocationId").toString();
                //landmark_edit=dataJsonArr.getJSONObject(0).optString("LandMark").toString();
               // pincode_edit=dataJsonArr.getJSONObject(0).optString("PostCode").toString().trim();
                // cityid_edit[citypos_edit]=dataJsonArr.getJSONObject(0).getString("CityId").toString();
                //locationname_edit=dataJsonArr.getJSONObject(0).optString("LocationName").toString();
                //cityname_edit=dataJsonArr.getJSONObject(0).optString("CityName").toString();

                //jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           /* if(jsonlen==0){
                new EditProfile.AsyncTaskCity().execute();
            }else {


                setValues();

                new EditProfile.AsyncTaskCity().execute();

            }*/

        }
    }

}
