package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;

public class cust_list_quiz_winner_list extends BaseAdapter{


    private Activity activity;
    String[] arrqstname, arrqstdate, arrwinname;


    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;

    public cust_list_quiz_winner_list(Activity context,  String[] qstname, String[] arrqstdate,String[] arrwinname) {


        this.activity=context;
        this.arrqstname=qstname;
        this.arrwinname =arrwinname;
        this.arrqstdate =arrqstdate;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return arrqstname.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public static class ViewHolder{
        public TextView orderno;
        public TextView orderitem;
        public TextView ordertotle;




    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.cust_winner_list_details, null);
            holder = new ViewHolder();

            holder.orderno = (TextView) vi.findViewById(R.id.qstname);
            holder.orderitem = (TextView) vi.findViewById(R.id.windate);
            holder.ordertotle = (TextView) vi.findViewById(R.id.winnnername);



            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{

            holder.orderno.setText(activity.getString(R.string.quiz_name)+ " "+ arrqstname[position]);
            holder.orderitem.setText(activity.getString(R.string.win_date)+" "+ arrqstdate[position]);
            holder.ordertotle.setText(activity.getString(R.string.win_name)+" "+ arrwinname[position]);


        }catch(Exception e){

        }
        return vi;
    }
}
