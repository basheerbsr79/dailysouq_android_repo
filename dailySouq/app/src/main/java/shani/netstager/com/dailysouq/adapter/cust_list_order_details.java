package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;

public class cust_list_order_details extends BaseAdapter{

    String[] str_imgpath,str_qntity;
    String []str_subtotal,str_unitprice;
    private Activity activity;

    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;
    public cust_list_order_details(Activity context, int length, String[] str_imgpath, String[] str_unitprice, String[] str_qntity, String[] str_subtotal) {

        this.activity=context;
        this.rec_count=length;
        this.str_imgpath=str_imgpath;
        this.str_unitprice=str_unitprice;
        this.str_qntity=str_qntity;
        this.str_subtotal=str_subtotal;

        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public static class ViewHolder{
        public TextView unitprdet;
        public TextView qntdet;
        public TextView subtdet;
       public TextView imgdet;


    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_order_details, null);
            holder = new ViewHolder();

            holder.unitprdet = (TextView) vi.findViewById(R.id.unitordet);
            holder.qntdet = (TextView) vi.findViewById(R.id.qntordet);
            holder.subtdet = (TextView) vi.findViewById(R.id.subordet);
            holder.imgdet = (TextView) vi.findViewById(R.id.imgordet);



            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{

            holder.unitprdet.setText(str_unitprice[position]+"0");
            holder.qntdet.setText(str_qntity[position]);
            holder.subtdet.setText(str_subtotal[position]+"0");
            holder.imgdet.setText(str_imgpath[position]);



        }catch(Exception e){

        }
        return vi;
    }
}
