package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.discountGridViewAdapter;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;

import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.support.OnLoadMoreListener;

public class OfferDsdeals extends AppCompatActivity {
//used for end less

    private TextView tvEmptyView;
    private RecyclerView gridViews;
    private discountGridViewAdapter mAdapter;

    int pg = 0;
    ArrayList<ProductModel> productModels;
    private GridLayoutManager lLayout;
    ProgressBar pgresbar_scrool_footer;
    protected Handler handler;




    ImageView footeshop,footerdeal,footersp,footergft,footermore;

    View footer;
    ProgressDialog dialog;
    //its used for new scroll

    int id=0;


    ImageView profiles,quickcheckout;
    ProgressDialog mProgressDialog;




    int sizeProduct;
    int productLength;


    int DEFAULT_LANGUGE;


    public SharedPreferences myProducts;
    Button back_btn;


    String catId,userVLogIn,qun;

    String sortItem;
    SharedPreferences myprefs;

    TextView title;


    JSONArray dataJsonArr;

    ImageView Notification;
    RelativeLayout cartcounttext;


    String srchtext;
    int str_json_cart_count;;



    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_dsdeals);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }






        //used for endless scroll
        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        gridViews = (RecyclerView) findViewById(R.id.gridView1);
        pgresbar_scrool_footer=(ProgressBar) findViewById(R.id.pgresbar_scrool_footer);
        handler = new Handler();
        productModels = new ArrayList<>();
        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            new AsyncTaskCartcounttJson().execute();
            new AsyncTaskParseJson(pg).execute();

        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(OfferDsdeals.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }

        lLayout = new GridLayoutManager(OfferDsdeals.this, 2);
        gridViews.setLayoutManager(lLayout);
        gridViews.setHasFixedSize(true);


        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);



        //badge

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }



        footeshop = (ImageView) findViewById(R.id.imageView11);
        footersp = (ImageView) findViewById(R.id.imageView13);
        footermore = (ImageView) findViewById(R.id.imageView15);
        footerdeal = (ImageView) findViewById(R.id.imageView12);
        footerdeal.setImageResource(R.drawable.footerdsdealselect);
        footergft = (ImageView) findViewById(R.id.imageView14);
        footermore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OfferDsdeals.this, Menu_List.class);

                startActivity(in);
            }
        });
        footersp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OfferDsdeals.this, DsSpecials.class);

                startActivity(in);
            }
        });
        footerdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(OfferDsdeals.this,DsDeals.class);

                startActivity(in);
            }
        });
        footergft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(OfferDsdeals.this, DsGiftCard.class);

                startActivity(in);
            }
        });


        profiles = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);

        back_btn = (Button) findViewById(R.id.menue_btn);
        title = (TextView) findViewById(R.id.namein_title);
        title.setText("Offers");

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();
            }
        });


        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OfferDsdeals.this, EditProfile.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(OfferDsdeals.this, Notification.class);

                startActivity(in);
            }
        });

        myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);


        Intent in = getIntent();
        catId = in.getStringExtra("ID");






        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(OfferDsdeals.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OfferDsdeals.this, CartView.class);

                startActivity(in);
            }
        });



    }









    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }



    public class AsyncTaskParseJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int pageNumber;
        int json_length=0;

        public AsyncTaskParseJson(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {
//            // Create a progressdialog
//            mProgressDialog = new ProgressDialog(Products.this);
//            // Set progressdialog title
//            mProgressDialog.setTitle("");
//
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
//            //  mProgressDialog.show();

            pgresbar_scrool_footer.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<ProductModel> doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.offeralldeals( userVLogIn, pg,DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                Log.i("discoundalldetails",dataJsonArr.toString()+"ok");
                productLength = dataJsonArr.length();

                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);


                    //Shakeeb code
                    ProductModel product = new ProductModel();
                    product.productId = dataJsonArr.getJSONObject(i).getString("ProductId");

                    product.productName = c.getString("ProductShowName").toString();
                    product.imagepath = c.getString("Imagepath").toString();
                    product.isNew = c.getString("IsNew");
                    product.brandId = c.getString("BrandId");
                    product.category = c.getString("CategoryId");
                    product.customerId = userVLogIn;

                    ArrayList<SizeModel> sizes = new ArrayList<>();

                    JSONArray sizeArray = dataJsonArr.getJSONObject(i).getJSONArray("Sizes");
                    sizeProduct = sizeArray.length();
                    if (sizeProduct > 0) {
                        for (int j = 0; j < sizeProduct; j++) {
                            SizeModel sizeModel = new SizeModel();
                            sizeModel.productsize = sizeArray.getJSONObject(j).getString("ProductSize");
                            sizeModel.productSizeId = sizeArray.getJSONObject(j).optInt("ProductSizeId");
                            sizeModel.actualprize = sizeArray.getJSONObject(j).optString("ActualPrice");
                            sizeModel.offerprize = sizeArray.getJSONObject(j).optString("OfferPrice");
                            sizeModel.savaPrice = sizeArray.getJSONObject(j).optString("Saveprice");
                            sizeModel.discount = sizeArray.getJSONObject(j).optInt("Discount");
                            sizeModel.stockstatus=sizeArray.getJSONObject(j).optString("Stockstatus");
                            sizes.add(sizeModel);

                        }
                    }
                    product.sizes = sizes;
                    productModels.add(product);
                    json_length=dataJsonArr.length();
                    // prdt.add(productModels);


                    SharedPreferences.Editor editor = myProducts.edit();


                    editor.putString("myCatid", product.category);
                    editor.putString("mypid", product.productId);
                    editor.apply();

                }



                return productModels;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<ProductModel> productModels) {
            // mProgressDialog.dismiss();

            pgresbar_scrool_footer.setVisibility(View.GONE);
            if (json_length == 0) {
                Toast.makeText(OfferDsdeals.this,getString(R.string.no_more_prdct), Toast.LENGTH_SHORT).show();

            }else {


                pg = pg + 1;
                if (productModels.isEmpty()) {
                    gridViews.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);

                } else {
                    gridViews.setVisibility(View.VISIBLE);
                    tvEmptyView.setVisibility(View.GONE);
                }
                if (productModels.size() <= 10) {
                    mAdapter = new discountGridViewAdapter(productModels, gridViews, OfferDsdeals.this, cartcounttext, cartcountbadge, DEFAULT_LANGUGE);
                    gridViews.setAdapter(mAdapter);
                } else {
                    mAdapter.setProductlist(productModels);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();
                }


                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {


                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                new AsyncTaskParseJson(pg).execute();


                            }
                        }, 100);

                    }
                });


            }


        }
    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(OfferDsdeals.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            // mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(OfferDsdeals.this).create();

                alertDialog.setMessage(getString(R.string.error));

                alertDialog.show();
            }else {

                if (str_json_cart_count==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }

                // Log.i("arrivedcount", str_json_cart_count);

            } //  mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(OfferDsdeals.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(OfferDsdeals.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(OfferDsdeals.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}
