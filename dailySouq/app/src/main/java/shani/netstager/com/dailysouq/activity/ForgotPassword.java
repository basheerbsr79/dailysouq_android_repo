package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 03/10/15.
 */
public class ForgotPassword extends Activity {
    EditText ed_frogot;
    Button btn_forgot;
    ImageButton forLogin;
    String forEmail;
    String forpass;
    Button Indback;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String MobilePattern = "[0-9]{10}";
    boolean tagphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        ed_frogot = (EditText) findViewById(R.id.forgotEdit);
        btn_forgot = (Button) findViewById(R.id.forgotbutton);
        forLogin = (ImageButton) findViewById(R.id.forgotimageButton);
        Indback=(Button) findViewById(R.id.menue_btn);

        Indback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(ForgotPassword.this,Signin.class);

                startActivity(in);

            }
        });
        ed_frogot.setHint(Html.fromHtml("<font size=\"16\">" + "" + "</font>" + "<small>" + "Email/Mobile No" + "</small>"));
        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forEmail = ed_frogot.getText().toString();
                    //email
                if (forEmail.matches(emailPattern) && forEmail.length() > 0) {
                    //internet checking
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if(isInternetPresent.equals(true)){
                        tagphone=false;
                        new AsyncTaskForgotPassword().execute();


                    }
                    else{

                        AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_net_failed));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        onBackPressed();

                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }
                }
                else{
                    //mobile
                    if(forEmail.matches(MobilePattern)&&forEmail.length()>0){
                        //internet checking
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if(isInternetPresent.equals(true)){
                            tagphone=true;
                            new AsyncTaskForgotPassword().execute();

                        }
                        else{

                            AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(getString(R.string.alert_net_failed));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            onBackPressed();

                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();

                        }
                    }
                    else{
                        ed_frogot.setError(getString(R.string.emai_phone_error));
                    }
                }







            }
        });
        forLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(ForgotPassword.this,Signin.class);

                startActivity(in);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    public class AsyncTaskForgotPassword extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prd_length=0;
        String result;
        ProgressDialog  mProgressDialog;
        @Override
        protected void onPreExecute() {


            // Create a progressdialog
            mProgressDialog= new ProgressDialog(ForgotPassword.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String... arg0) {


            try {

                Json jParser = new Json();
                JSONObject json = jParser.forgotpassword(forEmail);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result=dataJsonArr.getJSONObject(0).getString("result");


                prd_length=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (prd_length == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                alertDialog.setTitle("Email not send please try again");
                alertDialog.show();
            } else {
                if(result.equals("Does not exist")){
                    AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alertnotregisteremail));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else if(result.equals("success")){
                    if(tagphone==true){
                        AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_psd_forgot_send_via_mail_phone));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent in = new Intent(ForgotPassword.this, ResetForgotPassword.class);

                                        in.putExtra("useremail", forEmail);
                                        startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }else if(tagphone==false){
                        AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.alert_psd_forgot_send_via_mail_email));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent in = new Intent(ForgotPassword.this, ResetForgotPassword.class);

                                        in.putExtra("useremail", forEmail);
                                        startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();
                    }
                }else{
                    AlertDialog alertDialog = new AlertDialog.Builder(ForgotPassword.this).create();
                    alertDialog.setMessage(getString(R.string.msgisnotworkingplztrymail));
                    alertDialog.show();
                }







            }
        }
    }


}
