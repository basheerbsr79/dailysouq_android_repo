package shani.netstager.com.dailysouq.activity;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomGallery;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;

/**
 * Created by prajeeshkk on 06/10/15.
 */
public class BannerDetail extends Activity {

    ImageView selectedImage,  Notification;
    RelativeLayout cartcounttext;
    Gallery gallery;
    ProgressDialog mProgressDialog;
    TextView prdName, act_price, offer_price, product_disc, showValue, savePrice;
    int prdoctid_frombanner;
    int productLength, RproductLength, RsizeProduct, BproductLength, BsizeProduct;
    String imgPath, productUrl;
    String[] productSize;
    String productId;
    String productName;
    String isNew;
    int[] productSizeId;
    String[] actualPrice, offerPrice, saveparice;
    String[] stockstatus;
    int[] discount;
    int cartcount;
    Context context = BannerDetail.this;
    int sizeProduct, pos;
    String productDescription;
    String brandId, res,pid;
    String disc;
    JSONArray dataJsonArr;
    //String images;
//    String bid, userVLogin;
    // public SharedPreferences myprefs;
    int  galleryLength;
    ArrayAdapter<String> adapter;
    // public SharedPreferences myproductdetail;
    //ImageView profile, cart, quickcheckout, Notification;
    String categ, qun, result;
    int str_json_cart_count;
    String bid;
    String userVLogIn;
    public SharedPreferences myprefs;
    ImageView profile, quickcheckout, wishlist, shareit;
    Button back_btn;
    TextView title, offer, stck_img;

    //branddddd


    TwoWayView lvTest, BrTest, recTest;
    TextView lvTestText,BrTestText,recTestText;
    View view;
    String[] items, galleryImg;
    //String item;
    ImageButton addcart, valuePlus, valueMinus;

    ImageView isnew,btn_update_cart;

    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    Spinner prdSize;
    int DEFAULT_LANGUGE,cartid;



    //ArrayList<String> items = new ArrayList<String>();
// its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout,str_current_sizetext;
    int str_paymentmethod_payment,int_current_quanitity;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.banner_product_show);

        cartcounttext = (RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);
        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }


        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", userVLogIn);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        if (userVLogIn != null) {
            userVLogIn = myprefs.getString("shaniusrid", userVLogIn);

        } else {
            userVLogIn = "0";

        }

        btn_update_cart=(ImageView)findViewById(R.id.btn_update_cart);
        profile = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        shareit = (ImageView) findViewById(R.id.img_share);
        Notification = (ImageView) findViewById(R.id.notification);


        back_btn = (Button) findViewById(R.id.menue_btn);
        title = (TextView) findViewById(R.id.namein_title);
        stck_img = (TextView) findViewById(R.id.stck_img_prddetail);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(BannerDetail.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(BannerDetail.this, CartView.class);

                startActivity(in);
            }
        });
        cartcountbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(BannerDetail.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(BannerDetail.this, Notification.class);

                startActivity(in);
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });


        gallery = (Gallery) findViewById(R.id.imgGallery);
        prdName = (TextView) findViewById(R.id.prdName);
        act_price = (TextView) findViewById(R.id.actPrice);
        offer_price = (TextView) findViewById(R.id.offer_price);
        selectedImage = (ImageView) findViewById(R.id.prdImg);
        product_disc = (TextView) findViewById(R.id.prd_description);
        lvTest = (TwoWayView) findViewById(R.id.lvItems);
        lvTestText=(TextView)findViewById(R.id.textView28);
        BrTest = (TwoWayView) findViewById(R.id.brandItems);
        BrTestText=(TextView)findViewById(R.id.bText);
        recTest = (TwoWayView) findViewById(R.id.RecentItems);
        recTestText=(TextView)findViewById(R.id.RecentText);
        addcart = (ImageButton) findViewById(R.id.add_Button);
        isnew = (ImageView) findViewById(R.id.newImg);
        valuePlus = (ImageButton) findViewById(R.id.valuePlus);
        valueMinus = (ImageButton) findViewById(R.id.valueMinus);
        showValue = (TextView) findViewById(R.id.valueText);
        prdSize = (Spinner) findViewById(R.id.productSpinner);
        wishlist = (ImageView) findViewById(R.id.wishlist);
        savePrice = (TextView) findViewById(R.id.save);
        offer = (TextView) findViewById(R.id.isoffer);


        Intent in = getIntent();

        prdoctid_frombanner = in.getIntExtra("ProductId",0);
            pid=String.valueOf(prdoctid_frombanner);






        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {

            new AsyncTaskCartcounttJson().execute();
            new AsynchShowProduct().execute();
        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userVLogIn != "0") {

                    qun = showValue.getText().toString();
                    int lst_qun=Integer.parseInt(qun);
                    if(lst_qun<=0){

                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_quntit));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();


                    }else{

                        cartid=0;

                        new AsyncTaskAddCartJson().execute();
                    }


                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(BannerDetail.this, Signin.class);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            }
        });



        btn_update_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userVLogIn != "0") {

                    qun = showValue.getText().toString();
                    int lst_qun=Integer.parseInt(qun);
                    if(lst_qun<=0){

                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_quntit));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();


                    }else{

                        new AsyncTaskAddCartJson().execute();
                    }


                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(BannerDetail.this, Signin.class);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            }
        });





        shareit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                share.putExtra(Intent.EXTRA_TEXT, productUrl);

                startActivity(Intent.createChooser(share, "Share product!"));


            }
        });
        wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userVLogIn != "0") {


                    new AsyncTaskAddWishListJson().execute();


                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setTitle("");
                    alertDialog.setMessage(getString(R.string.alert_pls_signin));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent in = new Intent(BannerDetail.this, Signin.class);


                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public class AsynchShowProduct extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {

//            // Create a progressdialog
            mProgressDialog = new ProgressDialog(BannerDetail.this);
//            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
//            // Show progressdialog
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            try {

                Json jParser = new Json();


                JSONObject json = jParser.detailProduct(pid, userVLogIn, DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);
                System.out.println("[[[" + productObj);
                dataJsonArr = productObj.getJSONArray("d");
                productLength = dataJsonArr.length();

                JSONArray sizeArray = dataJsonArr.getJSONObject(0).getJSONArray("Sizes");
                sizeProduct = sizeArray.length();
                productSize = new String[sizeProduct];

                actualPrice = new String[sizeProduct];
                offerPrice = new String[sizeProduct];
                productSizeId = new int[sizeProduct];
                saveparice = new String[sizeProduct];

                discount = new int[sizeProduct];
                stockstatus = new String[sizeProduct];

                JSONObject c = dataJsonArr.getJSONObject(0);
                productId = dataJsonArr.getJSONObject(0).getString("ProductId");
                productName = c.getString("ProductShowName").toString();
                imgPath = c.getString("Imagepath").toString();
                productUrl = c.getString("ProductUrl");
                isNew = c.getString("IsNew").toString();
                productDescription = c.getString("ProductDescription").toString();
                brandId = c.getString("BrandId");
                JSONArray gallerArray = c.getJSONArray("ImageUrl");

                galleryLength = gallerArray.length();
                galleryImg = new String[galleryLength];
                for (int g = 0; g < galleryLength; g++) {
                    galleryImg[g] = gallerArray.getJSONObject(g).getString("Imagepathurl");
                }

                for (int j = 0; j < sizeProduct; j++) {
                    productSize[j] = sizeArray.getJSONObject(j).optString("ProductSize").toString();
                    productSizeId[j] = sizeArray.getJSONObject(j).getInt("ProductSizeId");

                    actualPrice[j] = sizeArray.getJSONObject(j).optString("ActualPrice");
                    // System.out.println("pricee" + actualPrice[i]);
                    offerPrice[j] = sizeArray.getJSONObject(j).optString("OfferPrice");
                    saveparice[j] = sizeArray.getJSONObject(j).optString("Saveprice");

                    discount[j] = sizeArray.getJSONObject(j).optInt("Discount", 0);
                    stockstatus[j] = sizeArray.getJSONObject(j).optString("Stockstatus");

                }


                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                alertDialog.setTitle(getString(R.string.not_found));
                alertDialog.show();
            } else {


                savePrice.setVisibility(View.GONE);
                title.setText(productName);
                prdName.setText(productName);

                product_disc.setText(productDescription);
                Picasso.with(BannerDetail.this).load(imgPath).into(selectedImage);
                adapter = new ArrayAdapter<String>(BannerDetail.this, android.
                        R.layout.simple_spinner_dropdown_item, productSize);

                prdSize.setAdapter(adapter);



                    //used to set edit cart option

                int index = -1;
                for (int i=0;i<productSize.length;i++) {
                    if (productSize[i].equals(str_current_sizetext)) {
                        index = i;
                        Log.i("strposindex","ok"+index);
                        break;
                    }
                }


                    prdSize.setSelection(index);
                prdSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        pos = productSizeId[i];
                        if (actualPrice[i].equals(offerPrice[i])) {
                            offer_price.setText(offerPrice[i] + "0 ");


                            Typeface face1 = Typeface.createFromAsset(getAssets(),
                                    "fonts/Bold.otf");
                            //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                            offer_price.setTypeface(face1);

                            TextView txt_rs2 = (TextView) findViewById(R.id.textView20);


                            txt_rs2.setVisibility(View.GONE);

                            act_price.setVisibility(View.GONE);
                        } else {
                            //if (pos == 0) {
                            act_price.setText(actualPrice[i] + "0 ");

                            act_price.setPaintFlags(act_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            Typeface face1 = Typeface.createFromAsset(getAssets(),
                                    "fonts/ExoBold.otf");
                            //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                            act_price.setTypeface(face1);

                            offer_price.setText(offerPrice[i] + "0 ");
                            Typeface face = Typeface.createFromAsset(getAssets(),
                                    "fonts/Bold.otf");
                            //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                            offer_price.setTypeface(face);
                        }

                        savePrice.setText(getString(R.string.you_save) + " " + saveparice[i] + "0");
                        if (saveparice[i].equals("0.0")) {
                            savePrice.setVisibility(View.GONE);
                        } else {
                            savePrice.setVisibility(View.VISIBLE);

                        }

                        offer.setText(discount[i] + "%");
                        disc = offer.getText().toString();

                        if (disc.trim().equals("0%")) {
                            offer.setVisibility(View.GONE);
                        } else {
                            offer.setVisibility(View.VISIBLE);

                        }

                        if (stockstatus[i].trim().equals("1")) {
                            stck_img.setTextColor(Color.GREEN);
                            stck_img.setText(getString(R.string.in_stock));
                        } else if (stockstatus[i].trim().equals("2")) {
                            stck_img.setTextColor(Color.rgb(26, 167, 156));
                            stck_img.setText(getString(R.string.limted_stock));

                        } else {
                            stck_img.setTextColor(Color.RED);
                            stck_img.setText(getString(R.string.out_stock));
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });


                showValue.setText(Integer.toString(int_current_quanitity));

                qun = showValue.getText().toString();
                valuePlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int_current_quanitity++;
                        showValue.setText(Integer.toString(int_current_quanitity));
                        qun = showValue.getText().toString();
                    }
                });


                valueMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int_current_quanitity--;
                        showValue.setText(Integer.toString(int_current_quanitity));
                        qun = showValue.getText().toString();
                    }
                });

                if (isNew == "true") {
                    isnew.setVisibility(View.VISIBLE);
                } else {
                    isnew.setVisibility(View.GONE);
                }

                CustomGallery adGallery = new CustomGallery(BannerDetail.this, galleryLength, galleryImg);
                gallery.setAdapter(adGallery);


                gallery.setSpacing(1);


                gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        Picasso.with(BannerDetail.this).load(galleryImg[position]).into(selectedImage);


                    }
                });




            }
        }

    }









    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prd_length=0;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addCart(cartid,userVLogIn, pid, pos, qun);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).getString("result");
                prd_length=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (prd_length == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                alertDialog.setMessage(getString(R.string.error));


                alertDialog.show();
            } else {


                if (result.equals("success")) {
                    new AsyncTaskCartcounttJson().execute();
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BannerDetail.this);
                    LayoutInflater inflater1 = (LayoutInflater) BannerDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);

//                    new AsyncTaskCartcounttJson().execute();
//                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//
//                    alertDialog.setMessage(getString(R.string.alert_added_cart));
//
//                    alertDialog.show();


                } else if (result.equals("updated")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BannerDetail.this);
                    LayoutInflater inflater1 = (LayoutInflater) BannerDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("Out of Stock")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BannerDetail.this);
                    LayoutInflater inflater1 = (LayoutInflater) BannerDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                    TextView ok = (TextView) layout.findViewById(R.id.textok);


                    builder.setView(layout);


                    final android.app.AlertDialog alertDialog = builder.create();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();


                        }
                    });
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(getString(R.string.error));


                    alertDialog.show();


                }
            }
        }
    }


        public class AsyncTaskAddWishListJson extends AsyncTask<String, String, String> {

            final String TAG = "AsyncTaskParseJson.java";
            JSONArray dataJsonArr;
            int jsonlen=0;

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected String doInBackground(String... strings) {

                try {
                    Json jParser = new Json();

                    JSONObject json = jParser.addWishList(userVLogIn, pid);
                    JSONObject productObj = new JSONObject(Json.prdetail);

                    dataJsonArr = productObj.getJSONArray("d");
                    res = dataJsonArr.getJSONObject(0).getString("result");
                    jsonlen=dataJsonArr.length();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                if (jsonlen == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(getString(R.string.error));

                    alertDialog.show();
                } else {


                    if (res.equals("success")) {


                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BannerDetail.this);
                        LayoutInflater inflater1 = (LayoutInflater) BannerDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater1.inflate(R.layout.custom_alert_wishlist, null);


                        builder.setView(layout);

                        final android.app.AlertDialog alertDialog = builder.create();
                        alertDialog.getWindow().getAttributes().windowAnimations =


                                R.style.Bounce;


                        alertDialog.show();

                        final Handler handler = new Handler();
                        final Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                if (alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                            }
                        };

                        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                handler.removeCallbacks(runnable);
                            }
                        });
                        alertDialog.setCanceledOnTouchOutside(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = alertDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                        handler.postDelayed(runnable, 2000);


                    } else if (res.equals("Alredy exist")) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(BannerDetail.this);
                        LayoutInflater inflater1 = (LayoutInflater) BannerDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater1.inflate(R.layout.custom_alert_exist, null);


                        builder.setView(layout);

                        final android.app.AlertDialog alertDialog = builder.create();
                        alertDialog.getWindow().getAttributes().windowAnimations =


                                R.style.Bounce;

                        alertDialog.show();

                        final Handler handler = new Handler();
                        final Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                if (alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                            }
                        };

                        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                handler.removeCallbacks(runnable);
                            }
                        });

                        alertDialog.setCanceledOnTouchOutside(true);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = alertDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                        handler.postDelayed(runnable, 2000);


                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                        alertDialog.setMessage(getString(R.string.error));

                        alertDialog.show();

                    }


                }
            }

        }

        public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

            final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;


            @Override
            protected String doInBackground(String... arg0) {
                try {


                    Json jParser = new Json();
                    JSONObject json = jParser.cartcount(userVLogIn);
                    JSONObject productObj = new JSONObject(Json.prdetail);

                    dataJsonArr = productObj.getJSONArray("d");

                    str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                    jsonlen=dataJsonArr.length();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(jsonlen==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(getString(R.string.error));

                    alertDialog.show();
                }else {
                    sp.setcartcount(str_json_cart_count);

                    if (sp.getcartcount()==0) {
                        cartcountbadge.setVisibility(View.GONE);
                    } else {
                        if(userVLogIn.trim().equals("0")){
                            cartcountbadge.setVisibility(View.GONE);
                        }else {
                            cartcountbadge.setVisibility(View.VISIBLE);
                            cartcountbadge.setText(sp.getcartcount()+"");
                        }
                    }

                    //Log.i("arrivedcount", str_json_cart_count);
                }
                //  mProgressDialog.dismiss();
            }
        }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;

        int jsonlen=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(BannerDetail.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(BannerDetail.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }
    }



