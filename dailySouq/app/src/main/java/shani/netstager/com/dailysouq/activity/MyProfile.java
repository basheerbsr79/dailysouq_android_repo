package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class MyProfile extends AppCompatActivity {

    SharedPreferences myprefs;
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    String userVLogIn,useremail,userphone,str_json_cart_count,userName;
    TextView UserNameed,UserEmailed,Userphoneed,Title,txt_Adtitle,txt_Adres,txt_Loction,txt_pin;
    Button bck_btn,Add,UserPassed;



    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    String address,postcode,landmark,contactname,contactno,deliveryaddressid,cityname,locationname;
    Boolean defaultaddress;
    int locationid,cityid;
    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    RelativeLayout fullview;
    LinearLayout profile_head,lnr_full_address;

    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile);
        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        //badge
        fullview=(RelativeLayout)findViewById(R.id.my_profile_full) ;
        profile_head=(LinearLayout)findViewById(R.id.ln_profile_title);
        lnr_full_address=(LinearLayout)findViewById(R.id.full_del);
        fullview.setVisibility(View.GONE);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }

        if (userVLogIn != "0") {

            fullview.setVisibility(View.VISIBLE);



        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_pls_signin));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent in = new Intent(MyProfile.this, Signin.class);

                            MyProfile.this.startActivity(in);
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }


        Title=(TextView)findViewById(R.id.namein_title);
         txt_Adtitle=(TextView)findViewById(R.id.textView40);
        txt_Adres=(TextView)findViewById(R.id.textView41);
        txt_Loction=(TextView)findViewById(R.id.textView42);
        txt_pin=(TextView)findViewById(R.id.textView43);

        UserEmailed=(TextView)findViewById(R.id.emailtext);
        UserNameed=(TextView)findViewById(R.id.nametext);
        Userphoneed=(TextView)findViewById(R.id.calltext);
        UserPassed=(Button)findViewById(R.id.psd_txt_profile);
        profile=(ImageView)findViewById(R.id.profile);

        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);

        bck_btn=(Button)findViewById(R.id.menue_btn);
        Add=(Button)findViewById(R.id.ad_btn_prf);

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        if(userVLogIn!=null){



            //internet checking
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent.equals(true)){

              //  new AsyncTaskCartcounttJson().execute();
                setDetails();
                new AsyncTaskCarttJson().execute();
            }
            else{

                AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_net_failed));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                alertDialog.show();

            }





        }
        else{
            AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_pls_signin));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent in=new Intent(getApplicationContext(),Signin.class);

                            startActivity(in);
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }




        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MyProfile.this, AddDeliveryAddress.class);
                in.putExtra("arrivedfrom",3);

                startActivity(in);

            }
        });





        Title.setText(getString(R.string.personal_profile));
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();}
        });
        Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {onBackPressed();

            }
        });



        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyProfile.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MyProfile.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(MyProfile.this, Notification.class);

                startActivity(in);
            }
        });




        UserPassed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MyProfile.this,ChangePassword.class);

                startActivity(in);
            }
        });


    }

    private void setAddress() {
        Add.setVisibility(View.GONE);



        txt_Adres.setText(contactname + " , " + address + " , " + landmark);
        txt_Loction.setText(locationname+" , "+cityname+" - "+postcode );
        txt_pin.setText(getString(R.string.phone)+" "+contactno);


        // used to set new sp based count
        setcartcountwihoutapi();
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    void setDetails(){

        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);



        userName = myprefs.getString("bsrnme", null);
        useremail = myprefs.getString("useremail", null);
        userphone = myprefs.getString("userphone", null);
        UserNameed.setText(userName);
        Userphoneed.setText(userphone);
        UserEmailed.setText(useremail);


    }


    public class AsyncTaskCarttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(MyProfile.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
                userVLogIn = myprefs.getString("shaniusrid", null);

                Json jParser = new Json();
                JSONObject json = jParser.defaultaddress(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                for(int i=0;i<dataJsonArr.length();i++) {


                    address = dataJsonArr.getJSONObject(i).optString("Address");
                    postcode = dataJsonArr.getJSONObject(i).optString("PostCode");
                    landmark = dataJsonArr.getJSONObject(i).optString("LandMark");
                    contactname = dataJsonArr.getJSONObject(i).optString("ContactName");
                    contactno = dataJsonArr.getJSONObject(i).optString("ContactNo");


                    locationname = dataJsonArr.getJSONObject(i).optString("LocationName");
                    locationid = dataJsonArr.getJSONObject(i).optInt("LocationId");
                    cityid = dataJsonArr.getJSONObject(i).optInt("CityId");
                    cityname = dataJsonArr.getJSONObject(i).optString("CityName");



                   deliveryaddressid = dataJsonArr.getJSONObject(i).optString("DeliveryAddressId");
                    defaultaddress = dataJsonArr.getJSONObject(i).optBoolean("DefaultAddress");


                }
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();


    if(jsonlen!=0){
        setAddress();
    }
    else {
        Add.setVisibility(View.VISIBLE);
        profile_head.setVisibility(View.GONE);
        txt_Adres.setVisibility(View.GONE);
        txt_Loction.setVisibility(View.GONE);
        txt_pin.setVisibility(View.GONE);

        lnr_full_address.setVisibility(View.GONE);
    }
        }
    }


    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                jsonlen=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }else {

                if (str_json_cart_count.trim().equals("0")) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(str_json_cart_count);
                    }
                }
            }
           // Log.i("arrivedcount", str_json_cart_count);

            //mProgressDialog.dismiss();
        }
    }

    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                 jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(MyProfile.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(MyProfile.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}
