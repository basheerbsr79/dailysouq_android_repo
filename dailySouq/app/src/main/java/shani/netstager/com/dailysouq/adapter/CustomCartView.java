package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.CartView;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.ProductDetail;
import shani.netstager.com.dailysouq.activity.Signin;
import shani.netstager.com.dailysouq.support.DS_SP;


/**
 * Created by prajeeshkk on 15/10/15.
 */
public class CustomCartView extends BaseAdapter {

    ViewHolder holder;
    Context con;
    String[] productName;
    String[] imagePath;
    int[] quanti,cartID;
    int cart;
    String result;
    String[] prodSize,prdSizeId;
    int produtLength;
    LayoutInflater inf;
    String[]offer;
    String customerId,qun,addPid;
   int quan;
    ArrayAdapter<String>adapter;
    SharedPreferences mypref;
    String[] ProdectId;
    String testresult;
    int qun_test;
    String tst_qun;
    int int_prd_sizeid;
    DS_SP sp;



    public CustomCartView(Context context, int productLength, String name[], String image[], int[] quantity, String[] productSize, String[] off, int[] cart, String customer, String[] productId, String[] productSizeId) {
        con = context;
        imagePath = image;
        productName = name;
        quanti = quantity;
        prodSize = productSize;
        produtLength = productLength;
        offer=off;
        cartID=cart;
        prdSizeId=productSizeId;
        customerId=customer;
        ProdectId=productId;
        sp=new DS_SP(con.getApplicationContext());

        inf = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return produtLength;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        ImageView img;
        TextView pName;
        TextView offerPrice;
        Spinner prdSize;
        CustomSpinnerAdapter adapter;
        ImageButton valuePlus, valueMinus,removeButton,addButton;
        TextView showValue;
        TextView size;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup viewGroup) {

        final int rollnocount = arg0;
        View vi = arg1;
        if (arg1 == null) {
            vi = inf.inflate(R.layout.custom_cart_view, null);
            holder = new ViewHolder();
            holder.img = (ImageView) vi.findViewById(R.id.productImg);
            holder.pName = (TextView) vi.findViewById(R.id.prName);
            holder.offerPrice=(TextView)vi.findViewById(R.id.offer);
            holder.prdSize=(Spinner)vi.findViewById(R.id.productSpinner);
            holder.valuePlus = (ImageButton) vi.findViewById(R.id.valuePlus);
            holder.valueMinus = (ImageButton) vi.findViewById(R.id.valueMinus);
            holder.showValue = (TextView) vi.findViewById(R.id.valueText);
            holder.addButton=(ImageButton)vi.findViewById(R.id.addButton);
            holder.removeButton=(ImageButton)vi.findViewById(R.id.removeBtn);
            holder.size = (TextView) vi.findViewById(R.id.size);

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        try {
            final View finalVi = vi;

            String[]size={prodSize[rollnocount]};


            int_prd_sizeid=Integer.parseInt(prdSizeId[rollnocount]);
            Picasso.with(con).load(imagePath[rollnocount]).into(holder.img);
            holder.pName.setText(productName[rollnocount]);

            holder.size.setText(prodSize[rollnocount]);

            //TextView tv=(TextView)findViewById(R.id.custom);
            Typeface face=Typeface.createFromAsset(con.getAssets(),
                    "fonts/Bold.otf");
            //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

            holder.offerPrice.setTypeface(face);
            holder.offerPrice.setText(offer[rollnocount]);
            final ViewHolder myViewHolder = (ViewHolder) finalVi.getTag();

          adapter=new ArrayAdapter<String>(con,android.R.layout.simple_spinner_dropdown_item,size);
            holder.prdSize.setAdapter(adapter);

             quan=Integer.valueOf(quanti[rollnocount]);

            holder.showValue.setText(Integer.toString(quan));


            myViewHolder.valuePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vplus=1;

                    vplus= Integer.parseInt(myViewHolder.showValue.getText().toString());

                    myViewHolder.showValue.setText(Integer.toString(vplus+1));
                    myViewHolder.showValue.getText().toString();
                }
            });


            myViewHolder.valueMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vminus=1;

                    vminus= Integer.parseInt(holder.showValue.getText().toString());


                        myViewHolder.showValue.setText(Integer.toString(vminus - 1));


                      myViewHolder.showValue.getText().toString();

                }
            });


            //implimenting new intent to go to show page
            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //using to get current qty and pass it

                    qun = myViewHolder.showValue.getText().toString();

                    int lst_qun=Integer.parseInt(qun);

                //cart edit
                    Intent in=new Intent(con, ProductDetail.class);
                    ((Activity)con).finish();
                    in.putExtra("Product",ProdectId[rollnocount] );
                    in.putExtra("Brand", "0");
                    in.putExtra("CART_ID",cartID[rollnocount]);
                    in.putExtra("CATEGORY", "0");
                    in.putExtra("EDIT_QUANTITY",lst_qun);
                    in.putExtra("EDIT_SIZEARRYPOSITION",prodSize[rollnocount]);


                    con.startActivity(in);
                }
            });



            holder.removeButton.setOnClickListener(new View.OnClickListener() {
                //                @Override
                public void onClick(View view) {

                    cart = cartID[rollnocount];
                    AlertDialog alertDialog=new AlertDialog.Builder(con).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(con.getString(R.string.douwanttodelete));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,con.getString(R.string.yes),new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new CartRemoveTask().execute();
                            dialogInterface.dismiss();
                        }
                    });


                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,con.getString(R.string.no),new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });
                    alertDialog.show();


                }
            });

            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(customerId!="0"){

                        qun = myViewHolder.showValue.getText().toString();

                        int lst_qun=Integer.parseInt(qun);
                        if(lst_qun<=0){

                            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(con).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(con.getString(R.string.alert_pls_quntit));
                            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, con.getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();


                        }else{
                           // new AsyncTaskAddCartJson().execute();
                        }


                    }
                    else{
                        android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(con).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(con.getString(R.string.alert_pls_signin));
                        alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, con.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in=new Intent(con,Signin.class);

                                        con.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }
                    addPid=ProdectId[rollnocount];
                    qun_test=quanti[rollnocount];


                    int quns=Integer.valueOf(qun);
                    quns=quns-qun_test;
                    tst_qun=String.valueOf(quns);

                }
            });





        } catch (Exception e) {

        }
        return vi;
    }

    public  class CartRemoveTask extends AsyncTask<String,String,String> {
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected String doInBackground(String... strings) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.cartRemove(customerId,cart);
                JSONObject productObj = new JSONObject(Json.prdetail);

               dataJsonArr = productObj.getJSONArray("d");
               result=dataJsonArr.getJSONObject(0).optString("result").toString();

                jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                Toast.makeText(con, con.getString(R.string.failedtoremove), Toast.LENGTH_SHORT).show();
            }else {

                if (result.equals("1")) {


                    Intent in = new Intent(con, CartView.class);
                    in.putExtra("TAG_FROM_CART_EXIT_VIEW", true);
                    if (result.length() == 0) {
                        ((Activity) con).finish();

                    }
                    ((Activity) con).finish();
                    con.startActivity(in);
                   // Toast.makeText(con, con.getString(R.string.succsesfullyremved), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(con, con.getString(R.string.failedtoremove), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


   /* public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addCart(cartID[0],customerId, addPid,int_prd_sizeid, tst_qun);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                testresult = dataJsonArr.getJSONObject(0).getString("result");



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (testresult.equals("success")) {


                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(con);
                LayoutInflater inflater1 = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater1.inflate(R.layout.custom_alert, null);




                builder.setView(layout);

                final android.app.AlertDialog alertDialog = builder.create();
                alertDialog.getWindow().getAttributes().windowAnimations =


                        R.style.Bounce;

                alertDialog.show();
                final Handler handler = new Handler();
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                    }
                };

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        handler.removeCallbacks(runnable);
                    }
                });

                handler.postDelayed(runnable, 2000);


            } else if (testresult.equals("updated")) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(con);
                LayoutInflater inflater1 = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);




                builder.setView(layout);

                final android.app.AlertDialog alertDialog = builder.create();
                alertDialog.getWindow().getAttributes().windowAnimations =


                        R.style.Bounce;

                alertDialog.show();
                final Handler handler = new Handler();
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        Intent in=new Intent(con,CartView.class);
                        ((Activity)con).finish();
                        con.startActivity(in);
                    }
                };

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        handler.removeCallbacks(runnable);
                    }
                });

                handler.postDelayed(runnable, 2000);



            } else if (testresult.equals("Out of Stock")) {


                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(con);
                LayoutInflater inflater1 = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                TextView ok=(TextView)layout.findViewById(R.id.textok);



                builder.setView(layout);



                final android.app.AlertDialog alertDialog = builder.create();

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.getWindow().getAttributes().windowAnimations =


                        R.style.Bounce;

                alertDialog.show();
            }else {

                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(con).create();

                alertDialog.setMessage(con.getString(R.string.error));


                alertDialog.show();



            }


        }


    }*/


}
