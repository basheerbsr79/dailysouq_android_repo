 package shani.netstager.com.dailysouq.activity;

 import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
 import android.os.Build;
 import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
 import android.util.DisplayMetrics;
 import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
 import android.widget.EditText;
 import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomCartView;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
 import shani.netstager.com.dailysouq.support.DS_SP;
 import shani.netstager.com.dailysouq.support.Json;

 /**
 * Created by prajeeshkk on 15/10/15.
 */
public class CartView extends Activity
{
    public SharedPreferences myprefs,myProducts;
    ProgressDialog mProgressDialog;
    String userVLogIn;
    String categoryId;
    int productLength;
    int RproductLength;
    String[]productId;
    String[]RproductId;
    ImageView profile,quickcheckout,Notification;
    RelativeLayout cartcounttext;
    String[]productName;
    String[]RproductName;
    String[]offer;
    Button back_btn;
    String[]offerPrice;
    String[]imgPath;
    String[]RImage;
    int[]quantity;
    String[]productSize,productSizeId;
    int[]totalItem;
    String[]total,nettotal;
    int[]discount,yousave;
    int[]deliverycharge;
    int[]cartid;
    String []subTotal;
    int totallength;
    CustomCartView adapt;
    ListView cartList;
    TextView items,subtotal,disc,delivery,tot,txtyousave;
    String tItem,tToatal,tDiscount,tDelivery,tSub,tnettotal,tYousave;
    String prid,result;
    TextView title;
    JSONArray dataJsonArr;
    String str_address_quick_checkout,str_opt_payment,str_contact_name_quick_checkout,str_location_quick_checkout,str_phone_quick_checkout;
    int sizeProduct,DEFAULT_LANGUGE,str_paymentmethod_payment;
    Button qucicCheck,checkOut;
    RelativeLayout fullview;
    Boolean TAG_FROM_CART_EXIT_VIEW;
    TextView txt_avil_free_amnt;
    LinearLayout lnr_delivery_layout;
    int str_del_amnt,str_json_cart_count;


    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    //promo code
    EditText ed_promo;
    Button btn_promo;
    LinearLayout ln_promo_one,ln_promo_two;
    String str_promocde,str_promo_status;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cartview);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        back_btn=(Button)findViewById(R.id.menue_btn);
        profile=(ImageView)findViewById(R.id.profile);
        fullview=(RelativeLayout)findViewById(R.id.cart_view_full) ;
        fullview.setVisibility(View.GONE);
        quickcheckout=(ImageView)findViewById(R.id.apload);
        Notification=(ImageView) findViewById(R.id.notification);
        title=(TextView)findViewById(R.id.namein_title);
        title.setText(getString(R.string.view_basket));


        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge =(TextView)findViewById(R.id.badge);



        //badge
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        DEFAULT_LANGUGE = myprefs.getInt("DEFAULT_LANGUGE", 1);
        userVLogIn = myprefs.getString("shaniusrid", null);

        Intent in=getIntent();
        TAG_FROM_CART_EXIT_VIEW=in.getBooleanExtra("TAG_FROM_CART_EXIT_VIEW",false);

        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }




        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent in=new Intent(CartView.this,MainActivity.class);
                onBackPressed();
                //startActivity(in);
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent in = new Intent(CartView.this, MainActivity.class);
                onBackPressed();
                //startActivity(in);
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(CartView.this,EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(CartView.this,CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(CartView.this, Notification.class);

                startActivity(in);

            }
        });

        try {

            if (userVLogIn!="0") {

                myProducts = getSharedPreferences("MYPRODUCTS", Context.MODE_PRIVATE);
                categoryId = myProducts.getString("myCatid", null);

                if (categoryId == null) {
                    categoryId = "1";
                }
                prid = myProducts.getString("mypid", null);

                if (prid == null) {
                    prid = "1";
                }




                //internet checking
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if(isInternetPresent.equals(true)){
                    new AsyncTaskDeliveryAmount().execute();
                    new AsyncTaskCartcounttJson().execute();
                    new AsyncTaskCarttJson().execute();
                }
                else{

                    AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_net_failed));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    onBackPressed();

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                }


            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_pls_signin));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in=new Intent(CartView.this,Signin.class);

                               startActivity(in);
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();

            }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
        cartList = (ListView) findViewById(R.id.cartList);

//        cartList.setOnTouchListener(new View.OnTouchListener() {
//            // Setting on Touch Listener for handling the touch inside ScrollView
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // Disallow the touch request for parent scroll on touch of child view
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });


        checkOut=(Button)findViewById(R.id.checkout);

        qucicCheck=(Button)findViewById(R.id.quick_check);

       qucicCheck.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               new AsyncQuickCheckOut().execute();

           }
       });

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(CartView.this,CheckOut.class);

                //remove the promode
                sp.settestpromo(0);


                startActivity(in);
            }
        });


        //used for promo code

        ln_promo_one=(LinearLayout)findViewById(R.id.ln_promo_full_one);
        ln_promo_two=(LinearLayout)findViewById(R.id.ln_promo_full_two);
        ed_promo=(EditText)findViewById(R.id.ed_prmo_enter);
        btn_promo=(Button) findViewById(R.id.btn_prormocode);
        Log.i("prom_tag",sp.getbolpromoentred()+"ok");
        if(sp.gettestpromo()!=0){
            ln_promo_one.setVisibility(View.GONE);
            ln_promo_two.setVisibility(View.VISIBLE);
        }

        btn_promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_promocde=ed_promo.getText().toString();

                new AsyncPromoEnter().execute();

            }
        });

        setnewProgressbarClicks();
    }

    private void setnewProgressbarClicks() {
        ImageView ivstpcart,ivstpdelivery,ivstpPayment,ivstpConfirm;
        ivstpcart=(ImageView)findViewById(R.id.ivStpCart);
        ivstpdelivery=(ImageView)findViewById(R.id.ivStpDelivery);
        ivstpPayment=(ImageView)findViewById(R.id.ivStpPayment);
        ivstpConfirm=(ImageView)findViewById(R.id.ivStpConfirm);

        ivstpcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivstpdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(CartView.this,CheckOut.class);

                startActivity(in);
            }
        });
        ivstpPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressAlert("Can't select at this time,Please select delivery details first.");
            }
        });
        ivstpConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressAlert("Can't select at this time,Please select delivery details first.");
            }
        });

    }

    public void showProgressAlert(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(CartView.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }



    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ActionBar.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
         /* if(listAdapter.getCount()>10&&listAdapter.getCount()<20){

                  params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()-(listAdapter.getCount()*5)));



        }else if(listAdapter.getCount()>20&&listAdapter.getCount()<30){
              params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()-(listAdapter.getCount()*10)));

          }
          else if(listAdapter.getCount()>40&&listAdapter.getCount()<50){
              params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()-(listAdapter.getCount()*20)));

          }
          else if(listAdapter.getCount()>50&&listAdapter.getCount()<60){
              params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()-(listAdapter.getCount()*30)));

          }else if(listAdapter.getCount()>60){
              params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()-(listAdapter.getCount()*40)));

          } else {*/
       /* if (listAdapter.getCount() > 14 && listAdapter.getCount() < 18) {

            params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount() - ((listAdapter.getCount() / 5) * 65)));

        } else{

            params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount() - ((listAdapter.getCount() / 5) * 75)));
        }*/

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        int dens=dm.densityDpi;
        double wi=(double)width/(double)dens;
        double hi=(double)height/(double)dens;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);
        Log.i("screen inch",screenInches+"height"+height);

        if(screenInches>4.7){
            //new code by me
            int tconsize=height/10;
            int ttenper=tconsize/7;
            int atsize= tconsize-ttenper;

            Log.i("screen consize",tconsize+"height"+height+"persize"+ttenper+"finalsize"+atsize);
            params.height=atsize*listAdapter.getCount();
        }else {
            //new code by me
            int tconsize=height/10;
            int ttenper=tconsize/9;
            int atsize= tconsize-ttenper;

            Log.i("screen consize",tconsize+"height"+height+"persize"+ttenper+"finalsize"+atsize);
            params.height=atsize*listAdapter.getCount();

        }


       // }


        listView.setLayoutParams(params);

        listView.requestLayout();

    }


    public class AsyncTaskCarttJson extends AsyncTask<String, String, ArrayList<ProductModel>> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prdlength=0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(CartView.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
             mProgressDialog.show();
        }
          @Override
                   protected ArrayList<ProductModel> doInBackground(String... arg0) {
            try {
                Json jParser = new Json();
                JSONObject json = jParser.showCart(userVLogIn,DEFAULT_LANGUGE);
                JSONObject productObj = new JSONObject(Json.prdetail);

                JSONArray test=productObj.getJSONArray("d");

                if(test.length()>0) {

                    dataJsonArr = productObj.getJSONArray("d").getJSONObject(0).getJSONArray("Details");

                    productLength = dataJsonArr.length();

                    imgPath = new String[dataJsonArr.length()];

                    productSize = new String[dataJsonArr.length()];
                    productSizeId = new String[dataJsonArr.length()];
                    productId = new String[dataJsonArr.length()];
                    productName = new String[dataJsonArr.length()];
                    quantity = new int[dataJsonArr.length()];
                    total = new String[dataJsonArr.length()];
                    totalItem = new int[dataJsonArr.length()];
                    nettotal = new String[dataJsonArr.length()];
                    offer = new String[dataJsonArr.length()];
                    ///offerName = new String[dataJsonArr.length()];
                    discount = new int[dataJsonArr.length()];
                    yousave = new int[dataJsonArr.length()];
                    deliverycharge = new int[dataJsonArr.length()];
                    subTotal = new String[dataJsonArr.length()];
                    cartid=new int[dataJsonArr.length()];


                    // brandId=new  String[dataJsonArr.length()];
                    //ArrayList<ProductModel> productModels = new ArrayList<>();
                    for (int i = 0; i < dataJsonArr.length(); i++) {


                        JSONObject c = dataJsonArr.getJSONObject(i);
                        productId[i] = dataJsonArr.getJSONObject(i).getString("ProductId");
                        prid = String.valueOf(productId[i]);
                        productName[i] = c.optString("ProductShowName");

                        imgPath[i] = c.optString("Imagepath");

                        quantity[i] = c.optInt("Quantity");
                        productSize[i] = c.optString("ProductSize");
                        productSizeId[i]= c.optString("ProductSizeId");
                        subTotal[i] = c.optString("subtotal");
                        tSub = String.valueOf(subTotal[i]);
                        offer[i] = c.optString("OfferPrice");
                        cartid[i]=c.optInt("CartId");




                    }
                    JSONArray totatalArray = productObj.getJSONArray("d").getJSONObject(1).getJSONArray("Total");
                    totallength = totatalArray.length();


                    for (int j = 0; j < totallength; j++) {
                        totalItem[j] = totatalArray.getJSONObject(j).optInt("Totalitem");
                        tItem = String.valueOf(totalItem[j]);
                        nettotal[j] = totatalArray.getJSONObject(j).optString("nettotal");
                        tnettotal = String.valueOf(nettotal[j]);
                        total[j] = totatalArray.getJSONObject(j).optString("Total").toString();
                        tToatal = String.valueOf(total[j]);
                        deliverycharge[j] = totatalArray.getJSONObject(j).optInt("Deliverycharge");
                        tDelivery = String.valueOf(deliverycharge[j]);
                        discount[j] = totatalArray.getJSONObject(j).optInt("Discount");
                        tDiscount = Integer.toString(discount[j]);
                        yousave[j] = totatalArray.getJSONObject(j).optInt("totalsave");
                        tYousave = Integer.toString(yousave[j]);
                    prdlength=dataJsonArr.length();

                    }
                }


                return null;


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<ProductModel> productModels) {
            //super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (prdlength == 0) {

                mProgressDialog.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("Worked"," "+TAG_FROM_CART_EXIT_VIEW);
                                if(TAG_FROM_CART_EXIT_VIEW==true){
                                    Intent in=new Intent(CartView.this,Categories.class);

                                    startActivity(in);
                                }else{
                                    onBackPressed();
                                }


                                dialog.dismiss();
                            }
                        });

                alertDialog.show();


            } else {
                fullview.setVisibility(View.VISIBLE);
                adapt = new CustomCartView(CartView.this, productLength, productName, imgPath, quantity, productSize, subTotal,cartid,userVLogIn,productId,productSizeId);
                cartList.setAdapter(adapt);
                if(Build.VERSION.SDK_INT >= 21 ){
                    // Do some stuff  for s5 only
                    setListViewHeightBasedOnChildren(cartList);
                }else {
                    setListViewHeightBasedOnItems(cartList);
                }


//                View footerView = ((LayoutInflater) CartView.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout_emptybasket, null, false);
//                cartList.addFooterView(footerView);

              TextView emptybasket;

                //implimented footer view
                emptybasket=(TextView)findViewById(R.id.footer_remove_all);
                items = (TextView) findViewById(R.id.totalItems);
                subtotal = (TextView) findViewById(R.id.subtotal);
                disc = (TextView) findViewById(R.id.discount);
                delivery = (TextView) findViewById(R.id.delivery);
                txtyousave = (TextView) findViewById(R.id.yousave);
                tot = (TextView) findViewById(R.id.netpay);
                lnr_delivery_layout=(LinearLayout)findViewById(R.id.del_rlt_full) ;
                txt_avil_free_amnt=(TextView)findViewById(R.id.txt_shop_amnt_delivery) ;


                //footer to empty basket
                emptybasket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog alertDialog=new AlertDialog.Builder(CartView.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.douwanttoremove));
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                new AsyncCartAllRemove().execute();
                                dialogInterface.dismiss();
                            }
                        });


                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE,getString(R.string.no), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });
                        alertDialog.show();
                    }
                });


                txt_avil_free_amnt.setText(getString(R.string.ad_rs)+" "+str_del_amnt+".00 "+getString(R.string.alert_sure_checkout2));


                items.setText(tItem);

                subtotal.setText(tToatal);
                disc.setText(tDiscount+".00");
                delivery.setText(tDelivery+".00");
                txtyousave.setText(tYousave+".00");
                int delvalue=Integer.valueOf(tDelivery);

                if(delvalue==0){
                    lnr_delivery_layout.setVisibility(View.GONE);

                }
                tot.setText(tnettotal+"0");


            }
        }
    }
    public class AsyncCartAllRemove extends AsyncTask<String,String,String> {
     JSONArray dataJsonArr;
     int prdlength = 0;

     @Override
     protected void onPreExecute() {

         // Create a progressdialog
         mProgressDialog = new ProgressDialog(CartView.this);
         // Set progressdialog title
         mProgressDialog.setTitle("");

         mProgressDialog.setMessage(getString(R.string.loading));
         mProgressDialog.setIndeterminate(false);
         mProgressDialog.setCancelable(false);
         mProgressDialog.setCanceledOnTouchOutside(false);
         // Show progressdialog
         mProgressDialog.show();
     }

     @Override
     protected String doInBackground(String... strings) {
         try {


             Json jParser = new Json();
             JSONObject json = jParser.RemoveAllcart(userVLogIn);
             JSONObject productObj = new JSONObject(Json.prdetail);
             dataJsonArr = productObj.getJSONArray("d");
             result = dataJsonArr.getJSONObject(0).optString("result");
             prdlength = dataJsonArr.length();
         } catch (JSONException e) {
             e.printStackTrace();
         }
         return null;
     }

     @Override
     protected void onPostExecute(String s) {
         super.onPostExecute(s);
         mProgressDialog.dismiss();
         if (prdlength == 0) {
             AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
             alertDialog.setTitle(getString(R.string.error));
             alertDialog.show();
         } else {


             if (result.equals("1")) {
                 //set 0 while cart full claer
                 sp.setcartcount(0);
                 // used to set new sp based count
                 setcartcountwihoutapi();



                 Toast.makeText(CartView.this, getString(R.string.succsesfullyremved), Toast.LENGTH_SHORT).show();
                 Intent in = new Intent(CartView.this, Categories.class);

                 startActivity(in);


             } else {
                 Toast.makeText(CartView.this, getString(R.string.failedtoremove), Toast.LENGTH_LONG).show();
             }

         }
     }

        void setcartcountwihoutapi(){


            if (sp.getcartcount()==0) {
                cartcountbadge.setVisibility(View.GONE);
            } else {
                if(userVLogIn.trim().equals("0")){
                    cartcountbadge.setVisibility(View.GONE);
                }else {
                    cartcountbadge.setVisibility(View.VISIBLE);
                    cartcountbadge.setText(sp.getcartcount()+"");
                }
            }
        }
 }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                prdlength=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prdlength==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }
            else {
                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(userVLogIn.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }

            }

          //  mProgressDialog.dismiss();
        }
    }
    public class AsyncTaskDeliveryAmount extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlegth=0;


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.bal_amt_to_avil_freedelivery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_del_amnt= dataJsonArr.getJSONObject(0).optInt("result");
                prdlegth=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prdlegth==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            }



        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlegth=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr.optJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr.optJSONObject(0).optString("ContactNo");
                prdlegth=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(prdlegth==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{

                if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                    str_paymentmethod_payment = 1;


                }
                else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                    str_paymentmethod_payment = 2;

                }
                else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                    str_paymentmethod_payment = 3;


                }
                else {
                    str_paymentmethod_payment = 1;


                }
                SharedPreferences.Editor editor=myprefs.edit();
                editor.putString("ORDERREVIEW_NAME",str_contact_name_quick_checkout );
                editor.putString("ORDERREVIEW_ADDRESS",str_address_quick_checkout );
                editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                editor.apply();


               Intent in =new Intent( CartView.this,OrderReview.class);
                in.putExtra("PAYMENT_METHOD_TYPE_ID",str_paymentmethod_payment);
                in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                startActivity(in);


            }

        }
    }


    public class AsyncPromoEnter extends AsyncTask<String,String,String> {
        JSONArray dataJsonArr;
        int prdlength = 0;

        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(CartView.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.promocodesave(userVLogIn,str_promocde);
                Log.i("JSSOONNN",json+" "+userVLogIn+" "+str_promocde);
                JSONObject productObj = new JSONObject(Json.prdetail);
                Log.i("JSSOONNN2",productObj+"");
                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("status");
                prdlength = dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (prdlength == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                alertDialog.setTitle(getString(R.string.error));
                alertDialog.show();
            } else {
                if(result.equalsIgnoreCase("success")){
                    //used to cahngue the value of entred
                    sp.setbolpromoentrd("Applied");

                    int test=1;
                    sp.settestpromo(test);

                    AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(result);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent in=new Intent(CartView.this,CartView.class);
                                    CartView.this.finish();
                                    startActivity(in);
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {
                    AlertDialog alertDialog = new AlertDialog.Builder(CartView.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(result);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }



            }
        }

    }



    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }


}