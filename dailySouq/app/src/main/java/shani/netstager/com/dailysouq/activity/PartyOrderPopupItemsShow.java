package shani.netstager.com.dailysouq.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.partyOrderItemshowPopup;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


public class PartyOrderPopupItemsShow extends AppCompatActivity {
    partyOrderItemshowPopup popadpter;
    String partyoderid="",cusId,noofperson="0";
    int language;
    String[] str_prdname,str_prdid,str_prd_qty,str_subtotal,str_stockstatus,str_need_act_qty;
    Button btnAdd;
    Button btnMinus;
    EditText edQty;
    Button btnDismiss,btnSubmit;

    String responce_json;

    int id=0;


    ImageView profiles,quickcheckout;
    ProgressDialog mProgressDialog;




    int sizeProduct;
    int productLength;


    int DEFAULT_LANGUGE;


    public SharedPreferences myProducts;
    Button back_btn;


    String catId,qun;

    String sortItem;
    SharedPreferences myprefs;

    TextView title;


    JSONArray dataJsonArr;

    ImageView Notification;
    RelativeLayout cartcounttext;


    String srchtext;
    int str_json_cart_count;;



    TextView cartcountbadge,notifbadge;
    DS_SP sp;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_order_popup_items_show);


        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }

        profiles = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);

        back_btn = (Button) findViewById(R.id.menue_btn);
        title = (TextView) findViewById(R.id.namein_title);



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });


        profiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PartyOrderPopupItemsShow.this, EditProfile.class);

                startActivity(in);
            }
        });

        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(PartyOrderPopupItemsShow.this, Notification.class);

                startActivity(in);
            }
        });


        //Update TextView in PopupWindow dynamically
        edQty = (EditText) findViewById(R.id.ed_count_poup);

        btnAdd = (Button)findViewById(R.id.btn_add);

        btnMinus = (Button)findViewById(R.id.btnMinus);
        btnDismiss= (Button)findViewById(R.id.dismiss);
        btnSubmit = (Button)findViewById(R.id.submit);
        partyoderid=getIntent().getStringExtra("PID");
        cusId=getIntent().getStringExtra("USERID");
        language=getIntent().getIntExtra("LANID",1);
        noofperson=getIntent().getStringExtra("NOPER");
        title.setText(getIntent().getStringExtra("NAME"));
        edQty.setText(noofperson+"");



        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent.equals(true)){

            //  new AsyncTaskCartcounttJson().execute();
            new  AsyncTaskAddCartJson().execute();

        }
        else{

            AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderPopupItemsShow.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
      /*  btnDismiss.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }});*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    void setShow(){


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty= Integer.parseInt(edQty.getText().toString());
                qty++;
                edQty.setText(qty+"");
                String curqty=(edQty.getText().toString());
                new AsyncTaskUpdatetJson(curqty).execute();
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty= Integer.parseInt(edQty.getText().toString());
                qty--;
                edQty.setText(qty+"");
                String curqty=(edQty.getText().toString());
                new AsyncTaskUpdatetJson(curqty).execute();
            }
        });


        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new  AsyncTaskSubmittJson().execute();
            }
        });


        ListView popupSpinner = (ListView)findViewById(R.id.popupspinner);


        popadpter = new partyOrderItemshowPopup( PartyOrderPopupItemsShow.this, str_prdid, str_prdname, str_prd_qty, str_subtotal, str_stockstatus,cusId,partyoderid,language,noofperson,str_need_act_qty);

        popupSpinner.setAdapter(popadpter);
    }



    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;
        View v;


        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PartyOrderPopupItemsShow.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.toviewprtoderepoupitems(cusId,partyoderid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                str_prdid =new String[dataJsonArr.length()];
                str_prdname =new String[dataJsonArr.length()];
             //   str_prdsizeid =new String[dataJsonArr.length()];
                str_prd_qty =new String[dataJsonArr.length()];
                str_subtotal =new String[dataJsonArr.length()];
                str_stockstatus =new String[dataJsonArr.length()];
                str_need_act_qty =new String[dataJsonArr.length()];
                for(int i=0;i<dataJsonArr.length();i++) {
                    str_prdid[i] = dataJsonArr.getJSONObject(i).optString("ProductId");
                    //need t chnge to prodctid
                    str_prdname[i] = dataJsonArr.getJSONObject(i).optString("ProductShowName");
                   // str_prdsizeid[i] = dataJsonArr.getJSONObject(i).optString("ProductSizeId");
                    str_prd_qty[i] = dataJsonArr.getJSONObject(i).optString("ActualQuantity");
                    str_subtotal[i] = dataJsonArr.getJSONObject(i).optString("Price");
                    str_stockstatus[i] = dataJsonArr.getJSONObject(i).optString("RequiredQuantity");
                    str_need_act_qty[i] = dataJsonArr.getJSONObject(i).optString("RequiredQuantity");
                }
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if (jsonlen == 0) {
                Toast.makeText(getBaseContext(),"Product not found",Toast.LENGTH_LONG).show();
                onBackPressed();
            } else {

                //mypopup(str_prty_oredr_id,v);


                setShow();

            }

        }




    }

    public class AsyncTaskUpdatetJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        String qty;

        AsyncTaskUpdatetJson(String qty){
            this.v=v;
            this.qty=qty;
        }

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PartyOrderPopupItemsShow.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.temppartyordercart(partyoderid,cusId,qty);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");


                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderPopupItemsShow.this).create();
                alertDialog.setTitle(PartyOrderPopupItemsShow.this.getString(R.string.error));
                alertDialog.show();
            }else {


                new AsyncTaskAddCartJson().execute();

                // do stufff here
               /*// Toast.makeText(PartyOrderPopupItemsShow.this,responce_json+"",Toast.LENGTH_LONG).show();
                Intent in=new Intent(PartyOrderPopupItemsShow.this,PartyOrderPopupItemsShow.class);
                in.putExtra("PID",partyoderid);
                in.putExtra("LANID",language);
                in.putExtra("USERID",cusId);
                in.putExtra("NOPER",noofperson);
                finish();
                startActivity(in);*/
            }

            // mProgressDialog.dismiss();
        }
    }

    public class AsyncTaskSubmittJson extends AsyncTask<String, String, String> {
        View v;
        final String TAG = "AsyncTaskParseJson.java";
        int jsonlen=0;
        int qty;

        @Override
        protected void onPreExecute() {
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(PartyOrderPopupItemsShow.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            mProgressDialog.show();
        }


        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.partyordersubmittocart(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                responce_json = dataJsonArr.getJSONObject(0).optString("result");

                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderPopupItemsShow.this).create();
                alertDialog.setTitle(PartyOrderPopupItemsShow.this.getString(R.string.error));
                alertDialog.show();
            }else {
                Toast.makeText(PartyOrderPopupItemsShow.this,"Succsessfully added to cart",Toast.LENGTH_LONG).show();

                // do stufff here
                Intent in=new Intent(PartyOrderPopupItemsShow.this,CartView.class);

                startActivity(in);
            }

            // mProgressDialog.dismiss();
        }
    }
    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                jsonlen=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderPopupItemsShow.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(str_json_cart_count==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(PartyOrderPopupItemsShow.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(PartyOrderPopupItemsShow.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }

}
