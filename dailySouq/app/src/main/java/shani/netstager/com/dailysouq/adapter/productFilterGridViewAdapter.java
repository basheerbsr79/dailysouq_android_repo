package shani.netstager.com.dailysouq.adapter;

/**
 * Created by user on 12/11/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.FilterResult;
import shani.netstager.com.dailysouq.support.Json;
import shani.netstager.com.dailysouq.activity.ProductDetail;
import shani.netstager.com.dailysouq.activity.Signin;
import shani.netstager.com.dailysouq.models.ProductModel;
import shani.netstager.com.dailysouq.models.SizeModel;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.OnLoadMoreListener;

public class productFilterGridViewAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    ProgressDialog mProgressDialog;
    JSONArray dataJsonArr;
    int str_json_cart_count;
    RelativeLayout cartcounttext;
    TextView cartcountbadge;

    //item qntity
    int count = 1;
    int pos=0;
    //intent passing datas
    String cusId, pid, qun, result,res,bid,cid,addPid,wishPid;
    //size list
    SizeModel sizeModel;


    private List<ProductModel> productlist;
    private ArrayList<SizeModel> sizeModels;
    static Context context;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    public boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    int language;
    DS_SP sp;

    public productFilterGridViewAdapter(ArrayList<ProductModel> productModels, RecyclerView recyclerView, FilterResult filterResult, RelativeLayout cartcounttext, TextView cartcountbadge, int default_languge) {

        this.context = filterResult;
        setProductlist(productModels);
        //badge
        this.cartcounttext=cartcounttext;
        this.cartcountbadge=cartcountbadge;
        this.language=default_languge;
        sp= new DS_SP(context.getApplicationContext());

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {

            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }



    }

    public void setProductlist(List<ProductModel> productlist) {
        this.productlist = productlist;
    }


    @Override
    public int getItemViewType(int position) {
        return productlist.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.product_grid, parent, false);

        vh = new MainViewHolder(v);

        return vh;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MainViewHolder) {

            final ProductModel pro = (ProductModel) productlist.get(position);

            ((MainViewHolder) holder).prdname.setText(pro.productName);

            ((MainViewHolder) holder).showValue.setText(Integer.toString(count));


            //stepper
            qun=((MainViewHolder) holder).showValue.getText().toString();
            ((MainViewHolder) holder).valuePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vplus=1;

                    vplus= Integer.parseInt(((MainViewHolder) holder).showValue.getText().toString());

                    ((MainViewHolder) holder).showValue.setText(Integer.toString(vplus+1));
                    qun = ((MainViewHolder) holder).showValue.getText().toString();
                }
            });


            ((MainViewHolder) holder).valueMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int vminus=1;
                    vminus= Integer.parseInt(((MainViewHolder) holder).showValue.getText().toString());

                    ((MainViewHolder) holder).showValue.setText(Integer.toString(vminus-1));

                    qun = ((MainViewHolder) holder).showValue.getText().toString();

                }
            });




            Picasso.with(context).load(pro.imagepath).into(((MainViewHolder) holder).prdimg);
            //getting user id

            cusId = pro.customerId;

            ((MainViewHolder) holder).prdimg.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    pid = pro.productId;
                    bid = pro.brandId;
                    cid = pro.category;



                    Intent in = new Intent(context, ProductDetail.class);

                    in.putExtra("Product", pid);
                    in.putExtra("productname",pro.productName);
                    in.putExtra("Brand", bid);
                    in.putExtra("CATEGORY", cid);
                    context.startActivity(in);

                }
            });

            ((MainViewHolder) holder).adapter.setdata(pro.sizes);
            ((MainViewHolder) holder).adapter.notifyDataSetChanged();
            ((MainViewHolder) holder).prdSize.setFocusable(true);
            ((MainViewHolder) holder).prdSize.setAdapter(((MainViewHolder) holder).adapter);

            ((MainViewHolder) holder).prdSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    sizeModel =  ((MainViewHolder) holder).adapter.getItem(position);
                    //this is used to hoide same rate tag and save tag
                    if(sizeModel.actualprize.equals(sizeModel.offerprize)){
                        ((MainViewHolder) holder).actual_rate_full.setVisibility(View.GONE);

                        ((MainViewHolder) holder).save.setVisibility(View.GONE);
                        ((MainViewHolder) holder).save_text.setVisibility(View.GONE);
                        ((MainViewHolder) holder).save_off_text.setVisibility(View.GONE);
                    }
                    else{
                        ((MainViewHolder) holder).actual_rate_full.setVisibility(View.VISIBLE);

                        ((MainViewHolder) holder).save.setVisibility(View.VISIBLE);
                        ((MainViewHolder) holder).save_text.setVisibility(View.VISIBLE);
                        ((MainViewHolder) holder).save_off_text.setVisibility(View.VISIBLE);
                    }

                    if (sizeModel.stockstatus.trim().equals("0")) {
                        ((MainViewHolder)holder).stockstauts.setTextColor(Color.RED);
                        ((MainViewHolder)holder).stockstauts.setText(context.getString(R.string.out_stock));
                    }  else if (sizeModel.stockstatus.trim().equals("1")) {
                        ((MainViewHolder)holder).stockstauts.setTextColor(Color.WHITE);
                        ((MainViewHolder)holder).stockstauts.setText(context.getString(R.string.in_stock));
                    }else if (sizeModel.stockstatus.trim().equals("2")) {
                        ((MainViewHolder)holder).stockstauts.setTextColor(Color.WHITE);
                        ((MainViewHolder)holder).stockstauts.setText(context.getString(R.string.limted_stock));
                    }else {
                        ((MainViewHolder)holder).stockstauts.setVisibility(View.GONE);

                    }

                    ((MainViewHolder) holder).sizeid.setText(String.valueOf(sizeModel.productSizeId));
                    //Log.i("ffff",pos+"");
                    ((MainViewHolder) holder).actualprice.setText(sizeModel.actualprize+"0");
                    ((MainViewHolder) holder).actualprice.setPaintFlags( ((MainViewHolder) holder).actualprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);


                    //TextView tv=(TextView)findViewById(R.id.custom);
                    Typeface face=Typeface.createFromAsset(context.getAssets(),
                            "fonts/ExoBold.otf");
                    //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                    ((MainViewHolder) holder).actualprice.setTypeface(face);


                    ((MainViewHolder) holder).offprice.setText(sizeModel.offerprize+"0");
                    //TextView tv=(TextView)findViewById(R.id.custom);
                    Typeface faces=Typeface.createFromAsset(context.getAssets(),
                            "fonts/Bold.otf");
                    //tv_SuraName.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "suralist_font.ttf"));

                    ((MainViewHolder) holder).offprice.setTypeface(faces);


                    ((MainViewHolder) holder).save.setText(sizeModel.savaPrice+"0");


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });



            //add button
            ((MainViewHolder) holder).addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    View prdd=((MainViewHolder) holder).prdSize.getSelectedView();
                    TextView vv=(TextView)prdd;
                    String aiwa=vv.getText().toString();
                    Log.i("addd",aiwa);
                    View ids=  ((MainViewHolder) holder).sizeid.getRootView();
                    //TextView bb=(TextView)ids;
                    String sid=((MainViewHolder) holder).sizeid.getText().toString();
                    pos=Integer.parseInt(sid);





                    if(cusId!="0"){
                        qun = ((MainViewHolder) holder).showValue.getText().toString();
                        int lst_qun=Integer.parseInt(qun);
                        if(lst_qun<=0){

                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(context.getString(R.string.alert_pls_quntit));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();


                        }else{

                            pid = pro.productId;
                            bid = pro.brandId;
                            cid = pro.category;



//                            Intent in = new Intent(context, ProductDetail.class);
//
//                            in.putExtra("Product", pid);
//                            in.putExtra("productname",pro.productName);
//                            in.putExtra("Brand", bid);
//                            in.putExtra("CATEGORY", cid);
//                            context.startActivity(in);
                            //need  edit
                            new AsyncTaskAddCartJson().execute();
                        }



                    }
                    else{
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(context.getString(R.string.alert_pls_signin));
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in=new Intent(context,Signin.class);

                                        context.startActivity(in);
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    }




                    addPid=pro.productId;

                    ((MainViewHolder) holder).wishList.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            wishPid=pro.productId;



                            if(cusId!="0"){

                                new  AsyncTaskAddWishListJson().execute();


                            }
                            else{
                                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                                alertDialog.setCancelable(false);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.setMessage(context.getString(R.string.alert_pls_signin));
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                Intent in=new Intent(context,Signin.class);

                                                context.startActivity(in);
                                                dialog.dismiss();
                                            }
                                        });

                                alertDialog.show();

                            }


                        }
                    });





                }

            });





            ((MainViewHolder) holder).student = pro;

        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class MainViewHolder extends RecyclerView.ViewHolder {

        //newly added for hide
        LinearLayout actual_rate_full;
        TextView save_text,save_off_text;

        ImageView isnew;
        ImageButton prdimg;
        TextView prdname;
        TextView actualprice;
        TextView offprice;
        TextView discont,save;
        Spinner prdSize;
        ImageButton valuePlus, valueMinus, addButton;
        TextView showValue;
        ImageView wishList;
        CustomSpinnerAdapter adapter;
        ProductModel productModel;
        SizeModel sizeModel;
        TextView sizeid;
        TextView stockstauts;

        public ProductModel student;

        public MainViewHolder(View v) {
            super(v);
            //newly added for hide
            save_text=(TextView)v.findViewById(R.id.save);
            save_off_text=(TextView)v.findViewById(R.id.textView12);
            actual_rate_full=(LinearLayout)v.findViewById(R.id.prd_actuel_rate_full);
            isnew = (ImageView) v.findViewById(R.id.new_product);
            discont = (TextView) v.findViewById(R.id.offer);
            prdimg = (ImageButton) v.findViewById(R.id.product_image);
            prdname = (TextView) v.findViewById(R.id.product_name);
            actualprice = (TextView) v.findViewById(R.id.mrpPrice);
            offprice = (TextView) v.findViewById(R.id.offPrice);
            prdSize = (Spinner) v.findViewById(R.id.productSpinner);
            valuePlus = (ImageButton) v.findViewById(R.id.valuePlus);
            valueMinus = (ImageButton) v.findViewById(R.id.valueMinus);
            showValue = (TextView) v.findViewById(R.id.valueText);
            adapter = new CustomSpinnerAdapter(context);
//           productModel = productModels.get(position);
            addButton = (ImageButton) v.findViewById(R.id.addButton);
            wishList = (ImageView) v.findViewById(R.id.wishlist);
            save=(TextView)v.findViewById(R.id.save_new);
            sizeid=(TextView)v.findViewById(R.id.textid);
            stockstauts=(TextView)v.findViewById(R.id.txt_stock_status_prd_grid);




        }
    }

    public class AsyncTaskAddCartJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();
                JSONObject json = jParser.addCart(0,cusId, addPid, pos, qun);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");
                result = dataJsonArr.getJSONObject(0).optString("result");
                    jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (jsonlen == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            } else {
                if (result.equals("success")) {
                    new AsyncTaskCartcounttJson().execute();

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;


                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("updated")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (result.equals("Out of Stock")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert_outstock, null);
                    TextView ok = (TextView) layout.findViewById(R.id.textok);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();
                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));


                    alertDialog.show();

                }

            }
        }
    }

    public class AsyncTaskAddWishListJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int jsonlen=0;


        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Json jParser = new Json();

                JSONObject json = jParser.addWishList(cusId, wishPid);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");
                res = dataJsonArr.getJSONObject(0).optString("result");
                jsonlen=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                if (res.equals("success")) {


                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.custom_alert, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;
                    new AsyncTaskCartcounttJson().execute();

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else if (res.equals("Alredy exist")) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout = inflater1.inflate(R.layout.cutom_alert_updated, null);


                    builder.setView(layout);

                    final android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations =


                            R.style.Bounce;

                    alertDialog.show();

                    final Handler handler = new Handler();
                    final Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }
                    };

                    alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            handler.removeCallbacks(runnable);
                        }
                    });
                    alertDialog.setCanceledOnTouchOutside(true);

                    handler.postDelayed(runnable, 2000);


                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();

                    alertDialog.setMessage(context.getString(R.string.error));


                    alertDialog.show();


                }

            }
        }


    }
    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
            int jsonlen=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(cusId);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optInt("Cartcount");
                    jsonlen=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(jsonlen==0){
                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.error));
                alertDialog.show();
            }else {
                sp.setcartcount(str_json_cart_count);

                if (sp.getcartcount()==0) {
                    cartcountbadge.setVisibility(View.GONE);
                } else {
                    if(cusId.trim().equals("0")){
                        cartcountbadge.setVisibility(View.GONE);
                    }else {
                        cartcountbadge.setVisibility(View.VISIBLE);
                        cartcountbadge.setText(sp.getcartcount()+"");
                    }
                }

            }
        }
    }
}

