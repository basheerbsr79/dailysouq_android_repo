package shani.netstager.com.dailysouq.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.adapter.CustomChooseAddress;
import shani.netstager.com.dailysouq.adapter.CustomTimeSlot;
import shani.netstager.com.dailysouq.adapter.Custom_choose_address_add;
import shani.netstager.com.dailysouq.support.ConnectionDetector;
import shani.netstager.com.dailysouq.support.DS_SP;
import shani.netstager.com.dailysouq.support.Json;


/**
 * Created by prajeeshkk on 19/10/15.
 */
public class CheckOut extends Activity {
    ProgressDialog mProgressDialog;
    String[] customerId, toTime, slotid;
    String[] address;
    String[] postCode;
    String[] landMark;
    String[] contactName;
    String[] contactNo;
    String[] locationId;

    String[] deliveryAddressId;

    TextView cartcountbadge,notifbadge;
    DS_SP sp;

    JSONArray dataJsonArr;
    int productLength, y, m, d;
    String dtest,mtest,str_json_cart_count;
    int str_del_amnt_free;
    static final int DATE_ID = 0;
    String cid, userVLogIn, deliverydate, time,poscurreload;
    ListView gridView;
    ImageView footerdssp, footerdsshop, footerdsdeal, footerdsgift, footerdsmore;
    TextView title;
    EditText ed_date;
    CustomChooseAddress adapt;
    Custom_choose_address_add adp;
    Button submit,bck_btn,cntinueshop;
    ImageView profile, quickcheckout, Notification;
    RelativeLayout cartcounttext;
    SharedPreferences myprefs;
    boolean[]deliveryAddress;
    String categ,pid;
    TwoWayView recTest;



    ListView list;

    String[] imageId;
    boolean booluser_noaddress;
    // its used for quick check out
    String str_address_quick_checkout,str_contact_name_quick_checkout,str_opt_payment,str_location_quick_checkout,str_phone_quick_checkout;
    int str_paymentmethod_payment;

    //used for new
      String addressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_choose_address);
        myprefs = getSharedPreferences("MYPREFS", Context.MODE_PRIVATE);
        userVLogIn = myprefs.getString("shaniusrid", null);
        poscurreload = myprefs.getString("positionofcurrentback", null);
        recTest=(TwoWayView)findViewById(R.id.RecentItems);
        cntinueshop=(Button)findViewById(R.id.continueshop);


        cartcounttext=(RelativeLayout) findViewById(R.id.count_text_cart);
        //badge
        cartcountbadge = (TextView)findViewById(R.id.badge);

        //notif badge full
        sp=new DS_SP(getApplicationContext());
        notifbadge=(TextView)findViewById(R.id.badgenotif);
        if(sp.getnotcount()!=0){
            notifbadge.setVisibility(View.VISIBLE);
            notifbadge.setText(sp.getnotcount()+"");
        }
        //badge

        if(userVLogIn!=null){
            userVLogIn=myprefs.getString("shaniusrid",userVLogIn);

        }
        else{
            userVLogIn="0";

        }


        //progress loading
        // Create a progressdialog
        mProgressDialog = new ProgressDialog(CheckOut.this);
        // Set progressdialog title
        mProgressDialog.setTitle("");

        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        // Show progressdialog



        //internet checking
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent.equals(true)) {
          //  new AsyncTaskCartcounttJson().execute();
            new AsyncTaskDeliveryAmount().execute();

            new AsyncTaskCheckOutJson().execute();
        } else {

            AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage(getString(R.string.alert_net_failed));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            onBackPressed();

                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        ed_date = (EditText) findViewById(R.id.choose_date);
        submit = (Button) findViewById(R.id.submit_btn);
            //call tomarrow method

        tomorrowDateSet();

        new AsynchTimeSlot().execute();
        profile = (ImageView) findViewById(R.id.profile);

        quickcheckout = (ImageView) findViewById(R.id.apload);
        Notification = (ImageView) findViewById(R.id.notification);
        title = (TextView) findViewById(R.id.namein_title);
        bck_btn = (Button) findViewById(R.id.menue_btn);
        title.setText(getString(R.string.choose_addres));
        bck_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
onBackPressed();
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, EditProfile.class);

                startActivity(in);
            }
        });
        cartcounttext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, CartView.class);

                startActivity(in);
            }
        });
        quickcheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncQuickCheckOut().execute();
            }
        });
        Notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, Notification.class);

                startActivity(in);
            }
        });
        footerdsmore = (ImageView) findViewById(R.id.imageView15);
        footerdsmore.setImageResource(R.drawable.footer_more_select);
        footerdssp = (ImageView) findViewById(R.id.imageView13);
        footerdsshop = (ImageView) findViewById(R.id.imageView11);
        footerdsdeal = (ImageView) findViewById(R.id.imageView12);
        footerdsgift = (ImageView) findViewById(R.id.imageView14);
        footerdsmore = (ImageView) findViewById(R.id.imageView15);

        //footer clicks

        footerdsshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, Categories.class);

                startActivity(in);
            }
        });


        footerdsdeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, DsDeals.class);

                startActivity(in);

            }
        });

        footerdsgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, DsGiftCard.class);

                startActivity(in);

            }
        });

        footerdsmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CheckOut.this, Menu_List.class);

                startActivity(in);
            }
        });
        footerdssp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(CheckOut.this, DsSpecials.class);

                startActivity(in);
            }
        });



        cntinueshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(CheckOut.this,Categories.class);

                startActivity(in);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("clickaiwa","ok"+addressId);



                //bsr edits for payment start

                if(addressId!=null&&time!=null){
                    if(str_del_amnt_free==0){
                        Intent in = new Intent(CheckOut.this, PaymentOptions.class);

                        in.putExtra("DELIVERYDATE_CHECKOUT", deliverydate);
                        in.putExtra("DELIVERYTIME_CHECKOUT", time);
                        in.putExtra("DELIVERYADDRESS_CHECKOUT", addressId);
                        startActivity(in);
                    }else{
                        AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setMessage(getString(R.string.ad_rs)+str_del_amnt_free+".00\n"+
                                getString(R.string.alert_sure_checkout2)+"\n"+getString(R.string.alert_sure_checkout));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent in = new Intent(CheckOut.this, PaymentOptions.class);

                                        in.putExtra("DELIVERYDATE_CHECKOUT", deliverydate);
                                        in.putExtra("DELIVERYTIME_CHECKOUT", time);
                                        in.putExtra("DELIVERYADDRESS_CHECKOUT", addressId);
                                        startActivity(in);


                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }




                }else{

                        if(addressId==null){
                            if(booluser_noaddress==true){
                                AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                                alertDialog.setCancelable(false);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.setMessage(getString(R.string.alert_select_address));
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                dialog.dismiss();
                                            }
                                        });

                                alertDialog.show();
                            }else {
                                //   used for selecting dflt
                                //settings addres to set in order review
                                SharedPreferences.Editor editor=myprefs.edit();
                                editor.putString("ORDERREVIEW_NAME", contactName[0]);
                                editor.putString("ORDERREVIEW_ADDRESS", address[0]);
                                editor.putString("ORDERREVIEW_LOCATION", landMark[0]);
                                editor.putString("ORDERREVIEW_PHONE",contactNo[0]);
                                editor.apply();
                                addressId=deliveryAddressId[0];
                            }





                        }else if(time==null){
                            AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setMessage(getString(R.string.alert_select_address_timeslot));
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                           // new AsynchTimeSlot().execute();
                                            dialog.dismiss();
                                        }
                                    });

                            alertDialog.show();
                        }






                }

                //bsr edits for payment end
            }
        });


        final Calendar calendar = Calendar.getInstance();
        y = calendar.get(Calendar.YEAR);
        m = calendar.get(Calendar.MONTH);
        d = calendar.get(Calendar.DAY_OF_WEEK);

        System.out.println("day"+d);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);




        ed_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                showDialog(DATE_ID);
                ed_date.setEnabled(false);
                return false;
            }
        });
        categ="1";
        pid="1";

        // used to set new sp based count
        setcartcountwihoutapi();
        setnewProgressbarClicks();
    }

    private void setnewProgressbarClicks() {
        ImageView ivstpcart,ivstpdelivery,ivstpPayment,ivstpConfirm;
        ivstpcart=(ImageView)findViewById(R.id.ivStpCart);
        ivstpdelivery=(ImageView)findViewById(R.id.ivStpDelivery);
        ivstpPayment=(ImageView)findViewById(R.id.ivStpPayment);
        ivstpConfirm=(ImageView)findViewById(R.id.ivStpConfirm);

        ivstpcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(CheckOut.this,CartView.class);
                startActivity(in);
            }
        });
        ivstpdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivstpPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkfullok();
               // Intent in=new Intent(CheckOut.this,PaymentOptions.class);
                //startActivity(in);
            }
        });
        ivstpConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressAlert("Can't select at this time,Please select Payment details first.");
            }
        });

    }

    public void showProgressAlert(String msg){
        AlertDialog.Builder builder=new AlertDialog.Builder(CheckOut.this);
        builder.setTitle("DailySouq");
        builder.setMessage(msg)
                .setInverseBackgroundForced(false)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // do nothing
                                //  onBackPressed();

                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void checkfullok(){
        if(addressId!=null&&time!=null){
            if(str_del_amnt_free==0){
                Intent in = new Intent(CheckOut.this, PaymentOptions.class);

                in.putExtra("DELIVERYDATE_CHECKOUT", deliverydate);
                in.putExtra("DELIVERYTIME_CHECKOUT", time);
                in.putExtra("DELIVERYADDRESS_CHECKOUT", addressId);
                startActivity(in);
            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.ad_rs)+str_del_amnt_free+".00\n"+
                        getString(R.string.alert_sure_checkout2)+"\n"+getString(R.string.alert_sure_checkout));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent in = new Intent(CheckOut.this, PaymentOptions.class);

                                in.putExtra("DELIVERYDATE_CHECKOUT", deliverydate);
                                in.putExtra("DELIVERYTIME_CHECKOUT", time);
                                in.putExtra("DELIVERYADDRESS_CHECKOUT", addressId);
                                startActivity(in);


                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

        }else{

            if(addressId==null){
                if(booluser_noaddress==true){
                    AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_select_address));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {
                    //   used for selecting dflt
                    //settings addres to set in order review
                    SharedPreferences.Editor editor=myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", contactName[0]);
                    editor.putString("ORDERREVIEW_ADDRESS", address[0]);
                    editor.putString("ORDERREVIEW_LOCATION", landMark[0]);
                    editor.putString("ORDERREVIEW_PHONE",contactNo[0]);
                    editor.apply();
                    addressId=deliveryAddressId[0];
                }

            }else if(time==null){
                AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.alert_select_address_timeslot));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // new AsynchTimeSlot().execute();
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }


        }
    }

    void setcartcountwihoutapi(){
        if (sp.getcartcount()==0) {
            cartcountbadge.setVisibility(View.GONE);
        } else {
            if(userVLogIn.trim().equals("0")){
                cartcountbadge.setVisibility(View.GONE);
            }else {
                cartcountbadge.setVisibility(View.VISIBLE);
                cartcountbadge.setText(sp.getcartcount()+"");
            }
        }
    }



    //method to send tomarrow partyorderpersons aand time loading
   void  tomorrowDateSet(){
       Calendar calendar = Calendar.getInstance();
       Date today = calendar.getTime();

       calendar.add(Calendar.DAY_OF_YEAR, 0);
       Date tomorrow = calendar.getTime();

       DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

       String todayAsString = dateFormat.format(today);
       String tomorrowAsString = dateFormat.format(tomorrow);

       System.out.println(todayAsString);
       System.out.println("tomarrow"+tomorrowAsString);
       deliverydate=tomorrowAsString;
       System.out.println("tomarrow"+tomorrowAsString);
        ed_date.setText(deliverydate);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_ID:
                // set partyorderpersons picker as current partyorderpersons

                DatePickerDialog dialog = new DatePickerDialog(this, datePickerListener, y, m, d);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(CheckOut.this,"clicked cancel",Toast.LENGTH_LONG).show();
                    }
                });
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, 2);
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                return dialog;



        }
        return null;
    }



    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            y = selectedYear;
            m = selectedMonth;





            if(selectedMonth<9)
            {
                m=0+selectedMonth;
                mtest="0"+String.valueOf(selectedMonth+1);

                m=Integer.parseInt(mtest);

            }
            else{
                mtest=String.valueOf(selectedMonth+1);

            }

            if(selectedDay<10)
            {
                d=0+selectedDay;
                dtest="0"+String.valueOf(selectedDay);

                d=Integer.parseInt(dtest);

            }
            else{
                dtest=String.valueOf(selectedDay);
                d=Integer.parseInt(dtest);
            }
            // set selected partyorderpersons into datepicker also
            //used to s5 issue fix
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 2);
            int testdate=c.get(Calendar.DATE);
            int curday=new Date().getDate();
            Log.i("testdate",testdate+"...."+d+"..."+curday);

            //if(d>=curday&&d<=testdate){
            if(true){
               // Toast.makeText(CheckOut.this,"ok",Toast.LENGTH_LONG).show();
                // set selected partyorderpersons into textview
                ed_date.setText(new StringBuilder().append(dtest)
                        .append("/").append(mtest).append("/").append(y));
                deliverydate = ed_date.getText().toString();
                new AsynchTimeSlot().execute();
                // set selected partyorderpersons into datepicker also
                ed_date.setEnabled(true);
            }else{
                ed_date.setEnabled(true);
              //  Toast.makeText(CheckOut.this,"not",Toast.LENGTH_LONG).show();
                AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                alertDialog.setMessage("Delivery is only possible within 3 days from the date of purchase");
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }




        }
    };




    public class AsyncTaskCheckOutJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr;
        int prdlength=0;
        String []test;

        @Override
        protected String doInBackground(String... arg0) {

            try {
                Json jParser = new Json();
                JSONObject jsonObject = jParser.chooseAddress(userVLogIn);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                productLength = dataJsonArr.length();

                customerId = new String[dataJsonArr.length()];
                contactName = new String[dataJsonArr.length()];
                postCode = new String[dataJsonArr.length()];
                address = new String[dataJsonArr.length()];
                landMark = new String[dataJsonArr.length()];
                contactNo = new String[dataJsonArr.length()];
                locationId = new String[dataJsonArr.length()];
                deliveryAddressId = new String[dataJsonArr.length()];
                deliveryAddress = new boolean[dataJsonArr.length()];
                for (int i = 0; i < productLength; i++) {


                    JSONObject c = dataJsonArr.getJSONObject(i);
                    customerId[i] = dataJsonArr.getJSONObject(i).optString("CustomerId");
                    contactName[i] = c.optString("ContactName").toString();

                    postCode[i] = c.optString("PostCode").toString();
                    address[i] = c.optString("Address").toString();

                    landMark[i] = c.optString("LandMark").toString();

                    //System.out.println("DefaultAddress" + actualPrice[i]);
                    locationId[i] = c.optString("LocationId").toString();
                    contactNo[i] = c.optString("ContactNo").toString();

                    deliveryAddressId[i] = c.optString("DeliveryAddressId");
                    deliveryAddress[i] = c.optBoolean("DefaultAddress");



                }
                prdlength=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

                // mProgressDialog.dismiss();
                gridView = (ListView) findViewById(R.id.choose_address);
                gridView.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });


                if (prdlength != 0) {
                    booluser_noaddress=false;
                    adapt = new CustomChooseAddress(CheckOut.this, productLength, contactName, address, landMark, contactNo, deliveryAddressId, deliveryAddress);
                        adapt.setSelectedPosition(0);
                    // used for getting dflt while not select aaddress
                    //settings addres to set in order review
                    SharedPreferences.Editor editor=myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", contactName[0]);
                    editor.putString("ORDERREVIEW_ADDRESS", address[0]);
                    editor.putString("ORDERREVIEW_LOCATION", landMark[0]);
                    editor.putString("ORDERREVIEW_PHONE",contactNo[0]);
                    editor.apply();
                    addressId=deliveryAddressId[0];



                    gridView.setAdapter(adapt);

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Log.i("pos",position+"");
                            int pos=position;

                            SharedPreferences.Editor editor=myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", contactName[pos]);
                    editor.putString("ORDERREVIEW_ADDRESS", address[pos]);
                    editor.putString("ORDERREVIEW_LOCATION", landMark[pos]);
                    editor.putString("ORDERREVIEW_PHONE",contactNo[pos]);

                    editor.apply();
                    addressId=deliveryAddressId[pos];



                            adapt.setSelectedPosition(pos);
                        }
                    });


                   // gridView.setAdapter(adapt);
                } else {

                    booluser_noaddress=true;
                    // Custom_choose_address_add adp;
                    adp = new Custom_choose_address_add(CheckOut.this);
                    gridView.setAdapter(adp);
                }

        }

    }

    public class AsynchTimeSlot extends AsyncTask<String, String, String> {
        JSONArray dataJsonArr;
        int prdlength=0;
        @Override
        protected void onPreExecute() {

            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                Json jParser = new Json();
                JSONObject jsonObject = jParser.time_slot(deliverydate, userVLogIn);
                Log.i("value",deliverydate);
                JSONObject productOb = new JSONObject(Json.prdetail);
                dataJsonArr = productOb.getJSONArray("d");
                productLength = dataJsonArr.length();
                toTime = new String[dataJsonArr.length()];
                slotid = new String[dataJsonArr.length()];
                imageId=new String[dataJsonArr.length()];
                for (int i = 0; i < productLength; i++) {
                    JSONObject c = dataJsonArr.getJSONObject(i);
                    toTime[i] = dataJsonArr.getJSONObject(i).optString("ToTime");
                    slotid[i] = c.optString("DeliverySlotId").toString();
                    imageId[i]=c.optString("SlotImage").toString();

                }
                prdlength=dataJsonArr.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            if(prdlength==0){

            }else {
                CustomTimeSlot adapter = new
                        CustomTimeSlot(CheckOut.this, productLength, slotid, toTime, imageId);
                list = (ListView) findViewById(R.id.list);
                list.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                list.setAdapter(adapter);
                //time = CustomTimeSlot.time;

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        time = slotid[position];
                    }
                });

            }
        }
    }



    public class AsyncTaskCartcounttJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prd_length=0;
        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.cartcount(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);
                dataJsonArr = productObj.getJSONArray("d");

                str_json_cart_count = dataJsonArr.getJSONObject(0).optString("Cartcount");
                prd_length=dataJsonArr.length();


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
if(prd_length==0){
    AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}else {
    if (str_json_cart_count.trim().equals("0")) {
        cartcountbadge.setVisibility(View.GONE);
    } else {
        if(userVLogIn.trim().equals("0")){
            cartcountbadge.setVisibility(View.GONE);
        }else {
            cartcountbadge.setVisibility(View.VISIBLE);
            cartcountbadge.setText(str_json_cart_count);
        }
    }
}



        }
    }


    public class AsyncTaskDeliveryAmount extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        int prdlength=0;


        @Override
        protected void onPreExecute() {

            // Create a progressdialog
            mProgressDialog = new ProgressDialog(CheckOut.this);
            // Set progressdialog title
            mProgressDialog.setTitle("");

            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            //  mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.bal_amt_to_avil_freedelivery(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr = productObj.getJSONArray("d");

                str_del_amnt_free = dataJsonArr.getJSONObject(0).optInt("result");

                prdlength=dataJsonArr.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
if(prdlength==0){
    AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
    alertDialog.setTitle(getString(R.string.error));
    alertDialog.show();
}
        }
    }


    public class AsyncQuickCheckOut extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        JSONArray dataJsonArr_quick;
        int prdlength=0;



        @Override
        protected String doInBackground(String... arg0) {
            try {


                Json jParser = new Json();
                JSONObject json = jParser.quick_checkout(userVLogIn);
                JSONObject productObj = new JSONObject(Json.prdetail);

                dataJsonArr_quick = productObj.getJSONArray("d");

                str_address_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("Address");
                str_opt_payment= dataJsonArr_quick.getJSONObject(0).optString("PaymentMethodName");
                str_contact_name_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactName");
                str_location_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("PostCode");
                str_phone_quick_checkout= dataJsonArr_quick.getJSONObject(0).optString("ContactNo");
                prdlength=dataJsonArr_quick.length();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//
            if(prdlength==0){
                AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage(getString(R.string.user_not_appliacable_quick_checkout));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
            else{


                if(sp.getcartcount()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(CheckOut.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage(getString(R.string.alert_no_item_in_cart));

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();
                }else {


                    if (str_opt_payment.equals(getString(R.string.cash_on_delivery))) {
                        str_paymentmethod_payment = 1;


                    } else if (str_opt_payment.equals(getString(R.string.creditdebitcard))) {
                        str_paymentmethod_payment = 2;

                    } else if (str_opt_payment.equals(getString(R.string.ewallet))) {
                        str_paymentmethod_payment = 3;


                    } else {
                        str_paymentmethod_payment = 1;


                    }
                    SharedPreferences.Editor editor = myprefs.edit();
                    editor.putString("ORDERREVIEW_NAME", str_contact_name_quick_checkout);
                    editor.putString("ORDERREVIEW_ADDRESS", str_address_quick_checkout);
                    editor.putString("ORDERREVIEW_LOCATION", str_location_quick_checkout);
                    editor.putString("ORDERREVIEW_PHONE", str_phone_quick_checkout);

                    editor.apply();


                    Intent in = new Intent(CheckOut.this, OrderReview.class);
                    in.putExtra("PAYMENT_METHOD_TYPE_ID", str_paymentmethod_payment);
                    in.putExtra("PAYMENT_METHOD_TYPE_NAME",str_opt_payment);
                    in.putExtra("TAG_QUICK_CHECKOUT_INTENT","back_finish");
                    startActivity(in);

                }
            }

        }
    }


}
