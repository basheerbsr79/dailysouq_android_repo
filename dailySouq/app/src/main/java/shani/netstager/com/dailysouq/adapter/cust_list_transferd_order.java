package shani.netstager.com.dailysouq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.OrderDetails;
import shani.netstager.com.dailysouq.activity.TransferOrderDetails;

public class cust_list_transferd_order extends BaseAdapter{


    private Activity activity;
    String[]orderno,orderitem,status,date;
    String[] totalitem;
    int rec_count;
    LayoutInflater inf;
    ViewHolder holder;

    public cust_list_transferd_order(Activity context, int length, String[] orderno, String[] orderitem, String[] status, String[] totalitem, String[] date) {


        this.activity=context;
        this.rec_count=length;
        this.orderno=orderno;
        this.orderitem=orderitem;
        this.status=status;
        this.date=date;
        this.totalitem=totalitem;
        inf=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return rec_count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public static class ViewHolder{
        public TextView orderno;
        public TextView orderitem;
        public TextView ordertotle;
        public TextView date;
        public TextView status;



    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {

            vi = inf.inflate(R.layout.activity_cust_list_transfred_order, null);
            holder = new ViewHolder();

            holder.orderno = (TextView) vi.findViewById(R.id.orderno_txt);
            holder.orderitem = (TextView) vi.findViewById(R.id.orderitem_txt);
            holder.ordertotle = (TextView) vi.findViewById(R.id.ordertotal_txt);
            holder.date = (TextView) vi.findViewById(R.id.date_txt);
            holder.status = (TextView) vi.findViewById(R.id.status_txt);


            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }
        try{


            holder.orderno.setText(activity.getString(R.string.name)+" "+status[position]);
            holder.orderitem.setText(activity.getString(R.string.order_no)+" DSQORD"+orderno[position]);
            holder.ordertotle.setText(activity.getString(R.string.date)+" "+date[position]);
            holder.date.setText(activity.getString(R.string.total_rs)+" "+orderitem[position]);
            holder.status.setText(activity.getString(R.string.delivery_charge)+" "+totalitem[position]);


            holder.orderno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent in=new Intent(activity,TransferOrderDetails.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("notif_id",orderno[position]);
                   // Toast.makeText(activity.getApplicationContext(),"Order no: DSQORD "+notif_id[position],Toast.LENGTH_LONG).show();
                    activity.startActivity(in);


                }
            });
            holder.ordertotle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent in=new Intent(activity,TransferOrderDetails.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("notif_id",orderno[position]);
                   // Toast.makeText(activity.getApplicationContext(),"Order no: DSQORD"+notif_id[position],Toast.LENGTH_LONG).show();
                    activity.startActivity(in);


                }
            });
            holder.orderitem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent in=new Intent(activity,TransferOrderDetails.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    in.putExtra("notif_id",orderno[position]);
                   // Toast.makeText(activity.getApplicationContext(),"Order no: "+notif_id[position],Toast.LENGTH_LONG).show();
                    activity.startActivity(in);


                }
            });




        }catch(Exception e){

        }
        return vi;
    }
}
