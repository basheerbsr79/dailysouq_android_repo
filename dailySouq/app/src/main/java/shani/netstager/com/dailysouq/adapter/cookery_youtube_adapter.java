package shani.netstager.com.dailysouq.adapter;

/**
 * Created by MohammedBasheer on 20-06-2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import shani.netstager.com.dailysouq.R;
import shani.netstager.com.dailysouq.activity.CookeryPlay;

/**
 * Created by ofaroque on 8/13/15.
 */
public class cookery_youtube_adapter extends RecyclerView.Adapter<cookery_youtube_adapter.VideoInfoHolder> {

    //these ids are the unique id for each video
   // String[] VideoID = {"P3mAtvs5Elc", "nCgQDjiotG0", "P3mAtvs5Elc"};
    Context ctx;
    public static final String API_KEY = "AIzaSyBCkZGHdBZbTe3cyn-oEKpRpfHquUTnsiU";
    String []CookeryId, CookeryTitle, CookeryVideoUrl, CookeryDetails, CookeryImage, CookeryList;

    public cookery_youtube_adapter(Context context,String[] CookeryId,String[] CookeryTitle,String[] CookeryVideoUrl,String[] CookeryDetails,String[] CookeryImage,String[] CookeryList) {
        this.ctx = context;
        this.CookeryId=CookeryId;
        this.CookeryTitle=CookeryTitle;
        this.CookeryVideoUrl=CookeryVideoUrl;
        this.CookeryDetails=CookeryDetails;
        this.CookeryImage=CookeryImage;
        this.CookeryList=CookeryList;

    }

    @Override
    public VideoInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cookery_youtube_child, parent, false);

        return new VideoInfoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoInfoHolder holder, final int position) {


        try {


            final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                @Override
                public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                }

                @Override
                public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                    youTubeThumbnailView.setVisibility(View.VISIBLE);
                    holder.relativeLayoutOverYouTubeThumbnailView.setVisibility(View.VISIBLE);
                }
            };

            holder.youTubeThumbnailView.initialize(API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                    youTubeThumbnailLoader.setVideo(CookeryVideoUrl[position]);
                    youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
                }

                @Override
                public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                    //write something for failure
                }
            });

            holder.title.setText(CookeryTitle[position]);

            holder.full_lnr_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(ctx.getApplicationContext(), CookeryPlay.class);
                    in.putExtra("cookery id", CookeryId[position]);
                    in.putExtra("cookery url", CookeryVideoUrl[position]);
                    in.putExtra("cookery details", CookeryDetails[position]);
                    in.putExtra("cookery name", CookeryTitle[position]);
                    ctx.startActivity(in);
                }
            });
        }catch (Exception e){
            //do nothing
        }

    }

    @Override
    public int getItemCount() {
        return CookeryVideoUrl.length;
    }

    public class VideoInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected RelativeLayout relativeLayoutOverYouTubeThumbnailView;
        YouTubeThumbnailView youTubeThumbnailView;
        protected ImageView playButton;
        TextView title;
        LinearLayout full_lnr_title;
        public VideoInfoHolder(View itemView) {
            super(itemView);
            playButton=(ImageView)itemView.findViewById(R.id.btnYoutube_player);
            playButton.setOnClickListener(this);
            relativeLayoutOverYouTubeThumbnailView = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_over_youtube_thumbnail);
            youTubeThumbnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.youtube_thumbnail);
            title=(TextView)itemView.findViewById(R.id.cookery_tiltle_child);
            full_lnr_title=(LinearLayout)itemView.findViewById(R.id.full_lnr);
           // title.setText(CookeryTitle[getPosition()]);

        }

        @Override
        public void onClick(View v) {

            Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) ctx, API_KEY, CookeryVideoUrl[getLayoutPosition()]);
            ctx.startActivity(intent);
        }
    }
}
